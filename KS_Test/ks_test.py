#
# amatools example 7a
#
# displacement histogram and persistence
# Ellie's data - AC-TGFP
#
# jpm mar 2013
#

import amatools
import matplotlib
from matplotlib import pyplot
import numpy
import math
import glob
import scipy.stats
pi=math.pi


### Ellie's data
#csv_root='./Ellie CSV format/'
#csv_directorys=['AC-TGFP','AC-TNULL','Act-TGFP','Act-TNULL','Chi-TGFP','Chi-TNULL']
#csv_directorys=['AC-TGFP']#,'AC-TNULL','Act-TGFP']
#angles_dict={}

#fig=pyplot.figure()
#fig.suptitle('Cell Step Direction')
#plot_num=1
#for csv_directory in csv_directorys:
	#ct=amatools.AMACellTracks()
	#ct.read_mtrackj_csv_dir(csv_root+csv_directory)
	##ct.filter_by_track_size(10)
	#ct.compute_tracks()
	
	## movment histogram	
	#p=fig.add_subplot(1,len(csv_directorys),plot_num,polar=True) 
	
	#mag_filter=numpy.array(ct.d)>3 #filter small steps with quantisation error - NB in microns not pixels (ie MTrackJ csv format)
	#dx=numpy.array(ct.dx)[mag_filter]
	#dy=numpy.array(ct.dy)[mag_filter]
	
	
	#v1=numpy.vstack((dx,dy)).transpose()
	#v2=v1[1:]
	#v1=v1[0:-1]
	#angles=numpy.arctan2(v1[:,0]*v2[:,1]-v1[:,1]*v2[:,0],v1[:,0]*v2[:,0]+v1[:,1]*v2[:,1])
	#angles_dict[csv_directory]=angles
	
	#hi,bins=numpy.histogram(angles,bins=40)
	
	##ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
	#n=len(hi)
	#bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
	#for i in range(n):
		#bar[i].set_facecolor(matplotlib.cm.jet(0.7))
		#bar[i].set_alpha(0.5)
		
	## KS test
	##ks=scipy.stats.kstest(angles,'uniform',args=(-pi,2*pi))
	##p.set_xlabel(csv_directory+'\nN=%i of %i'%(len(angles),len(ct.dx))+'\nKS Test: D=%f p=%f'%ks)
	#p.set_xlabel(csv_directory+'\nN=%i of %i'%(len(angles),len(ct.dx)))
		
	#plot_num+=1

	#ct.show_plots(block=False)
	#pass

####
## 2 sample ks tests
#cond1_count=0
#cond2_count=0
#total=0
#num_conds=len(csv_directorys)
#p_values=numpy.zeros((num_conds,num_conds))
#significance=numpy.zeros((num_conds,num_conds))

#for cond1_count in range(num_conds):
	#for cond2_count in range(cond1_count+1,num_conds):
		#condition1=csv_directorys[cond1_count]
		#condition2=csv_directorys[cond2_count]
		#ks=scipy.stats.ks_2samp(angles_dict[condition1],angles_dict[condition2])
		#print condition1,condition2,ks
		#p_values[cond1_count,cond2_count]=ks[1]
		##significance[cond1_count,cond2_count]=total
		#total+=1

#mask=numpy.tri(significance.shape[0])
#significance=numpy.ma.array(p_values,mask=mask)
#significance=-numpy.log10(significance)

#fig=pyplot.figure()
#fig.suptitle('Two Sample KS Test significance')
#heatmap = pyplot.pcolor(significance,cmap=pyplot.cm.Blues)
#for y in range(0,num_conds):
	#for x in range(y+1,num_conds):
		#pyplot.text(x+0.5,y+0.5,'%1.2f'%significance[y,x],horizontalalignment='center',verticalalignment='center')
	
## put the major ticks at the middle of each cell
#fig.axes[0].set_xticks(numpy.arange(num_conds)+0.5, minor=False)
#fig.axes[0].set_yticks(numpy.arange(num_conds)+0.5, minor=False)
#fig.axes[0].set_xticklabels(csv_directorys,minor=False)
#fig.axes[0].set_yticklabels(csv_directorys,minor=False)
#pyplot.colorbar(heatmap)
#ct.show_plots(block=True)
#pass

### Ellie's data - split by overall track movement

csv_root='./Ellie CSV format/'
csv_directorys=['AC-TGFP','AC-TNULL','Act-TGFP','Act-TNULL','Chi-TGFP','Chi-TNULL']
#csv_directorys=['AC-TGFP']#,'AC-TNULL','Act-TGFP']
angles_dict={}

fig=pyplot.figure()
fig.suptitle('Cell Step Direction')
plot_num=1
for csv_directory in csv_directorys:
	ct=amatools.AMACellTracks()
	ct.read_mtrackj_csv_dir(csv_root+csv_directory)
	#ct.filter_by_track_size(10)
	ct.compute_tracks()
	
	d_overall_mean=sum(ct.d_overall)/len(ct.d_overall)
	d_filter=numpy.array(ct.d_overall)<d_overall_mean/5
	tracks=[numpy.array(ct.tracks)[d_filter],numpy.array(ct.tracks)[numpy.logical_not(d_filter)]]
	
	for track in tracks:
	# movment histogram	
		p=fig.add_subplot(len(csv_directorys),2,plot_num,polar=True) 
		plot_num+=1
		
		tracks_d=[]
		tracks_dx=[]
		tracks_dy=[]		
		for t in track:
			tracks_d+=t.d
			tracks_dx+=t.dx
			tracks_dy+=t.dy		
		
		mag_filter=numpy.array(tracks_d)>3 #filter small steps with quantisation error - NB in microns not pixels (ie MTrackJ csv format)
		dx=numpy.array(tracks_dx)[mag_filter]
		dy=numpy.array(tracks_dy)[mag_filter]	
		
		v1=numpy.vstack((dx,dy)).transpose()
		v2=v1[1:]
		v1=v1[0:-1]
		angles=numpy.arctan2(v1[:,0]*v2[:,1]-v1[:,1]*v2[:,0],v1[:,0]*v2[:,0]+v1[:,1]*v2[:,1])
		angles_dict[csv_directory]=angles
		
		hi,bins=numpy.histogram(angles,bins=40)
		
		#ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
		n=len(hi)
		bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
		for i in range(n):
			bar[i].set_facecolor(matplotlib.cm.jet(0.7))
			bar[i].set_alpha(0.5)
			
		# KS test
		#ks=scipy.stats.kstest(angles,'uniform',args=(-pi,2*pi))
		#p.set_xlabel(csv_directory+'\nN=%i of %i'%(len(angles),len(ct.dx))+'\nKS Test: D=%f p=%f'%ks)
		p.set_xlabel(csv_directory+'\nN=%i of %i'%(len(angles),len(ct.dx)))
			
	
	
		ct.show_plots(block=False)

ct.show_plots(block=True)

pass


#### Ellie's data over time
#csv_root='./Ellie CSV format/'
#csv_directorys=['AC-TGFP','AC-TNULL','Act-TGFP','Act-TNULL','Chi-TGFP','Chi-TNULL']
##csv_directorys=['AC-TGFP']#,'AC-TNULL','Act-TGFP']
#angles_dict={}
#ct_dict={}

#fig=pyplot.figure()
#fig.suptitle('Cell Step Direction')
#plot_num=1
#t_begins=[]
#t_ends=[]
#for csv_directory in csv_directorys:
	#ct=amatools.AMACellTracks()
	#ct.read_mtrackj_csv_dir(csv_root+csv_directory)
	##ct.filter_by_track_size(10)
	#ct.compute_tracks()
	#ct_dict[csv_directory]=ct
	#t_begins+=ct.t_begin
	#t_ends+=ct.t_end
	
#t_min=min(t_begins)
#t_max=max(t_ends)
#num_t_bins=5

#for csv_directory in csv_directorys:
	#ct=ct_dict[csv_directory]
	
	#t_bins=numpy.linspace(t_min,t_max,num_t_bins)
	#t_begins_digitized=numpy.digitize(ct.t_begin,t_bins)
	
	#for t_bin in range(1,num_t_bins):

		## movment histogram	
		#p=fig.add_subplot(len(csv_directorys),num_t_bins-1,plot_num,polar=True) 		
		#plot_num+=1	
		
		#t_filter=t_begins_digitized==t_bin
		#tracks=numpy.array(ct.tracks)[t_filter]
		#tracks_d=[]
		#tracks_dx=[]
		#tracks_dy=[]
		#for t in tracks:
			#tracks_d+=t.d
			#tracks_dx+=t.dx
			#tracks_dy+=t.dy
		
		#mag_filter=numpy.array(tracks_d)>3 #filter small steps with quantisation error - NB in microns not pixels (ie MTrackJ csv format)

		#dx=numpy.array(tracks_dx)[mag_filter]
		#dy=numpy.array(tracks_dy)[mag_filter]	
		
		#v1=numpy.vstack((dx,dy)).transpose()
		#v2=v1[1:]
		#v1=v1[0:-1]
		#angles=numpy.arctan2(v1[:,0]*v2[:,1]-v1[:,1]*v2[:,0],v1[:,0]*v2[:,0]+v1[:,1]*v2[:,1])
		#angles_dict[csv_directory]=angles
		
		#hi,bins=numpy.histogram(angles,bins=40)
		
		##ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
		#n=len(hi)
		#bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
		#for i in range(n):
			#bar[i].set_facecolor(matplotlib.cm.jet(0.7))
			#bar[i].set_alpha(0.5)
			
		## KS test
		##ks=scipy.stats.kstest(angles,'uniform',args=(-pi,2*pi))
		##p.set_xlabel(csv_directory+'\nN=%i of %i'%(len(angles),len(ct.dx))+'\nKS Test: D=%f p=%f'%ks)
		#if csv_directory is csv_directorys[-1]:
			##p.set_xlabel('from T=%d hrs\nN=%i of %i'%(t_bins[t_bin-1]/3600,len(angles),len(dx)))
			#p.set_xlabel('from T=%d hrs'%(t_bins[t_bin-1]/3600))
		#if t_bin==1:
			#p.set_ylabel(csv_directory,labelpad=150)
	

	#ct.show_plots(block=False)

#pass
#ct.show_plots(block=True)


### Daniel's data

##d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27/raw data'
##d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27+Act+Chi 1/raw data'
##d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27 + Act + Chi 2/raw data'
##ct.read_cge_csvs(d)
#csv_root='./DATA october/'
#csv_directorys=['N2B27', 'N2B27 + Act + Chi 2', 'N2B27 2', 'N2B27 3', 'N2B27+Act+Chi 1']
##csv_directorys=['N2B27']
##csv_directorys=[]
#fig=pyplot.figure()
#fig.suptitle('Cell Step Direction')
#plot_num=1
#for csv_directory in csv_directorys:
	#ct=amatools.AMACellTracks()
	##ct.read_mtrackj_csv_dir(csv_root+csv_directory)
	#ct.read_cge_csvs(csv_root+csv_directory+'/raw data')
	##ct.filter_by_track_size(10)
	#ct.compute_tracks()
	
	## movment histogram	
	#p=fig.add_subplot(1,len(csv_directorys),plot_num,polar=True) 
	
	#mag_filter=numpy.array(ct.d)>3 #filter small steps with quantisation error - NB in microns not pixels (ie MTrackJ csv format)
	#dx=numpy.array(ct.dx)[mag_filter]
	#dy=numpy.array(ct.dy)[mag_filter]
	
	#v1=numpy.vstack((dx,dy)).transpose()
	#v2=v1[1:]
	#v1=v1[0:-1]
	#angles=numpy.arctan2(v1[:,0]*v2[:,1]-v1[:,1]*v2[:,0],v1[:,0]*v2[:,0]+v1[:,1]*v2[:,1])
	
	## angle = atan2( a.x*b.y - a.y*b.x, a.x*b.x + a.y*b.y ) perp dot product
	## http://stackoverflow.com/questions/2150050/finding-signed-angle-between-vectors
	
	##ddx=dx[1:]-dx[0:-1]
	##ddy=dy[1:]-dy[0:-1]
	
	##angles=[math.atan2(y,x) for x,y in zip(dx,dy)]
	##angles=[math.atan2(y,x) for x,y in zip(ddx,ddy)]
	
	#hi,bins=numpy.histogram(angles,bins=40)
	
	##ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
	#n=len(hi)
	#bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
	#for i in range(n):
		#bar[i].set_facecolor(matplotlib.cm.jet(0.7))
		#bar[i].set_alpha(0.5)
		
	## KS test
	#ks=scipy.stats.kstest(angles,'uniform',args=(-pi,2*pi))

	#p.set_xlabel(csv_directory+'\nN=%i of %i'%(len(angles),len(ct.dx))+'\nKS Test: D=%f p=%f'%ks)
		
	#plot_num+=1

	#ct.show_plots(block=False)
	#pass

#ct.show_plots(block=True)
	
## test KS
#fig=pyplot.figure()
#fig.suptitle('KS Test')
#plot_num=1
#R=[0.01,0.05,0.1,0.5,1.]
#R=[]
#N=1000
#for s in R:

	## movment histogram	
	#p=fig.add_subplot(1,len(R),plot_num,polar=True) 
	
	#angles=s*numpy.random.triangular(-pi,0,pi,N)+(1-s)*numpy.random.uniform(-pi,pi,N)
	#hi,bins=numpy.histogram(angles,bins=40)
	
	##ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
	#n=len(hi)
	#bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
	#for i in range(n):
		#bar[i].set_facecolor(matplotlib.cm.jet(0.3))
		#bar[i].set_alpha(0.5)
		
	## KS test
	#ks=scipy.stats.kstest(angles,'uniform',args=(-pi,2*pi))

	#p.set_xlabel('Test'+'\nN=%i'%len(angles)+'\nKS Test: D=%f p=%f'%ks)
		
	#plot_num+=1

	#ct.show_plots(block=False)
	#pass

#ct.show_plots(block=True)
#pass

## test KS
#fig=pyplot.figure()
#fig.suptitle('KS Test')
#plot_num=1
#R=[100,500,1000,5000,10000]
#for N in R:

	## movment histogram	
	#p=fig.add_subplot(1,len(R),plot_num,polar=True) 
	
	#angles=numpy.random.uniform(-pi,pi,N)
	#hi,bins=numpy.histogram(angles,bins=40)
	
	##ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
	#n=len(hi)
	#bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
	#for i in range(n):
		#bar[i].set_facecolor(matplotlib.cm.jet(0.3))
		#bar[i].set_alpha(0.5)
		
	## KS test
	#ks=scipy.stats.kstest(angles,'uniform',args=(-pi,2*pi))

	#p.set_xlabel('Test'+'\nN=%i'%len(angles)+'\nKS Test: D=%f p=%f'%ks)
		
	#plot_num+=1

	#ct.show_plots(block=False)
	#pass

#d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27/raw data'
#d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27+Act+Chi 1/raw data'
#d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27 + Act + Chi 2/raw data'
#ct.read_cge_csvs(d)

#ct.plot_tracks()
#ct.plot_movement_distributions()
#ct.plot_track_lengths()
#ct.plot_msd()
#ct.plot_displacment()
#ct.plot_persistence()

