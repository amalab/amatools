from chaco.api import ArrayPlotData
from traits.api import Instance,HasTraits
from traitsui.api import Item,View

from enable.api import Component,ComponentEditor

class Test(HasTraits):
	plotdata = ArrayPlotData()
	plot = Instance(Component)
	
	traits_view=View(Item('plot',editor=ComponentEditor()))
	
	
test = Test()
if __name__ == "__main__":
	test.configure_traits()
