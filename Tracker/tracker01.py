#
# Tracker
# Cell Segment and Track Tool
# JPM
# July 2012
#

"""
 - Left-drag pans the plot.
 - Mousewheel up and down zooms the plot in and out.
 - Pressing "z" brings up the Zoom Box, and you can click-drag a rectangular
   region to zoom.  If you use a sequence of zoom boxes, pressing alt-left-arrow
   and alt-right-arrow moves you forwards and backwards through the "zoom
   history".
"""

# Major library imports
from numpy import empty,zeros, ones, uint8, transpose, exp, hypot
import numpy.random

# Enthought library imports
from enable.api import Component, ComponentEditor
from traits.api import HasTraits, Instance, Property, Array, Range, Bool, Button, Enum, on_trait_change
from traitsui.api import Item, Group, View, RangeEditor, Tabbed

# Chaco imports
from chaco.api import ArrayPlotData, Plot, OverlayPlotContainer
from chaco.tools.api import PanTool, ZoomTool
from chaco.tools.image_inspector_tool import ImageInspectorTool, ImageInspectorOverlay
import chaco.default_colormaps

import tifffile
# sci-kits image processing
import skimage.filter, skimage.morphology
import scipy.ndimage.morphology
import scipy.ndimage.filters
#import sklearn.feature_extraction.image 
#import sklearn.cluster

import h5py

###GET DATA
data_source='h5'
#data_source='ome-tiff'

##from xinghua's h5
if data_source=='h5':
	h5fn=r'/Users/jpm/Projects/Xinghua/xlou-BOT-b7abf65/data/dcelliq-sequence-training.h5'
	h5f=h5py.File(h5fn,'r')
	nt=len(h5f['Raw']) #num time steps
	nc=2 #num channels
	nx=len(h5f['Raw']['00000000']['Data'])
	ny=len(h5f['Raw']['00000000']['Data'][0])
	
	imgarray=empty((nt,nc,nx,ny))
	t=0
	for i in h5f['Raw']:
		imgarray[t][0][:][:]=h5f['Raw'][i]['Data'][:]
		t+=1
	t=0
	for i in h5f['Segmentation']:
		imgarray[t][1][:][:]=h5f['Segmentation'][i]['Data'][:]
		t+=1

	pass
else:
##from ome-tiff
	#fn = tkFileDialog.askopenfilename()
	#fn=r'/Users/jpm/DATA/David - Images for tracking/2012-03-19 Wntch 2iLIF to labelled-0002_2i to ESminusLIF_01.zvi - C=2 slices=90-100 8-bit.tif'
	#fn=r'/Users/jpm/DATA/_scratch/120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2.ome.tiff'
	fn=r'/Users/jpm/DATA/_scratch/120813_LED_test-0004_Gata6Venus_500dox_#3.ome.tiff'
	#fn=r'/Users/jpm/DATA/_scratch/esgZ_GSHg_ArmPros_DAPI_4-6DO_G5F30.ome.tiff'

	# get tiff file as numpy array
	tiffimg = tifffile.TIFFfile(fn)
	imgarray = tiffimg.asarray()
	#print imgarray.shape

	# prune array - [time,channel,z,y,x] becomes [time,channel,y,x]
	imgarray=imgarray[:,:,0,:,:]

	#ch1 is GFP for 120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2

	imgarray=transpose(imgarray,(0,1,3,2)) #[time,channel,x,y]


num_channels=imgarray.shape[1]                 
time_max=imgarray.shape[0]-1
time_init=43
level_max=4096
shape_x=imgarray.shape[3]
shape_y=imgarray.shape[2]
image_crop=(shape_x,shape_y)#(500,500)

# Attributes to use for the plot view.
size = (900, 900)
title="Image Viewer"
bg_color="lightgray"

# gaussian filter for scikits-image
def filt_func(r, c):
	return exp(-hypot(r, c)/1)
gaussian_filter = skimage.filter.LPIFilter2D(filt_func)

# for morphological filters
structuring_element=scipy.ndimage.morphology.generate_binary_structure(2, 2)

# colour maps
# random for segmentation view
r=numpy.random.random((level_max,4))
r[0,:]=(1.0,1.0,1.0,0.0) #white background
rand_col_map=chaco.api.ColorMapper.from_palette_array(r)
# greens for normal
greens_col_map=chaco.default_colormaps.reverse(chaco.default_colormaps.Greens)

from enthought.traits.ui.key_bindings import KeyBinding, KeyBindings

key_bindings = KeyBindings(
    KeyBinding( binding1    = 'Left',
                description = 'Time Backward',
                method_name = '_time_backward_button_changed' ),
    KeyBinding( binding1    = 'Right',
                description = 'Time Forward',
                method_name = '_time_forward_button_changed' )    
)

class ImageViewer(HasTraits):
	#plot = Instance(Component)
	plotdata = ArrayPlotData()

	plot_orig = Instance(Component)
	plot_processed = Instance(Component)
	plot_segmented = Instance(Component)

	image_orig=Array
	image_processed=Array
	image_segmented=Array

	image=Enum(('orig','processed','segmented'))
	channel=Range(0,num_channels-1,1,editor=RangeEditor(low=0, high=num_channels-1, mode="spinner"))
	time=Range(0,time_max,time_init,editor=RangeEditor(low=0, high=time_max, mode="slider"))
	block_size=Range(10,200,100,editor=RangeEditor(low=10, high=199, mode="slider"))
	morph_iterations=Range(1,10,editor=RangeEditor(low=1, high=9, mode="slider"))
	LoG_sigma1=Range(1.0,20.0,editor=RangeEditor(low=1.0, high=19.0, mode="slider"))
	LoG_sigma2=Range(1.0,20.0,editor=RangeEditor(low=1.0, high=19.0, mode="slider"))

	LoG_switch=Bool(label='LoG')
	thresh_switch=Bool(True)	
	opening_switch=Bool(True)
	closing_switch=Bool
	watershed_switch=Bool(True)

	time_backward_button=Button(label='<-',name='')
	time_forward_button=Button(label='->')
	key_bindings_button=Button()

	traits_view = View(
		Group(
		    Group(	
		        Item('key_bindings_button',show_label=False),
		        Item('image'),
		        Item('channel'),
		        Item('LoG_switch'),
		        Item('LoG_sigma1'),
		        Item('LoG_sigma2'),
		        Item('thresh_switch'),
		        Item('block_size'),
		        Item('opening_switch'),
		        Item('closing_switch'),
		        Item('morph_iterations'),
		        Item('watershed_switch'),            
		        orientation = 'vertical'
		        ),
		    Group( 
		        Item('plot_orig',editor=ComponentEditor(size=size),show_label=False),
		        #Tabbed(
		        #Item('plot_orig', 
		        #editor=ComponentEditor(size=size),#,bgcolor=bg_color),
		        #show_label=False
		        #),
		        #Item('plot_processed', 
		        #editor=ComponentEditor(size=size),#,bgcolor=bg_color),
		        #show_label=False
		        #),
		        #Item('plot_segmented', 
		        #editor=ComponentEditor(size=size),#,bgcolor=bg_color),
		        #show_label=False
		        #),
		        #),
		        Group(
		            Item('time_backward_button',show_label=False),	
		            Item('time_forward_button',show_label=False),
		            Item('time',springy=True),
		            orientation='horizontal'
		            ),
		        orientation = 'vertical'
		        ),
		    orientation = 'horizontal'
		    ),
		resizable=True, title=title, key_bindings=key_bindings
	)

	def _time_backward_button_changed(self):
		try:
			self.time-=1
		except:
			pass

	def _time_forward_button_changed(self):
		try:
			self.time+=1
		except:
			pass

	def _key_bindings_button_fired(self,event):
		key_bindings.edit_traits()


	@on_trait_change('LoG_switch,LoG_sigma1,LoG_sigma2,thresh_switch,opening_switch,closing_switch,watershed_switch,morph_iterations,block_size,channel,time')		
	def update(self):
		#update images and plot data
		self.image_orig=imgarray[self.time,self.channel,0:image_crop[0],0:image_crop[1]]
		self.image_processed=self.image_orig.copy()

		#im_thresh=self.image > (self.thresh*level_max)
		#self.image=im_thresh 

		#self.image=gaussian_filter(self.image)	

		if self.LoG_switch:
			self.image_processed=scipy.ndimage.filters.gaussian_laplace(self.image_processed,(self.LoG_sigma1,self.LoG_sigma2))

		if self.thresh_switch:
			#block_size=3+self.thresh*100
			if self.block_size%2==0:
				self.block_size+=1 #must be odd
			self.image_processed=skimage.filter.threshold_adaptive(self.image_processed, self.block_size)		

		#if self.denoise_switch:
			#self.image_processed=skimage.filter.tv_denoise(self.image_processed)	

		if self.opening_switch:
			self.image_processed=scipy.ndimage.morphology.binary_opening(self.image_processed,structuring_element,self.morph_iterations)

		if self.closing_switch:
			self.image_processed=scipy.ndimage.morphology.binary_closing(self.image_processed,structuring_element,self.morph_iterations)

		if self.watershed_switch:	
			distance = scipy.ndimage.distance_transform_edt(self.image_processed)
			local_max = skimage.morphology.is_local_maximum(distance, self.image_processed, ones((3, 3)))
			markers = scipy.ndimage.label(local_max)[0]
			labels = skimage.morphology.watershed(-distance, markers, mask=self.image_processed)	
			self.image_segmented=labels


			## Convert the image into a graph with the value of the gradient on the
			## edges.
			#mask=self.image_processed
			#graph = sklearn.feature_extraction.image.img_to_graph(self.image_processed,mask=mask)

			## Take a decreasing function of the gradient: we take it weakly
			## dependant from the gradient the segmentation is close to a voronoi
			#graph.data = exp(-graph.data / graph.data.std())

			## Force the solver to be arpack, since amg is numerically
			## unstable on this example
			#labels = sklearn.cluster.spectral_clustering(graph, n_components=4, mode='arpack')
			#label_im = -ones(mask.shape)
			#label_im[mask] = labels	

			#self.image_segmented=label_im

		bb_slices=scipy.ndimage.measurements.find_objects(self.image_segmented)
		#for i in range(100):#len(bb_slices)):	
			#s=bb_slices[i]
			#y1=s[0].start
			#y2=s[0].stop
			#x1=s[1].start
			#x2=s[1].stop
			#self.plotdata.set_data("x"+str(i),[x1,x2,x2,x1])
			#self.plotdata.set_data("y"+str(i),[y1,y1,y2,y2])	

		x=[]
		y=[]
		for i in range(len(bb_slices)):	
			s=bb_slices[i]
			y1=s[0].start
			y2=s[0].stop
			x1=s[1].start
			x2=s[1].stop	
			x.append((x1+x2)/2)
			y.append((y1+y2)/2)

		self.plotdata.set_data("x",x)
		self.plotdata.set_data("y",y)


		self.plotdata.set_data("image_orig_data", self.image_orig)	
		self.plotdata.set_data("image_processed_data", self.image_processed)
		self.plotdata.set_data("image_segmented_data", self.image_segmented)


	#@on_trait_change('image')
	def _image_changed(self):
		self._plot_orig_default()

	def _plot_orig_default(self):
		self.update()

		# Create the plot from the data 
		self.plot_orig = Plot(self.plotdata, default_origin="top left")
		self.plot_orig.x_axis.orientation = "top"

		if self.image=='orig':
			image_plot = self.plot_orig.img_plot("image_orig_data","image_plot",colormap=greens_col_map)[0]
		elif self.image=='processed':
			image_plot = self.plot_orig.img_plot("image_processed_data","image_plot",colormap=greens_col_map)[0]
		elif self.image=='segmented':
			image_plot = self.plot_orig.img_plot("image_segmented_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		# Tweak some of the plot properties
		#self.plot.bgcolor = "white"

		#poly plot of segmented bounding boxes
		#for i in range(100):#len(bb_slices)):	
			#polyplot = self.plot_orig.plot(("x"+str(i), "y"+str(i)), 
							#type="polygon", 
							#hittest_type="poly")[0]	

		#polyplot = self.plot_orig.plot(("x"+str(i), "y"+str(i)), 
							#type="polygon", 
							#hittest_type="poly")[0]


		self.plot_orig.plot(("x","y"),type="scatter",marker="circle")		


		# Attach some tools to the plot
		pantool=PanTool(self.plot_orig, constrain_key="shift")
		self.plot_orig.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_orig,tool_mode="box", always_on=False)
		self.plot_orig.overlays.append(zoomtool)

		#imgtool = ImageInspectorTool(image_orig_plot)
		#image_orig_plot.tools.append(imgtool)
		#self.plot_orig.overlays.append(ImageInspectorOverlay(component=image_orig_plot,image_inspector=imgtool))	


		return self.plot_orig

	#def _plot_processed_default(self):
		#self.update()


		## Create the plot
		#self.plot_processed = Plot(self.plotdata, default_origin="top left")
		#self.plot_processed.x_axis.orientation = "top"
		#image_processed_plot = self.plot_processed.img_plot("image_processed_data","image_processed_plot",colormap=greens_col_map)[0]

		## Tweak some of the plot properties
		##self.plot.bgcolor = "white"

		## Attach some tools to the plot
		#pantool=PanTool(self.plot_processed, constrain_key="shift")
		#self.plot_processed.tools.append(pantool)
		#zoomtool=ZoomTool(component=self.plot_processed,tool_mode="box", always_on=False)
		#self.plot_processed.overlays.append(zoomtool)

		##sync range set by zoom and pan tools
		##self.plot_processed.range2d=self.plot_orig.range2d		
		#self.plot_processed.sync_trait('range2d',self.plot_orig)

		##imgtool = ImageInspectorTool(image_plot)
		##img_plot.tools.append(imgtool)
		##self.plot_processed.overlays.append(ImageInspectorOverlay(component=image_plot,
											##image_inspector=imgtool))	

		#return self.plot_processed

	#def _plot_segmented_default(self):
		#self.update()

		## Create the plot
		#self.plot_segmented = Plot(self.plotdata, default_origin="top left")
		#self.plot_segmented.x_axis.orientation = "top"
		#image_segmented_plot = self.plot_segmented.img_plot("image_segmented_data","image_segmented_plot",colormap=rand_col_map,bg_color='transparent')[0]

		## Tweak some of the plot properties
		##self.plot.bgcolor = "white"

		## Attach some tools to the plot
		#pantool=PanTool(self.plot_segmented, constrain_key="shift")
		#self.plot_segmented.tools.append(pantool)
		#zoomtool=ZoomTool(component=self.plot_segmented,tool_mode="box", always_on=False)
		#self.plot_segmented.overlays.append(zoomtool)

		##sync range set by zoom and pan tools
		##self.plot_processed.range2d=self.plot_orig.range2d		
		#self.plot_segmented.sync_trait('range2d',self.plot_orig)

		##imgtool = ImageInspectorTool(image_plot)
		##img_plot.tools.append(imgtool)
		##self.plot_processed.overlays.append(ImageInspectorOverlay(component=image_plot,
											##image_inspector=imgtool))	

		#return self.plot_segmented



###MAIN###
iv = ImageViewer()
if __name__ == "__main__":
	iv.configure_traits()

#--EOF---