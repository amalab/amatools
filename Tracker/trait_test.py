from chaco.api import ArrayPlotData,Plot,ScatterInspectorOverlay
from chaco.tools.api import ScatterInspector
from traits.api import Instance,HasTraits,Button
from traitsui.api import Item,View
from enable.api import Component,ComponentEditor
from random import randint

class Test(HasTraits):
	plotdata = ArrayPlotData()
	plot = Instance(Component)
	button=Button(label='Test')	
	
	traits_view=View(Item('plot',editor=ComponentEditor()),Item('button'))
	
	first_time=True
	sel_stored=0
		
	def _plot_default(self):	
		self.plot = Plot(self.plotdata)	
	
		#update scatter plot
		x=[randint(0,100) for i in range(10)]
		y=[randint(0,100) for i in range(10)]
		
		self.plotdata.set_data("x",x)
		self.plotdata.set_data("y",y)
	
		# for selection
		marker_size=5
		self.scatter_plot=self.plot.plot(("x","y"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		# Attach the inspector and its overlay
		self.scatter_inspector=ScatterInspector(self.scatter_plot,selection_mode='single')
		self.scatter_plot.tools.append(self.scatter_inspector)
		overlay = ScatterInspectorOverlay(self.scatter_plot,
					      hover_marker="circle",
					      hover_marker_size=marker_size,		                                  
					      hover_color="grey",
					      selection_marker="circle",
					      selection_marker_size=marker_size,
					      selection_color="white",
					      selection_line_width=0)
		self.scatter_plot.overlays.append(overlay)
		
		#called when data metadata changes to indicate data selected
		self.scatter_plot.value.on_trait_change(self.selection_changed)		
	
		return self.plot
	
	def selection_changed(self,object,name,old,new):
		sel=object.metadata['selections']

		print sel,self.sel_stored
		
		if len(sel)>0 and self.first_time:
			self.sel_stored=object.metadata['selections'][0]
			self.first_time=False
		
	def _button_fired(self,event):	
		self.set_selections([0])
		print self.sel_stored
			
	def set_selections(self,sel):
		self.scatter_inspector.component.index.metadata['selections']=sel		
		self.scatter_inspector.component.index.metadata_changed=True
		self.scatter_inspector.component.value.metadata['selections']=sel	
		self.scatter_inspector.component.value.metadata_changed=True
			
	
test = Test()
if __name__ == "__main__":
	test.configure_traits()
