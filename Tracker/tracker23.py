#
# Tracker
# Cell Segment and Track Tool
# JPM
# July 2012
#

# Major library imports
from numpy import *
import numpy.random
#import wx

# Enthought library imports
from enable.api import Component,ComponentEditor,ColorTrait
from traits.api import *
from traitsui.api import *
#from enthought.traits.ui.wx.extra.bounds_editor import BoundsEditor
from enthought.pyface.api import FileDialog, OK, confirm, YES
from pyface.timer.api import Timer

# Chaco imports
from chaco.api import ArrayPlotData,Plot,OverlayPlotContainer,HPlotContainer,AbstractOverlay,ScatterInspectorOverlay
from chaco.tools.api import PanTool,ZoomTool,SelectTool,ScatterInspector,DrawPointsTool
from chaco.tools.image_inspector_tool import ImageInspectorTool,ImageInspectorOverlay
import chaco.default_colormaps
#from chaco.default_colormaps import fix

import tifffile
# sci-kits image processing
import skimage.filter, skimage.morphology
import scipy.ndimage.morphology
import scipy.ndimage.filters

import h5py
import easygui
import subprocess
import os
import shutil
import threading
from multiprocessing import Process


from sklearn.decomposition import PCA

#file locations
this_path=os.path.dirname(os.path.abspath(__file__))
bot_path=this_path+r'/BOT'
data_path=r'/Users/jpm/DATA/OME'

# Attributes to use for the plot view.
size = (1600,600)
title="Cell Tracker"
bg_color="lightgray"
pi=3.14159
# gaussian filter for scikits-image
def filt_func(r, c):
	return exp(-hypot(r, c)/1)
gaussian_filter = skimage.filter.LPIFilter2D(filt_func)

# for morphological filters
structuring_element=scipy.ndimage.morphology.generate_binary_structure(2, 2)

# colour maps
# random for segmentation view
r=numpy.random.random((1024,4))
r[0,:]=(1.0,1.0,1.0,0.0) #white background
rand_col_map=chaco.api.ColorMapper.from_palette_array(r)
# greens for GFP
greens_col_map=chaco.default_colormaps.reverse(chaco.default_colormaps.Greens)
reds_col_map=chaco.default_colormaps.Reds
# greys for brightfield
greys_col_map=chaco.default_colormaps.reverse(chaco.default_colormaps.Greys)

#rand_col_map = chaco.default_colormaps.fix(rand_col_map, (0,1024))
greens_col_map = chaco.default_colormaps.fix(chaco.default_colormaps.reverse(chaco.default_colormaps.Greens),(0,4096))


from enthought.traits.ui.key_bindings import KeyBinding, KeyBindings

def h5_time_string(time):
	t=str(time)
	t='0'*(8-len(t))+t # make 8 digit time string
	return t

class PixelSelectTool(SelectTool):
	def normal_left_down(self,event):
		print 'SelectTool:',event

class Cell:
	def __init__(self):
		self.id=0 #segmentation id
		self.bb=[0,0,0,0] #bounding box [x1,x2,y1,y2]
		self.track_type='None'
		self.targets=[]
		self.source=None
		self.pc=(0,0)

	def centre(self):
		return ((self.bb[0]+self.bb[1])/2,(self.bb[2]+self.bb[3])/2)

class ImageViewer(HasTraits):
	'''
	 images are stored as ndarrays which are accessed [row][column] ie [y][x]

	 imgarray - numpy array - [time,ch,y,x] 

	 image_orig - numpy array - [y,x] for given t,ch and x,y, crop
	 image_orig_plus - ditto for t+1
	 image_processed - after filtering
	 image_segmented - pixel value is segmentation id

	 plotdata - ArrayPlotData holding all data for one t,t+1 pair
	 plot_orig - Instance(Component) Trait for LH plot
	 plot_orig_plus - Instance(Component) Trait for RH plot
	 plot_container - holds above LH and RH plots and for drawing associations etc

	 image_plot (local) returned image plot created on plot		
	'''

	plotdata = ArrayPlotData()
	#plot_orig = Instance(Component)
	#plot_orig_plus = Instance(Component)
	#plot_11=Instance(Component)
	#plot_12=Instance(Component)
	plot_container_1=Instance(Component)
	plot_container_2=Instance(Component)

	imgarray=Array
	#image_orig=Array
	#image_orig_plus=Array
	#image_processed=Array
	#image_processed_plus=Array
	#image_segmented=Array
	#image_segmented_plus=Array
	#image_outline=Array
	#image_outline_plus=Array

	#cells=[[],[]]

	zero=Int(0)
	num_channels=Int(1)
	time_max=Int(1)

	#data_source=String('')
	image_enum=List(['orig','processed','segmented','distance','local_max'])
	image_type=Str('orig')

	#channel=Range(0,4,0,editor=RangeEditor(low=0, high=4, mode="enum"))
	channel=Range(low='zero',high='num_channels',value=0,editor=RangeEditor(low_name='zero',high_name='num_channels',mode="spinner"))
	#time=Range(0,time_max,time_init,editor=RangeEditor(low=0, high=time_max, mode="slider"))
	time=Range(low='zero',high='time_max',value=0,editor=RangeEditor(low_name='zero',high_name='time_max',mode="slider"))
	time_end=Range(low='zero',high='time_max',value=0,editor=RangeEditor(low_name='zero',high_name='time_max',mode="slider"))
	block_size=Range(10,200,100,editor=RangeEditor(low=10, high=199, mode="slider"))
	morph_iterations=Range(1,10,2,editor=RangeEditor(low=1, high=9, mode="slider"))
	LoG_sigma1=Range(1.0,20.0,editor=RangeEditor(low=1.0, high=19.0, mode="slider"))
	LoG_sigma2=Range(1.0,20.0,editor=RangeEditor(low=1.0, high=19.0, mode="slider"))
	footprint=Range(0,10,4,editor=RangeEditor(low=0, high=9, mode="slider"))

	LoG_switch=Bool(label='LoG')
	thresh_switch=Bool(True)	
	opening_switch=Bool(True)
	closing_switch=Bool
	watershed_switch=Bool(True)
	outline_switch=Bool(False)
	tracks_enum=List(('none','all','selected'))
	tracks_mode=Str('all')

	time_backward_button=Button(label='<-',name='')
	time_forward_button=Button(label='->')
	#key_bindings_button=Button()
	output_text=Str('No Selection')

	bot_button=Button(label='Run BOT')	
	open_tiff_button=Button(label='Open TIFF')
	segment_button=Button(label='Segment')
	open_h5_button=Button(label='Open H5')
	save_h5_button=Button(label='Save H5')

	label_1=Str('Step 1 - Open TIFF')
	label_2=Str('Step 2 - Segment')
	label_3=Str('Step 3 - Edit Segmentation')
	label_4=Str('Step 4 - Save H5')
	label_5=Str('Step 5 - Run BOT')
	label_1b=Str('OR - Open H5')
	label_6=Str('Step 6 - Edit Tracks')
	label_7=Str('Step 7 - Save H5')
	label_8=Str('Step 8 - Track Cells')
	label_ctrls=Str('Image Controls')

	delete_track_button=Button(label='Delete Track')

	track_switch=Bool(False,label='Group')
	track_button=Button(label='Track')
	
	timer = Instance(Timer)

	space=10      
	traits_view=View(
		Group( # whole UI
		       Group( # steps 	                       
		              Group(
		                  Item('label_1',show_label=False,style='readonly'),
		                  Item('open_tiff_button',show_label=False),
		                  orientation='vertical'),	               
		              Item(str(space)),#spacer	    

		              Group(  
		                  Item('label_2',show_label=False,style='readonly'),	 
		                  Item('segment_button',show_label=False),
		                  Item('block_size'),
		                  Item('opening_switch'),
		                  Item('closing_switch'),
		                  Item('morph_iterations'),
		                  Item('footprint'),
		                  orientation='vertical'),
		              Item(str(space)),#spacer	     

		              Group(
		                  Item('label_5',show_label=False,style='readonly'),	                       
		                  Item('bot_button',show_label=False), 
		                  orientation='vertical'),		
		              Item(str(space)),#spacer	  

		              Group(
		                  Item('label_1',show_label=False,style='readonly'),	                       	                       
		                  Item('open_h5_button',show_label=False),
		                  orientation='vertical'),
		              Item(str(space)),#spacer	   

		              Group(
		                  Item('label_2',show_label=False,style='readonly'),	                       	                       	                       
		                  Item('tracks_mode',label='Show Tracks',editor=EnumEditor(name='tracks_enum',mode='radio')),	                      
		                  Item('delete_track_button',show_label=False),
		                  orientation='vertical'),
		              Item(str(space)),#spacer

		              Group(
		                  Item('label_3',show_label=False,style='readonly'),	                       	                       	                       
		                  Item('save_h5_button',show_label=False),	 
		                  orientation='vertical'),
		              Item(str(space)),#spacer	

		              Group(
		                  Item('label_4',show_label=False,style='readonly'),	                       	                       	                       
		                  Item('track_switch',show_label=False),	 
		                  Item('track_button',show_label=False),	 
		                  orientation='vertical'),
		              Item(str(space)),#spacer		               

		              Group(
		                  Item('label_ctrls',show_label=False,style='readonly'),	                       	                       	                       	                       
		                  Item('image_type',editor=EnumEditor(name='image_enum')),
		                  Item('channel'),	                      
		                  orientation='vertical'),
		              Item(str(space)),#spacer		               

		              Item('output_text',show_label=False,width=-200,height=-20),

		              orientation='vertical'),

		       Group(#plots
		             Item('plot_container_1',editor=ComponentEditor(),show_label=False,width=-1200,height=-450), #plots
		             Item('plot_container_2',editor=ComponentEditor(),show_label=False,width=-1200,height=-450), #plots

		             Group( #time bar
		                    Item('time_backward_button',show_label=False),	
		                    Item('time_forward_button',show_label=False),
		                    Group(
		                        Item('time',springy=True),   
		                        Item('time_end',springy=True),  
		                        orientation='vertical',
		                        springy=True),
		                    orientation='horizontal'
		                    ),
		             orientation='vertical'
		             ),

		       orientation='horizontal'
		       ),        
		resizable=True
	)

	def __init__(self):
		HasTraits.__init__(self)

		self.fn_tiff='' #absolute path
		self.fn_h5='' #local to bot_path/data	
		self.h5f=None
		self.data_source=''

		#self.imgarray_sources={'orig':0,'proc':1,'seg':2}
		self.imgarray_sources={'orig':0,'seg':1}
		self.num_sources=len(self.imgarray_sources)
		#self.imgarray=zeros((self.num_sources,1,1,480,640),dtype=numpy.uint16) #s,t,ch,y,x
		self.imgarray=zeros((self.num_sources,2,1,520,692)) #s,t,ch,y,x
		self.imgarray[self.imgarray_sources['orig'],0,0,250:270,:]=256
		self.reset_state()
		self.update_data()

		self.selected_cell=None
		self.selected_cells=[]
		self.mouse_state='up'
		self.hover_selection_plus=None

		self.block_selection_changed=False

		#self.count=0		
		#self.timer = Timer(40, self.onTimer)
		
	#def onTimer(self, *args):
		#self.output_text=str(self.count)
		#self.count+=1

	def reset_state(self):		
		self.time=0
		self.channel=0
		self.num_channels=self.imgarray.shape[2]-1               
		self.time_max=self.imgarray.shape[1]-2 #for t+1

		#level_max=4096
		#self.shape_x=self.imgarray.shape[4]
		#self.shape_y=self.imgarray.shape[3]
		#image_crop=(self.shape_x,self.shape_y)#(500,500)	

		#self.cells=[[],[]]

		self.clear_all_selections()
		#self.cells=[]
		#for i in range(self.imgarray.shape[0]):
			#self.cells.append([])

		#self.channel=Range(0,self.num_channels,0,editor=RangeEditor(low=0, high=self.num_channels, mode="spinner"))
		#self.time=Range(0,self.time_max,self.time_init,editor=RangeEditor(low=0, high=self.time_max-1, mode="slider"))

	def _open_tiff_button_fired(self,events):
		#self.fn_tiff=easygui.fileopenbox(default=data_path+'/')
		dialog = FileDialog(action="open", default_path=data_path+'/')
		dialog.open()
		if dialog.return_code == OK:		
			self.fn_tiff=dialog.path

			if self.fn_tiff:
				# get tiff file as numpy array
				tiffimg = tifffile.TIFFfile(self.fn_tiff)
				self.imgarray=tiffimg.asarray()
				# prune array - [time,channel,z,y,x] becomes [time,channel,y,x]
				self.imgarray=self.imgarray[:,:,0,:,:]
				
				##reduce to 8-bit
				#self.imgarray/=((numpy.max(self.imgarray)+1)/256)
				#self.imgarray=self.imgarray.astype('uint8')
				
				# clone x number of sources, and zero
				s=[self.num_sources]
				s+=self.imgarray.shape
				self.imgarray=numpy.resize(self.imgarray,s)
				#print imgarray.shape
				self.imgarray[1:]=0
				
				self.data_source='ome-tiff'
				self.reset_state()
				
				self.update_data()
				self._plot_container_1_default()		
				self._plot_container_2_default()	


	def _bot_button_fired(self,events):
		#close dcelliq-sequence-training.h5 if already open
		if self.h5f:
			self.h5f.close()

		time_starts=[]
		time_ends=[]	

		time_start=self.time
		if self.time_end>time_start:
			time_end=self.time_end
		else:
			time_end=self.time_max	

		time_starts.append(time_start)
		time_ends.append(time_end)

		for time_start,time_end in zip(time_starts,time_ends):

			# save segmented images to new h5 file			
			h5fn=bot_path+r'/data/dcelliq-sequence-training.h5'
			h5=h5py.File(h5fn,'w')
			raw=h5.create_group('Raw')
			seg=h5.create_group('Segmentation')

			tt=0
			for self.time in range(time_start,time_end):
				#self.update() #should happen automatically
				#print self.time

				t_str=str(tt)
				t_str='0'*(8-len(t_str))+t_str # make 8 digit time string

				t=raw.create_group(t_str)
				#transpose image aray back again
				data=t.create_dataset('Data',data=transpose(self.image_orig))

				t=seg.create_group(t_str)
				#transpose image aray back again
				data=t.create_dataset('Data',data=transpose(self.image_segmented))

				tt+=1

			h5.close()

			# run BOT on h5 file
			#print subprocess.check_output(['/Users/jpm/Progs/Tracker/BOT/bin/test-TrackingPredictor'])
			os.chdir(bot_path+'/bin')
			r=os.system('./test-TrackingPredictor')

			print 'BOT completed, returning',r

			# rename h5 
			if self.data_source=='ome-tiff':
				f=os.path.splitext(os.path.split(self.fn_tiff)[1])[0]+'.1.h5'
				f=f.replace('.ome','') #remove .ome
				f=f.replace(' ','_') #remove spaces
			else:
				f=os.path.split(self.fn_h5)[1]

			self.fn_h5=self.check_for_file(f)
			c='cp dcelliq-sequence-training.h5 '+self.fn_h5
			os.chdir(bot_path+'/data')		
			os.system(c)

		self.load_h5(bot_path+'/data/'+self.fn_h5)

	def check_for_file(self,fn):#fn local to bot_path/data
		if os.path.exists(bot_path+'/data/'+fn):
			#generate new filename
			s=fn.split('.')
			try:
				s[-2]=str(int(s[-2])+1)
				fn_new=''
				for i in s:
					fn_new+=i+'.'
				fn_new=fn_new[:-1]
				return fn_new
			except:
				pass
		return fn


	def _open_h5_button_fired(self,events):
		# easygui causes crash when focussing away from app then back to it
		#f=easygui.fileopenbox(default=bot_path+r'/data/')	
		##copy chosen h5 to working.h5 and open that
		#if f:
			#self.fn_h5=f
			#self.load_h5(f)

		dialog = FileDialog(action="open", default_path=bot_path+r'/data/')
		dialog.open()
		if dialog.return_code == OK:		
			f=dialog.path
			if f:
				self.fn_h5=f
				self.load_h5(f)			

	def load_h5(self,fn):#absolute fn
		if self.h5f:
			self.h5f.close()

		self.clear_all_selections()

		#copy to working.h5	
		shutil.copy(fn,bot_path+'/data/working.h5')
		fn=bot_path+'/data/working.h5'

		self.h5f=h5py.File(fn,'r+')
		nt=len(self.h5f['Raw']) #num time steps
		nc=1 #num channels
		nx=len(self.h5f['Raw']['00000000']['Data'])
		ny=len(self.h5f['Raw']['00000000']['Data'][0])

		self.imgarray=empty((self.num_sources,nt,nc,nx,ny))
		t=0
		for i in self.h5f['Raw']:
			self.imgarray[self.imgarray_sources['orig']][t][0][:][:]=self.h5f['Raw'][i]['Data'][:]
			t+=1
		t=0
		for i in self.h5f['Segmentation']:
			self.imgarray[self.imgarray_sources['seg']][t][0][:][:]=self.h5f['Segmentation'][i]['Data'][:]
			t+=1

		#transpose x and y so we have [s,t,ch,y,x]
		self.imgarray=transpose(self.imgarray,(0,1,2,4,3))
		self.data_source='h5'
		self.reset_state()
		self._plot_container_1_default()
		self._plot_container_2_default()

		# make cells from track info
		self.init_cells()
		self.update_data()
		self._plot_container_1_default()
		self._plot_container_2_default()

	def init_cells(self):
		self.cells=[]

		pca=PCA()

		#cells		
		for time in range(self.imgarray.shape[1]):
			self.cells.append([])		

			image_segmented=self.imgarray[self.imgarray_sources['seg'],time,self.channel,:,:] 
			image_segmented=image_segmented.astype(int)

			# bounding boxes as python slices of image ndarray
			bb_slices=scipy.ndimage.measurements.find_objects(image_segmented)
			for i in range(len(bb_slices)):	
				s=bb_slices[i]
				y1=s[0].start
				y2=s[0].stop
				x1=s[1].start
				x2=s[1].stop	

				#id=image_segmented[(y1+y2)/2,(x1+x2)/2]
				cell=Cell()
				#cell.id=id
				cell.bb=[x1,x2,y1,y2]
				self.cells[time].append(cell)

				# id & principal components
				c=image_segmented[y1:y2,x1:x2] #subsection of image_segmented within bb
				d=where(c!=0) #all indices not zero
				cell.id=c[d[0][0],d[1][0]]

				#dx=mean(d[0])
				#dy=mean(d[1])
				#e=zip(d[0]-dx,d[1]-dy) #indices as tuples
				#pca.fit(e)
				#cell.pc=pca.components_

		#tracking					
		for time in range(self.imgarray.shape[1]-1):
			linesets=(('Move',(1,1,0,1)),#yellow
		              ('Division',(1,0,0,1)),#red
		              ('Split',(0,0,1,1)),#blue
		              ('Merge',(0,1,1,1)))#,#cyan
						#('Appearance',(0,0,1,1)),#blue
						#('Disappearance',(1,0,1,1)))#purple

			for ls in linesets:
				track_type=ls[0]	
				t=h5_time_string(time)

				#ids
				try:
					source=self.h5f['Tracking'][t][track_type]['Source']['Data'][0][:]
					target1=self.h5f['Tracking'][t][track_type]['Target']['Data'][0][:]
					if len(self.h5f['Tracking'][t][track_type]['Target']['Data'])>1:
						target2=self.h5f['Tracking'][t][track_type]['Target']['Data'][1][:]
					else:
						target2=None

					proceed=True

				except:
					proceed=False

				if proceed:
					for i in range(len(source)):
						#record track_type in cell

						source_cell=self.cells[time][source[i]-1]
						source_cell.track_type=track_type
						source_cell.targets=[]
						if target1[i]>0:#-1 for disappearance
							source_cell.targets.append(self.cells[time+1][target1[i]-1])
						self.cells[time+1][target1[i]-1].source=source_cell

						if target2 is not None:
							source_cell.targets.append(self.cells[time+1][target2[i]-1])
							self.cells[time+1][target2[i]-1].source=source_cell						

			pass

	def _save_h5_button_fired(self,events):
		#path=easygui.fileopenbox(default=r'/Users/jpm/DATA/')

		#close working.h5
		self.h5f.close()
		self.fn_h5=self.check_for_file(self.fn_h5)

		shutil.copy(bot_path+'/data/working.h5',bot_path+'/data/'+self.fn_h5)			

	def debug(self):
		if self.selected_cell:
			if self.selected_cell.source:
				print self.selected_cell.source.id,
			else:
				print 'None',

			print self.selected_cell.id,[c.id for c in self.selected_cell.targets]

	def _time_backward_button_changed(self):
		print '<--'
		self.debug()
		if self.time>0:
			if not self.track_switch: #single selection
				if self.selected_cell:
					if self.selected_cell.source:
						self.selected_cell=self.selected_cell.source
						self.set_selections(self.scatter_inspector,[self.selected_cell.id-1])
						self.set_selections(self.scatter_inspector_plus,[self.selected_cell.targets[0].id-1])
					else:
						self.clear_all_selections()

			else: #multiple selection
				if len(self.selected_cells)>0:
					new_selections=[]
					new_selections_plus=[]
					for c in self.selected_cells:
						s=c.source
						if s not in new_selections:
							new_selections.append(s)
					for c in new_selections:
						for t in c.targets:
							new_selections_plus.append(t)

					self.selected_cells=new_selections
					self.set_selections(self.scatter_inspector,[c.id-1 for c in new_selections])
					self.set_selections(self.scatter_inspector_plus,[c.id-1 for c in new_selections_plus])

				else:
					self.clear_all_selections()

			#self.clear_all_selections()
			self.time-=1
		#self.debug()

	def _time_forward_button_changed(self):
		print '-->'
		self.debug()
		if self.time<self.time_max:
			if not self.track_switch: #single selection		
				if self.selected_cell:
					if len(self.selected_cell.targets)>0:			
						self.selected_cell=self.selected_cell.targets[0]
						self.set_selections(self.scatter_inspector,[self.selected_cell.id-1])
						self.set_selections(self.scatter_inspector_plus,[self.selected_cell.targets[0].id-1])
					else:
						self.clear_all_selections()

			else: #multiple selection			
				if len(self.selected_cells)>0:
					new_selections=[]
					new_selections_plus=[]
					for c in self.selected_cells:
						for t in c.targets:
							new_selections.append(t)
					for c in new_selections:
						for t in c.targets:
							new_selections_plus.append(t)

					self.selected_cells=new_selections
					self.set_selections(self.scatter_inspector,[c.id-1 for c in new_selections])
					self.set_selections(self.scatter_inspector_plus,[c.id-1 for c in new_selections_plus])

				else:
					self.clear_all_selections()

			#self.clear_all_selections()			
			self.time+=1
		#self.debug()
	#def _key_bindings_button_fired(self,event):
		#key_bindings.edit_traits()

	def _image_type_changed(self):
		#keep zoom ranges 
		xlow=self.plot_11.range2d.x_range.low
		xhigh=self.plot_11.range2d.x_range.high
		ylow=self.plot_11.range2d.y_range.low
		yhigh=self.plot_11.range2d.y_range.high

		self._plot_container_1_default()		

		self.plot_11.range2d.x_range.low=xlow
		self.plot_11.range2d.x_range.high=xhigh
		self.plot_11.range2d.y_range.low=ylow
		self.plot_11.range2d.y_range.high=yhigh


	def _data_source_changed(self):
		if self.data_source=='h5':
			self.image_enum=['orig','segmented']
		else:
			self.image_enum=['orig','processed','segmented']	

	def set_selections(self,inspector,sel):
		self.block_selection_changed=True
		inspector.component.index.metadata['selections']=sel		
		inspector.component.index.metadata_changed=True
		inspector.component.value.metadata['selections']=sel	
		inspector.component.value.metadata_changed=True
		self.block_selection_changed=False

	def clear_all_selections(self):
		self.selected_cell=None
		self.output_text='No Selection'

		if hasattr(self,'scatter_inspector'):
			self.set_selections(self.scatter_inspector,[])
		if hasattr(self,'scatter_inspector_plus'):
			self.set_selections(self.scatter_inspector_plus,[])

	def selection_changed(self,object,name,old,new):
		#print object.metadata['selections']
		#print [self.cells[0][i].centre() for i in object.metadata['selections']]	
		#if self.mouse_state=='up': return
		if self.block_selection_changed:
			return

		if not self.track_switch: #single cell selection for track editing
			if len(object.metadata['selections'])>0:
				self.hover_selection_plus=None
				#self.selected=self.cells[0][object.metadata['selections'][0]].centre()
				self.sel_index=int(object.metadata['selections'][0])
				self.selected_cell=self.cells[self.time][self.sel_index]

				targets=[]
				for t in self.selected_cell.targets:
					targets.append(t.id-1)
					#self.scatter_inspector_plus._select(t.id-1)

				#update RH selections		
				self.set_selections(self.scatter_inspector_plus,targets)
				self.output_text='id: '+str(self.selected_cell.id)+' '+self.selected_cell.track_type+' '+str([x+1 for x in targets])

			else:
				#self.selected=(0,0)
				self.selected_cell=None
				self.output_text='No Selection'
				self.set_selections(self.scatter_inspector_plus,[])

		else: #multiple cell selection for tracking
			if len(object.metadata['selections'])>0:
				self.hover_selection_plus=None
				#self.selected=self.cells[0][object.metadata['selections'][0]].centre()
				self.selected_cells=[self.cells[self.time][x] for x in object.metadata['selections']]

				targets=[]
				for c in self.selected_cells:
					for t in c.targets:
						targets.append(t.id-1)

				#update RH selections		
				self.set_selections(self.scatter_inspector_plus,targets)

				self.output_text=''
				for c in self.selected_cells:			
					self.output_text='id: '+str(c.id)+' '+c.track_type+' '+str([x.id for x in c.targets])

			else:
				#self.selected=(0,0)
				self.selected_cells=[]
				self.output_text='No Selection'
				self.set_selections(self.scatter_inspector_plus,[])


	def selection_plus_changed(self,object,name,old,new):
		#print self.scatter_inspector_plus.component.index.metadata
		#print self.hover_selection_plus

		#do everything with cell ids		
		sc=self.selected_cell
		if sc:
			#selected=self.selected_cell.id

			selections_plus=[x+1 for x in self.scatter_inspector_plus.component.index.metadata['selections']] #+1 scatter index -> cell id
			selected_cell_targets=[x.id for x in sc.targets]
			selections_plus=[x for x in selections_plus if x not in selected_cell_targets]

			if len(selections_plus)>0:
				selected_plus=selections_plus[0]	
				sc_plus=self.cells[self.time+1][selections_plus[0]-1] #-1 cell id -> scatter index			
				sc_plus_source=sc_plus.source

				#sc has no targets and sc_plus has no source
				if len(sc.targets)==0 and not sc_plus_source:
					sc.targets.append(sc_plus)
					sc_plus.source=sc

					sc.track_type='Move'

				#sc has no targets and sc_plus has a source
				elif len(sc.targets)==0 and sc_plus_source:	
					sc.targets.append(sc_plus)
					sc_plus.source=sc

					sc_plus_source.targets.remove(sc_plus)
					if len(sc_plus_source.targets)==0:
						sc_plus_source.track_type='None' # 1 to 0
					else:
						sc_plus_source.track_type='Move' # 2 to 1

					sc.track_type='Move'

				#sc has targets but sc_plus has no source
				elif len(sc.targets)>0 and not sc_plus_source:
					i=0 
					if len(sc.targets)==2 and self.hover_selection_plus: #split or div
						if sc.targets[0].id==(self.hover_selection_plus+1):
							i=0
						elif sc.targets[1].id==(self.hover_selection_plus+1):
							i=1

					old_target=sc.targets[i]

					#swap  targets and sources
					sc.targets[i]=sc_plus
					sc_plus.source=sc	
					old_target.source=None

				#sc has targets and sc_plus has source
				else:
					i=0 
					j=0
					if len(sc.targets)==2 and self.hover_selection_plus: #split or div
						if sc.targets[0].id==(self.hover_selection_plus+1):
							i=0
						elif sc.targets[1].id==(self.hover_selection_plus+1):
							i=1

					if len(sc_plus_source.targets)==2:
						if sc_plus_source.targets[0]==sc_plus:
							j=0
						elif sc_plus_source.targets[1]==sc_plus:
							j=1

					old_target=sc.targets[i]

					#swap  targets and sources
					sc.targets[i]=sc_plus
					sc_plus.source=sc
					sc_plus_source.targets[j]=old_target
					old_target.source=sc_plus_source

				#update _plus selections
				selected_cell_targets=[x.id-1 for x in sc.targets]				
				self.set_selections(self.scatter_inspector_plus,selected_cell_targets)

				self.overlay.request_redraw()	

		if 'hover' in self.scatter_inspector_plus.component.index.metadata:
			if self.scatter_inspector_plus.component.index.metadata['hover'][0] in self.scatter_inspector_plus.component.index.metadata['selections']:
				self.hover_selection_plus=self.scatter_inspector_plus.component.index.metadata['hover'][0]
				self.overlay.request_redraw()	

	def _delete_track_button_fired(self,events):
		sc=self.selected_cell
		if sc:		
			if len(sc.targets)>0:
				for t in sc.targets:
					t.source=None
				sc.targets=[]
				sc.track_type='None'	

				#update _plus selections
				selected_cell_targets=[]				
				self.set_selections(self.scatter_inspector_plus,selected_cell_targets)

	def init_selection_scatter_plot(self,mode='single'):
		# for selection
		marker_size=5
		self.scatter_plot=self.plot_11.plot(("x","y"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		# Attach the inspector and its overlay
		self.scatter_inspector=ScatterInspector(self.scatter_plot,selection_mode=mode)
		self.scatter_plot.tools.append(self.scatter_inspector)
		overlay = ScatterInspectorOverlay(self.scatter_plot,
	                                      hover_marker="circle",
	                                      hover_marker_size=marker_size,		                                  
	                                      hover_color="grey",
	                                      selection_marker="circle",
	                                      selection_marker_size=marker_size,
	                                      selection_color="white",
	                                      selection_line_width=0)
		self.scatter_plot.overlays.append(overlay)

		#called when data metadata changes to indicate data selected
		self.scatter_plot.value.on_trait_change(self.selection_changed)


	def _track_switch_changed(self):
		if self.track_switch:
			self.init_selection_scatter_plot('toggle')
			self.clear_all_selections()
		else:
			self.init_selection_scatter_plot('single')
			self.clear_all_selections()

	def normal_key_pressed(self,event):
		print 'normal_key_pressed'

	#segment
	def _segment_button_fired(self,events):
		#put on a thread so UI remains responsive
		threading.Thread(target=self.segment).start()
		
		#p = Process(target=self.segment)
		#p.start()
		#p.join()		
		
	def segment(self):
		time_start=self.time
		if self.time_end>time_start:
			time_end=self.time_end
		else:
			time_end=self.time_max	

		for t in range(time_start,time_end):
			print t,
			self.output_text=str(t)
			#self.request_redraw()	
			#self.trait_view('traits_view').updated = True

			image_processed=self.imgarray[self.imgarray_sources['orig'],t,self.channel,:,:]

			if self.LoG_switch:
				image_processed=scipy.ndimage.filters.gaussian_laplace(image_processed,(self.LoG_sigma1,self.LoG_sigma2))

			if self.thresh_switch:
				#block_size=3+self.thresh*100
				if self.block_size%2==0:
					self.block_size+=1 #must be odd
				image_processed=skimage.filter.threshold_adaptive(image_processed, self.block_size)		

			#if self.denoise_switch:
				#image_processed=skimage.filter.tv_denoise(image_processed)	

			if self.opening_switch:
				image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)

			if self.closing_switch:
				image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

			if self.watershed_switch:				
				distance = scipy.ndimage.distance_transform_edt(image_processed)
				#distance=gaussian_filter(distance)			
				distance = scipy.ndimage.gaussian_filter(distance,4)
				local_max = skimage.morphology.is_local_maximum(distance, image_processed,ones((self.footprint*2+1,self.footprint*2+1)))
				markers = scipy.ndimage.label(local_max)[0]
				labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)

				#if self.image_type=='distance':
					#self.image_segmented=distance.astype(int)	
				#elif self.image_type=='local_max':
					#self.image_segmented=local_max.astype(int)
				#else:
					#self.image_segmented=labels

			#self.imgarray[self.imgarray_sources['proc'],t,self.channel,:,:]=image_processed
			self.imgarray[self.imgarray_sources['seg'],t,self.channel,:,:]=labels

		#refresh seg plot
		self.update_data()
		self.plot_21.request_redraw()
		self.plot_22.request_redraw()
		self.plot_container_2.request_redraw()

				##add outline to segmented
				#if self.outline_switch:
					#segmented_binary=self.image_segmented>0
					#self.image_outline=segmented_binary-scipy.ndimage.morphology.binary_erosion(segmented_binary)	
					#self.image_outline=self.image_outline*self.imgarray.max()
					#self.image_orig=self.image_orig+self.image_outline

	@on_trait_change('LoG_switch,LoG_sigma1,LoG_sigma2,thresh_switch,opening_switch,closing_switch,\
watershed_switch,morph_iterations,block_size,channel,time,outline_switch,footprint,tracks_switch')		
	def update_data(self):
		print self.time,
		self.output_text=str(self.time)
		#update images and plot data
		#self.image_orig=self.imgarray[self.imgarray_sources['orig'],self.time,self.channel,:,:]
		#self.image_orig_plus=self.imgarray[self.imgarray_sources['orig'],self.time+1,self.channel,:,:]
		#image_processed=self.image_orig.copy()
		#image_processed_plus=self.image_orig_plus.copy()


		#update plot data
		#self.plotdata.set_data("image_orig_data", self.image_orig)	
		#self.plotdata.set_data("image_orig_plus_data", self.image_orig_plus)	
		#self.plotdata.set_data("image_processed_data", image_processed)
		#self.plotdata.set_data("image_processed_plus_data", image_processed_plus)
		#self.plotdata.set_data("image_segmented_data", self.image_segmented)
		#self.plotdata.set_data("image_segmented_plus_data", self.image_segmented_plus)
		#self.plotdata.set_data("image_outline_data", self.image_outline)

		self.plotdata.set_data("data_11",self.imgarray[self.imgarray_sources['orig'],self.time,self.channel,:,:])	
		self.plotdata.set_data("data_12",self.imgarray[self.imgarray_sources['orig'],self.time+1,self.channel,:,:])	
		self.plotdata.set_data("data_21",self.imgarray[self.imgarray_sources['seg'],self.time,self.channel,:,:])	
		self.plotdata.set_data("data_22",self.imgarray[self.imgarray_sources['seg'],self.time+1,self.channel,:,:])	

		#cells

		x=[]
		y=[]

		if self.data_source=='h5':
			for c in self.cells[self.time]:
				x.append(c.centre()[0])
				y.append(c.centre()[1])

		#update scatter plot
		self.plotdata.set_data("x",x)
		self.plotdata.set_data("y",y)

		x=[]
		y=[]

		if self.data_source=='h5':
			for c in self.cells[self.time+1]:
				x.append(c.centre()[0])
				y.append(c.centre()[1])

		#update scatter plot
		self.plotdata.set_data("xplus",x)
		self.plotdata.set_data("yplus",y)	


	#initialise plots
	def _plot_container_1_default(self):		
		#self.update_plots()

		### CONTAINER
		self.plot_container_1=HPlotContainer(spacing=0)
		self.overlay = TheOverlay()
		self.plot_container_1.overlays.append(self.overlay)		

		### LEFT PLOT
		# Create the plot from the data 
		self.plot_11 = Plot(self.plotdata,default_origin="top left")
		self.plot_11.x_axis.orientation = "top"

		#if self.image_type=='orig':
		image_plot_11 = self.plot_11.img_plot("data_11","image_plot_11",colormap=greens_col_map)[0]
		#elif self.image_type=='processed':
			#image_plot = self.plot_11.img_plot("image_processed_data","image_plot",colormap=greens_col_map)[0]
		#elif self.image_type in ['segmented','distance','local_max']:
			#image_plot = self.plot_11.img_plot("image_segmented_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		# Attach some tools to the plot
		pantool=PanTool(self.plot_11, constrain_key="shift")
		self.plot_11.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_11,tool_mode="box", always_on=False)
		self.plot_11.overlays.append(zoomtool)

		imgtool = ImageInspectorTool(image_plot_11)
		image_plot_11.tools.append(imgtool)
		self.plot_11.overlays.append(ImageInspectorOverlay(component=image_plot_11,
	                                                       image_inspector=imgtool))

		selecttool=PixelSelectTool(image_plot_11)		
		image_plot_11.tools.append(selecttool)
		#pointstool=DrawPointsTool(component=self.plot_orig)
		#image_plot.tools.append(pointstool)

		# for selection
		marker_size=5
		self.scatter_plot=self.plot_11.plot(("x","y"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		# Attach the inspector and its overlay
		self.scatter_inspector=ScatterInspector(self.scatter_plot,selection_mode='single')
		self.scatter_plot.tools.append(self.scatter_inspector)
		overlay = ScatterInspectorOverlay(self.scatter_plot,
	                                      hover_marker="circle",
	                                      hover_marker_size=marker_size,		                                  
	                                      hover_color="grey",
	                                      selection_marker="circle",
	                                      selection_marker_size=marker_size,
	                                      selection_color="white",
	                                      selection_line_width=0)
		self.scatter_plot.overlays.append(overlay)

		#called when data metadata changes to indicate data selected
		self.scatter_plot.value.on_trait_change(self.selection_changed)

		### RIGHT PLOT
		# Create the plot from the data 
		self.plot_12 = Plot(self.plotdata, default_origin="top left")
		self.plot_12.x_axis.orientation = "top"
		#self.plot_orig_plus.orientation='v'

		#image_plus_plot = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plus_plot",colormap=greens_col_map)[0]
		#if self.image_type=='orig':
		image_plot_12 = self.plot_12.img_plot("data_12","image_plot_12",colormap=greens_col_map)[0]
		#elif self.image_type=='processed':
			#image_plot_plus = self.plot_12.img_plot("image_processed_plus_data","image_plot",colormap=greens_col_map)[0]
		#elif self.image_type in ['segmented','distance','local_max']:
			#image_plot_plus = self.plot_12.img_plot("image_segmented_plus_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		#if self.image_enum=='segmented':
			#image_plus_plot = self.plot_orig_plus.img_plot("image_segmented_plus_data","image_plus_plot",colormap=rand_col_map,bg_color='none')[0]
		#else:
			#image_plus_plot = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plus_plot",colormap=greens_col_map)[0]

		# Attach some tools to the plot
		pantool=PanTool(self.plot_12, constrain_key="shift")
		self.plot_12.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_12,tool_mode="box", always_on=False)
		self.plot_12.overlays.append(zoomtool)

		imgtool = ImageInspectorTool(image_plot_12)
		image_plot_12.tools.append(imgtool)
		self.plot_12.overlays.append(ImageInspectorOverlay(component=image_plot_12,
	                                                       image_inspector=imgtool))


		#sync range set by zoom and pan tools
		self.plot_12.sync_trait('range2d',self.plot_11)	

		# for selection
		self.scatter_plot_plus=self.plot_12.plot(("xplus","yplus"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		# Attach the inspector and its overlay
		self.scatter_inspector_plus=ScatterInspector(self.scatter_plot_plus,selection_mode='multi')
		self.scatter_plot_plus.tools.append(self.scatter_inspector_plus)
		overlay = ScatterInspectorOverlay(self.scatter_plot_plus,
	                                      hover_marker="circle",
	                                      hover_marker_size=marker_size,		                                  
	                                      hover_color="grey",
	                                      selection_marker="circle",
	                                      selection_marker_size=marker_size,
	                                      selection_color="white",
	                                      selection_line_width=0)
		self.scatter_plot_plus.overlays.append(overlay)

		#called when data metadata changes to indicate data selected
		self.scatter_plot_plus.value.on_trait_change(self.selection_plus_changed)		

		########
		self.plot_container_1.add(self.plot_11)
		self.plot_container_1.add(self.plot_12)
		return self.plot_container_1

	#initialise plots
	def _plot_container_2_default(self):		
		#self.update_plots()

		### CONTAINER
		self.plot_container_2=HPlotContainer()
		#self.overlay = TheOverlay()
		#self.plot_container_1.overlays.append(self.overlay)		

		### LEFT PLOT
		# Create the plot from the data 
		self.plot_21 = Plot(self.plotdata,default_origin="top left")
		self.plot_21.x_axis.orientation = "top"

		#if self.image_type=='orig':
		image_plot_21 = self.plot_21.img_plot("data_21","image_plot_21",colormap=rand_col_map)[0]
		#elif self.image_type=='processed':
			#image_plot = self.plot_11.img_plot("image_processed_data","image_plot",colormap=greens_col_map)[0]
		#elif self.image_type in ['segmented','distance','local_max']:
			#image_plot = self.plot_11.img_plot("image_segmented_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		# Attach some tools to the plot
		pantool=PanTool(self.plot_21, constrain_key="shift")
		self.plot_21.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_21,tool_mode="box", always_on=False)
		self.plot_21.overlays.append(zoomtool)

		#sync range set by zoom and pan tools
		self.plot_21.sync_trait('range2d',self.plot_11)	

		imgtool = ImageInspectorTool(image_plot_21)
		image_plot_21.tools.append(imgtool)
		self.plot_21.overlays.append(ImageInspectorOverlay(component=image_plot_21,
	                                                       image_inspector=imgtool))

		#selecttool=PixelSelectTool(image_plot_21)		
		#image_plot_21.tools.append(selecttool)
		##pointstool=DrawPointsTool(component=self.plot_orig)
		##image_plot.tools.append(pointstool)

		## for selection
		#marker_size=5
		#self.scatter_plot=self.plot_11.plot(("x","y"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		## Attach the inspector and its overlay
		#self.scatter_inspector=ScatterInspector(self.scatter_plot,selection_mode='single')
		#self.scatter_plot.tools.append(self.scatter_inspector)
		#overlay = ScatterInspectorOverlay(self.scatter_plot,
								#hover_marker="circle",
								#hover_marker_size=marker_size,		                                  
								#hover_color="grey",
								#selection_marker="circle",
								#selection_marker_size=marker_size,
								#selection_color="white",
								#selection_line_width=0)
		#self.scatter_plot.overlays.append(overlay)

		##called when data metadata changes to indicate data selected
		#self.scatter_plot.value.on_trait_change(self.selection_changed)

		### RIGHT PLOT
		# Create the plot from the data 
		self.plot_22 = Plot(self.plotdata, default_origin="top left")
		self.plot_22.x_axis.orientation = "top"
		#self.plot_orig_plus.orientation='v'

		#image_plus_plot = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plus_plot",colormap=greens_col_map)[0]
		#if self.image_type=='orig':
		image_plot_22 = self.plot_22.img_plot("data_22","image_plot_22",colormap=rand_col_map)[0]
		#elif self.image_type=='processed':
			#image_plot_plus = self.plot_12.img_plot("image_processed_plus_data","image_plot",colormap=greens_col_map)[0]
		#elif self.image_type in ['segmented','distance','local_max']:
			#image_plot_plus = self.plot_12.img_plot("image_segmented_plus_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		#if self.image_enum=='segmented':
			#image_plus_plot = self.plot_orig_plus.img_plot("image_segmented_plus_data","image_plus_plot",colormap=rand_col_map,bg_color='none')[0]
		#else:
			#image_plus_plot = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plus_plot",colormap=greens_col_map)[0]

		# Attach some tools to the plot
		pantool=PanTool(self.plot_22, constrain_key="shift")
		self.plot_22.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_22,tool_mode="box", always_on=False)
		self.plot_22.overlays.append(zoomtool)

		imgtool = ImageInspectorTool(image_plot_22)
		image_plot_22.tools.append(imgtool)
		self.plot_22.overlays.append(ImageInspectorOverlay(component=image_plot_22,
	                                                       image_inspector=imgtool))


		#sync range set by zoom and pan tools
		self.plot_22.sync_trait('range2d',self.plot_11)	

		## for selection
		#self.scatter_plot_plus=self.plot_12.plot(("xplus","yplus"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		## Attach the inspector and its overlay
		#self.scatter_inspector_plus=ScatterInspector(self.scatter_plot_plus,selection_mode='multi')
		#self.scatter_plot_plus.tools.append(self.scatter_inspector_plus)
		#overlay = ScatterInspectorOverlay(self.scatter_plot_plus,
								#hover_marker="circle",
								#hover_marker_size=marker_size,		                                  
								#hover_color="grey",
								#selection_marker="circle",
								#selection_marker_size=marker_size,
								#selection_color="white",
								#selection_line_width=0)
		#self.scatter_plot_plus.overlays.append(overlay)

		##called when data metadata changes to indicate data selected
		#self.scatter_plot_plus.value.on_trait_change(self.selection_plus_changed)		

		########
		self.plot_container_2.add(self.plot_21)
		self.plot_container_2.add(self.plot_22)
		return self.plot_container_2


class TheOverlay(AbstractOverlay):		
	line_color = ColorTrait((1.0, 0.0, 0.0, 1))
	line_width = Int(2)
	alpha = Float(0.8)

	def overlay(self, component, gc, view_bounds=None, mode="normal"):
		# Draws this overlay onto 'component', rendering onto 'gc'.

		if iv.data_source!='h5':
			return

		#selection_plus hover 
		if iv.hover_selection_plus:
			gc.set_stroke_color((1,0,1,1))				
			gc.begin_path()	
			x=iv.plot_12.index_mapper.map_screen(iv.cells[iv.time+1][iv.hover_selection_plus].centre()[0])
			y=iv.plot_12.value_mapper.map_screen(iv.cells[iv.time+1][iv.hover_selection_plus].centre()[1])
			gc.arc(x,y,6,0,pi*2)
			gc.stroke_path()

		if iv.tracks_mode=='none':
			cells=[]
		elif iv.tracks_mode=='selected':
			if iv.track_switch:
				cells=iv.selected_cells

			elif iv.selected_cell==None:
				cells=[]
			else:
				cells=[iv.selected_cell]
		else:
			cells=iv.cells[iv.time]

		linesets={'Move':(1,1,0,1),#yellow
				  'Division':(1,0,0,1),#red
				  'Split':(0,0,1,1),#blue
				  'Merge':(0,1,1,1),#cyan
				  'Appearance':(0,0,1,1),#blue
				  'Disappearance':(1,0,1,1),#purple
				  'None':(0,0,0,1)}#black

		lh_plot_pos=component._components[0].position #1st component of container ie LH plot
		rh_plot_pos=component._components[1].position #2nd component of container ie RH plot

		with gc:

			gc.set_alpha(self.alpha)
			gc.set_line_width(self.line_width)

			index_low=iv.plot_11.index_mapper.low_pos
			index_high=iv.plot_11.index_mapper.high_pos
			value_low=iv.plot_11.value_mapper.low_pos
			value_high=iv.plot_11.value_mapper.high_pos

			index_low_plus=iv.plot_12.index_mapper.low_pos
			index_high_plus=iv.plot_12.index_mapper.high_pos
			value_low_plus=iv.plot_12.value_mapper.low_pos
			value_high_plus=iv.plot_12.value_mapper.high_pos		

			gc.clip_to_rect(index_low,value_high_plus,index_high_plus-index_low,value_low-value_high_plus)

			for c in cells:
				track_col=linesets[c.track_type]
				gc.begin_path()							
				gc.set_stroke_color(track_col)	

				if c.track_type in ['Move','Division','Split']:
					x_from=iv.plot_11.index_mapper.map_screen(c.centre()[0])
					y_from=iv.plot_11.value_mapper.map_screen(c.centre()[1])
					# the y/value axis is upside down
					if x_from>index_low and x_from<index_high \
					   and y_from<value_low and y_from>value_high:

						gc.move_to(x_from,y_from)			
						x_to=iv.plot_12.index_mapper.map_screen(c.targets[0].centre()[0])
						y_to=iv.plot_12.value_mapper.map_screen(c.targets[0].centre()[1])	
						gc.line_to(x_to,y_to)

				if c.track_type in ['Division','Split']:
					x_from=iv.plot_11.index_mapper.map_screen(c.centre()[0])
					y_from=iv.plot_11.value_mapper.map_screen(c.centre()[1])
					# the y/value axis is upside down
					if x_from>index_low and x_from<index_high \
					   and y_from<value_low and y_from>value_high:					
						gc.move_to(x_from,y_from)						
						x_to=iv.plot_12.index_mapper.map_screen(c.targets[1].centre()[0])
						y_to=iv.plot_12.value_mapper.map_screen(c.targets[1].centre()[1])				
						gc.line_to(x_to,y_to)

				gc.stroke_path()		

			##principal components
			#gc.clip_to_rect(index_low,value_high,index_high-index_low,value_low-value_high)

			#cells=iv.cells[iv.time]
			#for c in cells:

				#gc.begin_path()							
				#gc.set_stroke_color((0,0,0,1))	

				#x_from=iv.plot_orig.index_mapper.map_screen(c.centre()[0])
				#y_from=iv.plot_orig.value_mapper.map_screen(c.centre()[1])
				#x_to=iv.plot_orig.index_mapper.map_screen(c.centre()[0]+c.pc[0][0]*50)
				#y_to=iv.plot_orig.value_mapper.map_screen(c.centre()[1]+c.pc[0][1]*50)				
				#gc.move_to(x_from,y_from)
				#gc.line_to(x_to,y_to)
				##x_to=iv.plot_orig.index_mapper.map_screen(c.centre()[0]+c.pc[1][0]*50)
				##y_to=iv.plot_orig.value_mapper.map_screen(c.centre()[1]+c.pc[1][1]*50)				
				##gc.move_to(x_from,y_from)
				##gc.line_to(x_to,y_to)

				#gc.stroke_path()

		return

	def normal_left_down(self,event):
		#print 'left_down'
		self.mouse_state='down'

	def normal_left_up(self,event):
		#print 'left_up'
		self.mouse_state='up'

	def ctrl_left_down(self,event):
		#print 'ctrl left_down'
		self.mouse_state='down'

	def normal_key_pressed(self,event):
		#print 'key_pressed'
		pass

###MAIN###
iv = ImageViewer()
if __name__ == "__main__":
	iv.configure_traits()

#--EOF---