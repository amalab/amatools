#!/usr/bin/env python
"""
Draws an simple RGB image
 - Left-drag pans the plot.
 - Mousewheel up and down zooms the plot in and out.
 - Pressing "z" brings up the Zoom Box, and you can click-drag a rectangular
   region to zoom.  If you use a sequence of zoom boxes, pressing alt-left-arrow
   and alt-right-arrow moves you forwards and backwards through the "zoom
   history".
"""

# Major library imports
from numpy import zeros, ones, uint8, transpose, exp, hypot
import numpy.random

# Enthought library imports
from enable.api import Component, ComponentEditor
from traits.api import HasTraits, Instance, Property, Array, Range, Bool
from traitsui.api import Item, Group, View, RangeEditor

# Chaco imports
from chaco.api import ArrayPlotData, Plot
from chaco.tools.api import PanTool, ZoomTool
from chaco.tools.image_inspector_tool import ImageInspectorTool, \
     ImageInspectorOverlay
import chaco.default_colormaps

import tifffile
# sci-kits image processing
import skimage.filter, skimage.morphology
import scipy.ndimage.morphology

#fn = tkFileDialog.askopenfilename()
#fn=r'/Users/jpm/DATA/David - Images for tracking/2012-03-19 Wntch 2iLIF to labelled-0002_2i to ESminusLIF_01.zvi - C=2 slices=90-100 8-bit.tif'
fn=r'/Users/jpm/DATA/_scratch/120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2.ome.tiff'
#fn=r'/Users/jpm/DATA/_scratch/esgZ_GSHg_ArmPros_DAPI_4-6DO_G5F30.ome.tiff'

# get tiff file as numpy array
tiffimg = tifffile.TIFFfile(fn)
imgarray = tiffimg.asarray()
print imgarray.shape

# prune array - [time,channel,z,y,x] becomes [time,channel,y,x]
imgarray2=imgarray[:,:,0,:,:]

#ch1 is GFP for 120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2

imgarray2=transpose(imgarray2,(0,1,3,2)) #[time,channel,x,y]

num_channels=imgarray2.shape[1]                 
time_max=imgarray2.shape[0]-1
level_max=4096
shape_x=imgarray2.shape[3]
shape_y=imgarray2.shape[2]

# Attributes to use for the plot view.
size = (800, 800)
title="Image Viewer"
bg_color="lightgray"

# gaussian filter for scikits-image
def filt_func(r, c):
	return exp(-hypot(r, c)/1)
gaussian_filter = skimage.filter.LPIFilter2D(filt_func)
# for morphological filters
structuring_element=scipy.ndimage.morphology.generate_binary_structure(2, 2)
# random color map for segmentation view
r=numpy.random.random((4096,3))
r[0,:]=(1.0,1.0,1.0) #white background
rand_col_map=chaco.api.ColorMapper.from_palette_array(r)


class ImageViewer(HasTraits):
	plot = Instance(Component)

	channel=Range(0,num_channels-1,1,editor=RangeEditor(low=0, high=num_channels-1, mode="spinner"))
	time=Range(0,time_max,100,editor=RangeEditor(low=0, high=time_max, mode="slider"))
	block_size=Range(10,201,100,editor=RangeEditor(low=10, high=200, mode="slider"))
	morph_iterations=Range(1,10,editor=RangeEditor(low=1, high=9, mode="slider"))
	
	image=Array

	orig_switch=Bool
	denoise_switch=Bool
	thresh_switch=Bool(True)
	opening_switch=Bool(True)
	closing_switch=Bool
	watershed_switch=Bool

	traits_view = View(
		Group(
		    Group(		        
	            Item('orig_switch'),
	            Item('thresh_switch'),
		        Item('denoise_switch'),
		        Item('opening_switch'),
		        Item('closing_switch'),
		        Item('watershed_switch'),
	            orientation = 'vertical'
		        ),
		    Group(                        
		        Item('plot', 
		             editor=ComponentEditor(size=size),#,bgcolor=bg_color),
		             show_label=False
		             ),
		        Item('morph_iterations'),
		        Item('block_size'),
		        Item('channel'),
		        Item('time'),
		        orientation = 'vertical'
		        ),
	        orientation = 'horizontal'
		    ),
		resizable=True, title=title
	)

	def update_plotdata(self):
		if self.orig_switch:
			self.pd.set_data("imagedata", self.image_orig)  
		else:
			self.pd.set_data("imagedata", self.image)   

	def update_image(self):
		self.image=imgarray2[self.time,self.channel,:,:]
		self.image_orig=self.image.copy()
		
		#im_thresh=self.image > (self.thresh*level_max)
		#self.image=im_thresh 

		#self.image=gaussian_filter(self.image)	

		if self.thresh_switch:
			#block_size=3+self.thresh*100
			if self.block_size%2==0:
				self.block_size+=1 #must be odd
			self.image=skimage.filter.threshold_adaptive(self.image, self.block_size)		
			
		if self.denoise_switch:
			self.image=skimage.filter.tv_denoise(self.image)	
			
		if self.opening_switch:
			self.image=scipy.ndimage.morphology.binary_opening(self.image,structuring_element,self.morph_iterations)

		if self.closing_switch:
			self.image=scipy.ndimage.morphology.binary_closing(self.image,structuring_element,self.morph_iterations)
					
			#self.image=skimage.morphology.opening(self.image, skimage.morphology.square(3))
		if self.watershed_switch:	
			distance = scipy.ndimage.distance_transform_edt(self.image)
			local_max = skimage.morphology.is_local_maximum(distance, self.image, ones((3, 3)))
			markers = scipy.ndimage.label(local_max)[0]
			labels = skimage.morphology.watershed(-distance, markers, mask=self.image)	
			
			self.plot.color_mapper=rand_col_map
			
			self.image=labels

		self.update_plotdata()   
		
	def _orig_switch_changed(self):
		self.update_plotdata()

	def _channel_changed(self): 
		self.update_image()

	def _time_changed(self):
		self.update_image()

	def _block_size_changed(self):
		self.update_image()
		
	def _denoise_switch_changed(self):
		self.update_image()	
		
	def _thresh_switch_changed(self):
		self.update_image()		
		
	def _opening_switch_changed(self):
		self.update_image()	
		
	def _closing_switch_changed(self):
		self.update_image()		
		
	def _morph_iterations_changed(self):
		self.update_image()
		
	def _watershed_switch_changed(self):
		self.update_image()
		
	def _plot_default(self):
		self.image=imgarray2[0,0,:,:]

		# Create a plot data obect and give it this data
		self.pd = ArrayPlotData()
		self.pd.set_data("imagedata", self.image)

		# Create the plot
		self.plot = Plot(self.pd, default_origin="top left")
		self.plot.x_axis.orientation = "top"
		img_plot = self.plot.img_plot("imagedata",colormap=chaco.default_colormaps.Greens)[0]
		#img_plot = self.plot.img_plot("imagedata",colormap=chaco.default_colormaps.Spectral)[0]

		# Tweak some of the plot properties
		self.plot.bgcolor = "white"

		# Attach some tools to the plot
		self.plot.tools.append(PanTool(self.plot, constrain_key="shift"))
		self.plot.overlays.append(ZoomTool(component=self.plot,
				                           tool_mode="box", always_on=False))

		imgtool = ImageInspectorTool(img_plot)
		img_plot.tools.append(imgtool)
		self.plot.overlays.append(ImageInspectorOverlay(component=img_plot,
				                                        image_inspector=imgtool))		
		self.update_image()
		
		return self.plot

###MAIN###
iv = ImageViewer()
if __name__ == "__main__":
	iv.configure_traits()

#--EOF---