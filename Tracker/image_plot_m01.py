#!/usr/bin/env python
"""
Draws an simple RGB image
 - Left-drag pans the plot.
 - Mousewheel up and down zooms the plot in and out.
 - Pressing "z" brings up the Zoom Box, and you can click-drag a rectangular
   region to zoom.  If you use a sequence of zoom boxes, pressing alt-left-arrow
   and alt-right-arrow moves you forwards and backwards through the "zoom
   history".
"""

# Major library imports
from numpy import zeros, uint8

# Enthought library imports
from enable.api import Component, ComponentEditor
from traits.api import HasTraits, Instance, Property, Array, Range
from traitsui.api import Item, Group, View, RangeEditor

# Chaco imports
from chaco.api import ArrayPlotData, Plot
from chaco.tools.api import PanTool, ZoomTool
from chaco.tools.image_inspector_tool import ImageInspectorTool, \
     ImageInspectorOverlay

#===============================================================================
# Attributes to use for the plot view.
size = (600, 600)
title="Simple image plot"
bg_color="lightgray"

#===============================================================================
# # Demo class that is used by the demo.py application.
#===============================================================================
class Demo(HasTraits):
    plot = Instance(Component)
    col=Range(0,255,editor=RangeEditor(low=0, high=255, mode="slider"))
    image=Array
    
    traits_view = View(
                        Group(                        
                            Item('plot', 
                                 editor=ComponentEditor(size=size,bgcolor=bg_color),
                                 show_label=False
                                ),
                            Item('col'),
                            orientation = "vertical"
                        ),
                        resizable=True, title=title
                      )
    
    #def _get_image(self):
    def _col_changed(self):
        #self.image = zeros((200,400,4), dtype=uint8)
        self.image[:,0:40,0] = self.col#*255     # Vertical red stripe      
        self.pd.set_data("imagedata", self.image)
    
    def _plot_default(self):
        # Create some RGBA image data
        self.image = zeros((200,400,4), dtype=uint8)
        #image[:,0:40,0] += 255     # Vertical red stripe
        self.image[0:25,:,1] += 255     # Horizontal green stripe; also yellow square
        #image[-80:,-160:,2] += 255 # Blue square
        self.image[:,:,3] = 255
        
        # Create a plot data obect and give it this data
        self.pd = ArrayPlotData()
        self.pd.set_data("imagedata", self.image)
    
        # Create the plot
        self.plot = Plot(self.pd, default_origin="top left")
        self.plot.x_axis.orientation = "top"
        img_plot = self.plot.img_plot("imagedata")[0]
    
        # Tweak some of the plot properties
        self.plot.bgcolor = "white"
    
        # Attach some tools to the plot
        self.plot.tools.append(PanTool(self.plot, constrain_key="shift"))
        self.plot.overlays.append(ZoomTool(component=self.plot,
                                        tool_mode="box", always_on=False))
    
        imgtool = ImageInspectorTool(img_plot)
        img_plot.tools.append(imgtool)
        self.plot.overlays.append(ImageInspectorOverlay(component=img_plot,
                                                   image_inspector=imgtool))
        return self.plot

demo = Demo()

if __name__ == "__main__":
    demo.configure_traits()

#--EOF---
