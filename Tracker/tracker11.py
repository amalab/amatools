#
# Tracker
# Cell Segment and Track Tool
# JPM
# July 2012
#

# Major library imports
from numpy import empty,zeros,ones,uint8,transpose,exp,hypot,array
import numpy.random

# Enthought library imports
from enable.api import Component,ComponentEditor,ColorTrait
from traits.api import *
from traitsui.api import Item,Group,View,RangeEditor,Tabbed,EnumEditor,Handler,TextEditor,CodeEditor

# Chaco imports
from chaco.api import ArrayPlotData,Plot,OverlayPlotContainer,HPlotContainer,AbstractOverlay,ScatterInspectorOverlay
from chaco.tools.api import PanTool,ZoomTool,SelectTool,ScatterInspector
from chaco.tools.image_inspector_tool import ImageInspectorTool,ImageInspectorOverlay
import chaco.default_colormaps

import tifffile
# sci-kits image processing
import skimage.filter, skimage.morphology
import scipy.ndimage.morphology
import scipy.ndimage.filters

import h5py
import easygui
import subprocess
import os

# Attributes to use for the plot view.
size = (1600,600)
title="Cell Tracker"
bg_color="lightgray"
pi=3.14159
# gaussian filter for scikits-image
def filt_func(r, c):
	return exp(-hypot(r, c)/1)
gaussian_filter = skimage.filter.LPIFilter2D(filt_func)

# for morphological filters
structuring_element=scipy.ndimage.morphology.generate_binary_structure(2, 2)

# colour maps
# random for segmentation view
r=numpy.random.random((1024,4))
r[0,:]=(1.0,1.0,1.0,0.0) #white background
rand_col_map=chaco.api.ColorMapper.from_palette_array(r)
# greens for normal
greens_col_map=chaco.default_colormaps.reverse(chaco.default_colormaps.Greens)
reds_col_map=chaco.default_colormaps.Reds

from enthought.traits.ui.key_bindings import KeyBinding, KeyBindings

key_bindings = KeyBindings(
    KeyBinding( binding1    = 'Left',
                description = 'Time Backward',
                method_name = 'time_backward' ),
    KeyBinding( binding1    = 'Right',
                description = 'Time Forward',
                method_name = 'time_forward' )    
)

class KeyHandler(Handler):
	def time_forward(self,info):
		iv._time_forward_button_changed()
	def time_backward(self,info):
		iv._time_backward_button_changed()

class Cell:
	def __init__(self):
		self.id=0 #segmentation id
		self.bb=[0,0,0,0] #bounding box [x1,x2,y1,y2]
		self.track_type='None'
		self.targets=[]

	def centre(self):
		return ((self.bb[0]+self.bb[1])/2,(self.bb[2]+self.bb[3])/2)

class ImageViewer(HasTraits):
	'''
	 images are stored as ndarrays which are accessed [row][column] ie [y][x]

	 imgarray - numpy array - [time,ch,y,x] 

	 image_orig - numpy array - [y,x] for given t,ch and x,y, crop
	 image_orig_plus - ditto for t+1
	 image_processed - after filtering
	 image_segmented - pixel value is segmentation id

	 plotdata - ArrayPlotData holding all data for one t,t+1 pair
	 plot_orig - Instance(Component) Trait for LH plot
	 plot_orig_plus - Instance(Component) Trait for RH plot
	 plot_container - holds above LH and RH plots and for drawing associations etc

	 image_plot (local) returned image plot created on plot		
	'''

	plotdata = ArrayPlotData()
	plot_orig = Instance(Component)
	plot_orig_plus = Instance(Component)
	#plot_processed = Instance(Component)
	#plot_segmented = Instance(Component)
	plot_container=Instance(Component)#VPlotContainer()
	#plot_container.add(plot_orig)
	#plot_container.add(plot_orig_plus)

	imgarray=Array
	image_orig=Array
	image_orig_plus=Array
	image_processed=Array
	image_processed_plus=Array
	image_segmented=Array
	image_segmented_plus=Array
	image_outline=Array
	image_outline_plus=Array

	#cells=[[],[]]

	zero=Int(0)
	num_channels=Int(1)
	time_max=Int(1)

	data_source=String('')
	image_enum=List(['orig','processed','segmented'])
	image_type=Str('orig')

	#channel=Range(0,num_channels-1,0,editor=RangeEditor(low=0, high=num_channels-1, mode="spinner"))
	channel=Range(low='zero',high='num_channels',value=0,editor=RangeEditor(low_name='zero', high_name='num_channels', mode="spinner"))
	#time=Range(0,time_max,time_init,editor=RangeEditor(low=0, high=time_max, mode="slider"))
	time=Range(low='zero',high='time_max',value=0,editor=RangeEditor(low_name='zero',high_name='time_max',mode="slider"))
	block_size=Range(10,200,100,editor=RangeEditor(low=10, high=199, mode="slider"))
	morph_iterations=Range(1,10,2,editor=RangeEditor(low=1, high=9, mode="slider"))
	LoG_sigma1=Range(1.0,20.0,editor=RangeEditor(low=1.0, high=19.0, mode="slider"))
	LoG_sigma2=Range(1.0,20.0,editor=RangeEditor(low=1.0, high=19.0, mode="slider"))
	footprint=Range(0,10,editor=RangeEditor(low=0, high=9, mode="slider"))

	LoG_switch=Bool(label='LoG')
	thresh_switch=Bool(True)	
	opening_switch=Bool(True)
	closing_switch=Bool
	watershed_switch=Bool(True)
	outline_switch=Bool(False)
	tracks_enum=List(('none','all','selected'))
	tracks_mode=Str('all')

	time_backward_button=Button(label='<-',name='')
	time_forward_button=Button(label='->')
	key_bindings_button=Button()
	selected_cell_text=Str('No Selection')

	process_button=Button(label='Run BOT')	
	open_tiff_button=Button(label='Open TIFF')
	open_h5_button=Button(label='Open H5')
	save_h5_button=Button(label='Save H5')

	traits_view = View(
		Group(
		    Group(		
		        Group(
		            Group(
		                Group(
		                    Item('open_tiff_button',show_label=False),
		                    Item('process_button',show_label=False),
		                    Item('save_h5_button',show_label=False),	  
		                    Item('20'),#spacer
		                    Item('open_h5_button',show_label=False),
		                    orientation='vertical'
		                    ),
		                Group(	
		                    Item('selected_cell_text',style='custom',height=0.1),
		                    #Item('selected_cell_text',editor=TextEditor(multi_line=True),height=-100,style='custom'),
		                    #Item('selected_cell_text',editor=CodeEditor()),#,height=-100),
		                    Item('key_bindings_button',show_label=False),
		                    Item('image_type',editor=EnumEditor(name='image_enum')),
		                    Item('channel'),
		                    Item('outline_switch'),
		                    Item('footprint'),

		                    Item('tracks_mode',label='Show Tracks',editor=EnumEditor(name='tracks_enum',mode='radio')),
		                    orientation = 'vertical'
		                    ), 
		                Group(		                
		                    Item('LoG_switch'),
		                    Item('LoG_sigma1'),
		                    Item('LoG_sigma2'),
		                    Item('thresh_switch'),
		                    Item('block_size'),
		                    Item('opening_switch'),
		                    Item('closing_switch'),
		                    Item('morph_iterations'),
		                    Item('watershed_switch'),     
		                    orientation = 'vertical'
		                    ),
		                orientation='horizontal'
		                ),
		            Item(name=str(size[0]/2)),
		            orientation='horizontal'
		            ),
		        Item('plot_container',editor=ComponentEditor(size=size),show_label=False),

		        Group(
		            Item('time_backward_button',show_label=False),	
		            Item('time_forward_button',show_label=False),
		            Item('time',springy=True),
		            orientation='horizontal'
		            ),

		        orientation = 'vertical'
		        ),
		    orientation = 'horizontal'
		    ),
		resizable=True, title=title, key_bindings=key_bindings, handler=KeyHandler()
	)

	def __init__(self):
		HasTraits.__init__(self)

		self.fn=''
		self.data_source=''

		self.imgarray=zeros((5,2,480,640)) #t,ch,y,x
		self.init()
		self.update()

		self.selected_cell=None

	def init(self):
		self.channel=0
		self.num_channels=self.imgarray.shape[1]-1               
		self.time_max=self.imgarray.shape[0]-2 #for t+1
		self.time_init=0
		#level_max=4096
		self.shape_x=self.imgarray.shape[3]
		self.shape_y=self.imgarray.shape[2]
		image_crop=(self.shape_x,self.shape_y)#(500,500)	
		
		self.cells=[[]]

		#self.channel=Range(0,self.num_channels,0,editor=RangeEditor(low=0, high=self.num_channels, mode="spinner"))
		#self.time=Range(0,self.time_max,self.time_init,editor=RangeEditor(low=0, high=self.time_max-1, mode="slider"))

	def _open_tiff_button_fired(self,events):
		self.fn=easygui.fileopenbox(default=r'/Users/jpm/DATA/_scratch/')

		if self.fn:
			# get tiff file as numpy array
			tiffimg = tifffile.TIFFfile(self.fn)
			self.imgarray = tiffimg.asarray()
			#print imgarray.shape

			# prune array - [time,channel,z,y,x] becomes [time,channel,y,x]
			self.imgarray=self.imgarray[:,:,0,:,:]	
			self.data_source='ome-tiff'
			self.init()
			self._plot_container_default()		

	def _process_button_fired(self,events):
		#h5fn=r'/Users/jpm/Projects/Xinghua/xlou-BOT-b7abf65/data/SAVED/dcelliq-sequence-training.h5'

		# save segmented images to new h5 file
		h5fn=r'/Users/jpm/Progs/Tracker/BOT/data/dcelliq-sequence-training.h5'
		h5=h5py.File(h5fn,'w')
		raw=h5.create_group('Raw')
		seg=h5.create_group('Segmentation')

		start_time=self.time
		tt=0
		for self.time in range(start_time,self.time_max):
			self.update()
			print self.time

			t_str=str(tt)
			t_str='0'*(8-len(t_str))+t_str # make 8 digit time string

			t=raw.create_group(t_str)
			#transpose image aray back again
			data=t.create_dataset('Data',data=transpose(self.image_orig))

			t=seg.create_group(t_str)
			#transpose image aray back again
			data=t.create_dataset('Data',data=transpose(self.image_segmented))

			tt+=1

		h5.close()

		# run BOT on h5 file
		#print subprocess.check_output(['/Users/jpm/Progs/Tracker/BOT/bin/test-TrackingPredictor'])
		os.chdir('/Users/jpm/Progs/Tracker/BOT/bin/')
		os.system('./test-TrackingPredictor')

		print 'BOT completed.'

		# rename h5 
		f=os.path.splitext(os.path.split(self.fn)[1])[0]+'.h5'
		f.translate(None,'.ome') #remove .ome
		c='cp dcelliq-sequence-training.h5 '+f
		os.chdir('/Users/jpm/Progs/Tracker/BOT/data/')		
		os.system(c)

		self.load_h5('/Users/jpm/Progs/Tracker/BOT/data/dcelliq-sequence-training.h5')

	def _open_h5_button_fired(self,events):
		self.fn=easygui.fileopenbox(default=r'/Users/jpm/Progs/Tracker/BOT/data/')	

		if self.fn:
			self.load_h5(self.fn)

	def load_h5(self,fn):
		self.h5f=h5py.File(fn,'r')
		nt=len(self.h5f['Raw']) #num time steps
		nc=2 #num channels
		nx=len(self.h5f['Raw']['00000000']['Data'])
		ny=len(self.h5f['Raw']['00000000']['Data'][0])

		self.imgarray=empty((nt,nc,nx,ny))
		t=0
		for i in self.h5f['Raw']:
			self.imgarray[t][0][:][:]=self.h5f['Raw'][i]['Data'][:]
			t+=1
		t=0
		for i in self.h5f['Segmentation']:
			self.imgarray[t][1][:][:]=self.h5f['Segmentation'][i]['Data'][:]
			t+=1

		#transpose x and y so we have [t,ch,y,x]
		self.imgarray=transpose(self.imgarray,(0,1,3,2))
		self.data_source='h5'
		self.init()
		self._plot_container_default()

	def _save_h5_button_fired(self,events):
		path=easygui.fileopenbox(default=r'/Users/jpm/DATA/')

	def _time_backward_button_changed(self):
		try:
			self.time-=1
		except:
			pass

	def _time_forward_button_changed(self):
		try:
			self.time+=1
		except:
			pass

	def _key_bindings_button_fired(self,event):
		key_bindings.edit_traits()

	def _image_type_changed(self):
		self._plot_container_default()		

	def _data_source_changed(self):
		if self.data_source=='h5':
			self.image_enum=['orig','segmented']
		else:
			self.image_enum=['orig','processed','segmented']	

	def selection_changed(self,object,name,old,new):
		#print object.metadata['selections']
		#print [self.cells[0][i].centre() for i in object.metadata['selections']]
		if len(object.metadata['selections'])>0:
			#self.selected=self.cells[0][object.metadata['selections'][0]].centre()
			self.selected_cell=self.cells[self.time][object.metadata['selections'][0]]
			targets='Targets: '
			for t in self.selected_cell.targets:
				targets+=str(t.id)
			self.selected_cell_text=str(self.selected_cell.id)+'\n'+self.selected_cell.track_type+'\n'+targets
		else:
			#self.selected=(0,0)
			self.selected_cell=None
			self.selected_cell_text='No Selection'
			
	def selection_plus_changed(self,object,name,old,new):
		pass

	def normal_key_pressed(self,event):
		print 'normal_key_pressed'

	@on_trait_change('LoG_switch,LoG_sigma1,LoG_sigma2,thresh_switch,opening_switch,closing_switch,\
	watershed_switch,morph_iterations,block_size,channel,time,outline_switch,footprint,tracks_switch')		
	def update(self):
		print self.time
		#update images and plot data
		self.image_orig=self.imgarray[self.time,self.channel,:,:]
		self.image_orig_plus=self.imgarray[self.time+1,self.channel,:,:]
		self.image_processed=self.image_orig.copy()
		self.image_processed_plus=self.image_orig_plus.copy()

		#im_thresh=self.image > (self.thresh*level_max)
		#self.image=im_thresh 
		#self.image=gaussian_filter(self.image)	

		if self.data_source=='ome-tiff':

			if self.LoG_switch:
				self.image_processed=scipy.ndimage.filters.gaussian_laplace(self.image_processed,(self.LoG_sigma1,self.LoG_sigma2))
				self.image_processed_plus=scipy.ndimage.filters.gaussian_laplace(self.image_processed_plus,(self.LoG_sigma1,self.LoG_sigma2))

			if self.thresh_switch:
				#block_size=3+self.thresh*100
				if self.block_size%2==0:
					self.block_size+=1 #must be odd
				self.image_processed=skimage.filter.threshold_adaptive(self.image_processed, self.block_size)		
				self.image_processed_plus=skimage.filter.threshold_adaptive(self.image_processed_plus, self.block_size)		

			#if self.denoise_switch:
				#self.image_processed=skimage.filter.tv_denoise(self.image_processed)	

			if self.opening_switch:
				self.image_processed=scipy.ndimage.morphology.binary_opening(self.image_processed,structuring_element,self.morph_iterations)
				self.image_processed_plus=scipy.ndimage.morphology.binary_opening(self.image_processed_plus,structuring_element,self.morph_iterations)

			if self.closing_switch:
				self.image_processed=scipy.ndimage.morphology.binary_closing(self.image_processed,structuring_element,self.morph_iterations)
				self.image_processed_plus=scipy.ndimage.morphology.binary_closing(self.image_processed_plus,structuring_element,self.morph_iterations)

			if self.watershed_switch:
				###image
				if self.data_source=='h5':
					#channel 1 holds segmented data
					#self.image_segmented=imgarray[self.time,1,0:image_crop[0],0:image_crop[1]]
					self.image_segmented=self.imgarray[self.time,1,:,:]
					self.image_segmented=self.image_segmented.astype(int)
				else:				
					distance = scipy.ndimage.distance_transform_edt(self.image_processed)
					local_max = skimage.morphology.is_local_maximum(distance, self.image_processed, ones((self.footprint*2+1,self.footprint*2+1)))
					markers = scipy.ndimage.label(local_max)[0]
					labels = skimage.morphology.watershed(-distance, markers, mask=self.image_processed)	
					self.image_segmented=labels

				#add outline to segmented
				if self.outline_switch:
					segmented_binary=self.image_segmented>0
					self.image_outline=segmented_binary-scipy.ndimage.morphology.binary_erosion(segmented_binary)	
					self.image_outline=self.image_outline*self.imgarray.max()
					self.image_orig=self.image_orig+self.image_outline

				###image_plus
				if self.data_source=='h5':
					#self.image_segmented_plus=imgarray[self.time+1,1,0:image_crop[0],0:image_crop[1]]
					self.image_segmented_plus=self.imgarray[self.time+1,1,:,:]
					self.image_segmented_plus=self.image_segmented_plus.astype(int)
				else:							
					distance = scipy.ndimage.distance_transform_edt(self.image_processed_plus)
					local_max = skimage.morphology.is_local_maximum(distance, self.image_processed_plus, ones((self.footprint*2+1,self.footprint*2+1)))
					markers = scipy.ndimage.label(local_max)[0]
					labels = skimage.morphology.watershed(-distance, markers, mask=self.image_processed_plus)	
					self.image_segmented_plus=labels

				#add outline to segmented
				if self.outline_switch:
					segmented_binary=self.image_segmented_plus>0
					self.image_outline_plus=segmented_binary-scipy.ndimage.morphology.binary_erosion(segmented_binary)	
					self.image_outline_plus=self.image_outline_plus*self.imgarray.max()
					self.image_orig_plus=self.image_orig_plus+self.image_outline_plus

		else: # image source is h5
			self.image_segmented=self.imgarray[self.time,1,:,:] #channel 1 is segmented
			self.image_segmented=self.image_segmented.astype(int)

			self.image_segmented_plus=self.imgarray[self.time+1,1,:,:]
			self.image_segmented_plus=self.image_segmented_plus.astype(int)


		#update plot data
		self.plotdata.set_data("image_orig_data", self.image_orig)	
		self.plotdata.set_data("image_orig_plus_data", self.image_orig_plus)	
		self.plotdata.set_data("image_processed_data", self.image_processed)
		self.plotdata.set_data("image_processed_plus_data", self.image_processed_plus)
		self.plotdata.set_data("image_segmented_data", self.image_segmented)
		self.plotdata.set_data("image_segmented_plus_data", self.image_segmented_plus)
		#self.plotdata.set_data("image_outline_data", self.image_outline)

		#cells
		self.cells=[]
		for i in range(self.imgarray.shape[0]):
			self.cells.append([])
		x=[]
		y=[]
		### t
		# bounding boxes as python slices of image ndarray
		bb_slices=scipy.ndimage.measurements.find_objects(self.image_segmented)
		for i in range(len(bb_slices)):	
			s=bb_slices[i]
			y1=s[0].start
			y2=s[0].stop
			x1=s[1].start
			x2=s[1].stop	

			id=self.image_segmented[(y1+y2)/2,(x1+x2)/2]
			cell=Cell()
			cell.id=id
			cell.bb=[x1,x2,y1,y2]
			self.cells[self.time].append(cell)

			x.append(cell.centre()[0])
			y.append(cell.centre()[1])

		#self.cells[0].sort(key=lambda x: x.id)		
		
		#update scatter plot
		self.plotdata.set_data("x",x)
		self.plotdata.set_data("y",y)		

		### t+1
		x=[]
		y=[]		
		bb_slices=scipy.ndimage.measurements.find_objects(self.image_segmented_plus)
		for i in range(len(bb_slices)):	
			s=bb_slices[i]
			y1=s[0].start
			y2=s[0].stop
			x1=s[1].start
			x2=s[1].stop	

			id=self.image_segmented_plus[(y1+y2)/2,(x1+x2)/2]
			cell=Cell()
			cell.id=id
			cell.bb=[x1,x2,y1,y2]
			self.cells[self.time+1].append(cell)
			
			x.append(cell.centre()[0])
			y.append(cell.centre()[1])
			
		#self.cells[1].sort(key=lambda x: x.id)
		
		#update scatter plot
		self.plotdata.set_data("xplus",x)
		self.plotdata.set_data("yplus",y)		


	def _plot_container_default(self):		
		self.update()

		### CONTAINER
		self.plot_container=HPlotContainer()
		overlay = TheOverlay()
		self.plot_container.overlays.append(overlay)		

		### LEFT PLOT
		# Create the plot from the data 
		self.plot_orig = Plot(self.plotdata,default_origin="top left")
		self.plot_orig.x_axis.orientation = "top"

		if self.image_type=='orig':
			image_plot = self.plot_orig.img_plot("image_orig_data","image_plot",colormap=greens_col_map)[0]
		elif self.image_type=='processed':
			image_plot = self.plot_orig.img_plot("image_processed_data","image_plot",colormap=greens_col_map)[0]
		elif self.image_type=='segmented':
			image_plot = self.plot_orig.img_plot("image_segmented_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		# Attach some tools to the plot
		pantool=PanTool(self.plot_orig, constrain_key="shift")
		self.plot_orig.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_orig,tool_mode="box", always_on=False)
		self.plot_orig.overlays.append(zoomtool)

		imgtool = ImageInspectorTool(image_plot)
		image_plot.tools.append(imgtool)
		self.plot_orig.overlays.append(ImageInspectorOverlay(component=image_plot,
				                                             image_inspector=imgtool))

		# for selection
		marker_size=5
		scatter_plot=self.plot_orig.plot(("x","y"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		# Attach the inspector and its overlay
		inspector=ScatterInspector(scatter_plot,selection_mode='single')
		scatter_plot.tools.append(inspector)
		overlay = ScatterInspectorOverlay(scatter_plot,
		                                  hover_marker="circle",
		                                  hover_marker_size=marker_size,		                                  
		                                  hover_color="grey",
		                                  selection_marker="circle",
		                                  selection_marker_size=marker_size,
		                                  selection_color="white",
		                                  selection_line_width=0)
		scatter_plot.overlays.append(overlay)

		#called when data metadata changes to indicate data selected
		scatter_plot.value.on_trait_change(self.selection_changed)


		### RIGHT PLOT
		# Create the plot from the data 
		self.plot_orig_plus = Plot(self.plotdata, default_origin="top left")
		self.plot_orig_plus.x_axis.orientation = "top"
		#self.plot_orig_plus.orientation='v'

		#image_plus_plot = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plus_plot",colormap=greens_col_map)[0]
		if self.image_type=='orig':
			image_plot_plus = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plot",colormap=greens_col_map)[0]
		elif self.image_type=='processed':
			image_plot_plus = self.plot_orig_plus.img_plot("image_processed_plus_data","image_plot",colormap=greens_col_map)[0]
		elif self.image_type=='segmented':
			image_plot_plus = self.plot_orig_plus.img_plot("image_segmented_plus_data","image_plot",colormap=rand_col_map,bg_color='none')[0]

		#if self.image_enum=='segmented':
			#image_plus_plot = self.plot_orig_plus.img_plot("image_segmented_plus_data","image_plus_plot",colormap=rand_col_map,bg_color='none')[0]
		#else:
			#image_plus_plot = self.plot_orig_plus.img_plot("image_orig_plus_data","image_plus_plot",colormap=greens_col_map)[0]

		# Attach some tools to the plot
		pantool=PanTool(self.plot_orig_plus, constrain_key="shift")
		self.plot_orig_plus.tools.append(pantool)
		zoomtool=ZoomTool(component=self.plot_orig_plus,tool_mode="box", always_on=False)
		self.plot_orig_plus.overlays.append(zoomtool)

		#sync range set by zoom and pan tools
		self.plot_orig_plus.sync_trait('range2d',self.plot_orig)	
		
		# for selection
		scatter_plot_plus=self.plot_orig_plus.plot(("xplus","yplus"),type="scatter",marker="circle",color='black',marker_size=marker_size)[0]
		# Attach the inspector and its overlay
		inspector=ScatterInspector(scatter_plot_plus,selection_mode='single')
		scatter_plot_plus.tools.append(inspector)
		overlay = ScatterInspectorOverlay(scatter_plot_plus,
		                                  hover_marker="circle",
		                                  hover_marker_size=marker_size,		                                  
		                                  hover_color="grey",
		                                  selection_marker="circle",
		                                  selection_marker_size=marker_size,
		                                  selection_color="white",
		                                  selection_line_width=0)
		scatter_plot_plus.overlays.append(overlay)

		##called when data metadata changes to indicate data selected
		#scatter_plot_plus.value.on_trait_change(self._selection_plus_changed)		

		########
		self.plot_container.add(self.plot_orig)
		self.plot_container.add(self.plot_orig_plus)
		return self.plot_container


class TheOverlay(AbstractOverlay):		
	line_color = ColorTrait((1.0, 0.0, 0.0, 1))
	line_width = Int(2)
	alpha = Float(0.8)

	def overlay(self, component, gc, view_bounds=None, mode="normal"):
		#debug circle
		#if iv.selected_cell:
			#gc.begin_path()	
			#x=iv.plot_orig.index_mapper.map_screen(iv.selected_cell.centre()[0])
			#y=iv.plot_orig.value_mapper.map_screen(iv.selected_cell.centre()[1])
			#gc.arc(x,y,10,0,pi*2)
			#gc.stroke_path()

		if iv.tracks_mode=='none':
			return

		# Draws this overlay onto 'component', rendering onto 'gc'.
		linesets=(('Move',(1,1,0,1)),#yellow
				  ('Division',(1,0,0,1)),#red
				  ('Split',(0,0,1,1)),#blue
				  ('Merge',(0,1,1,1)))#cyan
				#('Appearance',(0,0,1,1)),#blue
				#('Disappearance',(1,0,1,1)))#purple

		for ls in linesets:
			track_type=ls[0]
			track_col=ls[1]

			#if track_type=='Division':
				#pass

			source_posns=[]
			target_posns=[]

			t=str(iv.time)
			t='0'*(8-len(t))+t # make 8 digit time string
			t_plus=str(iv.time+1)
			t_plus='0'*(8-len(t_plus))+t_plus # make 8 digit time string

			#ids
			try:
				source=iv.h5f['Tracking'][t][track_type]['Source']['Data'][0][:]
				target1=iv.h5f['Tracking'][t][track_type]['Target']['Data'][0][:]
				if len(iv.h5f['Tracking'][t][track_type]['Target']['Data'])>1:
					target2=iv.h5f['Tracking'][t][track_type]['Target']['Data'][1][:]
				else:
					target2=None
					
				proceed=True
					
			except:
				proceed=False
				
			if proceed:
				for i in range(len(source)):
					#record track_type in cell
					source_cell=iv.cells[iv.time][source[i]-1]
					source_cell.track_type=track_type
					source_cell.targets=[]
					source_cell.targets.append(iv.cells[iv.time+1][target1[i]-1])
					if target2 is not None:
						source_cell.targets.append(iv.cells[iv.time+1][target2[i]-1])
						
					#visibility
					if iv.tracks_mode=='selected' and iv.cells[iv.time][source[i]-1] is not iv.selected_cell:
						continue
					source_posns.append(iv.cells[iv.time][source[i]-1].centre()) #-1 because cells is 0 based, but segmentation id is 1 based
					target_posns.append(iv.cells[iv.time+1][target1[i]-1].centre())

					if track_type=='Division' or track_type=='Split':
						source_posns.append(iv.cells[iv.time][source[i]-1].centre()) #-1 because cells is 0 based, but segmentation id is 1 based						
						target_posns.append(iv.cells[iv.time+1][target2[i]-1].centre())

				lh_plot_pos=component._components[0].position #1st component of container ie LH plot
				rh_plot_pos=component._components[1].position #2nd component of container ie RH plot

				with gc:

					gc.set_alpha(self.alpha)
					gc.set_line_width(self.line_width)
					gc.set_stroke_color(track_col)

					index_low=iv.plot_orig.index_mapper.low_pos
					index_high=iv.plot_orig.index_mapper.high_pos
					value_low=iv.plot_orig.value_mapper.low_pos
					value_high=iv.plot_orig.value_mapper.high_pos			

					gc.begin_path()			

					for i in range(len(source_posns)):
						x_from=iv.plot_orig.index_mapper.map_screen(source_posns[i][0])
						y_from=iv.plot_orig.value_mapper.map_screen(source_posns[i][1])
						# the y/value axis is upside down
						if x_from>index_low and x_from<index_high \
						   and y_from<value_low and y_from>value_high:
							gc.move_to(x_from,y_from)			

							x_to=iv.plot_orig_plus.index_mapper.map_screen(target_posns[i][0])
							y_to=iv.plot_orig_plus.value_mapper.map_screen(target_posns[i][1])				
							gc.line_to(x_to,y_to)


					gc.stroke_path()


		#for x in range(size[0]):
			#for y in range(size[1]):
				#c=component._window.dc.GetPixel(x,y)
				#print c[0],


		return

	def normal_left_down(self,event):
		print 'left_down'
		self.x=event.x
		self.y=event.y
		self._request_redraw()

	def normal_key_pressed(self,event):
		print 'key_pressed'

###MAIN###
iv = ImageViewer()
if __name__ == "__main__":
	iv.configure_traits()

#--EOF---