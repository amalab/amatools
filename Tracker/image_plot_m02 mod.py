#!/usr/bin/env python
"""
Draws an simple RGB image
 - Left-drag pans the plot.
 - Mousewheel up and down zooms the plot in and out.
 - Pressing "z" brings up the Zoom Box, and you can click-drag a rectangular
   region to zoom.  If you use a sequence of zoom boxes, pressing alt-left-arrow
   and alt-right-arrow moves you forwards and backwards through the "zoom
   history".
"""

# Major library imports
from numpy import zeros, uint8, transpose, exp, hypot

# Enthought library imports
from enable.api import Component, ComponentEditor
from traits.api import HasTraits, Instance, Property, Array, Range, Bool
from traitsui.api import Item, Group, View, RangeEditor

# Chaco imports
from chaco.api import ArrayPlotData, Plot
from chaco.tools.api import PanTool, ZoomTool
from chaco.tools.image_inspector_tool import ImageInspectorTool, \
     ImageInspectorOverlay

import tifffile as tff
# sci-kits image processing
import skimage.filter

#fn = tkFileDialog.askopenfilename()
#fn=r'/Users/jpm/DATA/David - Images for tracking/2012-03-19 Wntch 2iLIF to labelled-0002_2i to ESminusLIF_01.zvi - C=2 slices=90-100 8-bit.tif'
fn=r'/Users/jpm/DATA/_scratch/120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2.ome.tiff'
#fn=r'/Users/jpm/DATA/_scratch/esgZ_GSHg_ArmPros_DAPI_4-6DO_G5F30.ome.tiff'

# get tiff file as numpy array
tiffimg = tff.TIFFfile(fn)
imgarray = tiffimg.asarray()
print imgarray.shape

# prune array - [time,channel,z,y,x] becomes [time,channel,y,x]
imgarray2=imgarray[:,:,0,:,:]

#ch1 is GFP for 120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2

imgarray2=transpose(imgarray2,(0,1,3,2)) #[time,channel,x,y]

num_channels=imgarray2.shape[1]                 
time_max=imgarray2.shape[0]-1
level_max=4096
shape_x=imgarray2.shape[3]
shape_y=imgarray2.shape[2]

# Attributes to use for the plot view.
size = (800, 800)
title="Image Viewer"
bg_color="lightgray"

def filt_func(r, c):
	return exp(-hypot(r, c)/1)

gaussian_filter = skimage.filter.LPIFilter2D(filt_func)

class ImageViewer(HasTraits):
	plot = Instance(Component)

	channel=Range(0,num_channels-1,editor=RangeEditor(low=0, high=num_channels-1, mode="spinner"))
	time=Range(0,time_max,editor=RangeEditor(low=0, high=time_max, mode="slider"))
	thresh=Range(0.0,1.0,editor=RangeEditor(low=0.0, high=1.0, mode="slider"))

	denoise_switch=Bool
	thresh_switch=Bool
	
	image=Property(Array,depends_on=['channel','time','thresh','denoise_switch','thresh_switch'])
	
	traits_view = View(
		Group(
		    Group(
		        Item('denoise_switch'),
		        Item('thresh_switch'),
	            orientation = 'vertical'
		        ),
		    Group(                        
		        Item('plot', 
		             editor=ComponentEditor(size=size),#,bgcolor=bg_color),
		             show_label=False
		             ),
		        Item('thresh'),
		        Item('channel'),
		        Item('time'),
		        orientation = 'vertical'
		        ),
	        orientation = 'horizontal'
		    ),
		resizable=True, title=title
	)
	
	def _get_image(self):
		image=imgarray2[self.time,self.channel,:,:]

		#im_thresh=self.image > (self.thresh*level_max)
		#self.image=im_thresh 

		#self.image=gaussian_filter(self.image)	

		if self.thresh_switch:
			block_size=3+self.thresh*100
			if block_size%2==0:
				block_size+=1 #must be odd
			image=skimage.filter.threshold_adaptive(image, block_size)		
			
		if self.denoise_switch:
			image=skimage.filter.tv_denoise(image)			

		self.pd.set_data("imagedata", image) 	
		
		return image
		
#	def update_image(self):
		#self.image=imgarray2[self.time,self.channel,:,:]

		##im_thresh=self.image > (self.thresh*level_max)
		##self.image=im_thresh 

		##self.image=gaussian_filter(self.image)	

		#if self.thresh_switch:
			#block_size=3+self.thresh*100
			#if block_size%2==0:
				#block_size+=1 #must be odd
			#self.image=skimage.filter.threshold_adaptive(self.image, block_size)		
			
		#if self.denoise_switch:
			#self.image=skimage.filter.tv_denoise(self.image)			

		#self.pd.set_data("imagedata", self.image)   

	#def _channel_changed(self): 
		#self.update_image()

	#def _time_changed(self):
		#self.update_image()

	#def _thresh_changed(self):
		#self.update_image()
		
	#def _denoise_switch_changed(self):
		#self.update_image()	
		
	#def _thresh_switch_changed(self):
		#self.update_image()		

	def _plot_default(self):
		## Create some RGBA image data
		#self.image = zeros((200,400,4), dtype=uint8)
		##image[:,0:40,0] += 255     # Vertical red stripe
		#self.image[0:25,:,1] += 255     # Horizontal green stripe; also yellow square
		##image[-80:,-160:,2] += 255 # Blue square
		#self.image[:,:,3] = 255

		#self.image=imgarray2[0,:,:]

		#self.image = zeros((shape_y,shape_x,4), dtype=uint8)
		#self.image[:,:,1]=imgarray2[0,:,:] #green
		#self.image[:,:,3] = 255 #alpha

		image=imgarray2[0,0,:,:]

		# Create a plot data obect and give it this data
		self.pd = ArrayPlotData()
		self.pd.set_data("imagedata", image)

		# Create the plot
		self.plot = Plot(self.pd, default_origin="top left")
		self.plot.x_axis.orientation = "top"
		img_plot = self.plot.img_plot("imagedata")[0]

		# Tweak some of the plot properties
		self.plot.bgcolor = "white"

		# Attach some tools to the plot
		self.plot.tools.append(PanTool(self.plot, constrain_key="shift"))
		self.plot.overlays.append(ZoomTool(component=self.plot,
				                           tool_mode="box", always_on=False))

		imgtool = ImageInspectorTool(img_plot)
		img_plot.tools.append(imgtool)
		self.plot.overlays.append(ImageInspectorOverlay(component=img_plot,
				                                        image_inspector=imgtool))
		return self.plot

###MAIN###
iv = ImageViewer()
if __name__ == "__main__":
	iv.configure_traits()

#--EOF---