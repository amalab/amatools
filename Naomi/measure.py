from pylsm import lsmreader
import numpy
import matplotlib
from matplotlib import pyplot
import scipy
import skimage.filter
import skimage.feature
import skimage.morphology
import os
import glob
import pickle
import readroi

def read_mtrackj_mdf(mdf_file,verbose=False):
	'''Read track data, including cluster info, from MTrackJ mdf file & return as list of Tracks()'''

	mdf_file=open(mdf_file,'rU')
	mdf_lines=mdf_file.readlines()
	tracks=[]

	l=0
	while True:
		line=mdf_lines[l]
		if 'Cluster' in line:
			break
		l+=1

	while 'Cluster' in line:
		line_elems=line.split(' ')
		cluster=int(line_elems[1])
		if verbose:
			print 'Cluster',cluster

		l+=1
		line=mdf_lines[l]

		while 'Track' in line and 'End' not in line:
			track=Track()
			line_elems=line.split(' ')
			track.number=int(line_elems[1])
			track.id=len(tracks)
			track.cluster=cluster
			tracks.append(track)
			if verbose:
				print 'Track',track.id,track.number

			l+=1
			line=mdf_lines[l]
			x=[]
			y=[]
			t=[]
			while 'Point' in line:
				line_elems=line.split(' ')
				x.append(float(line_elems[2]))
				y.append(float(line_elems[3]))
				t.append(float(line_elems[5]))

				l+=1
				line=mdf_lines[l]

			track.x=numpy.array(x)
			track.y=numpy.array(y)
			track.t=numpy.array(t)

	return tracks

class Track:
	'''Track of cells over time'''
	def __init__(self):	
		self.id=None # unique number from 0..len(tracks)
		self.number=None # mtrackj track number
		self.cluster=None #mtracksj cluster

		self.x=None # x pos in pixels
		self.y=None # y pos in pixels 
		self.t=None # time in frames
		self.f={} #fluorescence measurements

		self.parent=None
		self.children=[]
		self.sibling=None
		self.linked=False

		self.yplot=0 #height for plotting lineage

def measure(im,channels,points):
	measurements=[]
	#measurements['clickpoint']={}
	#measurements['segmented']={} 

	orig=im[BLUE]

	##segment
	##smooth
	#sigma=3.0
	#smooth=scipy.ndimage.filters.gaussian_filter(orig,(sigma,sigma))

	##thresh
	##threshold=skimage.filter.threshold_yen(smooth)
	##thresh=smooth<threshold
	#thresh=skimage.filter.threshold_adaptive(smooth,55)#,offset=10)

	##fill holes
	##thresh=scipy.ndimage.morphology.binary_fill_holes(thresh)

	##watershed
	#distance=scipy.ndimage.distance_transform_edt(thresh)
	#local_maxi=skimage.feature.peak_local_max(distance, indices=False, footprint=numpy.ones((3, 3)),labels=orig)
	#markers=scipy.ndimage.label(local_maxi)[0]

	#click_points=numpy.zeros_like(markers,dtype='uint8')
	#for p in points:
		#click_points[p[1],p[0]]=255

	#markers_click=scipy.ndimage.label(click_points)[0]

	#labels = skimage.morphology.watershed(-distance, markers_click, mask=thresh)
	##labels=numpy.mod(labels,256).astype('uint8')
	#print 'Segmented'

	#measure around click point
	r=8

	#gaussian weighted measurement area
	g=100
	sigma=128
	seed=numpy.zeros((2*g+1,2*g+1))
	seed[g,g]=1.0
	weight=scipy.ndimage.filters.gaussian_filter(seed,sigma) #2d gaussian kernal
	weight/=numpy.sum(weight) #sum of all weights=1.0

	for ch in channels:
		#measurements['clickpoint'][ch]=[]
	
		for p in points:
			x=p[0]
			y=p[1]
			m=numpy.sum(im[ch,y-r:y+r,x-r:x+r]) #square
			#mg=numpy.sum(im[ch,y-g:y+g+1,x-g:x+g+1]*weight) #gaussian weighted
			a=4*r*r            
			#m,a=t.measure_fluorescence(im,[ch])
			measurements.append(m/a) #value/area


	##measure segmented
	#for ch in channels:
		#measurements['segmented'][ch]=[]

		#for i in range(1,labels.max()):
			#p=numpy.where(labels==i) #points
			#area=len(p[0])

			#if area>0:
				##m,a=t.measure_fluorescence(im,[ch],points=points) 
				#v=[im[ch,y,x] for (y,x) in zip(p[0],p[1])] #NOTE points are (y,x)
				#m=numpy.sum(v)
				#a=len(v)

				#measurements['segmented'][ch].append((m,a))

	##show segmentation            
	#rc=numpy.random.rand(256,3)
	#rc[0]=(0,0,0)
	#rc[255]=(1,1,1)
	#rand_cmap=matplotlib.colors.ListedColormap(rc)
	##combined=numpy.maximum(thresh,click_points)

	#fig=pyplot.figure(figsize=(20,20))  
	#p=fig.add_subplot(1,1,1)    
	#p.imshow(labels,cmap=rand_cmap)#,aspect='auto')  
	##p.imshow(distance) 
	#fig.savefig(f+'_segmented.png')

	####overlay
	#fig=pyplot.figure(figsize=(20,20))

	#p1=fig.add_subplot(1,1,1)
	#p1.set_title('Measurement region')    
	#overlay=im[GREEN]/2
	##overlay=labels
	#for p in points:
		#x=p[0]
		#y=p[1]
		#overlay[y-g:y+g+1,x-g:x+g+1]=im[GREEN][y-g:y+g+1,x-g:x+g+1]*weight

		##error=numpy.where(labels[y-r:y+r,x-r:x+r] ==0 )#!= labels[y,x])
		##xx=error[1]+x-r
		##yy=error[0]+y-r
		##overlay[(yy,xx)]=255

	#p1.imshow(overlay,cmap='gray')

	#fig.savefig(f+'_overlay.png')    

	return measurements


###MAIN

#channels
BLUE=0
GREEN=1

cols=['r','g','b','c','m','y','olive','gray','plum','pink','crimson','gold']


path='/Users/jpm/Progs/AMATools/Naomi/new/'

lsmfiles=glob.glob(path+'*15.lsm')
filenames=[os.path.splitext(f)[0] for f in lsmfiles]
print filenames  

for f in filenames:  
	print f
	fi=open(f+' Points.roi', mode='r')
	points=readroi.read_roi(fi)

	lsm=lsmreader.Lsmimage(f+'.lsm')
	lsm.open()

	im=numpy.array(lsm.image['data'])[:,:,:,0]
	print im.shape
	num_channels=im.shape[0] 

	channels=[GREEN]
	measurements=measure(im, channels, points)

	##csv 
	fi=open(f+'_measurements.csv','w')
	fi.write('x, y, fluorescence\n')
	for p,m in zip(points,measurements):
		fi.write(str(p[0])+', '+str(p[1])+', '+str(m)+'\n')

