# Copyright: Luis Pedro Coelho <luis@luispedro.org>, 2012
# License: MIT
import numpy as np

def read_roi(fileobj):
    '''
    points = read_roi(fileobj)

    Read ImageJ's ROI format
    '''
# This is based on:
# http://rsbweb.nih.gov/ij/developer/source/ij/io/RoiDecoder.java.html
# http://rsbweb.nih.gov/ij/developer/source/ij/io/RoiEncoder.java.html

    SPLINE_FIT = 1
    DOUBLE_HEADED = 2
    OUTLINE = 4
    OVERLAY_LABELS = 8
    OVERLAY_NAMES = 16
    OVERLAY_BACKGROUNDS = 32
    OVERLAY_BOLD = 64
    SUB_PIXEL_RESOLUTION = 128
    DRAW_OFFSET = 256


    pos = [4]
    def get8():
        pos[0] += 1
        s = fileobj.read(1)
        if not s:
            raise IOError('readroi: Unexpected EOF')
        return ord(s)

    def get16():
        b0 = get8()
        b1 = get8()
        return (b0 << 8) | b1

    def get32():
        s0 = get16()
        s1 = get16()
        return (s0 << 16) | s1

    def getfloat():
        v = np.int32(get32())
        return v.view(np.float32)
    
    #  ImageJ/NIH Image 64 byte ROI outline header
        #2 byte numbers are big-endian signed shorts
        
        #0-3     "Iout"
        #4-5     version (>=217)
        #6-7     roi type
        #8-9     top
        #10-11   left
        #12-13   bottom
        #14-15   right
        #16-17   NCoordinates
        #18-33   x1,y1,x2,y2 (straight line)
        #34-35   stroke width (v1.43i or later)
        #36-39   ShapeRoi size (type must be 1 if this value>0)
        #40-43   stroke color (v1.43i or later)
        #44-47   fill color (v1.43i or later)
        #48-49   subtype (v1.43k or later)
        #50-51   options (v1.43k or later)
        #52-52   arrow style or aspect ratio (v1.43p or later)
        #53-53   arrow head size (v1.43p or later)
        #54-55   rounded rect arc size (v1.43p or later)
        #56-59   position
        #60-63   header2 offset
        #64-       x-coordinates (short), followed by y-coordinates
    #
    magic = fileobj.read(4)
    if magic != 'Iout':
        raise IOError('Magic number not found')
    version = get16()

    # It seems that the roi type field occupies 2 Bytes, but only one is used
    roi_type = get8()
    # Discard second Byte:
    get8()

    if not (0 <= roi_type < 11):
        raise ValueError('roireader: ROI type %s not supported' % roi_type)

    #if roi_type != 7:
        #raise ValueError('roireader: ROI type %s not supported (!= 7)' % roi_type)

    top = get16()
    left = get16()
    bottom = get16()
    right = get16()
    n_coordinates = get16()

    x1 = getfloat() 
    y1 = getfloat() 
    x2 = getfloat() 
    y2 = getfloat()
    stroke_width = get16()
    shape_roi_size = get32()
    stroke_color = get32()
    fill_color = get32()
    subtype = get16()
    if subtype != 0:
        raise ValueError('roireader: ROI subtype %s not supported (!= 0)' % subtype)
    options = get16()
    arrow_style = get8()
    arrow_head_size = get8()
    rect_arc_size = get16()
    position = get32()
    header2offset = get32()

    #if options & SUB_PIXEL_RESOLUTION:
        #getc = getfloat
        #points = np.empty((n_coordinates, 2), dtype=np.float32)
    #else:
        #getc = get16
        #points = np.empty((n_coordinates, 2), dtype=np.int16)

    getc=get16
    points = np.empty((n_coordinates, 2), dtype=np.int16)
        
    points[:,0] = [getc() for i in xrange(n_coordinates)]
    points[:,1] = [getc() for i in xrange(n_coordinates)]
    points[:,0] += left
    points[:,1] += top
    #points -= 1
    return points

def read_roi_zip(fname):
    import zipfile
    with zipfile.ZipFile(fname) as zf:
        return [read_roi(zf.open(n))
                    for n in zf.namelist()]
