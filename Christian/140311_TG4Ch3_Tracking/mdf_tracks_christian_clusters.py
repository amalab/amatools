#
# measure GATA in single red channel
# mdf's have two clusters, #1 is PrE, #2 is Epi fate at end of time course
#
# jpm 17-3-14
#

import matplotlib
matplotlib.use('TkAgg') # to allow interactive plots in Wing

import sys
sys.path.insert(0,'../../amatools_dist') #adds cwd to path, to pickup modules local to amatools_dist directory

import os
import amatools
import matplotlib.colors 
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
#from prettyplotlib import plt as pyplot
import numpy

import libtiff
from bs4 import BeautifulSoup

import pandas

#christian
filename_base='140311_TG4Ch3_TL-0001_AB02_%s'
file_suffixes='01','02','03','04','08','09','10'

def read_zvi_ome_tiff(fn):
	#read direct
	tiff_file=libtiff.TIFFfile(fn)
	tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array

	# t*c,y,x -> t,c,y,x

	# get info
	# get header as string
	s=tiff_file.get_info()
	# make list of tags
	l=s.splitlines()

	try:
		# get ome.tiff xml header
		xml=s[s.find('"""')+3:s.rfind('"""')]

	except:
		print 'No XML header'

	# parse XML
	header=BeautifulSoup(xml,"lxml")

	# get pixel tag attributes
	pixel_attributes=header.find('pixels').attrs
	print 'Pixel attributes:',pixel_attributes

	# get all channel tags
	ch=header.find_all('channel')

	# print channel name attributes
	print 'Channels:'
	numch=0
	for c in ch:
		print numch,c['name'] # in BeautifulSoup, attrs are dicts of bs tree objects
		numch+=1				

	print 'tiff_array shape:',tiff_array.shape # [time*channels,height,width]

	# reshape
	sh=tiff_array.shape
	tiff_array=tiff_array.reshape((-1,numch,sh[1],sh[2]))
	sh=tiff_array.shape
	print 'reshaped as:',sh
	T=sh[0]
	width=sh[3]
	height=sh[2]
	print 'T numch width height',T,numch,width,height

	return tiff_array


#plot images
#fig=pyplot.figure()		

#cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)],                                                      
								#'green': [(0.0,  0.0, 0.0),
									#(1.0,  1.0, 1.0)],                                                        
								#'blue':  [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)]})

#cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
									#(1.0,  1.0, 1.0)],                                                      
								#'green': [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)],                                                        
								#'blue':  [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)]})

#cmaps=['binary_r',cmap_g,cmap_r]
#t=T/2
#cc=0
#pp=[] #plots
#for c in ch:
	#p=fig.add_subplot(1,numch,cc+1) 
	#p.set_title(c['name'])	

	#im=tiff_array[t,cc,:,:]
	#imgplot=p.imshow(im)
	#imgplot.set_cmap(cmaps[cc])
	#pp.append(p)
	#cc+=1

#pyplot.show()

##image
#tiff=amatools.AMATiff()
#tiff.open(tiff_filename)
##tiff.reshape_fiji()
#p=tiff.plot(0)	

#fig.suptitle(os.path.split(mdf_filename)[1])
#tracks
#plot_titles=['Epi - Spry4','Epi - GATA4','PrE - Spry4','PrE - GATA4']
#plot_num=0
#plot_order=[1,3,2,4]
track_colours=['r','g','b','c','m','y','k']
channels={1:'gata'}

# calc dataframes
#data_sprouty=pandas.DataFrame()
data_gata=pandas.DataFrame()
track_count=0
r=3 #radius of square sample area around track point

for f in file_suffixes:
	mdf_filename=filename_base%f+'_all'
	bgr_filename=filename_base%f+'_bgr'
	tiff_filename=filename_base%f+'.ome.tiff'

	# read zvi video
	tiff_array=read_zvi_ome_tiff(tiff_filename)

	# measure fluorescence along tracks	
	
	pre_tracks=amatools.AMACellTracks()
	pre_tracks.read_mtrackj_mdf_cluster(mdf_filename,1)
	epi_tracks=amatools.AMACellTracks()
	epi_tracks.read_mtrackj_mdf_cluster(mdf_filename,2)
	# background tracks
	bgr_tracks=amatools.AMACellTracks()
	bgr_tracks.read_mtrackj_mdf(bgr_filename)
	
	for channel in channels:			
		#backgrounds
		backgrounds=pandas.DataFrame()
		bgr_count=0
		for track in bgr_tracks.tracks:
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)

				#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				bgr=pandas.Series(vv,tt)
				#smoothed=pandas.rolling_mean(smoothed,3)
				label=str(bgr_count)+'_'+channels[channel]
				d=pandas.DataFrame({label:bgr})

				backgrounds=backgrounds.join(d,how='outer')		
				bgr_count+=1	
				
		background_ave=backgrounds.mean(axis=1)
		#print '-----'
		#print 'background_ave'
		#print background_ave,'\n'
		
				
		#cells
		for tracks,fate in ((pre_tracks,'PrE'),(epi_tracks,'Epi')):
			for track in tracks.tracks:
				if track.duration>0.0:
					vv=[]
					tt=[]
					N=len(track.ti)
					for i in range(0,N):			
						x=track.x[i]
						y=track.y[i]
						t=track.ti[i]
						v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
						if v<-20:
							v=0
							print 'v < -20'
						vv.append(v)
						tt.append(t)
	
					#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')
	
					measurement=pandas.Series(vv,tt)
					corrected=measurement.sub(background_ave) #subtract background ave
					
					# zero few negative outliers
					#print 'measurement:',measurement.min(),measurement.max()
					#print 'corrected:',corrected.min(),corrected.max()
					corrected[corrected<-20]=0
					
					smoothed=pandas.rolling_mean(corrected,3)
				
					label=str(track_count)+'_'+fate+'_'+channels[channel]
					d=pandas.DataFrame({label:smoothed})
					#if channels[channel] is 'sprouty':
						#data_sprouty=data_sprouty.join(d,how='outer')
					#elif channels[channel] is 'gata':
					data_gata=data_gata.join(d,how='outer')
	
					#p.plot([track_count]*len(tt),tt,smoothed.values,color=track_colours[track_count%len(track_colours)])
	
					track_count+=1

		#plot_num+=1

# save dataframes
#data_sprouty.to_pickle('data_sprouty.df')
data_gata.to_pickle('data_gata.df')


####PLOTS

# plot gata over time for each fate
fig=pyplot.figure()
p=fig.add_subplot(1,1,1)
p.set_title('cyan - Epi   magenta - PrE')
p.set_xlabel('time')
p.set_ylabel('gata')

epi_tracks=data_gata.select(lambda s: 'Epi' in s,axis=1)
pre_tracks=data_gata.select(lambda s: 'PrE' in s,axis=1)

count=0
for c in epi_tracks.columns:
	series=epi_tracks[c]
	col='c'

	p.plot(series.index,series.values,color=col)	#x,y,col
	count+=1
	
for c in pre_tracks.columns:
	series=pre_tracks[c]
	col='m'

	p.plot(series.index,series.values,color=col)	#x,y,col
	count+=1


## 3D plot of sprouty signalling
#fig=pyplot.figure()
#p=fig.add_subplot(1,1,1,projection='3d')
#p.set_title('cyan - Epi   magenta - PrE')
#p.set_xlabel('gata_at_30')
#p.set_ylabel('time')
#p.set_zlabel('sprouty')

#count=0
##for c in sprouty_tracks.columns:
#for c in data_sprouty.columns:
	##sprouty_series=sprouty_tracks[c]
	#sprouty_series=data_sprouty[c]

	#if 'Epi' in c:
		#col='c'
	#else:
		#col='m'

	#gata_at_30=data_gata.iloc[30,count]

	#p.plot([gata_at_30]*len(sprouty_series.index),sprouty_series.index,sprouty_series.values,color=col)	#x,y,z,col
	#count+=1

pyplot.show(block=False)
pass


#tracks.show_plots(block=False)
#tracks.show_plots(block=True)