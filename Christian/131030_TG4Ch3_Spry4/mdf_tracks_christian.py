import sys
sys.path.insert(0,'') #adds cwd to path, to pickup modules local to amatools_dist directory

import os
import amatools
import matplotlib.colors 
import matplotlib.pyplot as pyplot
#from prettyplotlib import plt as pyplot
import numpy

import libtiff
from bs4 import BeautifulSoup

import pandas

#mdf_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/Clicked Data/C2-070311 tnga in 2i_1_20.tif.mdf'
#tiff_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/070311 tnga in 2i image converted/070311 tnga in 2i_1_20.tif'
#mdf_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/Clicked Data/C2-140111 tnga ln n2b27 chi_1_20.tif.mdf'
#tiff_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/140111 tnga ln n2b27 chi converted/140111 tnga ln n2b27 chi_1_20.tif'

#christian
#mdf_filenames=['/Users/jpm/DATA/Christian/Tracks/131030_TG4Ch3_Spry4-Epi_AA03_05.zvi.mdf','/Users/jpm/DATA/Christian/Tracks/131030_TG4Ch3_Spry4-PrE_AA03_05.zvi.mdf']
#tiff_filename='/Users/jpm/DATA/Christian/Tracks/131030_TG4Ch3_Spry4-0002_AA03_05.ome.tiff'
mdf_filenames=['131030_TG4Ch3_Spry4-Epi_AA03_05.zvi.mdf','131030_TG4Ch3_Spry4-PrE_AA03_05.zvi.mdf']
tiff_filename='131030_TG4Ch3_Spry4-0002_AA03_05.ome.tiff'

#read direct
tiff_file=libtiff.TIFFfile(tiff_filename)
tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array

# t*c,y,x -> t,c,y,x

# get info
# get header as string
s=tiff_file.get_info()
# make list of tags
l=s.splitlines()

try:
	# get ome.tiff xml header
	xml=s[s.find('"""')+3:s.rfind('"""')]
	
except:
	print 'No XML header'
							
# parse XML
header=BeautifulSoup(xml,"lxml")

# get pixel tag attributes
pixel_attributes=header.find('pixels').attrs
print 'Pixel attributes:',pixel_attributes

# get all channel tags
ch=header.find_all('channel')

# print channel name attributes
print 'Channels:'
numch=0
for c in ch:
	print numch,c['name'] # in BeautifulSoup, attrs are dicts of bs tree objects
	numch+=1				

print 'tiff_array shape:',tiff_array.shape # [time*channels,height,width]

# reshape
sh=tiff_array.shape
tiff_array=tiff_array.reshape((-1,numch,sh[1],sh[2]))
sh=tiff_array.shape
print 'reshaped as:',sh
T=sh[0]
width=sh[3]
height=sh[2]
print 'T numch width height',T,numch,width,height

#plot
fig=pyplot.figure()		

cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)],                                                      
                                                           'green': [(0.0,  0.0, 0.0),
                                                                     (1.0,  1.0, 1.0)],                                                        
                                                           'blue':  [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)]})

cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
                                                                     (1.0,  1.0, 1.0)],                                                      
                                                           'green': [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)],                                                        
                                                           'blue':  [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)]})

cmaps=['binary_r',cmap_g,cmap_r]
t=T/2
cc=0
pp=[] #plots
for c in ch:
	p=fig.add_subplot(1,numch,cc+1) 
	p.set_title(c['name'])	

	im=tiff_array[t,cc,:,:]
	imgplot=p.imshow(im)
	imgplot.set_cmap(cmaps[cc])
	pp.append(p)
	cc+=1
	
#pyplot.show()

##image
#tiff=amatools.AMATiff()
#tiff.open(tiff_filename)
##tiff.reshape_fiji()
#p=tiff.plot(0)

fig=pyplot.figure()
#fig.suptitle(os.path.split(mdf_filename)[1])
#tracks
plot_titles=['Epi - Spry4','Epi - GATA4','PrE - Spry4','PrE - GATA4']
plot_num=0
plot_order=[1,3,2,4]
track_colours=['r','g','b','c','m','y','k']

for mdf_filename in mdf_filenames:
	tracks=amatools.AMACellTracks()
	
	tracks.read_mtrackj_mdf(mdf_filename)
	tracks.plot_tracks(pp[0])
			
	
	# measure fluorescence along tracks
	for fluor_channel in [1,2]:
		p=fig.add_subplot(2,2,plot_order[plot_num])  
		p.set_title(plot_titles[plot_num])		
		
		r=3 #radius of square sample area around track point
		#fluor_channel=1
		track_count=0
		for track in tracks.tracks:
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,fluor_channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)
					
				p.plot(tt,vv,color=track_colours[track_count],linestyle='--')

				smoothed=pandas.Series(vv,tt)
				smoothed=pandas.rolling_mean(smoothed,3)
				p.plot(tt,smoothed.values,color=track_colours[track_count])
				
				track_count+=1
			
		plot_num+=1

## total	
#T=a.shape[0]
#X=a.shape[3]
#Y=a.shape[2]

#vv=[]
#tt=[]
##s=100
#for t in range(T):
	#b=a[t,0,:,:]
	#thresh=1000
	#c=b[b<thresh]
	#v=numpy.sum(c)/len(c)
	##v=numpy.sum(a[t,0,0:s,0:s])/(s*s)
	#vv.append(v)
	#tt.append(t)
#totals=vv

##fig=pyplot.figure()
#p=fig.add_subplot(222)  
#p.plot(tt,vv)	
#p.set_title('Total')	


## corrected
##fig=pyplot.figure()
#p=fig.add_subplot(223)  
#p.set_title('Tracks minus Total')	
#for vv,tt in traces:
	#for i in range(len(vv)):
		#vv[i]-=totals[int(tt[i])-1] #mtrackj time starts at 1
	#p.plot(tt,vv)

#tracks.show_plots(block=False)
tracks.show_plots(block=True)
pass