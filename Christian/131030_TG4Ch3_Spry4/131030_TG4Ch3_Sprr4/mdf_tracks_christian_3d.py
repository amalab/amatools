import sys
#sys.path.insert(0,'') #adds cwd to path, to pickup modules local to amatools_dist directory

import os
import amatools
import matplotlib.colors 
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
matplotlib.interactive(True)
#from prettyplotlib import plt as pyplot
import numpy

import libtiff
from bs4 import BeautifulSoup

import pandas

#mdf_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/Clicked Data/C2-070311 tnga in 2i_1_20.tif.mdf'
#tiff_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/070311 tnga in 2i image converted/070311 tnga in 2i_1_20.tif'
#mdf_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/Clicked Data/C2-140111 tnga ln n2b27 chi_1_20.tif.mdf'
#tiff_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/140111 tnga ln n2b27 chi converted/140111 tnga ln n2b27 chi_1_20.tif'

#christian
mdf_filenames=['131030_TG4Ch3_Spry4-Epi_AA03_05.zvi.mdf','131030_TG4Ch3_Spry4-PrE_AA03_05.zvi.mdf']
tiff_filename='131030_TG4Ch3_Spry4-0002_AA03_05.ome.tiff'

#read direct
tiff_file=libtiff.TIFFfile(tiff_filename)
tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array

# t*c,y,x -> t,c,y,x

# get info
# get header as string
s=tiff_file.get_info()
# make list of tags
l=s.splitlines()

try:
	# get ome.tiff xml header
	xml=s[s.find('"""')+3:s.rfind('"""')]
	
except:
	print 'No XML header'
							
# parse XML
header=BeautifulSoup(xml,"lxml")

# get pixel tag attributes
pixel_attributes=header.find('pixels').attrs
print 'Pixel attributes:',pixel_attributes

# get all channel tags
ch=header.find_all('channel')

# print channel name attributes
print 'Channels:'
numch=0
for c in ch:
	print numch,c['name'] # in BeautifulSoup, attrs are dicts of bs tree objects
	numch+=1				

print 'tiff_array shape:',tiff_array.shape # [time*channels,height,width]

# reshape
sh=tiff_array.shape
tiff_array=tiff_array.reshape((-1,numch,sh[1],sh[2]))
sh=tiff_array.shape
print 'reshaped as:',sh
T=sh[0]
width=sh[3]
height=sh[2]
print 'T numch width height',T,numch,width,height

#plot images
fig=pyplot.figure()		

cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)],                                                      
                                                           'green': [(0.0,  0.0, 0.0),
                                                                     (1.0,  1.0, 1.0)],                                                        
                                                           'blue':  [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)]})

cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
                                                                     (1.0,  1.0, 1.0)],                                                      
                                                           'green': [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)],                                                        
                                                           'blue':  [(0.0,  0.0, 0.0),
                                                                     (1.0,  0.0, 0.0)]})

cmaps=['binary_r',cmap_g,cmap_r]
t=T/2
cc=0
pp=[] #plots
for c in ch:
	p=fig.add_subplot(1,numch,cc+1) 
	p.set_title(c['name'])	

	im=tiff_array[t,cc,:,:]
	imgplot=p.imshow(im)
	imgplot.set_cmap(cmaps[cc])
	pp.append(p)
	cc+=1
	
#pyplot.show()

##image
#tiff=amatools.AMATiff()
#tiff.open(tiff_filename)
##tiff.reshape_fiji()
#p=tiff.plot(0)	

#fig.suptitle(os.path.split(mdf_filename)[1])
#tracks
#plot_titles=['Epi - Spry4','Epi - GATA4','PrE - Spry4','PrE - GATA4']
#plot_num=0
#plot_order=[1,3,2,4]
track_colours=['r','g','b','c','m','y','k']
channels={1:'FGF',2:'GATA'}

# calc dataframes

data_fgf=pandas.DataFrame()
data_gata=pandas.DataFrame()
track_count=0

for mdf_filename in mdf_filenames:
	if 'PrE' in mdf_filename:
		fate='PrE'
	elif 'Epi' in mdf_filename:
		fate='Epi'
	else:
		raise Exception('Filename fate type error')
	
		
	tracks=amatools.AMACellTracks()
	
	tracks.read_mtrackj_mdf(mdf_filename)
	tracks.plot_tracks(pp[0])
			
	
	# measure fluorescence along tracks
	for channel in channels:		
		r=3 #radius of square sample area around track point
		for track in tracks.tracks:
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)
					
				#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				smoothed=pandas.Series(vv,tt)
				smoothed=pandas.rolling_mean(smoothed,3)
				label=str(track_count)+'_'+fate+'_'+channels[channel]
				d=pandas.DataFrame({label:smoothed})
				
				if channels[channel] is 'FGF':
					data_fgf=data_fgf.join(d,how='outer')
				elif channels[channel] is 'GATA':
					data_gata=data_gata.join(d,how='outer')
				
				#p.plot([track_count]*len(tt),tt,smoothed.values,color=track_colours[track_count%len(track_colours)])
								
				track_count+=1
			
		#plot_num+=1
		
# save dataframes
data_fgf.to_pickle('data_fgf.df')
data_gata.to_pickle('data_gata.df')
		
#fgf_tracks=data.select(lambda s: 'FGF' in s,axis=1)

# 3D plot of fgf signalling
fig=pyplot.figure()
p=fig.add_subplot(1,1,1,projection='3d')
p.set_title('cyan - Epi   magenta - PrE')
p.set_xlabel('gata_at_30')
p.set_ylabel('time')
p.set_zlabel('FGF intensity')

count=0
#for c in fgf_tracks.columns:
for c in data_fgf.columns:
	#fgf_series=fgf_tracks[c]
	fgf_series=data_fgf[c]
	
	if 'Epi' in c:
		col='c'
	else:
		col='m'
		
	gata_at_30=data_gata.iloc[30,count]
		
	p.plot([gata_at_30]*len(fgf_series.index),fgf_series.index,fgf_series.values,color=col)	#x,y,z,col
	count+=1

pyplot.show(block=False)
pass
