import lineage
import glob
import os

###orig###

mdf=glob.glob('*.mdf')
pickles=glob.glob('*.pickle')

mdfs=[]
for m in mdf:
	f=os.path.split(m)[1]
	n,e=os.path.splitext(f)
	if n+'.pickle' not in pickles:
		mdfs.append(f)
	
#mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]

print 'processing',len(mdfs),'files...'

for filename in mdfs:
	try:
		filename=filename[0:filename.find('.')]
		print filename
	
		# make lineage
		#lin=lineage.Lineage(filename,channels=['brightfield','cerulian','nanog','GATA'],channels_to_measure=[2,3],flatfield_ometif_files=flatfield_ometif_files,background_correct=False)
		lin=lineage.Lineage(filename,channels=['Ch1-T1','Nanog','T PMT-T2','GATA6','Ch2-T4'],channels_to_measure=[1,3],flatfield_ometif_files=None,background_correct=False)
							
		#pickle
		lin.save(withoutTiff=True)
	except:
		print 'FAILED',filename

####process###
#NANOG=2
#GATA=3
#nanogs={}
#gatas={}
#for cond in ['0uM','15nM','31nM','62nM']:
	#nanogs[cond]=[]
	#gatas[cond]=[]
	#for pos in [1,2,3,4,5,6,7,8]:
		#print cond,pos
		#fn='140729_immuno_%s_%d.pickle'%(cond,pos)

		#lin=lineage.load(fn)
		
		#for track in lin.tracks:	
			#f=track.f
			#nanogs[cond].append(f[NANOG][0])
			#gatas[cond].append(f[GATA][0])
			
		
pass