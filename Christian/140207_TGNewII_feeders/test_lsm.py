#from libtiff import TIFF

## to open a tiff file for reading:
#tif = TIFF.open('TG6NewII_Nr2_8h_+dox_#1.lsm', mode='r')
## to read an image in the currect TIFF directory and return it as numpy array:
#image = tif.read_image()
## to read all images in a TIFF file:
#for image in tif.iter_images(): # do stuff with image
	#print image.shape
	
from pylsm import lsmreader
imageFile = lsmreader.Lsmimage('TG6NewII_Nr2_8h_+dox_#1.lsm')
imageFile.open()
for ch in [0,1,2,3,4]:
	imageData=imageFile.get_image(stack=0,channel=ch)
	print ch,imageData
