# 150223

# channels
# 0 brightfield - grey
# 1 cerulian - cyan
# 2 GATA4 - red
# 3 GATA6 - green

import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage
import pickle
import glob
import os

#movies
tiff_files=glob.glob('150223_RGB3_D6_TL_2/*.tif')

#tracks
mdf_files=['150223_RGB3_D6_mdfTracks/150223_RGB3_0uM_A%d.ome.tif.mdf'%i for i in range(1,9)]
mdf_files+=['150223_RGB3_D6_mdfTracks/150223_RGB3_0uM_B%d.ome.tif.mdf'%i for i in range(1,9)]

#backgrounds
bgr_mdf_files=['150223_RGB3_D6_mdfTracks/150223_RGB3_0uM_A%d_bgr.ome.tif.mdf'%i for i in range(1,9)]
bgr_mdf_files+=['150223_RGB3_D6_mdfTracks/150223_RGB3_0uM_B%d_bgr.ome.tif.mdf'%i for i in range(1,9)]

#flatfield
flatfield_ometif_files=['150318_RGB3_D6_Flatfield_2/150318_RGB3_D6_Flatfield_2_MMStack_FF%d.ome.tif'%i for i in [1,2,3,4]]

#for saving
if not os.path.exists('pickles'):
	os.makedirs('pickles')

#create Lineage object that does all the work
lin=lineage.Lineage()
lin.channels=['brightfield','cerulian','GATA6','GATA4']
lin.num_channels=len(lin.channels)
lin.channels_to_measure=[2,3]
lin.flatfield_correct_prepare(flatfield_ometif_files)
lin.sigma=32 #for gaussian weighted measurements

for tiff_file,mdf_file,bgr_mdf_file in zip(tiff_files,mdf_files,bgr_mdf_files)[0:1]:
	print tiff_file
	
	#read movie
	lin.read_ometif(tiff_file)

	#create lineage
	lin.read_mtrackj_mdf(mdf_file,lin.tracks,verbose=True)
	lin.process_clusters() #split cluster 1 into clusters 0(non-terminal) and 1(undecided)
	lin.link_tracks()
	
	#measure
	#without flat field correction
	#lin.measure_fluorescence()	
	#square measurement area
	#lin.measure_fluorescence_flatfield_corrected()
	#gaussian weighted
	lin.measure_fluorescence_flatfield_corrected_gaussian()

	#background correct
	lin.background_correct(bgr_mdf_file)
	
	#save to pickle
	lin.save('pickles/'+os.path.basename(tiff_file))
	
