# coding=utf-8
# for unicode comments

'''
cell track lineage data structures

jpm
sep 2014
'''

import numpy
import scipy.ndimage
import matplotlib
import matplotlib.pyplot as pyplot
import tifffile
import libtiff
import math
import pickle
import copy

class Track:
	'''Track of cells over time'''
	def __init__(self):		
		self.id=None # unique number from 0..len(tracks)
		self.number=None # mtrackj track number
		self.cluster=None #mtrackj cluster

		self.x=None # x pos in pixels
		self.y=None # y pos in pixels 
		self.t=None # time in frames	
		self.f=[] #fluorescence measurements

		self.parent=None
		self.children=[]
		self.sibling=None
		self.linked=False

		self.yplot=0 #height for plotting lineage
		self.t_max=0 #max to use because of diffferent bgr track lengths

	@property
	def parent_id(self):
		if self.parent:
			return self.parent.id
		else:
			return -1

class Lineage:
	def __init__(self):
		self.tiff_array=None
		self.tracks=[]
		self.background_tracks=[]
		self.mean_backgrounds=None
		self.yplot_max=0 #for plotting
		self.channels=[] #by name, in order
		self.num_channels=0
		self.channels_to_measure=[]

	def read_mtrackj_mdf(self,mdf_file,tracks,verbose=False):
		'''Read track data, including cluster info, from MTrackJ mdf file into tracks'''

		mdf_file=open(mdf_file,'rU')
		mdf_lines=mdf_file.readlines()
		
		l=0
		while True:
			line=mdf_lines[l]
			if 'Cluster' in line:
				break
			l+=1

		while 'Cluster' in line:
			line_elems=line.split(' ')
			cluster=int(line_elems[1])
			if verbose:
				print 'Cluster',cluster

			l+=1		
			line=mdf_lines[l]	

			while 'Track' in line and 'End' not in line:
				track=Track()
				line_elems=line.split(' ')
				track.number=int(line_elems[1])				
				track.id=len(tracks)
				track.cluster=cluster
				tracks.append(track)
				if verbose:
					print 'Track',track.id,track.number

				l+=1
				line=mdf_lines[l]
				x=[]
				y=[]
				t=[]		
				while 'Point' in line:
					line_elems=line.split(' ')
					x.append(float(line_elems[2]))
					y.append(float(line_elems[3]))
					t.append(float(line_elems[5]))

					l+=1
					line=mdf_lines[l]	

				track.x=numpy.array(x)
				track.y=numpy.array(y)
				track.t=numpy.array(t)


	def process_clusters(self):			
		# process clusters so that 1->0 if non-terminal
		t_maxes=[]
		for t in self.tracks:
			t_maxes.append(numpy.max(t.t))

		t_max=max(t_maxes)

		for t in self.tracks:
			if t.cluster==1 and t.t[-1]<t_max:
				t.cluster=0


	def link_tracks(self):
		'''link all tracks into lineage of parent child by proximity of end points in space and time'''
		tracks_to_link=list(self.tracks) #copy

		thresh_d=50.0 #(x,y) norm distance threshold
		thresh_t=2 # time distance

		while len(tracks_to_link)>0:
			t1=tracks_to_link.pop(0)
			potential_links=[]
			for t2 in tracks_to_link:			
				dt12=abs(t2.t[0]-t1.t[-1]) #temporal distance between end of t1 and begin of t2
				dt21=abs(t1.t[0]-t2.t[-1]) #temporal distance between end of t2 and begin of t1

				t1_begin=numpy.array([t1.x[0],t1.y[0]])
				t1_end=numpy.array([t1.x[-1],t1.y[-1]])
				t2_begin=numpy.array([t2.x[0],t2.y[0]])
				t2_end=numpy.array([t2.x[-1],t2.y[-1]])				
				d12=numpy.linalg.norm(t2_begin-t1_end) #spatial distance between end of t1 and begin of t2
				d21=numpy.linalg.norm(t1_begin-t2_end) #spatial distance between end of t2 and begin of t1

				if dt12<thresh_t:
					potential_links.append((t1,t2,d12))
				elif dt21<thresh_t:
					potential_links.append((t2,t1,d21))


			count=0
			while(len(potential_links)>0 and count<2):
				closest=min(potential_links,key = lambda l: l[2])

				if closest[2]<thresh_d:
					t1=closest[0]
					t2=closest[1]
					print t1.number,'-->',t2.number,'   ','d:',closest[2]
					for c in t1.children:
						c.sibling=t2
						t2.sibling=c
					t1.children.append(t2)
					t2.parent=t1		

				potential_links.remove(closest)
				count+=1

				#elif d21<thresh_d and dt21<thresh_t:
					#print t2.number,'-->',t1.number,'   ','d21:',dt21,d21
					#for c in t2.children:
						#c.sibling=t1
						#t1.sibling=c					
					#t2.children.append(t1)
					#t1.parent=t2

	def read_ometif(self,ometif_filename):
		#read tiff
		self.ometif_filename=ometif_filename
		
		##using libtiff
		#tiff_file=libtiff.TIFFfile(ometif_filename)
		#self.tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
		#sh=self.tiff_array.shape	
		##print sh
		
		#using tifffile
		tf=tifffile.TIFFfile(ometif_filename)	
		self.tiff_array=tf.asarray()	
		sh=self.tiff_array.shape

		# reshape
		self.tiff_array=self.tiff_array.reshape((-1,self.num_channels,sh[1],sh[2]))
		sh=self.tiff_array.shape
		#print 'reshaped as:',sh
		T=sh[0]
		width=sh[3]
		height=sh[2]
		print 'T numch width height = ',T,self.num_channels,width,height	

	def measure_fluorescence(self):
		r=8 #radius of square sample area around track point

		for track in self.tracks:
			track.f=[[]]*self.num_channels
			for channel in range(self.num_channels):				
				track.f[channel]=numpy.array([numpy.sum(self.tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) for (t,x,y) in zip(track.t,track.x,track.y)]) #mtrackj time starts at 1

	def measure_fluorescence_flatfield_corrected(self):
		#measures self.tracks
		r=8 #radius of square sample area around track point

		for track in self.tracks:
			track.f=[[]]*self.num_channels
			for channel in self.channels_to_measure:		
				uncorrected=self.tiff_array[:,channel,:,:]			
				corrected=uncorrected/self.mean_backgrounds[channel]				
				track.f[channel]=numpy.array([numpy.sum(corrected[t-1,y-r:y+r,x-r:x+r])/(4*r*r) for (t,x,y) in zip(track.t,track.x,track.y)]) #mtrackj time starts at 1
			print '.',

	def measure_fluorescence_flatfield_corrected(self,tracks):
		#measure tracks passed as argument (for backgrounds)
		r=8 #radius of square sample area around track point

		for track in tracks:
			track.f=[[]]*self.num_channels
			for channel in self.channels_to_measure:		
				uncorrected=self.tiff_array[:,channel,:,:]			
				corrected=uncorrected/self.mean_backgrounds[channel]				
				track.f[channel]=numpy.array([numpy.sum(corrected[t-1,y-r:y+r,x-r:x+r])/(4*r*r) for (t,x,y) in zip(track.t,track.x,track.y)]) #mtrackj time starts at 1
			print '.',
			
	def measure_fluorescence_flatfield_corrected_gaussian(self):
		# gaussian weighted measurement area
		r=8
		sigma=32
		seed=numpy.zeros((2*r+1,2*r+1))
		seed[r,r]=1.0
		weight=scipy.ndimage.filters.gaussian_filter(seed,sigma) #2d gaussian kernal
		weight/=numpy.sum(weight) #sum of all weights=1.0
		
		for track in self.tracks:
			track.f=[[]]*self.num_channels
			for channel in self.channels_to_measure:		
				uncorrected=self.tiff_array[:,channel,:,:]			
				corrected=uncorrected/self.mean_backgrounds[channel]
				try:
					track.f[channel]=numpy.array([numpy.sum(corrected[t-1,y-r:y+r+1,x-r:x+r+1]*weight) for (t,x,y) in zip(track.t,track.x,track.y)]) #mtrackj time starts at 1
				except:
					print 'Measure outside image border error.'
			print '.',
			

	def save(self,filename,withoutTiff=True):		
		#save to pickle

		tiff_array=None
		if withoutTiff:
			tiff_array=copy.copy(self.tiff_array)
			self.tiff_array=None

		pickle_file=open(filename+'.lin.pickle','w')
		pickle.dump(self,pickle_file)

		if withoutTiff:
			self.tiff_array=tiff_array


	def print_lineage(self):
		'''print lineage for all trees'''
		def lineage(track,indent=''):
			for c in track.children:
				print indent+'--> '+str(c.number)+'('+str(c.cluster)+')'
				lineage(c,indent+'\t')

		for t in self.tracks:
			if not t.parent:
				print str(t.number)+'('+str(t.cluster)+')'
				lineage(t,'\t')
				print '\n'

	def plot_lineage(self,channel_to_view=0,colour='grey',save_svg=True,block=True):
		#prelims
		roots=self.get_roots()
		r=roots[0]

		to_explore=list(roots)

		dummy=Track()
		in_order=[]
		for r in roots:
			in_order+=[r,dummy,dummy,dummy] #for inter-tree spacing

		while len(to_explore)>0:
			t=to_explore.pop(0)
			pos=in_order.index(t)
			count=0
			for c in t.children:
				in_order.insert(pos+count,c)
				to_explore.append(c)
				count+=2

		count=0
		for t in in_order:
			t.yplot=count
			count+=1

		self.yplot_max=count-1

		#start plotting
		fig=pyplot.figure(figsize=(12,self.yplot_max/6))
		#fig.suptitle(self.filename+'  '+self.channels[channel_to_view])
		fig.suptitle(self.channels[channel_to_view])
		colours=['#000000','#AAAAAA','#00FF00','#FF00FF'] #non-terminal, undecided, nanog, GATA

		#track number
		for t in self.tracks:
			if t.number:
				pyplot.text(t.t[0]-7, t.yplot-1, t.number)

		#track ends
		x=[t.t[-1] for t in self.tracks]
		y=[t.yplot for t in self.tracks]
		c=[colours[t.cluster] for t in self.tracks]			
		pyplot.scatter(x,y,c=c,s=100,zorder=10)	

		##horiz tracks
		#X=[[],[]]
		#Y=[[],[]]
		#for t in self.tracks:
			#if t.parent:
				#X[0].append(t.t[-1])
				#Y[0].append(t.yplot)
				#X[1].append(t.parent.t[-1])
				#Y[1].append(t.yplot)	
			#else:# root tracks
				#X[0].append(t.t[0])
				#Y[0].append(t.yplot)
				#X[1].append(t.t[-1])
				#Y[1].append(t.yplot)			
		#pyplot.plot(X, Y, 'k-')	

		##vert joins
		#X=[[],[]]
		#Y=[[],[]]
		#for t in self.tracks:
			#if len(t.children)==2:
				#X[0].append(t.t[-1])
				#Y[0].append(t.children[0].yplot)
				#X[1].append(t.t[-1])
				#Y[1].append(t.children[1].yplot)			
		#pyplot.plot(X,Y,'k-')	

		#fluorescence levels along tracks
		#get signal range
		fmin=[numpy.min(t.f[channel_to_view]) for t in self.tracks]
		fmax=[numpy.max(t.f[channel_to_view]) for t in self.tracks]
		fmin=numpy.min(fmin)
		fmax=numpy.max(fmax)
		#fmin=0.0
		#fmax=5.0
		print 'fmin,fmax:',fmin,fmax

		lines=[]
		cols=[]
		for track in self.tracks:
			for i in range(len(track.t)-1):
				lines.append([(track.t[i],track.yplot),(track.t[i+1],track.yplot)])
				c=1.0-float(track.f[channel_to_view][i]-fmin)/(fmax-fmin)
				if c<0:
					c=0
				if c>1.0:
					c=1.0
				if colour=='red':
					cols.append((1,c,c,1)) #red - GATA
				elif colour=='green':
					cols.append((c,1,c,1)) #green - nanog
				else:
					cols.append((c,c,c,1)) #gray					

		lc=matplotlib.collections.LineCollection(lines,colors=cols,linewidths=10)
		fig.axes[0].add_collection(lc)		

		if save_svg:
			fig.savefig(self.filename+'.'+self.channels[channel_to_view]+'.lineage_plot.svg',dpi=100)		

		try:	
			pyplot.show(block=block)			
		except:
			pyplot.show() #for ipython notebook with inline svg backend	

	def plot_fluorescence(self,channels=[],colours=[],save_svg=None,block=True):
		fig=pyplot.figure()
		p=fig.add_subplot(1,1,1)
		p.set_title(zip(colours,[self.channels[i] for i in channels]))
		p.set_xlabel('time')
		p.set_ylabel('fluorescence')
				
		#calc ranges	
		f_mins={}
		f_maxs={}
		for ch in channels:
			fmins=[]
			fmaxs=[]
			for t in self.tracks:
				f=t.f[ch][:self.t_max] #raw fluorescence measurements
				f_log=numpy.log(f+numpy.min(f)+1) #to avoid nans
				fmins.append(numpy.min(f_log))
				fmaxs.append(numpy.max(f_log))
			f_mins[ch]=min(fmins)
			f_maxs[ch]=max(fmaxs)
				
		
		#do plots		
		for col,ch in zip(colours,channels):
			for t in self.tracks:
				f=t.f[ch][:self.t_max] #raw fluorescence measurements
				f_log=numpy.log(f+numpy.min(f)+1)
				#f_log=numpy.nan_to_num(f_log)
				f_norm_log=(f_log-f_mins[ch])/f_maxs[ch]
				
				p.plot(t.t[:self.t_max],f_norm_log,color=col)
		
		if save_svg:
			fig.savefig(self.filename+save_svg+'.fluorescence.svg',dpi=100)		

		try:	
			pyplot.show(block=block)			
		except:
			pyplot.show() #for ipython notebook with inline svg backend	


	def flatfield_correct_prepare(self,flatfield_ometif_files):
		# load background movies

		#num_pos=4 #number of background movies
		#num_ch=4 #number of channels

		i=0
		for fn in flatfield_ometif_files:
			#fn='140729_Flatfield_2/140729_Flatfield_2_MMStack_Pos%d.ome.tif'%i
			tf=tifffile.TIFFfile(fn)	
			tiff_array=tf.asarray()	
			# reshape
			sh=tiff_array.shape
			#print 'shape:', sh
			tiff_array=tiff_array.reshape((-1,self.num_channels,sh[1],sh[2]))
			sh=tiff_array.shape
			#print 'reshaped as:','(T,numch,width,height)',sh

			if i==0:
				background_movies=tiff_array
			else:
				background_movies=numpy.concatenate((background_movies,tiff_array),0)

			i+=1

		print background_movies.shape

		#calc mean of backgrounds over all movies for all times
		self.mean_backgrounds=background_movies.astype('float').mean(0)
		print self.mean_backgrounds.shape	

		#show result		
		##colour maps for fluorescence channels		
		#cmap_c=matplotlib.colors.LinearSegmentedColormap('cmap_c',
									##x0,col0,alpha0  x1,col1,alpha1
							#{'red':   [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],                                                      
							#'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                        
							#'blue':  [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)]})

		#cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_r',
							#{'red':   [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],
								#'green': [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],
								#'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]})

		#cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',
							#{'red':   [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],                                                      
							#'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                        
							#'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]})

		#cmap_m=matplotlib.colors.LinearSegmentedColormap('cmap_m',
							#{'red':   [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                      
							#'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                        
							#'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]})

		#cmaps=['gray',cmap_c,cmap_r,cmap_g]	

		#fig=matplotlib.pyplot.figure(figsize=(16,4))		
		#for c in range(self.num_channels):
			#p=fig.add_subplot(1,self.num_channels,c+1) 
			#p.set_title('Channel '+str(c))	

			#p.imshow(self.mean_backgrounds[c],cmap=cmaps[c])		


	def background_correct(self,background_mdf_filename):
		self.read_mtrackj_mdf(background_mdf_filename,self.background_tracks)
		#self.measure_fluorescence(self.background_tracks)
		self.measure_fluorescence_flatfield_corrected(self.background_tracks)

		for ch in self.channels_to_measure:
			lens=[len(t.f[ch]) for t in self.background_tracks]
			self.t_max=min(lens) #make sure all tracks of same length
			background_aves=numpy.vstack([t.f[ch][:self.t_max] for t in self.background_tracks]) #average over background clicked tracks for each channel
			background_ave=numpy.mean(background_aves,0)
			for t in self.tracks:
				t.f[ch][:self.t_max]-=background_ave[t.t[:self.t_max].astype(int)-1] #mtrackj time starts at 1, t are float consecutive frame times

	def get_descendents(self,track):
		'''return list of all descendent tracks for given track '''
		def lineage(track,tracks):
			for c in track.children:
				tracks.append(c)
				lineage(c,tracks)
			return tracks

		return [track]+lineage(track,[track])

	def get_roots(self):
		'''get all root tracks'''
		roots=[]
		for t in self.tracks:
			if not t.parent:
				roots.append(t)
		return roots

	def get_leafs(self,tracks):
		'''get leaf tracks of tracks'''
		leafs=[]
		for t in tracks:
			if len(t.children)==0:
				leafs.append(t)
		return leafs

	def get_leafs_in_order(self):
		'''get all leaf nodes in order by traversing from all roots'''
		leafs=[]
		roots=self.get_roots()
		for r in roots:
			desc=self.get_descendents(r)
			l=self.get_leafs(desc)
			leafs+=l

		return leafs

	def get_terminals_in_order(self):
		leafs=self.get_leafs_in_order()
		for l in leafs:
			if l.cluster==0:
				leafs.remove(l)
		return leafs

	def get_terminal_pairs(self):
		pairs=[]
		terminals=self.get_terminals_in_order()
		while len(terminals)>0:
			t=terminals.pop(0)
			if t.sibling:
				if t.sibling in terminals:
					pairs.append((t,t.sibling))
					terminals.remove(t.sibling)

		return pairs				

def load(filename):
	#create lineage instance from pickle
	if '.pickle' not in filename:
		filename+='.pickle'

	pickle_file=open(filename,'r')
	lin=pickle.load(pickle_file)
	return lin