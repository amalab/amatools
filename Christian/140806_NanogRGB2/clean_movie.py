import libtiff
import numpy
import glob
import os

def read_ometif(filename):
	#read tiff
	tiff_file=libtiff.TIFFfile(filename)
	tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
	sh=tiff_array.shape	
	print sh

	# reshape
	info=tiff_file.get_info()
	idx=info.find('channels=')
	num_channels=int(info[idx+len('channels=')])
	tiff_array=tiff_array.reshape((-1,num_channels,sh[1],sh[2]))
	sh=tiff_array.shape
	print 'reshaped as:',sh,'(T,ch,h,w)'
	#T=sh[0]
	#width=sh[3]
	#height=sh[2]
	#print 'T numch width height = ',T,num_channels,width,height
	
	return tiff_array


def flatfield_correct_prepare(flatfield_ometif_files):
	# load flatfield movies

	for i,fn in enumerate(flatfield_ometif_files):
		print fn
		
		tiff_array=read_ometif(fn)

		if i==0:
			flatfield_movies=tiff_array
		else:
			flatfield_movies=numpy.concatenate((flatfield_movies,tiff_array),0)

		i+=1

	print flatfield_movies.shape

	#calc mean of flatfields over all movies for all times
	mean_flatfields=flatfield_movies.astype('float').mean(0)
	print mean_flatfields.shape	
	
	return mean_flatfields

class Track:
	'''Track of cells over time'''
	def __init__(self):		
		self.id=None # unique number from 0..len(tracks)
		self.number=None # mtrackj track number
		self.cluster=None #mtracksj cluster

		self.x=None # x pos in pixels
		self.y=None # y pos in pixels 
		self.t=None # time in frames	
		self.f=[] #fluorescence measurements

		self.parent=None
		self.children=[]
		self.sibling=None
		self.linked=False

		self.yplot=0 #height for plotting lineage

	@property
	def parent_id(self):
		if self.parent:
			return self.parent.id
		else:
			return -1
		
def read_mtrackj_mdf(mdf_file,verbose=False):
	'''Read track data, including cluster info, from MTrackJ mdf file & return as list of Tracks()'''

	mdf_file=open(mdf_file,'rU')
	mdf_lines=mdf_file.readlines()
	tracks=[]

	l=0
	while True:
		line=mdf_lines[l]
		if 'Cluster' in line:
			break
		l+=1

	while 'Cluster' in line:
		line_elems=line.split(' ')
		cluster=int(line_elems[1])
		if verbose:
			print 'Cluster',cluster

		l+=1		
		line=mdf_lines[l]	

		while 'Track' in line and 'End' not in line:
			track=Track()
			line_elems=line.split(' ')
			track.number=int(line_elems[1])				
			track.id=len(tracks)
			track.cluster=cluster
			tracks.append(track)
			if verbose:
				print 'Track',track.id,track.number

			l+=1
			line=mdf_lines[l]
			x=[]
			y=[]
			t=[]		
			while 'Point' in line:
				line_elems=line.split(' ')
				x.append(float(line_elems[2]))
				y.append(float(line_elems[3]))
				t.append(float(line_elems[5]))

				l+=1
				line=mdf_lines[l]	

			track.x=numpy.array(x)
			track.y=numpy.array(y)
			track.t=numpy.array(t)

	return tracks	

def clean(tiff_array,bgr_tracks,mean_flatfields):
	r=8 #radius of square sample area around track point

	sh=tiff_array.shape
	T=sh[0]
	channels=sh[1]
	
	tiff_array_clean=numpy.zeros_like(tiff_array,dtype=numpy.float)
	
	for t in range(T):
		for channel in range(channels):	
			ave_flatfield=numpy.mean(mean_flatfields[channel])	
			
			uncorrected=tiff_array[t,channel,:,:]			
			corrected=uncorrected/mean_flatfields[channel]*ave_flatfield
			
			fs=[numpy.sum(corrected[track.y[t]-r:track.y[t]+r,track.x[t]-r:track.x[t]+r])/(4*r*r) for track in bgr_tracks] 
			f=numpy.mean(fs)
			
			corrected-=f
			tiff_array_clean[t,channel,:,:]=corrected
	
	tiff_array_clean-=tiff_array_clean.min() #move everything up so min-s zero not -ve
	
	return tiff_array_clean.astype(numpy.uint16,copy=False)

### MAIN ###

flatfields=glob.glob('flatfield/*.tif')[0:1]
mean_flatfields=flatfield_correct_prepare(flatfields)

#fns=['140806_TL_1_MMStack_NanogRGB2_0uM_dox_1']
fns=glob.glob('timelapse/*.tif')
fns=[os.path.split(f)[1][0:os.path.split(f)[1].find('.')] for f in fns]

for fn in fns:
	print fn
	
	tiff_fn='timelapse/%s.ome.tif'%fn
	tiff_array=read_ometif(tiff_fn)
	
	mdf_file='tracks/%s_bgr.mdf'%fn
	bgr_tracks=read_mtrackj_mdf(mdf_file)
	
	sh=tiff_array.shape
	tiff_array_clean=clean(tiff_array,bgr_tracks,mean_flatfields)
	tiff_array_clean=tiff_array_clean.reshape((sh[0]*sh[1],sh[2],sh[3]))
	print tiff_array_clean.shape
	
	tiff=libtiff.TIFFimage(tiff_array_clean,description='')
	tiff.write_file('%s.cleaned.ome.tif'%fn,compression='none')
	del tiff # flushes data to disk
