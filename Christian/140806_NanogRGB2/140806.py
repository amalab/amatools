# 140806

# original mtrackj clusters:
# 1 - undecided or non-terminal
# 2 - nanog immuno
# 3 - GATA immuno

# processed to become
# 0 - non-terminal
# 1 - undecided

# channels
# 0 brightfield - grey
# 1 cerulian - cyan
# 2 nanog - green
# 3 GATA4 - red

import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage
import pickle
import glob
import os

flatfield_ometif_files=['flatfield/140808_flatfield_1_MMStack_NanogRGB2_flatfield%d.ome.tif'%i for i in [1,2,3,4]]

#mdfs=['140806_TL_1_MMStack_NanogRGB2_0uM_dox_1.mdf']
mdfs=glob.glob('tracks/*.mdf')
mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]

print 'processing',len(mdfs),'files...'

for filename in mdfs:
	try:
		filename=filename[0:filename.find('.')]
		print filename
	
		# make lineage
		lin=lineage.Lineage(filename,channels=['brightfield','cerulian','nanog','GATA4'],channels_to_measure=[2,3],flatfield_ometif_files=flatfield_ometif_files)
			
		# histogram terminal pairs
		pairs=lin.get_terminal_pairs()
		print len(pairs),'terminal pairs'
		
		#print
		lin.print_lineage()
		
		# plot
		lin.plot_lineage(channel_to_view=2,colour='green',save_svg=True,block=False)
		lin.plot_lineage(channel_to_view=3,colour='red',save_svg=True,block=False)
		
		#pickle
		lin.save(withoutTiff=True)
	except:
		print 'FAILED',filename

pass

