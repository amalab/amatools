# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import numpy as np
import scipy as sp
from scipy.optimize import curve_fit

# <codecell>

def f(t,C0,lbd):
    """Exponential function"""
    return C0*np.exp(-lbd*t)

# <codecell>

# Parameters
nsamples=10 # experimental samples
npoints=21   # time points

# <codecell>

# real half life
hl=1.5
# associated decay rate
lbd=np.log(2)/hl

# <codecell>

# Fake (noisy) experimental data
t=np.linspace(0,15,npoints).reshape(npoints,1).repeat(nsamples,axis=1)
t.shape

# add additive noise
n0=0.2
noise=n0*np.random.randn(npoints,nsamples)
x=f(t,1,lbd)+noise
ax=plt.plot(t,x,alpha=0.25)
plt.xlabel('time')
plt.ylabel('x')
plt.xlim([0,10])
plt.ylim([-0.25,1.5])
t[:,0].shape
xm=x.mean(1)
xsem=x.std(1)/np.sqrt(nsamples)
ax=plt.errorbar(t[:,0],xm,yerr=xsem,color='k')

# <codecell>

popt,pcov=curve_fit(f, t[:,0], xm, sigma=xsem)
print("Estimated parameter values:\n\tC0=%g (variance=%g)\n\tlambda=%g (variance=%g)"%(popt[0],pcov[0,0],popt[1],pcov[1,1]))
hl=np.log(2)/popt[1]
print("Estimated half-life: %g" %hl)

# <codecell>


