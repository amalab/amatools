import amatools
import matplotlib.pyplot as pyplot
import numpy

tracks=amatools.AMACellTracks()

#mdf_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/Clicked Data/C2-070311 tnga in 2i_1_20.tif.mdf'
#tiff_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/070311 tnga in 2i image converted/070311 tnga in 2i_1_20.tif'

mdf_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/Clicked Data/C2-140111 tnga ln n2b27 chi_1_20.tif.mdf'
tiff_filename='/Users/jpm/DATA/Ana/Patrick & Abigail/140111 tnga ln n2b27 chi converted/140111 tnga ln n2b27 chi_1_20.tif'


#image
tiff=amatools.AMATiff()
tiff.open(tiff_filename)
tiff.reshape_fiji()
p=tiff.plot(100)

#tracks
tracks.read_mtrackj_mdf(mdf_filename)
tracks.plot_tracks(p)

# analysis
a=tiff.tiff_array #[time,ch,y,x]

fig=pyplot.figure()
pyplot.hist(a[0,0,:,:].flatten(),256)

# tracks
traces=[]
r=5
for track in tracks.tracks:
	if track.duration>0.0:
		vv=[]
		tt=[]
		N=len(track.ti)
		for i in range(0,N):			
			x=track.x[i]
			y=track.y[i]
			t=track.ti[i]
			v=numpy.sum(a[t-1,0,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
			vv.append(v)
			tt.append(t)
			
	traces.append((vv,tt))
	print len(traces)

fig=pyplot.figure()
p=fig.add_subplot(221)  
p.set_title('Tracks')	
for vv,tt in traces:
	p.plot(tt,vv)

# total	
T=a.shape[0]
X=a.shape[3]
Y=a.shape[2]

vv=[]
tt=[]
#s=100
for t in range(T):
	b=a[t,0,:,:]
	thresh=1000
	c=b[b<thresh]
	v=numpy.sum(c)/len(c)
	#v=numpy.sum(a[t,0,0:s,0:s])/(s*s)
	vv.append(v)
	tt.append(t)
totals=vv

#fig=pyplot.figure()
p=fig.add_subplot(222)  
p.plot(tt,vv)	
p.set_title('Total')	


# corrected
#fig=pyplot.figure()
p=fig.add_subplot(223)  
p.set_title('Tracks minus Total')	
for vv,tt in traces:
	for i in range(len(vv)):
		vv[i]-=totals[int(tt[i])-1] #mtrackj time starts at 1
	p.plot(tt,vv)

#tracks.show_plots(block=False)
tracks.show_plots(block=True)
pass