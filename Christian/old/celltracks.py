# coding=utf-8
# for unicode comments

'''
simple cell track data structures

jpm
 jul 2014
'''

import numpy

class Track:
	'''Track of cells over time'''
	def __init__(self):		
		self.id=None # unique number from 0..len(tracks)
		self.number=None # mtrackjs track number
		self.cluster=None
		
		self.x=None # x pos in pixels
		self.y=None # y pos in pixels 
		self.t=None # time in frames	
		
		self.parent=None
		self.children=[]
		self.linked=False
		
	@property
	def parent_id(self):
		if self.parent:
			return self.parent.id
		else:
			return -1

class Tracks:
	'''Collection of tracks'''
	def  __init__(self):
		self.tracks=[]
			
	def read_mtrackj_mdf(self,mdf_file,verbose=False):
		'''Read track data, including cluster info, from MTrackJ mdf file'''
		
		mdf_file=open(mdf_file,'rU')
		mdf_lines=mdf_file.readlines()
		
		l=0
		while True:
			line=mdf_lines[l]
			if 'Cluster' in line:
				break
			l+=1
			
		while 'Cluster' in line:
			line_elems=line.split(' ')
			cluster=int(line_elems[1])
			if verbose:
				print 'Cluster',cluster

			l+=1		
			line=mdf_lines[l]	
			
			while 'Track' in line and 'End' not in line:
				track=Track()
				line_elems=line.split(' ')
				track.number=int(line_elems[1])				
				track.id=len(self.tracks)
				track.cluster=cluster
				self.tracks.append(track)
				if verbose:
					print 'Track',track.id,track.number
				
				l+=1
				line=mdf_lines[l]
				x=[]
				y=[]
				t=[]		
				while 'Point' in line:
					line_elems=line.split(' ')
					x.append(float(line_elems[2]))
					y.append(float(line_elems[3]))
					t.append(float(line_elems[5]))
	
					l+=1
					line=mdf_lines[l]	
					
				track.x=numpy.array(x)
				track.y=numpy.array(y)
				track.t=numpy.array(t)	
				
	def link_tracks(self):
		'''link all tracks into lineage of parent child by proximity of end points in space and time'''
		tracks_to_link=list(self.tracks) #copy
		
		while len(tracks_to_link)>0:
			t1=tracks_to_link.pop(0)
			for t2 in tracks_to_link:
				t1_begin=numpy.array([t1.t[0],t1.x[0],t1.y[0]])
				t1_end=numpy.array([t1.t[-1],t1.x[-1],t1.y[-1]])
				t2_begin=numpy.array([t2.t[0],t2.x[0],t2.y[0]])
				t2_end=numpy.array([t2.t[-1],t2.x[-1],t2.y[-1]])
				
				d12=numpy.linalg.norm(t2_begin-t1_end)
				d21=numpy.linalg.norm(t1_begin-t2_end)
				thresh=10.0 #(x,y,t) norm proximity threshold
				if d12<thresh:
					print t1.id,'-->',t2.id,'   ','d12:',d12
					t1.children.append(t2)
					t2.parent=t1
				if d21<thresh:
					print t2.id,'-->',t1.id,'   ','d21:',d21
					t2.children.append(t1)
					t1.parent=t2
					
	def print_lineage(self):
		'''print lineage for all trees'''
		def lineage(track,indent=''):
			for c in track.children:
				print indent+'--> '+str(c.id)+'('+str(c.cluster)+')'
				lineage(c,indent+'\t')
				
		for t in self.tracks:
			if not t.parent:
				print t.id,
				lineage(t,'\t')
				print '\n'
				
	def get_lineage(self,track):
		'''return list of all descendent tracks for given track '''
		def lineage(track,tracks):
			for c in track.children:
				tracks.append(c)
				lineage(c,tracks)
			return tracks
		
		return lineage(track,[track])

			