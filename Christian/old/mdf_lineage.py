# 140623

# original mtrackj clusters:
# 1 - undecided or non-terminal
# 2 - nanog immuno
# 3 - GATA immuno

# processed to become
# 0 - non-terminal
# 1 - undecided

import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage

tiff_filename='140623_TG4Ch3__1_MMStack_RGB2-2_0uM_1.ome.tif'

# make lineage
lin=lineage.Lineage(tiff_filename,3)

# process clusters so that 1->0 if non-terminal
t_maxes=[]
for t in lin.tracks.tracks:
	t_maxes.append(numpy.max(t.t))

t_max=max(t_maxes)

for t in lin.tracks.tracks:
	if t.cluster==1 and t.t[-1]<t_max:
		t.cluster=0

lin.tracks.print_lineage()

# plot lineage
lin.tracks.plot_lineage(channel_to_view=2)

