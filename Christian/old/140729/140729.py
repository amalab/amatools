# 140729

# original mtrackj clusters:
# 1 - undecided or non-terminal
# 2 - nanog immuno
# 3 - GATA immuno

# processed to become
# 0 - non-terminal
# 1 - undecided

# channels
# 0 brightfield - grey
# 1 cerulian - cyan
# 2 GATA4 - red

#import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage
import pickle
import glob
import os

#flatfield_ometif_files=['flatfield/140808_flatfield_1_MMStack_NanogRGB2_flatfield%d.ome.tif'%i for i in [0,1,2,3]]

#mdfs=glob.glob('tracks/*.mdf')
#mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]

#print 'processing',len(mdfs),'files...'

#for filename in mdfs:
	#try:
		#filename=filename[0:filename.find('.')]
		#print filename
	
		## make lineage
		#lin=lineage.Lineage(filename,channels=['brightfield','cerulian','GATA4'],channels_to_measure=[2],flatfield_ometif_files=flatfield_ometif_files)
			
		### histogram terminal pairs
		##pairs=lin.get_terminal_pairs()
		##print len(pairs),'terminal pairs'
		
		##print
		#lin.print_lineage()
		
		## plot
		#lin.plot_lineage(channel_to_view=2,colour='green',save_svg=True,block=False)
		
		##pickle
		#lin.save(withoutTiff=True)
	#except:
		#print 'FAILED',filename

#pass

###reload & clean###
GATA4=2
fig=pyplot.figure(figsize=(20,10))
p1=fig.add_subplot(1,2,1)
p2=fig.add_subplot(1,2,2)

#files=['140729_TL_1_MMStack_CHC3-D3_0uM_1','140729_TL_1_MMStack_CHC3-D3_0uM_5','140729_TL_1_MMStack_CHC3-D3_62nM_1']
files=['140729_TL_1_MMStack_CHC3-D3_62nM_1']

for fn in files:
	print fn
	
	lin=lineage.load('orig/'+fn+'.pickle')
	
	for track in lin.tracks[:]:
	
		f=track.f[GATA4]
		t=track.t
		
		##fill zeros and negatives
			
		N=10
		moving=numpy.convolve(f, numpy.ones((N,))/N)[(N-1):]
		#plot(moving,color='black')
		
		#spikes
		df=numpy.absolute(numpy.diff(f))
		df=numpy.append(df,0.0)
		fmasked=numpy.where(df>0.05,moving,f)
		
		#negs
		fmasked=numpy.where(fmasked<0.0,numpy.zeros(f.size),fmasked)
	
		#plot
		p1.plot(t,f,color='red')
		p2.plot(t,fmasked,'red')		
		
		#save
		track.f[GATA4]=fmasked
		
	lin.plot_lineage(channel_to_view=GATA4,colour='red',save_svg=True,block=False)
	lin.filename=fn
	lin.save()
		
pyplot.show(block=True)

#lin.plot_fluorescence(channels=[GATA4],colours=['red'],save_svg='1',block=False)

pass