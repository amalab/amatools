# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

# Preamble
import numpy as np
import numpy.random as random

from scipy import stats
from scipy.integrate import odeint


from matplotlib import font_manager as fm
from matplotlib import cm
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Helvetica']
rcParams['font.weight']='bold'
rcParams['axes.linewidth']=2 
rcParams['axes.labelweight']='bold'
rcParams['legend.fontsize']='medium'
import matplotlib.pyplot as plt

#%matplotlib inline
#color=['#EEEEEE',
       #'#4DB849',
       #'#104D92']
colors=['#000000','#AAAAAA','#00FF00','#FF00FF'] #non-terminal, undecided, nanog, GATA

# <codecell>
def draw_lineage(clonehist,fname=None,xlim=None):
    # convert the cell labels to indexes
    cid=clonehist['id']
    cid2num=dict(zip(cid,range(len(cid))))
    idx_parent=[-1 if pid==-1 else cid2num[pid] for pid in clonehist['mother']]
    
    Ncells=len(clonehist['type'])
    mother=np.array(idx_parent,dtype=np.int)
    divtime=np.array(clonehist['divtime'])
    ctype=clonehist['type']

    children=list()
    for cid in range(Ncells):
        ch=np.where(mother==cid)[0]
        if ch.size==2:
            children.append((ch[0],ch[1]))
        elif ch.size==0:
            children.append(None)
        else: print('Error')
    

    # Vertical sorting
    to_explore=[0] # Nodes to explore
    order=[0] # list with node order
    while len(to_explore)>0:
        i=to_explore.pop(0)
        j=order.index(i)
        c=children[i]
        if c is not None:
            order=order[:j]+[c[0],i,c[1]]+order[j+1:]
            to_explore+=c

    yorder=0.5*np.argsort(order)+1


    birdthtime=divtime[mother]
    birdthtime[0]=0
    xorder=birdthtime

    mxx=xorder.max()/35
    mxy=yorder.max()/2.5

    fig,ax=plt.subplots(1,1,figsize=(mxx,mxy))

    for i in range(Ncells):
        chld=children[i]
        if chld is not None:
            c=chld[0]
            x0=xorder[i]
            x1=x0
            y1=yorder[i]
            ax.plot([x0,x1],[y1,y1],'k-',lw=1)
            for c in chld:
                x2=xorder[c]
                y2=yorder[c]
                ax.plot([x1,x2,x2],[y1,y1,y2],'k-',lw=1)
        else:
            ax.plot([xorder[i],divtime[i]],[yorder[i],yorder[i]],'k-',lw=1)

        ax.plot(xorder[i],yorder[i],'o',ms=8,color=colors[ctype[i]],alpha=1)

    ax.set_xlim([xorder.min()-5,xorder.max()+5])
    ax.set_ylim([yorder.min()-.5,yorder.max()+.5])
    ax.set_xlabel('Time (frames)')
    ax.set_yticks([])
    if xlim is None:
        xlim=divtime.max()
    ax.set_xlim([-10,xlim])
    ax.set_xticks(np.arange(0,xlim+1,20))
    ax.set_frame_on(False)
    ax.get_xaxis().tick_bottom()
    ax.axes.get_yaxis().set_visible(False)
    ax.patch.set_facecolor('None')
    plt.tight_layout()
    if fname is not None:
        plt.savefig(fname)


def draw_lineage_old(clonehist,fname=None,xlim=60):
    Ncells=len(clonehist['type'])
    mother=np.array(clonehist['mother'],dtype=np.int)
    divtime=np.array(clonehist['divtime'])
    ctype=clonehist['type']

    children=list()
    for cid in range(Ncells):
        ch=np.where(mother==cid)[0]
        if ch.size==2:
            children.append((ch[0],ch[1]))
        elif ch.size==0:
            children.append(None)
        else: print('Error')
    

    # Vertical sorting
    to_explore=[0] # Nodes to explore
    order=[0] # list with node order
    while len(to_explore)>0:
        i=to_explore.pop(0)
        j=order.index(i)
        c=children[i]
        if c is not None:
            order=order[:j]+[c[0],i,c[1]]+order[j+1:]
            to_explore+=c

    yorder=0.3*np.argsort(order)+1
    yorder       


    birdthtime=divtime[mother]
    birdthtime[0]=0
    xorder=birdthtime

    mxx=xorder.max()/35
    mxy=yorder.max()/2.8

    fig,ax=plt.subplots(1,1,figsize=(mxx,mxy))

    for i in range(Ncells):
        chld=children[i]
        if chld is not None:
            c=chld[0]
            x0=xorder[i]
            x1=x0
            y1=yorder[i]
            ax.plot([x0,x1],[y1,y1],'k-',lw=1)
            for c in chld:
                x2=xorder[c]
                y2=yorder[c]
                ax.plot([x1,x2,x2],[y1,y1,y2],'k-',lw=1)

        ax.plot(xorder[i],yorder[i],'o',ms=8,color=colors[ctype[i]],alpha=1)

    ax.set_xlim([xorder.min()-5,xorder.max()+5])
    ax.set_ylim([yorder.min()-.5,yorder.max()+.5])
    ax.set_xlabel('Time (frames)')
    ax.set_yticks([])
    ax.set_xlim([-10,xlim])
    ax.set_xticks(np.arange(0,xlim+1,20))
    ax.set_frame_on(False)
    ax.get_xaxis().tick_bottom()
    ax.axes.get_yaxis().set_visible(False)
    ax.patch.set_facecolor('None')
    plt.tight_layout()
    if fname is not None:
        plt.savefig(fname)

# <codecell>

#clone={'type': np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 1, 2]),
 #'mother': np.array([-1,  0,  0,  1,  1,  2,  2,  4,  4,  3,  3,  6,  6,  5,  5,  9,  9]),
 #'divtime': np.array([  10,  28,  32,  50, 49,  55.5,  52.5,  74, 72,  69.5 ,  70.5,  74,
         #73,  74.5  ,  74.5,  88,  89.5])}

#draw_lineage(clone,xlim=80)

