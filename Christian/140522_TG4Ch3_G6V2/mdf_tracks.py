import matplotlib
#matplotlib.use('TkAgg') # to allow interactive plots in Wing

import sys
sys.path.insert(0,'../amatools') #adds cwd to path, to pickup modules local to amatools_dist directory

import os
import amacelltracks
import matplotlib.colors 
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
#from prettyplotlib import plt as pyplot
import numpy

import libtiff
from bs4 import BeautifulSoup
from lxml import etree 
import pandas

#inducible GATA4-mCherry and a green reporter for endogenous Gata6 transcription

mdf_filenames=['TL_1_MMStack_PDO3_to_0uM_6.ome.tif.mdf','TL_1_MMStack_PDO3_to_0uM_7.ome.tif.mdf']

bgr_filenames=['TL_1_MMStack_PDO3_to_0uM_6_bgr.ome.tif.mdf','TL_1_MMStack_PDO3_to_0uM_7_bgr.ome.tif.mdf']

tiff_filenames=['TL_1_MMStack_PDO3_to_0uM_6.ome.tif','TL_1_MMStack_PDO3_to_0uM_7.ome.tif']

def read_micromanager_ome_tiff(fn):
	#read direct
	tiff_file=libtiff.TIFFfile(fn)
	tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array

	## t*c,y,x -> t,c,y,x

	## get info
	## get header as string
	#s=tiff_file.get_info()
	## make list of tags
	#l=s.splitlines()
	
	###not working because showinf chokes on large file size. 32bit issue?
	#get xml metadata
	#s='/Users/jpm/Progs/AMATools/bftools/showinf -nopix -omexml-only '+fn
	#x=os.popen(s).read() #xml metadata
	#xml_root=etree.fromstring(x)

	#sizeC=int(xml_root.xpath("//*[@SizeC]")[0].xpath('@SizeC')[0])
	#sizeT=int(xml_root.xpath("//*[@SizeT]")[0].xpath('@SizeT')[0])
	#sizeX=int(xml_root.xpath("//*[@SizeX]")[0].xpath('@SizeX')[0])
	#sizeY=int(xml_root.xpath("//*[@SizeY]")[0].xpath('@SizeY')[0])	

	#try:
		## get ome.tiff xml header
		#xml=s[s.find('"""')+3:s.rfind('"""')]

	#except:
		#print 'No XML header'

	## parse XML
	#header=BeautifulSoup(xml,"lxml")

	## get pixel tag attributes
	#pixel_attributes=header.find('pixels').attrs
	#print 'Pixel attributes:',pixel_attributes

	## get all channel tags
	#ch=header.find_all('channel')

	## print channel name attributes
	#print 'Channels:'
	#numch=0
	#for c in ch:
		#print numch,c['name'] # in BeautifulSoup, attrs are dicts of bs tree objects
		#numch+=1				

	#print 'tiff_array shape:',tiff_array.shape # [time*channels,height,width]

	# reshape
	sh=tiff_array.shape	
	numch=3
	tiff_array=tiff_array.reshape((-1,numch,sh[1],sh[2]))
	sh=tiff_array.shape
	print 'reshaped as:',sh
	T=sh[0]
	width=sh[3]
	height=sh[2]
	print 'T numch width height',T,numch,width,height

	return tiff_array


#plot images
#fig=pyplot.figure()		

#cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)],                                                      
								#'green': [(0.0,  0.0, 0.0),
									#(1.0,  1.0, 1.0)],                                                        
								#'blue':  [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)]})

#cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
									#(1.0,  1.0, 1.0)],                                                      
								#'green': [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)],                                                        
								#'blue':  [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)]})

#cmaps=['binary_r',cmap_g,cmap_r]
#t=T/2
#cc=0
#pp=[] #plots
#for c in ch:
	#p=fig.add_subplot(1,numch,cc+1) 
	#p.set_title(c['name'])	

	#im=tiff_array[t,cc,:,:]
	#imgplot=p.imshow(im)
	#imgplot.set_cmap(cmaps[cc])
	#pp.append(p)
	#cc+=1

#pyplot.show()

##image
#tiff=amatools.AMATiff()
#tiff.open(tiff_filename)
##tiff.reshape_fiji()
#p=tiff.plot(0)	

#fig.suptitle(os.path.split(mdf_filename)[1])
#tracks
#plot_titles=['Epi - Spry4','Epi - GATA4','PrE - Spry4','PrE - GATA4']
#plot_num=0
#plot_order=[1,3,2,4]

track_colours=['r','g','b','c','m','y','k']
channels={1:'gata4',2:'gata6'} #red,green

# calc dataframes
data_gata6=pandas.DataFrame()
data_gata4=pandas.DataFrame()
track_count=0
r=8 #radius of square sample area around track point

for bgr_filename,mdf_filename,tiff_filename in zip(bgr_filenames,mdf_filenames,tiff_filenames):#[0:1]:

	print bgr_filename,mdf_filename,tiff_filename
	
	# read zvi video
	tiff_array=read_micromanager_ome_tiff(tiff_filename)

	# measure fluorescence along tracks	
	
	tracks=amacelltracks.AMACellTracks()
	tracks.read_mtrackj_mdf(mdf_filename)
	# background tracks
	bgr_tracks=amacelltracks.AMACellTracks()
	bgr_tracks.read_mtrackj_mdf(bgr_filename)
	
	for channel in channels:		
		print 'channel', channel
		
		#backgrounds
		backgrounds=pandas.DataFrame()
		bgr_count=0
		for track in bgr_tracks.tracks:
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)

				#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				bgr=pandas.Series(vv,tt)
				#smoothed=pandas.rolling_mean(smoothed,3)
				label=str(bgr_count)+'_'+channels[channel]
				d=pandas.DataFrame({label:bgr})

				backgrounds=backgrounds.join(d,how='outer')		
				bgr_count+=1	
				
		background_ave=backgrounds.mean(axis=1)
		#print '-----'
		#print 'background_ave'
		#print background_ave,'\n'
		
				
		#cells
		for track in tracks.tracks:
			print 'track',track_count
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)

				#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				measurement=pandas.Series(vv,tt)
				corrected=measurement.sub(background_ave) #subtract background ave
				#smoothed=pandas.rolling_mean(corrected,3)
			
				label=str(track_count)+'_'+channels[channel]
				d=pandas.DataFrame({label:corrected})
				if channels[channel] is 'gata6':
					data_gata6=data_gata6.join(d,how='outer')
				elif channels[channel] is 'gata4':
					data_gata4=data_gata4.join(d,how='outer')

				#p.plot([track_count]*len(tt),tt,smoothed.values,color=track_colours[track_count%len(track_colours)])

				track_count+=1


		#plot_num+=1

# save dataframes
data_gata4.to_pickle('data_gata4_unsmoothed.df')
data_gata6.to_pickle('data_gata6_unsmoothed.df')

####PLOTS

# plot gata over time for each fate
fig=pyplot.figure()
p=fig.add_subplot(1,1,1)
p.set_title('red - GATA4   green - GATA6')
p.set_xlabel('time')
p.set_ylabel('gata')

count=0
for c in data_gata4.columns:
	series=data_gata4[c]
	col='r'

	p.plot(series.index,series.values,color=col)	#x,y,col
	count+=1
	
for c in data_gata6.columns:
	series=data_gata6[c]
	col='g'

	p.plot(series.index,series.values,color=col)	#x,y,col
	count+=1

pyplot.show(block=True)
pass

## 3D plot of sprouty signalling
#fig=pyplot.figure()
#p=fig.add_subplot(1,1,1,projection='3d')
#p.set_title('cyan - Epi   magenta - PrE')
#p.set_xlabel('gata_at_30')
#p.set_ylabel('time')
#p.set_zlabel('sprouty')

#count=0
##for c in sprouty_tracks.columns:
#for c in data_sprouty.columns:
	##sprouty_series=sprouty_tracks[c]
	#sprouty_series=data_sprouty[c]

	#if 'Epi' in c:
		#col='c'
	#else:
		#col='m'

	#gata_at_30=data_gata.iloc[30,count]

	#p.plot([gata_at_30]*len(sprouty_series.index),sprouty_series.index,sprouty_series.values,color=col)	#x,y,z,col
	#count+=1

#pyplot.show(block=True)
#pass


#tracks.show_plots(block=False)
#tracks.show_plots(block=True)