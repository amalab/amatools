import lineage
import numpy
import glob

#pickles=glob.glob('lineage - corrected/*.pickle')
pickles=glob.glob('*.pickle')
fmins=[]
fmaxs=[]
channel_to_view=2

for p in pickles:
	lin=lineage.load(p)

	#fluorescence levels along tracks
	#get signal range
	fmin=[numpy.min(t.f[channel_to_view]) for t in lin.tracks]
	fmax=[numpy.max(t.f[channel_to_view]) for t in lin.tracks]
	fmin=numpy.min(fmin)
	fmax=numpy.max(fmax)
	print 'fmin,fmax',fmin,fmax
	fmins.append(fmin)
	fmaxs.append(fmax)

fmin=numpy.min(fmins)
fmax=numpy.max(fmaxs)
print 'ALL fmin,fmax',fmin,fmax

pass


#lin=lineage.load('140623_TG4Ch3__1_MMStack_RGB2-2_0uM_2')
#lin.plot_lineage(2,save_svg=False,block=False)
