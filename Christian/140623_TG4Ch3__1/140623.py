# 140623

# original mtrackj clusters:
# 1 - undecided or non-terminal
# 2 - nanog immuno
# 3 - GATA immuno

# processed to become
# 0 - non-terminal
# 1 - undecided

import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage
import pickle
import glob

flatfield_ometif_files=['flatfield/140729_Flatfield_2_MMStack_Pos%d.ome.tif'%i for i in range(4)]

#mdfs=['140623_TG4Ch3__1_MMStack_RGB2-2_0uM_1.mdf','140623_TG4Ch3__1_MMStack_RGB2-2_31nM_6.mdf']
#mdfs=['140623_TG4Ch3__1_MMStack_RGB2-2_0uM_2.mdf']
mdfs=glob.glob('*.mdf')
mdfs=[m for m in mdfs if 'bgr' not in m]

print 'processing',len(mdfs),'files...'

for filename in mdfs:
	try:
		filename=filename[0:filename.find('.')]
		print filename
	
		# make lineage
		numchannels=3
		lin=lineage.Lineage(filename,numchannels,channels_to_measure=[2],flatfield_ometif_files=flatfield_ometif_files)
		#lin=lineage.Lineage(filename,numchannels,channels_to_measure=[2]) #uncorrected
			
		# histogram terminal pairs
		pairs=lin.get_terminal_pairs()
		print len(pairs),'terminal pairs'
		
		#print
		lin.print_lineage()
		
		# plot
		lin.plot_lineage(channel_to_view=2,save_svg=True,block=False)
		
		#pickle
		lin.save(withoutTiff=True)
	except:
		print 'FAILED',filename

pass

