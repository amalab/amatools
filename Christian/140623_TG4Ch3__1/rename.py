#remove series suffix from mdf files

import glob
import os

mdfs=glob.glob('*.mdf')

# '140623_TG4Ch3__1_MMStack_RGB2-2_0uM_1.ome.tif - 140623_TG4Ch3__1_MMStack_RGB2-2_0uM_1.mdf'

for f in mdfs:
	l=f.find(' - ')
	if l>0:
		fnew=f[0:l]+'.mdf'
		os.rename(f,fnew)