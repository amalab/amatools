import sys
sys.path.insert(0,'') #adds cwd to path, to pickup modules local to amatools_dist directory

import os
import amatools
import matplotlib.colors 
import matplotlib.pyplot as pyplot
#from mpl_toolkits.mplot3d import Axes3D
#from prettyplotlib import plt as pyplot
import numpy

import libtiff
from bs4 import BeautifulSoup

import pandas
from scipy.optimize import curve_fit

# 2 constructs (GATA 4 and GATA6)
# 3 fields of view per construct
# 5/6 tracks per field of view
# 3 background tracks per field of view

# subtract background from track
# normalise wrt 1st frame
# average tracks for each construct, with error estimate
# fit exponential to average to get half-life, with fit weighted by error

#path='/Users/jpm/DATA/Christian/Tracks/131119_doxOFF_tracks/'
path=''

mdf_filenames=[['131119_Gata4_dox_OFF_AA02_01.zvi.mdf','131119_Gata4_dox_OFF_AA02_02.zvi.mdf','131119_Gata4_dox_OFF_AA02_03.zvi.mdf'],
               ['131119_Gata6_dox_OFF_AB02_01.zvi.mdf','131119_Gata6_dox_OFF_AB02_02.zvi.mdf','131119_Gata6_dox_OFF_AB02_03.zvi.mdf']]

bgr_filenames=[['131119_Gata4_dox_OFF_AA02_01_bgr.zvi.mdf','131119_Gata4_dox_OFF_AA02_02_bgr.zvi.mdf','131119_Gata4_dox_OFF_AA02_03_bgr.zvi.mdf'],
               ['131119_Gata6_dox_OFF_AB02_01_bgr.zvi.mdf','131119_Gata6_dox_OFF_AB02_02_bgr.zvi.mdf','131119_Gata6_dox_OFF_AB02_03_bgr.zvi.mdf']]

tiff_filenames=[['131119_Gata4_6_dox_OFF-0001_AA02_01.ome.tiff','131119_Gata4_6_dox_OFF-0001_AA02_02.ome.tiff','131119_Gata4_6_dox_OFF-0001_AA02_03.ome.tiff'],
                ['131119_Gata4_6_dox_OFF-0001_AB02_01.ome.tiff','131119_Gata4_6_dox_OFF-0001_AB02_02.ome.tiff','131119_Gata4_6_dox_OFF-0001_AB02_03.ome.tiff']]

fig=pyplot.figure()
plot_titles=['GATA4','GATA6']
plot_num=0
plot_order=[1,2]
plots=[]
track_colours=['r','g','b','c','m','y','k']

for construct in [0,1]:
#for construct in [0]:
	p=fig.add_subplot(2,1,plot_order[plot_num])
	p.set_title(plot_titles[plot_num])	
	plots.append(p)

	track_count=0
	measurements=pandas.DataFrame()
	backgrounds=pandas.DataFrame()
	
	for fov in [0,1,2]:	
	#for fov in [0]:	
		### MOVIE
		tiff_filename=path+tiff_filenames[construct][fov]

		#read movie direct
		tiff_file=libtiff.TIFFfile(tiff_filename)
		tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array

		# t*c,y,x -> t,c,y,x
		# get header as string
		s=tiff_file.get_info()
		# make list of tags
		l=s.splitlines()

		try:
			# get ome.tiff xml header
			xml=s[s.find('"""')+3:s.rfind('"""')]

		except:
			print 'No XML header'

		# parse XML
		header=BeautifulSoup(xml,"lxml")

		# get pixel tag attributes
		pixel_attributes=header.find('pixels').attrs
		print 'Pixel attributes:',pixel_attributes

		# get all channel tags
		ch=header.find_all('channel')

		# print channel name attributes
		print 'Channels:'
		numch=0
		for c in ch:
			try:
				print numch,c['name'] # in BeautifulSoup, attrs are dicts of bs tree objects
			except:
				print 'No name'
			numch+=1				

		print 'tiff_array shape:',tiff_array.shape # [time*channels,height,width]

		# reshape
		sh=tiff_array.shape
		tiff_array=tiff_array.reshape((-1,numch,sh[1],sh[2]))
		sh=tiff_array.shape
		print 'reshaped as:',sh
		T=sh[0]
		width=sh[3]
		height=sh[2]
		print 'T numch width height',T,numch,width,height

		### TRACKS
		channel=1
		
		# background tracks
		bgr_filename=path+bgr_filenames[construct][fov]		
		bgr_tracks=amatools.AMACellTracks()
		bgr_tracks.read_mtrackj_mdf(bgr_filename)
		
		r=3 #radius of square sample area around track point
		bgr_count=0
		for track in bgr_tracks.tracks:
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)

				#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				bgr=pandas.Series(vv,tt)
				#smoothed=pandas.rolling_mean(smoothed,3)
				label=str(construct)+'_'+str(fov)+'_'+str(bgr_count)
				d=pandas.DataFrame({label:bgr})

				backgrounds=backgrounds.join(d,how='outer')		
				bgr_count+=1
				
		background_ave=backgrounds.mean(axis=1)
		print '-----'
		print 'background_ave'
		print background_ave,'\n'

		# read tracks
		mdf_filename=path+mdf_filenames[construct][fov]		
		tracks=amatools.AMACellTracks()
		tracks.read_mtrackj_mdf(mdf_filename)

		r=3 #radius of square sample area around track point
		for track in tracks.tracks:
			if track.duration>0.0:
				vv=[]
				tt=[]
				N=len(track.ti)
				for i in range(0,N):			
					x=track.x[i]
					y=track.y[i]
					t=track.ti[i]
					v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4.0*r*r) #mtrackj time starts at 1
					vv.append(v)
					tt.append(t)

				#p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				measurement=pandas.Series(vv,tt)
				smoothed=pandas.rolling_mean(measurement,3)
				corrected=measurement.sub(background_ave) #subtract background ave
				normalised=corrected.div(corrected.iget(0))
				
				#print '-----'
				#print 'track',track_count
				#print 'measurement'
				#print measurement
				#print 'corrected'
				#print corrected
				#print '\n'
				
				#label=str(construct)+'_'+str(fov)+'_'+str(track_count)
				label=mdf_filenames[construct][fov]+'_'+str(track_count)
				#d=pandas.DataFrame({label:smoothed})
				d=pandas.DataFrame({label:normalised})

				measurements=measurements.join(d,how='outer')
				
				#p.plot(tt,smoothed.values,color=track_colours[track_count%len(track_colours)])				
				#p.plot(tt,vv,color=track_colours[track_count%len(track_colours)])				
				p.plot(normalised.index,normalised.values,color=track_colours[track_count%len(track_colours)])				
				pyplot.show(block=False)
				
				track_count+=1	
		
	# mean and standard error bars			
	mean=measurements.mean(1)
	sem=measurements.std(1)/numpy.sqrt(len(measurements.columns))
	p.errorbar(measurements.index,mean,yerr=sem,color='k',linewidth=2.5)
	pyplot.show(block=False)	
	
	#calc half-life
	# curve to fit
	def exp_fn(t,C0,lbd):
		return C0*numpy.exp(-lbd*t)	
	
	means=mean.dropna().values #drop nans
	times=mean.index.values.astype(numpy.float64)
	sems=sem.dropna().values
	lsems=len(sems)
	
	start_time=10
	popt,pcov=curve_fit(exp_fn,times[start_time:lsems],means[start_time:lsems],sigma=sems[start_time:])
	C0=popt[0]
	lbd=popt[1]
	#print 'C0:',popt[0],'lambda:',popt[1]
	#print 'pcov',pcov
	print("Estimated parameter values:\n\tC0=%g (variance=%g)\n\tlambda=%g (variance=%g)"%(popt[0],pcov[0,0],popt[1],pcov[1,1]))
	hl=numpy.log(2)/popt[1]
	print("Estimated half-life: %g" %hl)	
	
	p.plot(times[0:lsems],exp_fn(times[0:lsems],C0,lbd),color='r',linewidth=2.5)
				
				
	plot_num+=1
	
pass
pyplot.show(block=True)	

##plot images
#fig=pyplot.figure()		

#cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)],                                                      
								#'green': [(0.0,  0.0, 0.0),
									#(1.0,  1.0, 1.0)],                                                        
								#'blue':  [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)]})

#cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_g',{'red':   [(0.0,  0.0, 0.0),
									#(1.0,  1.0, 1.0)],                                                      
								#'green': [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)],                                                        
								#'blue':  [(0.0,  0.0, 0.0),
									#(1.0,  0.0, 0.0)]})

#cmaps=['binary_r',cmap_g,cmap_r]
#t=T/2
#cc=0
#pp=[] #plots
#for c in ch:
	#p=fig.add_subplot(1,numch,cc+1) 
	#p.set_title(c['name'])	

	#im=tiff_array[t,cc,:,:]
	#imgplot=p.imshow(im)
	#imgplot.set_cmap(cmaps[cc])
	#pp.append(p)
	#cc+=1

#pyplot.show()

##image
#tiff=amatools.AMATiff()
#tiff.open(tiff_filename)
##tiff.reshape_fiji()
#p=tiff.plot(0)	

#fig.suptitle(os.path.split(mdf_filename)[1])
#tracks
#plot_titles=['Epi - Spry4','Epi - GATA4','PrE - Spry4','PrE - GATA4']
#plot_num=0
#plot_order=[1,3,2,4]
#track_colours=['r','g','b','c','m','y','k']
#channels={1:'FGF',2:'GATA'}

## calc dataframes

#data_fgf=pandas.DataFrame()
#data_gata=pandas.DataFrame()
#track_count=0

#for mdf_filename in mdf_filenames:
	#if 'PrE' in mdf_filename:
		#fate='PrE'
	#elif 'Epi' in mdf_filename:
		#fate='Epi'
	#else:
		#raise Exception('Filename fate type error')


	#tracks=amatools.AMACellTracks()

	#tracks.read_mtrackj_mdf(mdf_filename)
	#tracks.plot_tracks(pp[0])


	## measure fluorescence along tracks
	#for channel in channels:		
		#r=3 #radius of square sample area around track point
		#for track in tracks.tracks:
			#if track.duration>0.0:
				#vv=[]
				#tt=[]
				#N=len(track.ti)
				#for i in range(0,N):			
					#x=track.x[i]
					#y=track.y[i]
					#t=track.ti[i]
					#v=numpy.sum(tiff_array[t-1,channel,y-r:y+r,x-r:x+r])/(4*r*r) #mtrackj time starts at 1
					#vv.append(v)
					#tt.append(t)

				##p.plot([track_count]*len(tt),tt,vv,color=track_colours[track_count],linestyle='--')

				#smoothed=pandas.Series(vv,tt)
				#smoothed=pandas.rolling_mean(smoothed,3)
				#label=str(track_count)+'_'+fate+'_'+channels[channel]
				#d=pandas.DataFrame({label:smoothed})

				#if channels[channel] is 'FGF':
					#data_fgf=data_fgf.join(d,how='outer')
				#elif channels[channel] is 'GATA':
					#data_gata=data_gata.join(d,how='outer')

				##p.plot([track_count]*len(tt),tt,smoothed.values,color=track_colours[track_count%len(track_colours)])

				#track_count+=1

		##plot_num+=1

## save dataframes
#data_fgf.to_pickle('data_fgf.df')
#data_gata.to_pickle('data_gata.df')

##fgf_tracks=data.select(lambda s: 'FGF' in s,axis=1)

## 3D plot of fgf signalling
#fig=pyplot.figure()
#p=fig.add_subplot(1,1,1,projection='3d')
#p.set_title('tracks')
#count=0
##for c in fgf_tracks.columns:
#for c in data_fgf.columns:
	##fgf_series=fgf_tracks[c]
	#fgf_series=data_fgf[c]

	#if 'Epi' in c:
		#col='c'
	#else:
		#col='m'

	#gata_at_30=data_gata.iloc[30,count]

	#p.plot([gata_at_30]*len(fgf_series.index),fgf_series.index,fgf_series.values,color=col)	#x,y,z,col
	#count+=1

#pyplot.show(block=False)
#pass


#tracks.show_plots(block=False)
#tracks.show_plots(block=True)