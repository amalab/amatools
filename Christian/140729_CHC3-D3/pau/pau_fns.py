import numpy as np

def treat_missings_dups(t,f):
    # we expect consecutive frames, but sometimes frames are missing or two values for the same
    # frame have been collected.
    tn=np.arange(t.min(),t.max()+1)
    fn=[]
    if f is None: pass
    for i,ff in enumerate(f):
        if len(ff)==0:
            # ignore empty fields
            fn.append([])
        else:
            fn.append(np.empty_like(tn))
            for j,tt in enumerate(tn):
                #print('j',j)
                idx,=np.where(t==tt)
                if idx.size>0:
                    # if one or more values recorded, average
                    fn[i][j]=ff[idx].mean()
                else:
                    # if none, then average previous and next.
                    idx,=np.where((tt<=t+1) & (t-1<=tt))
                    fn[i][j]=ff[idx].mean()
                    #print(idx)
    return tn,fn


t=np.array([0,1,2,4,5,6,9])
f=np.array([10,11,12,14,15,16,19])

treat_missings_dups(t,f)

print tn
print fn