# 140729

# original mtrackj clusters:
# 1 - undecided or non-terminal
# 2 - nanog immuno
# 3 - GATA immuno

# processed to become
# 0 - non-terminal
# 1 - undecided

# channels
# 0 brightfield - grey
# 1 cerulian - cyan
# 2 GATA4 - red

#import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage
import pickle
import glob
import os

###reload & strip frame 35 ###
GATA4=2
files=glob.glob('lineage_corr/*.pickle')

for fn in files:
	print fn

	lin=lineage.load(fn)
	
	for i,track in enumerate(lin.tracks):
	
		f=track.f[GATA4]
		t=track.t
		
		w35=numpy.where(t==35.0)[0]
		if w35.size>0:
			print i,w35[0]
			print f[w35-2:w35+3]

			track.f[GATA4]=numpy.delete(track.f[GATA4],w35)
			track.t=numpy.delete(track.t,w35)
			
	lin.save()
pass		


####reload & plot time series###
#GATA4=2
#files=glob.glob('*.pickle')

#for fn in files:
	#print fn

	#lin=lineage.load(fn)
	
	#ntracks=len(lin.tracks)
	
	##fig=pyplot.figure(figsize=(20,ntracks*10))
	#fig=pyplot.figure(figsize=(20,10))
	#p1=fig.add_subplot(1,2,1)
	#p2=fig.add_subplot(1,2,2)	
	
	#for i,track in enumerate(lin.tracks[1:2]):
	
		#f=track.f[GATA4]
		#t=track.t
		
		###find spikes in df
			
		##spikes
		#df=numpy.absolute(numpy.diff(f))
		#df=numpy.append(df,0.0)
		#spikes=t[numpy.where(df>0.8)[0]]
	
		##plot
		##p1=fig.add_subplot(ntracks,2,i*2+1)
		##p2=fig.add_subplot(ntracks,2,i*2+2)		
		#p1.plot(t,f,color='red')
		#p2.plot(t,df,'black')
		
		
#pyplot.show(block=False)

####corrected tracks###
#flatfield_ometif_files=['flatfield/140729_Flatfield_2_MMStack_Pos%d.ome.tif'%i for i in [0,1,2,3]]

#mdfs=glob.glob('tracks/*.mdf')
#mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]
##mdfs=['140729_TL_1_MMStack_CHC3-D3_0uM_6.mdf'] #bgr had gammy track at end
#mdfs=['140729_TL_1_MMStack_CHC3-D3_0uM_1.mdf'] #bgr had gammy track at end

#print 'processing',len(mdfs),'files...'

#for filename in mdfs:
	#try:
		#filename=filename[0:filename.find('.')]
		#print filename
	
		## make lineage
		#lin=lineage.Lineage(filename,channels=['brightfield','cerulian','GATA4'],channels_to_measure=[2],flatfield_ometif_files=flatfield_ometif_files)
					
		##print
		#lin.print_lineage()
		
		## plot
		#lin.plot_lineage(channel_to_view=2,colour='red',save_svg=True,block=False)
		
		##pickle
		#lin.save(withoutTiff=True)
	#except:
		#print 'FAILED',filename

#pass



###get raw values###
#mdfs=glob.glob('tracks/*.mdf')
#mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]

#print 'processing',len(mdfs),'files...'

#for filename in mdfs:
	#try:
		#filename=filename[0:filename.find('.')]
		#print filename
	
		## make lineage
		#lin=lineage.Lineage(filename,channels=['brightfield','cerulian','GATA4'],channels_to_measure=[2],flatfield_ometif_files=None,background_correct=False)
			
		### histogram terminal pairs
		##pairs=lin.get_terminal_pairs()
		##print len(pairs),'terminal pairs'
		
		##print
		#lin.print_lineage()
		
		## plot
		#lin.plot_lineage(channel_to_view=2,colour='red',save_svg=True,block=False)
		
		##pickle
		#lin.save(withoutTiff=True)
	#except:
		#print 'FAILED',filename

#pass

###reload & get GATA4 totals over time###
#GATA4=2
#fig=pyplot.figure(figsize=(10,10))
#p1=fig.add_subplot(1,1,1)

#files0=['140729_TL_1_MMStack_CHC3-D3_0uM_%d'%i for i in [1,2,4,5]]
#files62=['140729_TL_1_MMStack_CHC3-D3_62nM_%d'%i for i in [1,2,3,4,8]]

#ss0=[]
#for fn in files0:
	#print fn
	
	#lin=lineage.load(fn+'.pickle')
	
	#for track in lin.tracks[:]:
	
		#f=track.f[GATA4]
		#t=track.t
		
		#s=numpy.sum(f)
		#ss0.append(s)
		
#ss62=[]
#for fn in files62:
	#print fn
	
	#lin=lineage.load(fn+'.pickle')
	
	#for track in lin.tracks[:]:
	
		#f=track.f[GATA4]
		#t=track.t
		
		#s=numpy.sum(f)
		#ss62.append(s)
			
##plot
#x=numpy.random.uniform(0.9,1.1,len(ss0))
#p1.scatter(x,ss0,color='red')

#x=numpy.random.uniform(1.9,2.1,len(ss62))
#p1.scatter(x,ss62,color='red')
	
		
#pyplot.show(block=True)
#pass

###orig###
#flatfield_ometif_files=['flatfield/140729_Flatfield_2_MMStack_Pos%d.ome.tif'%i for i in [0,1,2,3]]

#mdfs=glob.glob('tracks/*.mdf')
#mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]

#print 'processing',len(mdfs),'files...'

#for filename in mdfs:
	#try:
		#filename=filename[0:filename.find('.')]
		#print filename
	
		## make lineage
		#lin=lineage.Lineage(filename,channels=['brightfield','cerulian','GATA4'],channels_to_measure=[2],flatfield_ometif_files=flatfield_ometif_files)
			
		### histogram terminal pairs
		##pairs=lin.get_terminal_pairs()
		##print len(pairs),'terminal pairs'
		
		##print
		#lin.print_lineage()
		
		## plot
		#lin.plot_lineage(channel_to_view=2,colour='red',save_svg=True,block=False)
		
		##pickle
		#lin.save(withoutTiff=True)
	#except:
		#print 'FAILED',filename

#pass

####reload & spot spikes###
#GATA4=2

##files=['140729_TL_1_MMStack_CHC3-D3_0uM_1','140729_TL_1_MMStack_CHC3-D3_0uM_5','140729_TL_1_MMStack_CHC3-D3_62nM_1']
##files=['140729_TL_1_MMStack_CHC3-D3_0uM_1']
#files=glob.glob('*.pickle')

#for fn in files:
	#print fn

	#output_file=open(fn+'.spikes.txt',mode='w')	
	#output_file.write(fn+'\n')
	
	##lin=lineage.load('lineage_2/'+fn+'.pickle')
	#lin=lineage.load(fn)
	
	#ntracks=len(lin.tracks)
	
	##fig=pyplot.figure(figsize=(20,ntracks*10))
	#fig=pyplot.figure(figsize=(20,10))
	#p1=fig.add_subplot(1,2,1)
	#p2=fig.add_subplot(1,2,2)	
	
	#for i,track in enumerate(lin.tracks):
	
		#f=track.f[GATA4]
		#t=track.t
		
		###find spikes in df
			
		##spikes
		#df=numpy.absolute(numpy.diff(f))
		#df=numpy.append(df,0.0)
		#spikes=t[numpy.where(df>0.6)[0]]
	
		##plot
		##p1=fig.add_subplot(ntracks,2,i*2+1)
		##p2=fig.add_subplot(ntracks,2,i*2+2)		
		#p1.plot(t,f,color='red')
		#p2.plot(t,df,'black')
		
		#c=track.cluster
		#if c==0:
			#c=1
			
		#s=(str(c)+'.'+str(track.number)+' '+str(spikes)+'\n')
		#if len(spikes>0):
			#print s
			#output_file.write(s)
		
	#fig.savefig(fn+'.spikes.svg')
	#output_file.close()
		
		##save
		##track.f[GATA4]=fmasked
		
	##lin.plot_lineage(channel_to_view=GATA4,colour='red',save_svg=True,block=False)
	##lin.filename=fn
	##lin.save()
		
#pyplot.show(block=True)

#lin.plot_fluorescence(channels=[GATA4],colours=['red'],save_svg='1',block=False)

###new tracks & clean
#GATA4=2
##mdfs=glob.glob('tracks/*.mdf')
##mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]
#mdfs=['140729_TL_1_MMStack_CHC3-D3_0uM_6.mdf']
#flatfield_ometif_files=['flatfield/140729_Flatfield_2_MMStack_Pos%d.ome.tif'%i for i in [0,1,2,3]]
#print 'flatfields:',flatfield_ometif_files
#print 'processing',len(mdfs),'files...'

#for filename in mdfs:
	#try:
		#filename=filename[0:filename.find('.')]
		#print filename
	
		## make lineage
		#lin=lineage.Lineage(filename,channels=['brightfield','cerulian','GATA4'],channels_to_measure=[2],flatfield_ometif_files=flatfield_ometif_files)
		
		##clean
		#for track in lin.tracks[:]:
		
			#f=track.f[GATA4]
			#t=track.t
			
			##fill zeros and negatives	
			#N=10
			#moving=numpy.convolve(f, numpy.ones((N,))/N)[(N-1):]
			##plot(moving,color='black')
			
			##spikes
			#df=numpy.absolute(numpy.diff(f))
			#df=numpy.append(df,0.0)
			#fmasked=numpy.where(df>0.05,moving,f)
			#fm=numpy.where(df>0.05)
			#print fm[0].size
			##negs
			#fmasked=numpy.where(fmasked<0.0,numpy.zeros(f.size),fmasked)
		
			##plot
			##p1.plot(t,f,color='red')
			##p2.plot(t,fmasked,'red')		
			
			##save
			#track.f[GATA4]=fmasked
			
		
		##print
		#lin.print_lineage()
		
		## plot
		#lin.plot_lineage(channel_to_view=2,colour='red',save_svg=True,block=False)
		
		##pickle
		#lin.save(withoutTiff=True)
	#except:
		#print 'FAILED',filename

#pass