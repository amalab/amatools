# 140902

# channels
# 0 brightfield - grey
# 1 cerulian - cyan
# 2 GATA4 - red
# 3 GATA6 - green

import libtiff
import matplotlib
import matplotlib.pyplot as pyplot
import numpy
import lineage
import pickle
import glob
import os

###orig###

flatfield_ometif_files=['flatfield/141001_Gata6RGB_C5_flatfield_2_MMStack_Pos%d.ome.tif'%i for i in [0,1,2]]#,3,4,5]]

##mdfs=['140806_TL_1_MMStack_NanogRGB2_0uM_dox_1.mdf']
##mdfs=glob.glob('tracks/*.mdf')
##mdfs=[os.path.split(m)[1] for m in mdfs if 'bgr' not in m]
mdfs=['140902_Gata6RBG_TL_1_MMStack_Gata6RGB_0uM_%d.mdf'%i for i in range(1,2)]
print 'processing',len(mdfs),'files...'

for filename in mdfs:
	try:
		filename=filename[0:filename.find('.')]
		print filename
	
		# make lineage
		for sigma in [32.0,64.0]:
			lin=lineage.Lineage(filename,channels=['brightfield','cerulian','GATA6','GATA4'],channels_to_measure=[2,3],flatfield_ometif_files=flatfield_ometif_files,sigma=sigma)
			
			#print
			#lin.print_lineage()
			
			# plot
			#lin.plot_lineage(channel_to_view=2,colour='green',save_svg=True,block=False)
			#lin.plot_lineage(channel_to_view=3,colour='red',save_svg=True,block=False)
			
			lin.plot_fluorescence(channels=[2,3], colours=['g','r'], save_svg='.'+str(sigma), block=False)
		
		#pickle
		#lin.save(withoutTiff=True)
	except:
		print 'FAILED',filename

pass

####replot###
#pickles=['140902_Gata6RBG_TL_1_MMStack_Gata6RGB_0uM_%d.pickle'%i for i in range(1,9)]
#for p in pickles:
	#print p
	#lin=lineage.load(p)
	#lin.plot_fluorescence(channels=[2,3], colours=['g','r'], save_svg=True, block=False)


####clustering####
#from sklearn.cluster import KMeans

#pickles=['140902_Gata6RBG_TL_1_MMStack_Gata6RGB_0uM_%d.pickle'%i for i in range(1,9)]
#GATA6=2
#GATA4=3
#t_max=334

#x=[]
#gata4=[]

#for p in pickles:
	#print p
	#lin=lineage.load(p)
	##lin.plot_lineage(channel_to_view=channel,colour='green',save_svg=False,block=False)
	
	#for t in lin.tracks:
		##GATA6
		#f=numpy.zeros(t_max)
		#f[t.t.astype('int')]=t.f[GATA6][:] #not all tracks are of same length nor have all time points, so zero-pad
		#x.append(f)
		
		##GATA4
		#g=numpy.sum(t.f[GATA4])/len(t.f[GATA4])
		#gata4.append(g)
		
		
		
#X=numpy.array(x)
#print 'X',X.shape

#kmeans=KMeans(n_clusters=3)
#kmeans.fit(X)
#print kmeans.labels_

#z=zip(kmeans.labels_,gata4)
#s=sorted(z,key=lambda x: x[0])
#print s
#s=sorted(z,key=lambda x: x[1])
#print s

#pyplot.plot([x[0] for x in s],[x[1] for x in s])
#pyplot.show()
#pass
