import numpy
import scipy.ndimage
import matplotlib.pyplot as pyplot

fig=pyplot.figure()

for sigma in [8.0,16.0,32.0]:
	r=100
	seed=numpy.zeros((2*r+1,2*r+1))
	seed[r,r]=1.0
	weight=scipy.ndimage.filters.gaussian_filter(seed,sigma) #2d gaussian kernal
	weight/=numpy.sum(weight) #sum of all weights=1.0
	
	pyplot.plot(weight[r])
	
pyplot.show()