run("Stack to Images");
selectWindow("T PMT-T2");
close();
selectWindow("Ch2-T1");

run("Gaussian Blur...", "sigma=4");
run("Enhance Local Contrast (CLAHE)", "blocksize=127 histogram=256 maximum=4 mask=*None* fast_(less_accurate)");
setAutoThreshold("Default dark");
//run("Threshold...");
setAutoThreshold("Mean dark");
setThreshold(52, 255);
run("Convert to Mask");
run("Watershed");
run("Analyze Particles...", "size=30-150 circularity=0.50-1.00 show=Nothing display exclude clear include add");
