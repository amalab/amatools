import numpy
from scipy import ndimage
from scipy import interpolate
from matplotlib import pyplot

im=ndimage.imread('/Users/jpm/DATA/Joaquin/stitchedANDseparate.zip Folder/G1_L2_Sum.tif')

x=[0,200,300,500]
y=[50,250,300,450]
tck,u = interpolate.splprep([x,y],s=0)
unew = numpy.arange(0,1,0.01)
out = interpolate.splev(unew,tck)
pyplot.figure()
pyplot.axis([0,500,500,0])
pyplot.imshow(im)
pyplot.plot(out[0],out[1],c='white')

unew = numpy.arange(0,1,0.02)
out = interpolate.splev(unew,tck)

cols=[]
for i in range(len(out[0])):
    x,y=out[0][i],out[1][i]
    b=im[y-3:y+3,x-3:x+3,:]
    c=b.mean(0).mean(0)/256.0
    #print i,b,c
    #c=im[y,x,:]/256.0
    cols.append(c)
    
pyplot.scatter(out[0],out[1],s=500,c=cols)

pyplot.show()

pass