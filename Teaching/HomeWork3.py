# making a histogram of intensity values derived from an imagej cell segmentation
# the csv file you have already been playing with (moverstest.csv) contains some
# intensity values, so we could also use those. They are in the column headed 'I(val)'.

f = open('/Users/jpm/Progs/Teaching/Nanog_intensities.csv','rU')
lines = f.readlines()
values = []
values_under50 = []
values_50over = []
for l in lines:
    try:
        vals = l.split(',')
        v = float(vals[2])
        values.append(v)
        if v < 50:
            values_under50.append(v)
        else:
            values_50over.append(v)
    except:
        print 'not a number'
        
# make a histogram of the intensities using the matplotlib.pyplot.hist function
import matplotlib
binwidth = round((ceil(max(values))-floor(min(values)))/20)
bins = [floor(min(values))+x*binwidth for x in range(0,21)]
matplotlib.pyplot.hist([values_under50], bins=bins, histtype='bar', align='mid', color='y')
matplotlib.pyplot.hist([values_50over], bins=bins, histtype='bar', align='mid', color='m')
