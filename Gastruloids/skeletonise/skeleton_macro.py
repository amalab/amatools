from ij import IJ
from skeleton_analysis import AnalyzeSkeleton_,Graph,Edge,Vertex

#def walk(vertex):
#	branches=vertex.getBranches()
#	for 
 
# open image, blur, make b/w, skeletonize
imp = IJ.openImage("/Users/jpm/Progs/AMATools/Gastruloids/skeletonise/Mask of C1-140611_Chi_Pulse_DMH_Pulse_1 BF only-96.tif")
#IJ.run(imp,"Gaussian Blur...","sigma=5")
#IJ.run(imp,"Make Binary","")
IJ.run(imp,"Skeletonize","")
 
# run AnalyzeSkeleton
# (see http://fiji.sc/AnalyzeSkeleton 
# and http://fiji.sc/javadoc/skeleton_analysis/package-summary.html)
analSkel=AnalyzeSkeleton_()
analSkel.setup("",imp)
skelResult=analSkel.run(analSkel.NONE, False, True, None, True, True)

graph=skelResult.getGraph()[0]
root=graph.getRoot()
print root
#print root.getPredecessor()
#print root.getBranches()
#print skelResult.getListOfEndPoints() 

print 'Done.'