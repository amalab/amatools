import skimage.filter
import scipy
import scipy.cluster
import skimage.morphology
import skimage.measure
import skimage.draw
import skimage.segmentation
import numpy 
#import libtiff 
import tifffile
import glob
import cv2
import os
import math
import pickle
import PIL.Image,PIL.ImageDraw,PIL.ImageFont
import mahotas
from matplotlib import pyplot

#def read_image(filename,decimate=1):  
	#tiff_file=libtiff.TIFFfile(filename)
	#tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
	#sh=tiff_array.shape	
	#print sh
	#return tiff_array[:,::decimate,::decimate]

def read_image(filename,decimate=1):  
	tiff_file=tifffile.TIFFfile(filename)
	tiff_array=tiff_file.asarray()
	sh=tiff_array.shape	
	print sh
	return tiff_array[:,::decimate,::decimate]

def branchedPoints(skel):
	branches=[]
	branches.append(numpy.array([[0, 1, 0], 
		                         [1, 1, 1], 
		                         [0, 0, 0]]))
	branches.append(numpy.array([[1, 0, 1], 
		                         [0, 1, 0], 
		                         [1, 0, 0]]))
	branches.append(numpy.array([[0, 1, 0], 
		                         [1, 1, 0], 
		                         [0, 1, 0]]))
	branches.append(numpy.array([[1, 0, 0], 
		                         [0, 1, 0], 
		                         [1, 0, 1]]))
	branches.append(numpy.array([[0, 0, 0], 
		                         [1, 1, 1], 
		                         [0, 1, 0]]))
	branches.append(numpy.array([[0, 0, 1], 
		                         [0, 1, 0], 
		                         [1, 0, 1]]))
	branches.append(numpy.array([[0, 1, 0], 
		                         [0, 1, 1], 
		                         [0, 1, 0]]))
	branches.append(numpy.array([[1, 0, 1], 
		                         [0, 1, 0], 
		                         [0, 0, 1]]))

	branches.append(numpy.array([[0, 1, 0], 
		                         [1, 1, 0], 
		                         [0, 0, 1]]))		
	branches.append(numpy.array([[1, 0, 0], 
		                         [0, 1, 1], 
		                         [1, 0, 0]]))	
	branches.append(numpy.array([[0, 0, 1], 
		                         [1, 1, 0], 
		                         [0, 1, 0]]))	
	branches.append(numpy.array([[0, 1, 0], 
		                         [0, 1, 0], 
		                         [1, 0, 1]]))	
	branches.append(numpy.array([[1, 0, 0], 
		                         [0, 1, 1], 
		                         [0, 1, 0]]))
	branches.append(numpy.array([[0, 0, 1], 
		                         [1, 1, 0], 
		                         [0, 0, 1]]))
	branches.append(numpy.array([[0, 1, 0], 
		                         [0, 1, 1], 
		                         [1, 0, 0]]))
	branches.append(numpy.array([[1, 0, 1], 
		                         [0, 1, 0], 
		                         [0, 1, 0]]))

	branches.append(numpy.array([[1, 0, 1], 
		                         [0, 1, 0], 
		                         [1, 0, 1]]))
	branches.append(numpy.array([[0, 1, 0], 
		                         [1, 1, 1], 
		                         [0, 1, 0]]))

	results=[mahotas.morph.hitmiss(skel,b) for b in branches]
	return sum(results)

def endPoints(skel):
	endpoint1=numpy.array([[0, 0, 0],
		                   [0, 1, 0],
		                   [2, 1, 2]])

	endpoint2=numpy.array([[0, 0, 0],
		                   [0, 1, 2],
		                   [0, 2, 1]])

	endpoint3=numpy.array([[0, 0, 2],
		                   [0, 1, 1],
		                   [0, 0, 2]])

	endpoint4=numpy.array([[0, 2, 1],
		                   [0, 1, 2],
		                   [0, 0, 0]])

	endpoint5=numpy.array([[2, 1, 2],
		                   [0, 1, 0],
		                   [0, 0, 0]])

	endpoint6=numpy.array([[1, 2, 0],
		                   [2, 1, 0],
		                   [0, 0, 0]])

	endpoint7=numpy.array([[2, 0, 0],
		                   [1, 1, 0],
		                   [2, 0, 0]])

	endpoint8=numpy.array([[0, 0, 0],
		                   [2, 1, 0],
		                   [1, 2, 0]])

	ep1=mahotas.morph.hitmiss(skel,endpoint1)
	ep2=mahotas.morph.hitmiss(skel,endpoint2)
	ep3=mahotas.morph.hitmiss(skel,endpoint3)
	ep4=mahotas.morph.hitmiss(skel,endpoint4)
	ep5=mahotas.morph.hitmiss(skel,endpoint5)
	ep6=mahotas.morph.hitmiss(skel,endpoint6)
	ep7=mahotas.morph.hitmiss(skel,endpoint7)
	ep8=mahotas.morph.hitmiss(skel,endpoint8)
	ep = ep1+ep2+ep3+ep4+ep5+ep6+ep7+ep8
	return ep

def trim_skeleton(skel):
	loop=True

	while(loop):
		#get end points
		ends=endPoints(skel)
		label,num=scipy.ndimage.label(ends)
		labels=numpy.unique(label).tolist()
		labels.remove(0)
		num_labels=len(labels)
		end_points=numpy.empty((num_labels,2))

		if len(labels)<=2:
			loop=False
		else:
			#get distances between end_points
			for i,l in enumerate(labels):
				end_points[i]=numpy.where(label==l)

			d=scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(end_points))
			coords_max=numpy.argwhere(d==d.max())[0]

			for i,l in enumerate(labels):
				if i not in coords_max:
					c=end_points[i].astype(int)
					#print c
					skel[c[0]][c[1]]=0

	return skel

def extend_skeleton(skel,ends,thresh):
	#ends 
	label,num=scipy.ndimage.label(ends)
	labels=numpy.unique(label).tolist()
	labels.remove(0)
	end_points=[numpy.where(label==l) for l in labels]

	if len(end_points)<2:
		return skel

	n=10
	ep1=[end_points[0][0][0],end_points[0][1][0]]
	ep2=[end_points[1][0][0],end_points[1][1][0]]
	nhood1=skel[ep1[0]-n:ep1[0]+n,ep1[1]-n:ep1[1]+n]
	nhood2=skel[ep2[0]-n:ep2[0]+n,ep2[1]-n:ep2[1]+n]

	if nhood1.shape!=(2*n,2*n) or nhood2.shape!=(2*n,2*n):
		print 'too close to edge - something wrong'	
		return skel 
	
	nhood1[n,n]=0 #remove end points	
	nhood2[n,n]=0 
	
	if nhood1.sum()==0 or nhood2.sum()==0:
		print 'nhood zero'
		return skel

	w1=numpy.where(nhood1==1)
	p1=[numpy.mean(w1[0])-n,numpy.mean(w1[1])-n] #ave of skeleton points near end point
	w2=numpy.where(nhood2==1)
	p2=[numpy.mean(w2[0])-n,numpy.mean(w2[1])-n] #ave of skeleton points near end point

	cont=True
	while cont:
		ep1_next=[int(ep1[0]-p1[0]),int(ep1[1]-p1[1])]
		if ep1_next[0]>=0 and ep1_next[0]<skel.shape[0] and ep1_next[1]>=0 and ep1_next[1]<skel.shape[1]:	
			r,c=skimage.draw.line(ep1[0],ep1[1],ep1_next[0],ep1_next[1])
			skel[r,c]=1
			ep1=ep1_next
		else:
			cont=False

	cont=True
	while cont:
		ep2_next=[int(ep2[0]-p2[0]),int(ep2[1]-p2[1])]
		if ep2_next[0]>=0 and ep2_next[0]<skel.shape[0] and ep2_next[1]>=0 and ep2_next[1]<skel.shape[1]:	
			r,c=skimage.draw.line(ep2[0],ep2[1],ep2_next[0],ep2_next[1])
			skel[r,c]=1
			ep2=ep2_next
		else:
			cont=False

	return numpy.multiply(skel,thresh)

def measure_skeleton(im,skel):
	m=[]
	ends=endPoints(skel)
	label,num=scipy.ndimage.label(ends)
	labels=numpy.unique(label).tolist()
	labels.remove(0)
	end_points=[numpy.where(label==l) for l in labels]
	if len(end_points)!=2:
		print "NOT 2 ENDS"
		return m
	
	print end_points
	
	def getNeighbour(im,(x,y)):
		for i in range(x-1,x+2):
			for j in range(y-1,y+2):
				if i is not x and j is not y:
					if im[i,j]>0:
						return (i,j)
		return None
	
	p=end_points[0]
	while True:
		r=getNeighbour(skel,p)
		if not r:
			break
		#print r,im[r]
		m.append( (r,im[r]) ) #(x,y),fluor
		p=r
		skel[r]=0
	
	return m
		
def find_nearest(array,value):
	idx = (numpy.abs(array-value)).argmin()
	return idx

class Expt:
	expt_list=[]
	def __init__(self,glob_loc):
		self.glob_loc=glob_loc
		self.files=glob.glob(glob_loc+'/*.jpg')+glob.glob(glob_loc+'/*.tif')
		Expt.expt_list.append(self)
		self.images=[]
		self.skels=[]
		self.areas=[]
		self.lengths=[]

class AutoVivification(dict):
	"""Implementation of perl's autovivification feature."""
	def __getitem__(self, item):
		try:
			return dict.__getitem__(self, item)
		except KeyError:
			value = self[item] = type(self)()
			return value

#########
### create pickle       
#E=AutoVivification()
#expt=141
#for med in ['2i','ES']:
	#for cond in ['NNC','NND']:
		#E[expt][48][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-17 (03-141) Gastruloids BraGFP 2i Exp (R3)/48h (Day2-3 10x)_1')      
		#E[expt][72][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-17 (03-141) Gastruloids BraGFP 2i Exp (R3)/72h (Day3-4 10x)_2')      
		#E[expt][96][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-17 (03-141) Gastruloids BraGFP 2i Exp (R3)/96h (Day4-5 10x)_1')      
		#E[expt][120][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-17 (03-141) Gastruloids BraGFP 2i Exp (R3)/120h (Day5-6 10x)_1')      

#for i,e in enumerate(Expt.expt_list):
	#print i,len(e.files)

#print ''

#for i,expt in enumerate([141]):
	#for j,time in enumerate([48,72,96,120]):
		#for k,medium in enumerate(['2i','ES']):
			#for l,cond in enumerate(['NNC','NND']):
				#print i,j,k,l,expt,time,medium,cond

				#e=E[expt][time][medium][cond]

				#files=[f for f in e.files if medium in os.path.split(f)[1] and cond in os.path.split(f)[1]]

				#for n,fn in enumerate(files):
					#print n

					##create for pickle
					#orig=read_image(fn,decimate=2) #all channels    
					#e.images.append(orig)

## save to pickle
#pickle.dump(E,file('expts_%d.pickle'%expt,'wb'),protocol=pickle.HIGHEST_PROTOCOL)
#########

###### read pickle & skeletonise
E=pickle.load(file('141/expts_141.pickle','rb'))
w=512
h=512
BRIGHT=0
FLUOR=1
N=2

fig=pyplot.figure(figsize=(20,10))
fig.suptitle('GFP')
p=fig.add_subplot(1,1,1)

expt=141
#for m,medium in enumerate(['2i','ES']):
#	for c,cond in enumerate(['NNC','NND']):

for m,medium in enumerate(['2i']):
	for c,cond in enumerate(['NNC']):

		regions=[]
		#for t,time in enumerate([48,72,96,120]): 
		for t,time in enumerate([120]): 
			print medium,cond,time

			e=E[expt][time][medium][cond]
			e.skels=[]
			e.areas=[]
			e.lengths=[]

			files=e.files

			for k,image in enumerate(e.images[0:N]):
				ss='GFP-'+str(expt)+'-'+str(time)+'-'+medium+'-'+cond+'-'+str(k)				
				print ss

				#read
				orig=image[BRIGHT]
				print orig.shape,orig.min(),orig.max()
				orig=orig.astype('float')
				immin=orig.min()
				immax=orig.max()
				orig/=immax
				orig=(orig*256.).astype('uint8')		
				
				gfp=image[FLUOR]
				gfp=gfp.astype('float')
				immin=gfp.min()
				immax=gfp.max()
				gfp/=immax
				gfp=(gfp*256.).astype('uint8')				
				
				#smooth
				sigma=12.0
				smooth=scipy.ndimage.filters.gaussian_filter(orig,(sigma,sigma))
				sigma=6.0
				smooth_gfp=scipy.ndimage.filters.gaussian_filter(gfp,(sigma,sigma))
			
				#thresh
				threshold=skimage.filter.threshold_yen(smooth)
				thresh=(smooth<threshold) #binary 0 & 1
				thresh=thresh.astype('uint8')
				
				#remove border
				skimage.segmentation.clear_border(thresh)
			
				#filter labels
				label,num=scipy.ndimage.label(thresh)
				labels=numpy.unique(label).tolist()
				labels.remove(0) #remove background
				
				regions={l:numpy.where(label==l) for l in labels}
				#centres={l:(numpy.mean(r[0]),numpy.mean(r[1])) for l,r in regions.iteritems()}
				#dist_to_middles={l:math.sqrt((c[0]-w/2)**2+(c[1]-h/2)**2) for l,c in centres.iteritems()}
				areas={l:len(region[0]) for l,region in regions.iteritems()}		
				
				#labels_tokeep=labels[:]
				
				#if len(labels_tokeep)>1:				
					#for l in labels_tokeep[:]:
						#if dist_to_middles[l]>w/2:
							#labels_tokeep.remove(l)
						
				if len(labels)>1:			
					#tokeep=find_nearest(numpy.array(sizes),10000)
					tokeep,v=max(areas.items(),key=lambda(_,v): abs(v))
				elif len(labels)==1:
					tokeep=labels[0]
				else:
					print '------NOTHING TO KEEP------'
					e.areas.append(None)
					e.lengths.append(None)
					e.skels.append(None)
					continue
				
				for l in labels:
					if l != tokeep:
						thresh[regions[l]]=0	
							
				e.areas.append(areas[tokeep])	
				print areas,'keep:',areas[tokeep]
			
				#skeletonize
				skel=skimage.morphology.skeletonize(thresh).astype('uint8')
				#trim	
				skel=trim_skeleton(skel)
			
				#ends 
				ends=endPoints(skel)
				label,num=scipy.ndimage.label(ends)
				labels=numpy.unique(label).tolist()
				labels.remove(0)
				end_points=[numpy.where(label==l) for l in labels]
			
				#branches
				branches=branchedPoints(skel)
				label,num=scipy.ndimage.label(branches)
				labels=numpy.unique(label).tolist()
				labels.remove(0)
				branch_points=[numpy.where(label==l) for l in labels]
			
				print 'ends,branches',ends.sum(),branches.sum()	
			
				#extend
				skel_ext=extend_skeleton(skel,ends,thresh)
				e.skels.append(skel_ext)
			
				#measure1
				length=skel_ext.sum()
				e.lengths.append(length)
				print length
				
				#measure2
				m=measure_skeleton(smooth_gfp,skel_ext)
				if len(m)>0:
					y=[a[1] for (i,a) in enumerate(m)] #fluor
					x=[i for (i,a) in enumerate(m)]
					p.plot(x,y,label=ss)
					
					#fit poly
					c=numpy.polyfit(x,y,10)[::-1] #reverse coz coeff order of polyval opposite to polyfit
					py=numpy.polynomial.polynomial.polyval(x,c)
					p.plot(x,py)
				
				#make composite image				
				#brightfield
				o=orig
				canvas=PIL.Image.new('RGB',(o.shape[0]*2,o.shape[1]*2),(255,255,255))				
				#z=numpy.zeros(o.shape,dtype='uint8')
				s=skel_ext*255
				b=branches*255
				en=ends*255
				a=numpy.array((numpy.maximum(o,s),numpy.maximum(o,en),numpy.maximum(o,b))).transpose()
				im=PIL.Image.fromarray(a,mode='RGB')
				draw=PIL.ImageDraw.Draw(im)
				draw.text((0,0),ss,fill='green',font=PIL.ImageFont.truetype("/Library/Fonts/Arial.ttf", 24))				
				#e.skels.append(numpy.asarray(im))
				canvas.paste(im,(0,0))
				
				#gfp
				o=gfp
				s=skel_ext*255
				b=branches*255
				en=ends*255
				a=numpy.array((numpy.maximum(o,s),numpy.maximum(o,en),numpy.maximum(o,b))).transpose()
				im=PIL.Image.fromarray(a,mode='RGB')
				canvas.paste(im,(o.shape[0],0))
				
				#thresh
				a=numpy.array((thresh*255,thresh*255,thresh*255)).transpose()				
				im=PIL.Image.fromarray(a,mode='RGB')
				canvas.paste(im,(o.shape[0],o.shape[1]))
				
				#smooth
				a=numpy.array((smooth,smooth,smooth)).transpose()				
				im=PIL.Image.fromarray(a,mode='RGB')
				canvas.paste(im,(0,o.shape[1]))
				
				
				canvas.save(str(expt)+'/'+ss+'.png')
				
#save pickle
pickle.dump(E,file('141/expts_skel_%d.pickle'%expt,'wb'),protocol=pickle.HIGHEST_PROTOCOL)

#save plot
#pyplot.show(block=False)
p.legend()
fig.savefig(str(expt)+'/'+ss+'_plot.svg')

#### time series
#fn='C1-140611_Chi_Pulse_DMH_Pulse_1 BF only.tif'

### orig
#orig=read_image(fn,decimate=2)
#sh=orig.shape
#orig=orig.astype('float')
#immin=orig.min()
#immax=orig.max()
#orig/=immax
#orig=(orig*256.).astype('uint8')

##orig=orig[48:]

## montage
#N=sh[0]
##N=4
#G=int(math.ceil(math.sqrt(N)))
#wim=sh[1]
#him=sh[2]
#w=G*wim
#h=G*him
#canvas=PIL.Image.new('RGB',(w,h),(255,255,255))
#for i in range(G):
	#for j in range(G):
		#print '.',
		#idx=i*G+j
		#if idx>=N:
			#continue
		#im=PIL.Image.fromarray(orig[idx],mode='L')
		#x=i*wim
		#y=j*him
		#canvas.paste(im,(x,y))  
#canvas.save('skeletonize_orig.png')
#print '\n'

### segment & skeletonize
#thresh=numpy.copy(orig)
#skel=numpy.copy(orig)
#ends=numpy.copy(orig)
#branches=numpy.copy(orig)
#region_areas=[]
#lengths=[]
#for i in range(N):	
	##smooth
	#sigma=6.0
	#smooth=scipy.ndimage.filters.gaussian_filter(orig[i],(sigma,sigma))

	##thresh
	#threshold=skimage.filter.threshold_yen(smooth)
	#thresh[i]=(smooth<threshold) #binary 0 & 1

	##filter labels
	#label,num=scipy.ndimage.label(thresh[i])
	#labels=numpy.unique(label)
	#regions=[numpy.where(label==l) for l in labels]
	#sizes=[len(region[0]) for region in regions]
	#tokeep=find_nearest(numpy.array(sizes),10000)
	#region_areas.append(sizes[tokeep])
	##print i,'size=',sizes[tokeep]

	#for l in labels:
		#if l != tokeep:
			#thresh[i][regions[l]]=0		

	##skeletonize
	#skel[i]=skimage.morphology.skeletonize(thresh[i])
	##trim	
	#skel[i]=trim_skeleton(skel[i])

	##ends 
	#ends[i]=endPoints(skel[i])
	#label,num=scipy.ndimage.label(ends[i])
	#labels=numpy.unique(label).tolist()
	#labels.remove(0)
	#end_points=[numpy.where(label==l) for l in labels]

	##branches
	#branches[i]=branchedPoints(skel[i])
	#label,num=scipy.ndimage.label(branches[i])
	#labels=numpy.unique(label).tolist()
	#labels.remove(0)
	#branch_points=[numpy.where(label==l) for l in labels]

	#print str(i)+':',ends[i].sum(),branches[i].sum()	

	##extend
	#skel[i]=extend_skeleton(skel[i],ends[i],thresh[i])

	##measure
	#length=skel[i].sum()
	#print length
	#lengths.append(length)

##montages
#canvas=PIL.Image.new('RGB',(w,h),(255,255,255))
#for i in range(G):
	#for j in range(G):
		#print '.',
		#idx=i*G+j
		#if idx>=N:
			#continue
		#im=PIL.Image.fromarray(thresh[idx]*255,mode='L')
		#x=i*wim
		#y=j*him
		#canvas.paste(im,(x,y))  
#canvas.save('skeletonize_thresh.png')
#print '\n'

#canvas=PIL.Image.new('RGB',(w,h),(255,255,255))
#for i in range(G):
	#for j in range(G):
		#print '.',
		#idx=i*G+j
		#if idx>=N:
			#continue

		#o=orig[idx]
		#z=numpy.zeros(o.shape,dtype='uint8')
		#s=skel[idx]*255
		#b=branches[idx]*255
		#e=ends[idx]*255
		#a=numpy.array((numpy.maximum(o,s),numpy.maximum(o,e),numpy.maximum(o,b))).transpose()
		#im=PIL.Image.fromarray(a,mode='RGB')
		#draw=PIL.ImageDraw.Draw(im)
		#draw.text((0,0),str(idx),fill='black',font=PIL.ImageFont.truetype("/Library/Fonts/Arial.ttf", 36))
		#x=i*wim
		#y=j*him
		#canvas.paste(im,(x,y))
#canvas.save('skeletonize_skel.png')
#print '\n'

#canvas=PIL.Image.new('RGB',(w,h),(255,255,255))
#for i in range(G):
	#for j in range(G):
		#print '.',
		#idx=i*G+j
		#if idx>=N:
			#continue

		#im=PIL.Image.fromarray(skel[idx]*255,mode='L')
		#draw=PIL.ImageDraw.Draw(im)
		#draw.text((0,0),str(idx),fill='black',font=PIL.ImageFont.truetype("/Library/Fonts/Arial.ttf", 36))
		#x=i*wim
		#y=j*him
		#canvas.paste(im,(x,y))
#canvas.save('skeletonize_skel_analysis.png')
#print '\n'


###save
#f=open('results.pickle','w')
#pickle.dump(region_areas,f)
#pickle.dump(lengths,f)