import pickle

class Expt:
	expt_list=[]
	def __init__(self,glob_loc):
		self.glob_loc=glob_loc
		self.files=glob.glob(glob_loc+'/*.jpg')+glob.glob(glob_loc+'/*.tif')
		Expt.expt_list.append(self)
		self.images=[]
		self.skels=[]
		self.areas=[]
		self.lengths=[]

class AutoVivification(dict):
	"""Implementation of perl's autovivification feature."""
	def __getitem__(self, item):
		try:
			return dict.__getitem__(self, item)
		except KeyError:
			value = self[item] = type(self)()
			return value

###### read pickle & skeletonise
E=pickle.load(file('141/expts_skel_141.pickle','rb'))
w=512
h=512
BRIGHT=0
FLUOR=1
N=-1

expt=141
for m,medium in enumerate(['2i']):
	for c,cond in enumerate(['NNC']):
		for t,time in enumerate([120]): 
			print expt,medium,cond,time

			e=E[expt][time][medium][cond]

			
			#measure_skeleton
			m=[]
			while True:
				ends=endPoints(skel)
				label,num=scipy.ndimage.label(ends)
				labels=numpy.unique(label).tolist()
				labels.remove(0)
				end_points=[numpy.where(label==l) for l in labels]
				if len(end_points)==0:
					break