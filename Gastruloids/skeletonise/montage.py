import tifffile
import glob
import math
import PIL.Image,PIL.ImageDraw

def read_image(filename,decimate=1):  
	tiff_file=tifffile.TIFFfile(filename)
	tiff_array=tiff_file.asarray() #need [:] to convert from TiffArray instance to numpy array
	sh=tiff_array.shape	
	print sh
	return tiff_array[::decimate,::decimate]

d='/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-29 (03-134) NotchDeltaE Dox or No Dox/NN(C)/'
files=glob.glob(d+'*.tif')
images=[] #all images

#load each tif into numpy array
for f in files:
	orig=read_image(f,decimate=4)
	sh=orig.shape
	orig=orig.astype('float')
	immin=orig.min()
	immax=orig.max()
	orig/=immax
	orig=(orig*256.).astype('uint8')
	
	images.append(orig)

# Create a new image with a white background
N=len(images)
G=int(math.ceil(math.sqrt(N)))
sh=images[0].shape
wim=sh[0] #width and height of single image
him=sh[1]
w=G*wim #width and height of montage
h=G*him
canvas=PIL.Image.new('RGB',(w,h),(255,255,255))
draw=PIL.ImageDraw.Draw(canvas)

for i in range(G):
	for j in range(G):
		print '.',
		idx=i*G+j
		if idx>=N:
			continue
		im=PIL.Image.fromarray(images[idx],mode='L')
		x=i*wim
		y=j*him
		canvas.paste(im,(x,y))  

canvas.save('montage.png')