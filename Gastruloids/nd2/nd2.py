import nd2reader
import math
import scipy.ndimage
import skimage.filter,skimage.segmentation,skimage.io,skimage.draw
import numpy
import PIL.Image,PIL.ImageDraw,PIL.ImageFont
import mahotas

def segment(orig):
	#read
	orig=orig.astype('float')
	immin=orig.min()
	immax=orig.max()
	print orig.shape,immin,immax
	
	orig/=immax
	orig=(orig*256.).astype('uint8')		
				
	#smooth
	sigma=6.0
	smooth=scipy.ndimage.filters.gaussian_filter(orig,(sigma,sigma))
	
	#thresh
	threshold=skimage.filter.threshold_yen(smooth)
	thresh=(smooth<threshold) #binary 0 & 1
	thresh=thresh.astype('uint8')
	
	#remove border
	cleared=numpy.ndarray.copy(thresh)
	skimage.segmentation.clear_border(cleared)
	
	#filter labels
	label,num=scipy.ndimage.label(cleared)
	labels=numpy.unique(label).tolist()
	labels.remove(0) #remove background
	
	regions={l:numpy.where(label==l) for l in labels}
	areas={l:len(region[0]) for l,region in regions.iteritems()}		
			
	if len(labels)>1:			
		#tokeep=find_nearest(numpy.array(sizes),10000)
		tokeep,v=max(areas.items(),key=lambda(_,v): abs(v)) #keep largest
	elif len(labels)==1:
		tokeep=labels[0]
	else:
		print '------NOTHING TO KEEP------'

	#zero everything not kept
	for l in labels:
		if l != tokeep:
			cleared[regions[l]]=0	
			
	return orig,smooth,thresh,cleared

def montage(images,filename):
	#save montsge .png of images
	
	# Create a new image with a white background
	N=len(images)
	G=int(math.ceil(math.sqrt(N)))
	D=2 #decimate
	img=images[0][::D,::D]
	sh=img.shape
	wim=sh[0] #width and height of single image
	him=sh[1]
	w=G*wim #width and height of montage
	h=G*him
	canvas=PIL.Image.new('RGB',(w,h),(255,255,255))
	draw=PIL.ImageDraw.Draw(canvas)
	
	for i in range(G):
		for j in range(G):
			idx=i*G+j
			#print idx,
			if idx>=N:
				continue
			
			im=images[idx][::D,::D]
			
			im2=im.astype('float')/float(im.max())
			im2=(im2*255.0).astype('uint8')
			pim=PIL.Image.fromarray(im2,mode='L')
			x=j*wim
			y=i*him
			canvas.paste(pim,(x,y))  
	
	canvas.save(filename+'_montage.png')
	

def skeletonize(orig,thresh,filename):
	def branchedPoints(skel):
		branches=[]
		branches.append(numpy.array([[0, 1, 0], 
			                         [1, 1, 1], 
			                         [0, 0, 0]]))
		branches.append(numpy.array([[1, 0, 1], 
			                         [0, 1, 0], 
			                         [1, 0, 0]]))
		branches.append(numpy.array([[0, 1, 0], 
			                         [1, 1, 0], 
			                         [0, 1, 0]]))
		branches.append(numpy.array([[1, 0, 0], 
			                         [0, 1, 0], 
			                         [1, 0, 1]]))
		branches.append(numpy.array([[0, 0, 0], 
			                         [1, 1, 1], 
			                         [0, 1, 0]]))
		branches.append(numpy.array([[0, 0, 1], 
			                         [0, 1, 0], 
			                         [1, 0, 1]]))
		branches.append(numpy.array([[0, 1, 0], 
			                         [0, 1, 1], 
			                         [0, 1, 0]]))
		branches.append(numpy.array([[1, 0, 1], 
			                         [0, 1, 0], 
			                         [0, 0, 1]]))
	
		branches.append(numpy.array([[0, 1, 0], 
			                         [1, 1, 0], 
			                         [0, 0, 1]]))		
		branches.append(numpy.array([[1, 0, 0], 
			                         [0, 1, 1], 
			                         [1, 0, 0]]))	
		branches.append(numpy.array([[0, 0, 1], 
			                         [1, 1, 0], 
			                         [0, 1, 0]]))	
		branches.append(numpy.array([[0, 1, 0], 
			                         [0, 1, 0], 
			                         [1, 0, 1]]))	
		branches.append(numpy.array([[1, 0, 0], 
			                         [0, 1, 1], 
			                         [0, 1, 0]]))
		branches.append(numpy.array([[0, 0, 1], 
			                         [1, 1, 0], 
			                         [0, 0, 1]]))
		branches.append(numpy.array([[0, 1, 0], 
			                         [0, 1, 1], 
			                         [1, 0, 0]]))
		branches.append(numpy.array([[1, 0, 1], 
			                         [0, 1, 0], 
			                         [0, 1, 0]]))
	
		branches.append(numpy.array([[1, 0, 1], 
			                         [0, 1, 0], 
			                         [1, 0, 1]]))
		branches.append(numpy.array([[0, 1, 0], 
			                         [1, 1, 1], 
			                         [0, 1, 0]]))
	
		results=[mahotas.morph.hitmiss(skel,b) for b in branches]
		return sum(results)
	
	def endPoints(skel):
		endpoint1=numpy.array([[0, 0, 0],
			                   [0, 1, 0],
			                   [2, 1, 2]])
	
		endpoint2=numpy.array([[0, 0, 0],
			                   [0, 1, 2],
			                   [0, 2, 1]])
	
		endpoint3=numpy.array([[0, 0, 2],
			                   [0, 1, 1],
			                   [0, 0, 2]])
	
		endpoint4=numpy.array([[0, 2, 1],
			                   [0, 1, 2],
			                   [0, 0, 0]])
	
		endpoint5=numpy.array([[2, 1, 2],
			                   [0, 1, 0],
			                   [0, 0, 0]])
	
		endpoint6=numpy.array([[1, 2, 0],
			                   [2, 1, 0],
			                   [0, 0, 0]])
	
		endpoint7=numpy.array([[2, 0, 0],
			                   [1, 1, 0],
			                   [2, 0, 0]])
	
		endpoint8=numpy.array([[0, 0, 0],
			                   [2, 1, 0],
			                   [1, 2, 0]])
	
		ep1=mahotas.morph.hitmiss(skel,endpoint1)
		ep2=mahotas.morph.hitmiss(skel,endpoint2)
		ep3=mahotas.morph.hitmiss(skel,endpoint3)
		ep4=mahotas.morph.hitmiss(skel,endpoint4)
		ep5=mahotas.morph.hitmiss(skel,endpoint5)
		ep6=mahotas.morph.hitmiss(skel,endpoint6)
		ep7=mahotas.morph.hitmiss(skel,endpoint7)
		ep8=mahotas.morph.hitmiss(skel,endpoint8)
		ep = ep1+ep2+ep3+ep4+ep5+ep6+ep7+ep8
		return ep
	
	def trim_skeleton(skel):
		loop=True
	
		while(loop):
			#get end points
			ends=endPoints(skel)
			label,num=scipy.ndimage.label(ends)
			labels=numpy.unique(label).tolist()
			labels.remove(0)
			num_labels=len(labels)
			end_points=numpy.empty((num_labels,2))
	
			if len(labels)<=2:
				loop=False
			else:
				#get distances between end_points
				for i,l in enumerate(labels):
					end_points[i]=numpy.where(label==l)
	
				d=scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(end_points))
				coords_max=numpy.argwhere(d==d.max())[0]
	
				for i,l in enumerate(labels):
					if i not in coords_max:
						c=end_points[i].astype(int)
						#print c
						skel[c[0]][c[1]]=0
	
		return skel
	
	def extend_skeleton(skel,ends,thresh):
		#ends 
		label,num=scipy.ndimage.label(ends)
		labels=numpy.unique(label).tolist()
		labels.remove(0)
		end_points=[numpy.where(label==l) for l in labels]
	
		if len(end_points)<2:
			return skel
	
		n=10
		ep1=[end_points[0][0][0],end_points[0][1][0]]
		ep2=[end_points[1][0][0],end_points[1][1][0]]
		nhood1=skel[ep1[0]-n:ep1[0]+n,ep1[1]-n:ep1[1]+n]
		nhood2=skel[ep2[0]-n:ep2[0]+n,ep2[1]-n:ep2[1]+n]
	
		if nhood1.shape!=(2*n,2*n) or nhood2.shape!=(2*n,2*n):
			print 'too close to edge - something wrong'	
			return skel 
		
		nhood1[n,n]=0 #remove end points	
		nhood2[n,n]=0 
		
		if nhood1.sum()==0 or nhood2.sum()==0:
			print 'nhood zero'
			return skel
	
		w1=numpy.where(nhood1==1)
		p1=[numpy.mean(w1[0])-n,numpy.mean(w1[1])-n] #ave of skeleton points near end point
		w2=numpy.where(nhood2==1)
		p2=[numpy.mean(w2[0])-n,numpy.mean(w2[1])-n] #ave of skeleton points near end point
	
		cont=True
		while cont:
			ep1_next=[int(ep1[0]-p1[0]),int(ep1[1]-p1[1])]
			if ep1_next[0]>=0 and ep1_next[0]<skel.shape[0] and ep1_next[1]>=0 and ep1_next[1]<skel.shape[1]:	
				r,c=skimage.draw.line(ep1[0],ep1[1],ep1_next[0],ep1_next[1])
				skel[r,c]=1
				ep1=ep1_next
			else:
				cont=False
	
		cont=True
		while cont:
			ep2_next=[int(ep2[0]-p2[0]),int(ep2[1]-p2[1])]
			if ep2_next[0]>=0 and ep2_next[0]<skel.shape[0] and ep2_next[1]>=0 and ep2_next[1]<skel.shape[1]:	
				r,c=skimage.draw.line(ep2[0],ep2[1],ep2_next[0],ep2_next[1])
				skel[r,c]=1
				ep2=ep2_next
			else:
				cont=False
	
		return numpy.multiply(skel,thresh)
	
	def measure_skeleton(im,skel):
		m=[]
		ends=endPoints(skel)
		label,num=scipy.ndimage.label(ends)
		labels=numpy.unique(label).tolist()
		labels.remove(0)
		end_points=[numpy.where(label==l) for l in labels]
		if len(end_points)!=2:
			print "NOT 2 ENDS"
			return m
		
		print end_points
		
		def getNeighbour(im,(x,y)):
			for i in range(x-1,x+2):
				for j in range(y-1,y+2):
					if i is not x and j is not y:
						if im[i,j]>0:
							return (i,j)
			return None
		
		p=end_points[0]
		while True:
			r=getNeighbour(skel,p)
			if not r:
				break
			#print r,im[r]
			m.append( (r,im[r]) ) #(x,y),fluor
			p=r
			skel[r]=0
		
		return m
			
	
	skel=skimage.morphology.skeletonize(thresh).astype('uint8')
	
	#trim	
	skel=trim_skeleton(skel)
	
	#ends 
	ends=endPoints(skel)
	label,num=scipy.ndimage.label(ends)
	labels=numpy.unique(label).tolist()
	labels.remove(0)
	end_points=[numpy.where(label==l) for l in labels]
	
	#branches
	branches=branchedPoints(skel)
	label,num=scipy.ndimage.label(branches)
	labels=numpy.unique(label).tolist()
	labels.remove(0)
	branch_points=[numpy.where(label==l) for l in labels]
	
	print 'ends,branches',ends.sum(),branches.sum()	
	
	#extend
	skel_ext=extend_skeleton(skel,ends,thresh)
	
	#measure1
	length=skel_ext.sum()
	
	##measure2
	#m=measure_skeleton(smooth_gfp,skel_ext)
	#if len(m)>0:
		#y=[a[1] for (i,a) in enumerate(m)] #fluor
		#x=[i for (i,a) in enumerate(m)]
		#p.plot(x,y,label=ss)
		
		##fit poly
		#c=numpy.polyfit(x,y,10)[::-1] #reverse coz coeff order of polyval opposite to polyfit
		#py=numpy.polynomial.polynomial.polyval(x,c)
		#p.plot(x,py)
	
	#make composite image				
	#brightfield
	o=orig#*255
	canvas=PIL.Image.new('RGB',(o.shape[0],o.shape[1]),(255,255,255))				
	#z=numpy.zeros(o.shape,dtype='uint8')
	s=skel_ext*255
	b=branches*255
	e=ends*255
	a=numpy.array((numpy.maximum(o,s).transpose(),numpy.maximum(o,e).transpose(),numpy.maximum(o,b).transpose())).transpose()
	#a=numpy.array((o.transpose(),o.transpose(),o.transpose())).transpose()
	im=PIL.Image.fromarray(a,mode='RGB')
	#draw=PIL.ImageDraw.Draw(im)
	#draw.text((0,0),ss,fill='green',font=PIL.ImageFont.truetype("/Library/Fonts/Arial.ttf", 24))				
	#e.skels.append(numpy.asarray(im))
	canvas.paste(im,(0,0))
	
	canvas.save(filename+'_skel.png')

### MAIN ###

f='/Users/jpm/Progs/AMATools/Gastruloids/nd2/2015-03-16 bragfp 2iexp_ES-Chi_6'
D=4 #decimate
nd2=nd2reader.Nd2(f+'.nd2')
num_t=nd2.time_index_count
num_channels=nd2.channel_count
num_zlevels=nd2.z_level_count
num_fovs=nd2.field_of_view_count	
channel_names=[n for n in nd2.channel_names]

for t in range(0,num_t,20):
#for t in [200]:
	print t
	orig=nd2.get_image(t, 0, channel_names[0], 0).data[::D,::D]	#time_index, fov, channel_name, z_level
	images=segment(orig)
	montage(images,f+'_'+str(t))
	
	#skeletonize
	orig=images[0]
	cleared=images[3]
	skel=skeletonize(orig,cleared,f+'_'+str(t))
	
	
	
	
	

