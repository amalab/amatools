import sys 
import PySide
sys.modules['PyQt4']=PySide # HACK for ImageQt
from PySide import QtCore,QtGui
import time 
import numpy
import tifffile #loads into numpy memory object
import glob
import os
from lxml import etree 
from PIL import Image,ImageDraw
import pandas
import pickle
from matplotlib import pyplot
import math
import scipy.interpolate

# M* are own subclasses of QtGui objects to access interaction

class MScene(QtGui.QGraphicsScene):
	#the scene
	def keyPressEvent(self,event):
		self.key_pressed=event.key()
		#print self.key_pressed

		if self.key_pressed==QtCore.Qt.Key_Delete:
			if isinstance(view.hover_item,MEllipse):
				view.line_path.removePoint(view.hover_item)
				view.updateGraphics()

		elif self.key_pressed==QtCore.Qt.Key_Equal:
			view.setViewScale(view.view_scale*2.0)

		elif self.key_pressed==QtCore.Qt.Key_Minus:
			view.setViewScale(view.view_scale/2.0)

	def keyReleaseEvent(self,event):
		#key_released=event.key()
		self.key_pressed=None

class MPixmap(QtGui.QGraphicsPixmapItem):
	# an image
	def mousePressEvent(self,event):
		mouse_pos=event.pos()
		button_pressed=event.button()        

		if view.hover_item is None:
			view.line_path.addPoint(mouse_pos)    
			view.updateGraphics()

      
class MEllipse(QtGui.QGraphicsEllipseItem):
	# a point  
	def init(self): #using init functions because super __init__ throws errors
		self.lineFrom=None
		self.lineTo=None
		self.setAcceptHoverEvents(True)
		self.setZValue(1)
		self.col=(255,255,255,255)
		self.col_hover=(255,255,0,255)

	def hoverEnterEvent(self,event):
		#print view.qscene.mouseGrabberItem()
		view.hover_item=self

		pen=self.pen()
		pen.setColor(QtGui.QColor(*self.col_hover)) 
		self.setPen(pen)        
		brush=self.brush()
		brush.setColor(QtGui.QColor(*self.col_hover)) 
		self.setBrush(brush)       

	def hoverLeaveEvent(self,event):
		view.hover_item=None

		pen=self.pen()
		pen.setColor(QtGui.QColor(*self.col)) 
		self.setPen(pen)
		brush=self.brush()
		brush.setColor(QtGui.QColor(*self.col)) 
		self.setBrush(brush)           

	def mousePressEvent(self,event): #need to override this to get mouseMoveEvents
		pass

	def mouseMoveEvent(self,event):
		#move a point and lines it's attached to

		mouse_pos=event.pos()
		mouse_pos_scene=event.scenePos()
		pos=self.pos()
		pos_scene=self.scenePos()

		#move point
		pos_new=pos+mouse_pos
		self.setPos(pos_new)

		#move lines
		if self.lineFrom:
			self.lineFrom.updatePos()

		if self.lineTo:
			self.lineTo.updatePos()

		view.updateGraphics()

       
class MLine(QtGui.QGraphicsLineItem):
	# a line 
	def init(self):
		self.setAcceptHoverEvents(True)
		self.col=(255,255,255,255)
		self.col_hover=(255,255,0,255)

	def hoverEnterEvent(self,event):
		view.hover_item=self

		pen=self.pen()
		pen.setColor(QtGui.QColor(*self.col_hover)) 
		self.setPen(pen)

	def hoverLeaveEvent(self,event):
		view.hover_item=None

		pen=self.pen()
		pen.setColor(QtGui.QColor(*self.col)) 
		self.setPen(pen)

	def mousePressEvent(self,event): #need to override this to get mouseMoveEvents
		mouse_pos=event.pos()
		button_pressed=event.button()   

		view.line_path.insertPoint(mouse_pos,self)
		view.updateGraphics()

	def mouseMoveEvent(self,event):
		pass

	def updatePos(self):
		#use associated points to update line position
		self.setPos(self.pointFrom.pos())
		#self.line().setP2(self.pointTo.pos()-self.pointFrom.pos())
		l=QtCore.QLineF(self.line())
		l.setP2((self.pointTo.pos()-self.pointFrom.pos()))#/view.view_scale)
		self.setLine(l)

class MLinePath(QtGui.QGraphicsSimpleTextItem):
	def init(self):        
		# lists of all points and lines
		self.points=[]
		self.lines=[]  
		self.point_size=10
		self.last_point=None

		self.col=(255,0,0,255)
		self.col_hover=(255,255,0,255)
		self.setText('LinePath1')     
		self.setBrush(QtGui.QBrush(QtGui.QColor(*self.col)))

		#enable/disable mouse events for interaction
		self.setEnabled(True)

	def clear(self):
		for p in self.points:
			view.qscene.removeItem(p)
		for l in self.lines:
			view.qscene.removeItem(l)

		self.points=[]
		self.lines=[]   
		self.last_point=None

	def addPoint(self,pos):
		#add point
		size=self.point_size  
		point=MEllipse(-size,-size,2*size,2*size)
		point.init()
		point.col=self.col
		point.col_hover=self.col_hover
		point.setPos(pos)#*view.view_scale)
		point.setPen(QtGui.QPen(QtGui.QColor(*self.col)))
		point.setBrush(QtGui.QBrush(QtGui.QColor(*self.col)))
		point.setParentItem(self)
		self.points.append(point)

		#add line
		if self.last_point:
			p1=self.last_point.pos()
			p2=point.pos()
			l=QtCore.QLineF(QtCore.QPointF(0.,0.),(p2-p1))#/view.view_scale)
			line=MLine(l)
			line.init()
			line.col=self.col
			line.col_hover=self.col_hover
			line.setPos(p1)
			pen=QtGui.QPen(QtGui.QColor(*self.col))
			pen.setWidth(4.0)
			line.setPen(pen)
			line.setParentItem(self)
			self.lines.append(line)

			self.last_point.lineFrom=line
			point.lineTo=line          
			line.pointFrom=self.last_point
			line.pointTo=point  

		self.last_point=point

	def insertPoint(self,pos,line):               
		#add point
		pos+=line.pos()
		size=self.point_size   
		point=MEllipse(-size,-size,2*size,2*size)
		point.init()
		point.col=self.col
		point.col_hover=self.col_hover        
		point.setPos(pos)#*view.view_scale)
		point.setPen(QtGui.QPen(QtGui.QColor(*self.col)))
		point.setBrush(QtGui.QBrush(QtGui.QColor(*self.col)))
		point.setParentItem(self)   
		self.points.insert(self.points.index(line.pointFrom)+1,point)

		#add new line
		p1=point.pos()
		p2=line.pointTo.pos()
		l=QtCore.QLineF(QtCore.QPointF(0.,0.),(p2-p1))#/view.view_scale)
		new_line=MLine(l)
		new_line.init()
		new_line.col=self.col
		new_line.col_hover=self.col_hover        
		new_line.setPos(p1)
		pen=QtGui.QPen(QtGui.QColor('red'))
		pen.setWidth(4.0)
		new_line.setPen(pen)
		new_line.setParentItem(self)
		self.lines.insert(self.lines.index(line)+1,new_line)

		#update links      
		point.lineFrom=new_line
		point.lineTo=line

		new_line.pointFrom=point
		new_line.pointTo=line.pointTo    
		line.pointTo.lineTo=new_line

		line.pointTo=point  
		line.updatePos()    
		#model.update()

	def removePoint(self,point):
		view.qscene.removeItem(point)
		self.points.remove(point)

		if point.lineTo: #2,3
			view.qscene.removeItem(point.lineTo)
			self.lines.remove(point.lineTo)
			point.lineTo.pointFrom.lineFrom=None

			if point.lineFrom: #2
				point.lineFrom.pointFrom=point.lineTo.pointFrom
				point.lineTo.pointFrom.lineFrom=point.lineFrom
				point.lineFrom.updatePos()

		elif point.lineFrom: #1
			view.qscene.removeItem(point.lineFrom)
			self.lines.remove(point.lineFrom)  
			point.lineFrom.pointTo.lineTo=None
			
		if point==self.last_point and len(self.points)>1:
			self.last_point=self.points[-1]

		view.hover_item=None		
		
		#model.update()
	def pointsAsList(self):
		pp=[]
		for p in self.points:
			pp.append(p.pos().toTuple())
		return pp

class MPolygon(QtGui.QGraphicsPolygonItem):
	def init(self):
		self.setBrush(QtGui.QBrush(QtGui.QColor(0,0,255,50)))
		self.points=[]

	def updateFromLinePath(self,line_path):
		self.points=[]
		
		if len(line_path.points)>2:			
			self.points.append(line_path.points[0].pos())
	
			for p in line_path.points[1::2]:
				self.points.append(p.pos())
	
			if len(line_path.points)%2==0:
				for p in line_path.points[-2:0:-2]:
					self.points.append(p.pos()) 
			else:
				for p in line_path.points[-1:0:-2]:
					self.points.append(p.pos())                 


		self.qpoly=QtGui.QPolygonF(self.points)
		self.setPolygon(self.qpoly)

class MBPolygon(QtGui.QGraphicsPolygonItem):
	def init(self):
		self.setBrush(QtGui.QBrush(QtGui.QColor(0,255,0,50)))
		self.spline_points=[] #to show
		self.spline1=[] #outside edges of measurement region
		self.spline2=[]
		self.degree=3
		
	def clear(self):			
		for p in self.spline_points:
			view.qscene.removeItem(p)			
		self.spline_points=[]

	def updateFromLinePath(self,line_path):
		self.clear()
		
		#need at least 3 points to fit cubic spline		
		if len(line_path.points)<8:
			return
		
		#fit b-spline to line path points
		self.spline1=[]
		x=[p.pos().x() for p in line_path.points[::2]]
		y=[p.pos().y() for p in line_path.points[::2]]
	
		self.tck,u=scipy.interpolate.splprep([x,y],k=self.degree)
			
		#create spline points	
		u=numpy.linspace(0.0,1.0,view.numSplinePoints)
		x,y=scipy.interpolate.splev(u,self.tck)	
		for i,(u,x,y) in enumerate(zip(u,x,y)):
			pos=QtCore.QPointF(x,y)
			self.spline1.append(pos)
			self.addSplinePoint(pos)
			
		#fit b-spline to line path points
		self.spline2=[]
		x=[p.pos().x() for p in line_path.points[1::2]]
		y=[p.pos().y() for p in line_path.points[1::2]]
	
		self.tck,u=scipy.interpolate.splprep([x,y],k=self.degree)
			
		#create spline points		
		u=numpy.linspace(0.0,1.0,view.numSplinePoints)
		x,y=scipy.interpolate.splev(u,self.tck)	
		for i,(u,x,y) in enumerate(zip(u,x,y)):
			pos=QtCore.QPointF(x,y)
			self.spline2.append(pos)
			self.addSplinePoint(pos)
			
		#create polygon
		points=[self.spline1[0]]
		points+=self.spline2 #down one side
		self.spline1.reverse() #and up the other
		points+=self.spline1
		self.spline1.reverse() #put back
           
		self.qpoly=QtGui.QPolygonF(points)
		self.setPolygon(self.qpoly)
		
			
	def addSplinePoint(self,pos):
		size=3
		point=MEllipse(-size,-size,2*size,2*size)
		point.col=(0,255,0,255)
		point.setPos(pos)
		point.setPen(QtGui.QPen(QtGui.QColor(*point.col)))
		point.setBrush(QtGui.QBrush(QtGui.QColor(*point.col)))
		point.setParentItem(self)	
		
		self.spline_points.append(point)	

class MSpline(MLinePath):
	#piece-wise linear
	def init(self):
		super(MSpline,self).init()

		self.point_size=5
		self.col=(0,0,255,255)       
		self.setEnabled(False) #cannot edit

	def updateFromLinepath(self,line_path):   
		self.clear()
		
		if len(line_path.points)<2:
			return

		for i in range(1,len(line_path.points)):
			p1=line_path.points[i-1].pos()
			p2=line_path.points[i].pos()
			mid_point=(p1+p2)/2.
			self.addPoint(mid_point)

class MBSpline(MLinePath):
	#cubic B-spline
	def init(self):
		super(MBSpline,self).init()

		self.point_size=5
		self.col=(0,255,0,255)       
		self.setEnabled(False) #cannot edit
		
		self.degree=3
		
		self.spline_points=[]
		
	def clear(self):			
		for p in self.spline_points:
			view.qscene.removeItem(p)			
		self.spline_points=[]
			
		super(MBSpline,self).clear()	
	
	def updateFromSpline(self,spline):   
		self.clear()
		
		if len(spline.points)<2:
			return

		#fit b-spline to linear spline points
		x=[p.pos().x() for p in spline.points]
		y=[p.pos().y() for p in spline.points]
			
		#need at least 3 points to fit cubic spline
		if len(x)<=self.degree:
			return
		
		#fit b-spline
		self.tck,u=scipy.interpolate.splprep([x,y],k=self.degree)
			
		#create spline points
		num_points=len(spline.points) 		
		u=numpy.linspace(0.0,1.0,num_points)
		x,y=scipy.interpolate.splev(u,self.tck)	
		for i,(u,x,y) in enumerate(zip(u,x,y)):
			#print i,u,x,y,d
			pos=QtCore.QPointF(x,y)
			self.addSplinePoint(pos)			
			
			
	def addSplinePoint(self,pos):
		size=3
		point=MEllipse(-size,-size,2*size,2*size)
		point.col=(0,255,0,255)
		point.setPos(pos)
		point.setPen(QtGui.QPen(QtGui.QColor(*point.col)))
		point.setBrush(QtGui.QBrush(QtGui.QColor(*point.col)))
		point.setParentItem(self)	
		
		self.spline_points.append(point)
		

class View(QtGui.QMainWindow):
	# the GUI view
	# View QWidget -> QVBoxLayout -> QGraphicsView(QWidget) -> QGraphicsScene  
	def __init__(self,parent=None):
		#QtGui.QMainWindow.__init__(self,parent)
		super(View, self).__init__()
		
		self.setWindowTitle('GutClicker')

		#toolbar
		self.toolbar=self.addToolBar('Tools')

		newProjectAction=QtGui.QAction('New Project',self)
		newProjectAction.triggered.connect(self.newProject)
		self.toolbar.addAction(newProjectAction)

		openProjectAction=QtGui.QAction('Open Project',self)
		openProjectAction.triggered.connect(self.openProject)
		#openProjectAction.setEnabled(False)
		self.toolbar.addAction(openProjectAction) 

		saveProjectAction=QtGui.QAction('Save Project',self)
		saveProjectAction.triggered.connect(self.saveProject)
		self.toolbar.addAction(saveProjectAction)
		
		self.toolbar.addWidget(QtGui.QLabel('     ')) #space
		
		spinBoxLabel=QtGui.QLabel('Channel')
		spinBoxLabel.setFont(QtGui.QFont("Arial",pointSize=11))
		self.toolbar.addWidget(spinBoxLabel)
		self.channelSpinBox=QtGui.QSpinBox(self)
		self.channelSpinBox.valueChanged[int].connect(self.channelChanged)
		self.toolbar.addWidget(self.channelSpinBox)
		
		zoomBoxLabel=QtGui.QLabel('Zoom')
		zoomBoxLabel.setFont(QtGui.QFont("Arial",pointSize=11))
		self.toolbar.addWidget(zoomBoxLabel)
		self.zoomSpinBox=QtGui.QSpinBox(self)
		self.zoomSpinBox.setRange(-3,3)
		self.zoomSpinBox.valueChanged[int].connect(self.zoomChanged)
		self.toolbar.addWidget(self.zoomSpinBox)
		
		splinePointsLabel=QtGui.QLabel('Spline Points')
		splinePointsLabel.setFont(QtGui.QFont("Arial",pointSize=11))
		self.toolbar.addWidget(splinePointsLabel)
		self.splineSpinBox=QtGui.QSpinBox(self)
		self.splineSpinBox.setRange(10,100)
		self.splineSpinBox.valueChanged[int].connect(self.splineChanged)
		self.toolbar.addWidget(self.splineSpinBox)
		
		chunksLabel=QtGui.QLabel('Chunks')
		chunksLabel.setFont(QtGui.QFont("Arial",pointSize=11))
		self.toolbar.addWidget(chunksLabel)
		self.chunksSpinBox=QtGui.QSpinBox(self)
		self.chunksSpinBox.setRange(10,100)
		self.toolbar.addWidget(self.chunksSpinBox)
		
		self.toolbar.addWidget(QtGui.QLabel('     ')) #space		
		
		clearAction=QtGui.QAction('Clear Path',self)
		clearAction.triggered.connect(self.clearGraphics)
		self.toolbar.addAction(clearAction)  

		measureAction=QtGui.QAction('Measure',self)
		measureAction.triggered.connect(self.measure)
		self.toolbar.addAction(measureAction)   
	
		#testAction=QtGui.QAction('Test',self)
		#testAction.triggered.connect(self.test)
		#self.toolbar.addAction(testAction)   		
		
		# init scene, view and layout
		self.qscene=MScene()
		self.qview=QtGui.QGraphicsView(self.qscene)

		qlayout=QtGui.QVBoxLayout()
		qlayout.addWidget(self.qview)
		self.setLayout(qlayout)
		self.setCentralWidget(self.qview)

		# overall scale of all objects in the view
		self.view_scale=1.0
		# if hovering over a point
		self.hover_item=None

		# view size
		self.resize(1000,1000)
		self.show()   
		self.qscene.setFocus()

		# NB- keep refs to all qt objects to avoid probs

		# project data
		self.poly=None
		self.tif_filename=''
		self.proj_filename=''
		self.tif_channel=0
		#self.dataframe=pandas.DataFrame(columns=['x','y','u','v','v_total','value'])   
		#self.num_measure_bins=100
		self.mpixmap=None
		self.numSplinePoints=10
		self.numChunks=10
		
	def test(self):
		pass
		
	def channelChanged(self,channel):
		#print channel
		self.tif_channel=channel
		if self.mpixmap:
			self.qscene.removeItem(self.mpixmap)
		self.initPixmap()
		self.updateGraphics()
		
	def zoomChanged(self,zoom):
		scale=2.0**zoom
		self.setViewScale(scale)
		
	def splineChanged(self,numPoints):
		self.numSplinePoints=numPoints
		self.updateGraphics()
				
	def setViewScale(self,scale):
		if scale>8.:
			scale=8.
		if scale<1./8.:
			scale=1./8.

		self.view_scale=scale
		self.mpixmap.setScale(self.view_scale)
		self.line_path.setScale(self.view_scale)
		self.poly.setScale(self.view_scale)
		#self.spline.setScale(self.view_scale)
		#self.bspline.setScale(self.view_scale)

		#fit scene to objects
		self.qscene.setSceneRect(self.qscene.itemsBoundingRect())

	#def wheelEvent(self,event):
		#print event.delta()

		#if event.delta()>0:
			#self.view_scale=(1.2*self.view_scale[0],1.2*self.view_scale[1])
		#else:
			#self.view_scale=(0.83*self.view_scale[0],0.83*self.view_scale[1])            

	def newProject(self):
		#self.tif_filename='./Hsp83[e64A]hetOR_PH3_20x_G10_sti+bld.tif'          

		self.tif_filename,sf=QtGui.QFileDialog.getOpenFileName(view,'Choose a .tif file...','.','*.tif')        
		if self.tif_filename=='':
			return
		
		self.readTiff()
		self.initProject()
		
	def initProject(self):
		# pixmap and line_path are root objects of scene
		# both are scaled by view_scale
		# points and lines are children of line_path

		#clear scene
		self.qscene.clear()  

		#image
		self.initPixmap()   

		#line_path
		self.line_path=MLinePath()
		self.line_path.init()
		self.line_path.setScale(self.view_scale)
		self.qscene.addItem(self.line_path)  
		self.line_path.setZValue(2)
		
		#poly
		#self.poly=MPolygon()
		#self.poly.init()
		#self.poly.setScale(self.view_scale)
		#self.qscene.addItem(self.poly)    
		#self.poly.setZValue(1)
		
		#B-poly
		self.poly=MBPolygon()
		self.poly.init()
		self.poly.setScale(self.view_scale)
		self.qscene.addItem(self.poly)    
		self.poly.setZValue(1)
		
		##spline
		#self.spline=MSpline()
		#self.spline.init()
		#self.spline.setScale(self.view_scale)
		#self.qscene.addItem(self.spline)    
		#self.spline.setZValue(1)   
		
		##B-spline
		#self.bspline=MBSpline()
		#self.bspline.init()
		#self.bspline.setScale(self.view_scale)
		#self.qscene.addItem(self.bspline)    
		#self.bspline.setZValue(1)   
		
		self.qscene.update()
		self.qscene.setFocus()

	def openProject(self):
		self.proj_filename,sf=QtGui.QFileDialog.getOpenFileName(self,'Open self..','.','*.proj')        
		if self.proj_filename=='':
			return

		pickle_file=open(self.proj_filename,'rb')
		self.tif_filename=pickle.load(pickle_file)
		self.num_tif_channels=pickle.load(pickle_file)
		self.tif_channel=pickle.load(pickle_file)
		self.dataframe=pickle.load(pickle_file)
		line_path_points=pickle.load(pickle_file)
		self.view_scale=pickle.load(pickle_file)
		self.numSplinePoints=pickle.load(pickle_file)
		self.numChunks=pickle.load(pickle_file)
		
		self.readTiff()
		self.initProject()
		
		for p in line_path_points:
			self.line_path.addPoint(QtCore.QPointF(p[0],p[1]))    
			self.updateGraphics()
			
		self.channelSpinBox.setValue(self.tif_channel)
		self.zoomSpinBox.setValue(int(math.log(self.view_scale,2)))
		self.splineSpinBox.setValue(self.numSplinePoints)

	def saveProject(self):
		self.proj_filename,sf=QtGui.QFileDialog.getSaveFileName(self,'Save project as..','.','*.proj')        
		if self.proj_filename=='':
			return

		pickle_file=open(self.proj_filename,'wb')
		pickle.dump(self.tif_filename,pickle_file,protocol=2) #binary fast protocol
		pickle.dump(self.num_tif_channels,pickle_file,protocol=2)
		pickle.dump(self.tif_channel,pickle_file,protocol=2)
		pickle.dump(self.dataframe,pickle_file,protocol=2)
		pickle.dump(self.line_path.pointsAsList(),pickle_file,protocol=2)
		pickle.dump(self.view_scale,pickle_file,protocol=2)
		pickle.dump(self.numSplinePoints,pickle_file,protocol=2)
		pickle.dump(self.numChunks,pickle_file,protocol=2)
		pickle_file.close()

	def readTiff(self):
		# read tiff image into numpy 
		self.tiffimg=tifffile.TIFFfile(self.tif_filename)
		self.tiff_array=self.tiffimg.asarray()	
		
		sh=self.tiff_array.shape
		if len(sh)==2:
			self.tiff_array=numpy.array((self.tiff_array,))
			sh=self.tiff_array.shape

		self.num_tif_channels=sh[0]
		self.channelSpinBox.setMaximum(self.num_tif_channels-1)
		self.channelSpinBox.setValue(self.tif_channel)
			
	def initPixmap(self):     
		# QImage -> QPixmap -> MPixmap(QGraphicsPixmapItem)

		#if self.mpixmap:
			#self.qscene.removeItem(self.mpixmap)
			#self.mpixmap=None
			
		# get chosen channel 
		self.image_array=self.tiff_array[self.tif_channel,:,:] 
		sh=self.image_array.shape
		self.image_w=sh[1]
		self.image_h=sh[0]
		
		#normalise intensity
		mn=self.image_array.min()
		mx=self.image_array.max()
		self.image_array=self.image_array.astype('float')
		self.image_array/=mx
		self.image_array*=255.0
		self.image_array=self.image_array.astype('uint8')
		
		# make good for Qt
		if self.image_w%2==1: #odd # columns causes problems - make even
			self.image_array=numpy.append(self.image_array,numpy.zeros((self.image_h,1),dtype='uint8'),1)
			self.image_w+=1

		# data for Qt QImage object
		self.data=numpy.ndarray.flatten(self.image_array)

		#self.data=a.copy() #to avoid contiguous array error

		self.qimage=QtGui.QImage(self.data,self.image_w,self.image_h,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.colortable=[QtGui.qRgb(i,i,i) for i in xrange(256)]
		self.qimage.setColorTable(self.colortable)        
		self.qpixmap=QtGui.QPixmap.fromImage(self.qimage)
		self.mpixmap=MPixmap(self.qpixmap)  
		self.mpixmap.setScale(self.view_scale)
		self.mpixmap.setFlag(QtGui.QGraphicsItem.GraphicsItemFlag.ItemIsFocusable)

		self.qscene.addItem(self.mpixmap)
		self.mpixmap.setZValue(0)
		self.qscene.update()

	def updateGraphics(self):  
		if not self.poly:
			return
		
		#called after any graphical editing 
		self.poly.updateFromLinePath(self.line_path)
		#self.spline.updateFromLinepath(self.line_path)
		#self.bspline.updateFromSpline(self.spline)
		self.qscene.update()
		
	def clearGraphics(self):
		self.initProject()		

	def measure(self):
		sp1=self.poly.spline1
		sp2=self.poly.spline2
		
		num_sections=len(sp1)-1
		self.numChunks=self.chunksSpinBox.value()
		
		sh=self.tiff_array.shape
		#channels=['channel_%s'%(ch) for ch in range(self.num_tif_channels)]
		channels=['channel_%s'%(ch) for ch in range(self.num_tif_channels)]
		self.dataframe=pandas.DataFrame(columns=['u','area','width','length']+channels)    		
		
		for s in range(num_sections):
			print '.',
			poly=(sp1[s].toTuple(),sp2[s].toTuple(),sp2[s+1].toTuple(),sp1[s+1].toTuple())
			
			#centre line across section
			cl=QtCore.QLineF((sp1[s]+sp1[s+1])/2,(sp2[s]+sp2[s+1])/2)
			#width across section
			w=cl.length()
			
			#centre line along section
			cl=QtCore.QLineF((sp1[s]+sp2[s])/2,(sp1[s+1]+sp2[s+1])/2)
			#section length
			l=cl.length()					
			
			# rasterise poly to pixels and then to numpy array
			#PIL
			img=Image.new('L',(sh[2],sh[1]),0)
			ImageDraw.Draw(img).polygon(poly,outline=0,fill=1)           
			mask=numpy.array(img) 		
			
			self.dataframe.loc[s,'u']=float(s)/float(num_sections)
			self.dataframe.loc[s,'area']=mask.sum()
			self.dataframe.loc[s,'width']=w
			self.dataframe.loc[s,'length']=l
			
			for ch in range(self.num_tif_channels):
				#m=(self.tiff_array[ch]*mask).sum()
				#d['channel_%s'%(ch+1)]=m
				#self.dataframe.loc[s,'channel_%s'%(ch+1)]=m
				
				#break this up into chunks across width
				dw1=(sp2[s]-sp1[s])/self.numChunks
				dw2=(sp2[s+1]-sp1[s+1])/self.numChunks
				m_list=[]
				for i in range(self.numChunks):
					p1=sp1[s]+i*dw1
					p2=p1+dw1
					p4=sp1[s+1]+i*dw2
					p3=p4+dw2
					
					poly=(p1.toTuple(),p2.toTuple(),p3.toTuple(),p4.toTuple())
					
					# rasterise poly to pixels and then to numpy array
					#PIL
					img=Image.new('L',(sh[2],sh[1]),0)
					ImageDraw.Draw(img).polygon(poly,outline=0,fill=1)           
					mask=numpy.array(img) 					
					
					m=(self.tiff_array[ch]*mask).sum()
					#m=mask.sum()
					m_list.append(m)
				self.dataframe.loc[s,'channel_%s'%(ch)]=m_list
								
		print '\n',self.dataframe
		sys.stdout.flush() 				

	#def measure_old(self):	
		## polygon = [(x1,y1),(x2,y2),...] or [x1,y1,x2,y2,...]
		## width = ?
		## height = ?
		#self.dataframe=pandas.DataFrame(columns=['x','y','u','v','v_total','value'])    

		#pointlist=self.line_path.pointsAsList()
		## split poly pointlist into 4 length sections overlapping by 2
		#sections=[]
		#for i in range(0,len(pointlist)):
			#section=pointlist[i:i+3]
			#if len(section)==3:
				#sections.append(section)

		#v_total=0
		## for each (triangle) poly section
		#for s in sections:
			## rasterise poly to pixels and then to numpy array
			##PIL
			#img=Image.new('L',(self.image_w,self.image_h),0)
			#ImageDraw.Draw(img).polygon(s,outline=0,fill=1)           
			#mask=numpy.array(img) 
			## get pixels as list of coords
			#pixels=numpy.nonzero(mask)
			#pixels_t=numpy.dstack((pixels[1],pixels[0]))[0] #make into list of (x,y) tuples shape (N,2)

			## and convert from x,y to u,v, the mapping coords wrt to spline midline of poly segment
			#s=numpy.array(s)
			
			#p0=(s[0]+s[1])/2 #(2,1) #spline start point for this section
			#p1=(s[1]+s[2])/2 #end
			#v=p1-p0 #spline vector
			#lv=numpy.linalg.norm(v) #length
			## rotate 90, so u is orthognoal to v
			#u=p1-p0 
			#t=u[0]
			#u[0]=u[1]
			#u[1]=-t		
			#lu=numpy.linalg.norm(u) #length
			
			#V=(pixels_t-p0).dot(v)/lv #projection onto V (p0->p1)
			#U=(pixels_t-p0).dot(u)/lu #projection onto u (p0->p1 rot 90)	
			#V_total=V+v_total #v accumulated along sections
			
			#for ch in range(self.num_tif_channels):
				## for each pixel get measurment
				#values=self.tiff_array[ch][pixels]
				#channel=numpy.ones(len(values))*ch
			
				## save in pandas dataframe
				#self.dataframe=self.dataframe.append(pandas.DataFrame({'x':pixels_t[:,0],'y':pixels_t[:,1],'channel':channel,'u':U,'v':V,'value':values,'v_total':V_total}))

			#v_total+=lv #accumulate v lengths of sections
			##print v_total
			##print s

		#pyplot.clf() #clear figure
		#d=self.dataframe
		#d=d[d['channel']==self.tif_channel] #select only records for current channel
		#V_hist=d['v_total']/(max(d['v_total'])/self.num_measure_bins) #scale V_total to be in range num_measure_bins
		#V_hist=abs(V_hist.astype(numpy.int16)) #make int and non-neg		
		##pyplot.plot(numpy.bincount(V_hist,weights=self.dataframe['value'])) #histogram V distances weighted by pixel values to give intensity along spline
		#bc=numpy.bincount(V_hist).astype(numpy.float64)
		#bcw=numpy.bincount(V_hist,weights=d['value'])
		#h=bcw/bc
		#pyplot.plot(h) #histogram V distances weighted by pixel values to give intensity along spline
		#pyplot.xlabel('Distance along spline (normalised)')
		#pyplot.ylabel('Average pixel intensity')
		#pyplot.show(block=False)
		#pass  
		

if __name__ == "__main__":

	# create app
	app=QtGui.QApplication.instance() # checks if QApplication already exists 
	if not app: # create QApplication if it doesnt exist 
		app=QtGui.QApplication(sys.argv)    

	#create view     
	view=View() 

	# run message loop
	app.exec_()