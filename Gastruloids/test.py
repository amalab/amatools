import pickle
from matplotlib import pyplot
import os
import scipy.misc
import numpy

def read_image(filename,channel=0,decimate=1):
    fi,ex=os.path.splitext(filename)
    
    if ex=='.tif':    
        tiff_file=libtiff.TIFFfile(filename)
        tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
        sh=tiff_array.shape	
        #print sh
        return tiff_array[channel][::decimate,::decimate]
    elif ex=='.jpg':
        a=scipy.misc.imread(filename)
        sh=a.shape
        #a=a.reshape((sh[2],sh[1],sh[0]))
        a=a.sum(2)
        return a[::decimate,::decimate].astype(numpy.uint16,copy=False)
    
class Expt:
    expt_list=[]
    def __init__(self,glob_loc):
        self.glob_loc=glob_loc
        self.files=glob.glob(glob_loc+'/*.jpg')+glob.glob(glob_loc+'/*.tif')
        Expt.expt_list.append(self)
        self.images=[]
        
class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

# read pickle
E=pickle.load(file('expts.pickle','rb'))
e=E[132]['2iL'][3]
orig=e.images[0]
pyplot.imshow(orig,cmap='bone')

im=read_image('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-27/From 2iL/2i2i(C)-1.jpg')
pyplot.imshow(im,cmap='bone')
pyplot.show()
pass