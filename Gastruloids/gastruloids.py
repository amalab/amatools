import matplotlib.pyplot as pyplot
import matplotlib.patches as mpatches
from  matplotlib import gridspec
import skimage.filter
import scipy
import scipy.cluster
import skimage.morphology
import skimage.measure
import numpy 
import libtiff 
import glob
import cv2
import os
import fnmatch
import math
import pickle

def read_image(filename,decimate=1):
    fi,ex=os.path.splitext(filename)
    if ex=='.tif':    
        tiff_file=libtiff.TIFFfile(filename)
        tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
        sh=tiff_array.shape	
        #print sh
        return tiff_array[:,::decimate,::decimate]
    
#def read_image(filename,channel=0,decimate=1):
    #fi,ex=os.path.splitext(filename)
    
    #if ex=='.tif':    
        #tiff_file=libtiff.TIFFfile(filename)
        #tiff_array=tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
        #sh=tiff_array.shape	
        ##print sh
        #return tiff_array[channel][::decimate,::decimate]
    #elif ex=='.jpg':
        #a=scipy.misc.imread(filename)
        #sh=a.shape
        ##a=a.reshape((sh[2],sh[1],sh[0]))
        #a=a.sum(2)
        #return a[::decimate,::decimate].astype(numpy.uint16,copy=False)
    
class Expt:
    expt_list=[]
    def __init__(self,glob_loc):
        self.glob_loc=glob_loc
        self.files=glob.glob(glob_loc+'/*.jpg')+glob.glob(glob_loc+'/*.tif')
        Expt.expt_list.append(self)
        self.images=[]
        
class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value
        
def find_nearest(array,value):
    idx = (numpy.abs(array-value)).argmin()
    return idx        

#########
### create pickle       
#E=AutoVivification()
        
#E[132]['2iL'][3]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-27/From 2iL')      
#E[132]['2iL'][4]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-28 Gastruloids BraGFP/From 2iL')       
#E[132]['2iL'][5]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-29 Gastruloids BraGFP/From 2iL')        
#E[132]['ESL'][3]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-27/From ESL')        
#E[132]['ESL'][4]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-28 Gastruloids BraGFP/From ESL')        
#E[132]['ESL'][5]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-29 Gastruloids BraGFP/From ESL')       

#E[133]['NICD'][3]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-133) Sox1GFP and NICD (N2,Chi)/2014-10-27 NN(C) /NICD')
#E[133]['NICD'][4]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-133) Sox1GFP and NICD (N2,Chi)/2014-10-28 NNC(N)')
#E[133]['NICD'][5]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-133) Sox1GFP and NICD (N2,Chi)/2014-10-29 NNCN(N)')
##E[133]['Sox1GFP'][3]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-133) Sox1GFP and NICD (N2,Chi)/2014-10-27 NN(C) /Sox1GFP')

#E[134]['Dox'][3]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-29 (03-134) NotchDeltaE Dox or No Dox/NN(C)')
#E[134]['Dox'][4]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-29 (03-134) NotchDeltaE Dox or No Dox/NNC(N)')
#E[134]['Dox'][5]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-29 (03-134) NotchDeltaE Dox or No Dox/NNCN(N)_1')
   
#for i,e in enumerate(Expt.expt_list):
    #print i,len(e.files)

#print ''
#nexpts=4
#ndays=3

#for i,expt in enumerate([(132,'2iL'),(132,'ESL'),(133,'NICD'),(134,'Dox')]):
    #for j,day in enumerate([3,4,5]):
        #print i,j,expt,day
        
        #e=E[expt[0]][expt[1]][day] # lab book, condition, day
        #files=e.files
        
        #for k,fn in enumerate(files):
            #print k
            
            ##create for pickle
            #orig=read_image(fn,channel=0,decimate=2)    
            #e.images.append(orig)

## save to pickle
#pickle.dump(E,file('expts.pickle','wb'),protocol=pickle.HIGHEST_PROTOCOL)
#########

#########
### create pickle       
#E=AutoVivification()
  
#for med in ['2i','ES']:
    #for cond in ['NNC','NND']:
        #E[143][48][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-19 (03-143) Gastruloids BraGFP 2i Exp (R4)/48h (Day2-3 10x)_1')      
        #E[143][72][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-19 (03-143) Gastruloids BraGFP 2i Exp (R4)/72h (Day3-4 10x)_1')      
        #E[143][96][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-19 (03-143) Gastruloids BraGFP 2i Exp (R4)/96h (Day4-5 10x)_1')      
        #E[143][120][med][cond]=Expt('/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-11-19 (03-143) Gastruloids BraGFP 2i Exp (R4)/120h (Day5-6 10x)/ca.120h (10x) Bra-GFP_1')      
   
#for i,e in enumerate(Expt.expt_list):
    #print i,len(e.files)

#print ''

#for i,expt in enumerate([143]):
    #for j,time in enumerate([48,72,96,120]):
        #for k,medium in enumerate(['2i','ES']):
            #for l,cond in enumerate(['NNC','NND']):
                #print i,j,k,l,expt,time,medium,cond
                
                #e=E[expt][time][medium][cond]
                
                #files=[f for f in e.files if medium in os.path.split(f)[1] and cond in os.path.split(f)[1]]
                
                #for n,fn in enumerate(files):
                    #print n
                    
                    ##create for pickle
                    #orig=read_image(fn,decimate=2) #all channels    
                    #e.images.append(orig)

## save to pickle
#pickle.dump(E,file('expts.pickle','wb'),protocol=pickle.HIGHEST_PROTOCOL)
#########

#########
### read pickle
### cluster images per medium & cond combo
#E=pickle.load(file('expts_143_4.pickle','rb'))

#w=512
#h=512
#BRIGHT=0
#FLUOR=1
#nfiles=20
     
#for m,medium in enumerate(['2i','ES']):
    #for c,cond in enumerate(['NNC','NND']):
        
        #regions=[]
        #for t,time in enumerate([48,72,96,120]): 
            #print medium,cond,time

            ##gs[i].append(gridspec.GridSpecFromSubplotSpec(10,10,subplot_spec=gs_outer))
            ##gs=gridspec.GridSpecFromSubplotSpec(10,20,subplot_spec=gs_outer[i,j],wspace=0.01,hspace=0.01)
            
            #e=E[143][time][medium][cond]
            #files=e.files
            

            #for k,fn in enumerate(files[0:nfiles]):
                #print k
                           
                ##read
                #orig=e.images[k]
                #print orig.shape,orig.min(),orig.max()
                
                #if orig is None:
                    #continue
                
                ###plot
                ##plt=fig.add_subplot(gs[k*2])
                ##plt.get_xaxis().set_visible(False)
                ##plt.get_yaxis().set_visible(False)
                ###plt.axis('off')
                
                ###orig
                ##plt.imshow(orig[FLUOR],cmap='bone')#,aspect='auto')
                
                ##smooth
                #sigma=6.0
                #smooth=scipy.ndimage.filters.gaussian_filter(orig[BRIGHT],(sigma,sigma))
                
                ##thresh
                #threshold=skimage.filter.threshold_yen(smooth)
                #thresh=smooth<threshold
                ##thresh=skimage.filter.threshold_adaptive(smooth,63,offset=10)
                ##plt.imshow(thresh,cmap='binary')#,aspect='auto')
                    
                ##label
                #label,nlabels=scipy.ndimage.label(thresh)
                
                #if nlabels==0:
                    #continue
                                                          
                ##props
                #props=skimage.measure.regionprops(label,intensity_image=orig[FLUOR])
                
                #areas=numpy.array([p.area for p in props])
                #idx=find_nearest(areas,10000)
                
                #if props[idx].area>20000: #too big
                    #continue
                
                #region=props[idx]
                
                ##add own properties for later clustering
                #region.orig=orig
                #region.time=time
                #region.medium=medium
                #region.cond=cond
 
                #dtc=math.sqrt((region.centroid[0]-w/2)**2+(region.centroid[1]-h/2)**2)
                #if dtc>w/5: #too off-centre
                    #continue
                
                #if region.eccentricity > 0.8: #too thin (to catch fluff etc.)
                    #continue
                
                ##add region to list
                #regions.append(region)                        
                                                       
                ### draw rectangle 
                ##minr, minc, maxr, maxc = region.bbox
                ##rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                          ##fill=False, edgecolor='red', linewidth=1)
                ##plt.add_patch(rect)
                
                ###plot
                ##plt=fig.add_subplot(gs[k*2+1])
                ##plt.get_xaxis().set_visible(False)
                ##plt.get_yaxis().set_visible(False)
                ###plt.axis('off')             
                ##plt.imshow(region.image,cmap='binary')#,aspect='auto')
                        
                        


        ##cluster
        ##X=[region.moments_central.flatten() for region in regions]
        ##X=[region.weighted_moments_central.flatten() for region in regions]
        #X=[region.weighted_moments_hu for region in regions]
        #d=scipy.cluster.hierarchy.distance.pdist(X,'euclidean')
        #Z=scipy.cluster.hierarchy.linkage(d,method='complete')
        
        #fig=pyplot.figure(figsize=(100,10))
        #gs_outer=gridspec.GridSpec(3,1,wspace=0.0,hspace=0.0)
        #plt_dend=fig.add_subplot(gs_outer[0,0])
        #dend=scipy.cluster.hierarchy.dendrogram(Z,count_sort='ascending',ax=plt_dend)
        #leaves=scipy.cluster.hierarchy.leaves_list(Z)
        
        ##gs2=gridspec.GridSpecFromSubplotSpec(1,1,subplot_spec=gs_outer[2,0],wspace=0.01,hspace=0.01)
        ##plt_text=fig.add_subplot(gs2[0])
        ##plt_text.get_xaxis().set_visible(False)
        ##plt_text.get_yaxis().set_visible(False)
        
        #gs1=gridspec.GridSpecFromSubplotSpec(1,len(regions),subplot_spec=gs_outer[1,0],wspace=0.01,hspace=0.01)
        #for i,r in enumerate(regions):
            ##plot
            #plt=fig.add_subplot(gs1[i])
            #plt.get_xaxis().set_visible(False)
            #plt.get_yaxis().set_visible(False)
            ##plt.axis('off')
            #region=regions[leaves[i]]
            #bb=region.bbox
            #fluor_image=region.orig[FLUOR,bb[0]:bb[2],bb[1]:bb[3]]
            #plt.imshow(fluor_image,cmap='bone')#,aspect='auto') 
            ##plt.imshow(region.image,cmap='binary')#,aspect='auto') 
            ##pyplot.title(str(region.time))
            #s=str(region.time)+'  '+region.medium+'  '+region.cond[-1]#+'   .'
            #plt.text((float(i)+0.5)/float(len(regions)),-1,s,transform=plt_dend.transAxes,rotation='vertical')
            
        
        ##for i,r in enumerate(regions):
            ###plot
        
            ##plt.get_xaxis().set_visible(False)
            ##plt.get_yaxis().set_visible(False)
            ##plt.text(0,0,'test')
            
        ##pyplot.tight_layout(pad=1.0)  
        #pyplot.savefig(medium+'_'+cond+'.png')  
            
#pyplot.show(block=True)       

######### 

########
## read pickle
## cluster all images at once
E=pickle.load(file('expts_143_4.pickle','rb'))

#nexpts=1
#ntimes=4
#fig=pyplot.figure(figsize=(15,15))
#gs_outer=gridspec.GridSpec(nexpts,ntimes,wspace=0.0,hspace=0.0)
#gs=[]
w=512
h=512
BRIGHT=0
FLUOR=1
regions=[]
nfiles=20

for i,expt in enumerate([143]):
    for j,time in enumerate([48,72,96,120]):       
        for k,medium in enumerate(['2i','ES']):
            for l,cond in enumerate(['NNC','NND']):
                print i,j,k,l,expt,time,medium,cond

                #gs[i].append(gridspec.GridSpecFromSubplotSpec(10,10,subplot_spec=gs_outer))
                #gs=gridspec.GridSpecFromSubplotSpec(10,20,subplot_spec=gs_outer[i,j],wspace=0.01,hspace=0.01)
                
                e=E[expt][time][medium][cond]
                files=e.files
                
                for k,fn in enumerate(files[0:nfiles]):
                    print k
                               
                    #read
                    orig=e.images[k]
                    print orig.shape,orig.min(),orig.max()
                    
                    if orig is None:
                        continue
                    
                    ##plot
                    #plt=fig.add_subplot(gs[k*2])
                    #plt.get_xaxis().set_visible(False)
                    #plt.get_yaxis().set_visible(False)
                    ##plt.axis('off')
                    
                    ##orig
                    #plt.imshow(orig[FLUOR],cmap='bone')#,aspect='auto')
                    
                    #smooth
                    sigma=6.0
                    smooth=scipy.ndimage.filters.gaussian_filter(orig[BRIGHT],(sigma,sigma))
                    
                    #thresh
                    threshold=skimage.filter.threshold_yen(smooth)
                    thresh=smooth<threshold
                    #thresh=skimage.filter.threshold_adaptive(smooth,63,offset=10)
                    #plt.imshow(thresh,cmap='binary')#,aspect='auto')
                                
                    ##fill holes
                    #filled=scipy.ndimage.morphology.binary_fill_holes(thresh)  
                    #label,nlabels=scipy.ndimage.label(filled)
                    
                    ##remove objects
                    #sizes=scipy.ndimage.sum(thresh,label,range(nlabels+1))               
                    ##small
                    #mask_size = sizes < 2000
                    #remove_pixel = mask_size[label]
                    ##remove_pixel.shape
                    #label[remove_pixel] = 0
                    ##large
                    #mask_size = sizes > 20000
                    #remove_pixel = mask_size[label]
                    ##remove_pixel.shape
                    #label[remove_pixel] = 0
                    
                    ##result
                    #result=label>0
                        
                    #label
                    label,nlabels=scipy.ndimage.label(thresh)
                    
                    if nlabels==0:
                        continue
                                                              
                    #props
                    props=skimage.measure.regionprops(label,intensity_image=orig[FLUOR])
                    
                    areas=numpy.array([p.area for p in props])
                    idx=find_nearest(areas,10000)
                    
                    if props[idx].area>20000: #too big
                        continue
                    
                    region=props[idx]
                    
                    #add own properties for later clustering
                    region.orig=orig
                    region.time=time
                    region.medium=medium
                    region.cond=cond
                    #regions.append(region)

                    dtc=math.sqrt((region.centroid[0]-w/2)**2+(region.centroid[1]-h/2)**2)
                    if dtc>w/5: #too off-centre
                        continue
                    
                    if region.eccentricity > 0.8: #too thin (to catch fluff etc.)
                        continue
                    
                    #add region to list
                    regions.append(region)                        
                                                           
                    ## draw rectangle 
                    #minr, minc, maxr, maxc = region.bbox
                    #rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                              #fill=False, edgecolor='red', linewidth=1)
                    #plt.add_patch(rect)
                    
                    ##plot
                    #plt=fig.add_subplot(gs[k*2+1])
                    #plt.get_xaxis().set_visible(False)
                    #plt.get_yaxis().set_visible(False)
                    ##plt.axis('off')             
                    #plt.imshow(region.image,cmap='binary')#,aspect='auto')
                            
                            


#cluster
#X=[region.moments_central.flatten() for region in regions]
#X=[region.weighted_moments_central.flatten() for region in regions]
X=[region.weighted_moments_hu for region in regions]
d=scipy.cluster.hierarchy.distance.pdist(X,'euclidean')
Z=scipy.cluster.hierarchy.linkage(d,method='single')#'complete')

fig=pyplot.figure(figsize=(100,10))
gs_outer=gridspec.GridSpec(3,1,wspace=0.0,hspace=0.0)
plt_dend=fig.add_subplot(gs_outer[0,0])
dend=scipy.cluster.hierarchy.dendrogram(Z,count_sort='ascending',ax=plt_dend)
leaves=scipy.cluster.hierarchy.leaves_list(Z)

#gs2=gridspec.GridSpecFromSubplotSpec(1,1,subplot_spec=gs_outer[2,0],wspace=0.01,hspace=0.01)
#plt_text=fig.add_subplot(gs2[0])
#plt_text.get_xaxis().set_visible(False)
#plt_text.get_yaxis().set_visible(False)

#fmins=[]
#fmaxs=[]
gs1=gridspec.GridSpecFromSubplotSpec(1,len(regions),subplot_spec=gs_outer[1,0],wspace=0.01,hspace=0.01)
for i,r in enumerate(regions):
    #plot
    plt=fig.add_subplot(gs1[i])
    plt.get_xaxis().set_visible(False)
    plt.get_yaxis().set_visible(False)
    #plt.axis('off')
    region=regions[leaves[i]]
    bb=region.bbox
    fluor_image=region.orig[FLUOR,bb[0]:bb[2],bb[1]:bb[3]]
    #fmins.append(numpy.min(fluor_image))
    #fmaxs.append(numpy.max(fluor_image))
    plt.imshow(fluor_image,cmap='bone')#,vmin=0,vmax=16384)#,aspect='auto') 
    #plt.imshow(region.image,cmap='binary')#,aspect='auto') 
    #pyplot.title(str(region.time))
    s=str(region.time)+'  '+region.medium+'  '+region.cond[-1]#+'   .'
    plt.text(float(i)/float(len(regions)),-1,s,transform=plt_dend.transAxes,rotation='vertical')
 
#print min(fmins),max(fmaxs)   

#for i,r in enumerate(regions):
    ##plot

    #plt.get_xaxis().set_visible(False)
    #plt.get_yaxis().set_visible(False)
    #plt.text(0,0,'test')
    
#pyplot.tight_layout(pad=1.0)  
pyplot.savefig('saved.png')  
pyplot.show(block=True)       

########  





            
##fn='/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-25 (03-132) BraGFP 2iExp (N2,Chi,DM)/2014-10-28 Gastruloids BraGFP/From 2iL/*.tif'
##fn='/Volumes/home/dat40/ESCs/Gastruloids/2014/'
#fn='/Volumes/home/dat40/ESCs/Gastruloids/2014/2014-10-29 (03-134) NotchDeltaE Dox or No Dox'

#files=[]
#for root, dirnames, filenames in os.walk(fn):
    #for filename in fnmatch.filter(filenames, '*.tif'):
        #files.append(os.path.join(root, filename))
#print len(files)

#i=0
#I=3
#J=3
#fsize=10
#ch=0 #brightfield
#fig_orig=pyplot.figure(figsize=(fsize,fsize))
#fig_result=pyplot.figure(figsize=(fsize,fsize))
#fig_contour=pyplot.figure(figsize=(fsize,fsize))

#for fn in files[100:I*J+100]:
    
    #tiff_array,sh=read_ometif(fn)
    #c=sh[0]
    #x=sh[1]
    #y=sh[2]
     
    ##orig
    #orig=tiff_array[ch]
    #p=fig_orig.add_subplot(I,J,i)
    #p.imshow(orig,cmap='bone')
        
    ##smooth
    #sigma=6.0
    #smooth=scipy.ndimage.filters.gaussian_filter(orig,(sigma,sigma))
    
    ##thresh
    #threshold=skimage.filter.threshold_yen(smooth)
    #thresh=smooth<threshold
    ##thresh=skimage.filter.threshold_adaptive(smooth,1001)
    
    ##fill holes
    #filled=scipy.ndimage.morphology.binary_fill_holes(thresh)  
    #label,nlabels=scipy.ndimage.label(filled)
    
    ##remove small objects
    #sizes=scipy.ndimage.sum(thresh,label,range(nlabels+1))
    #mask_size = sizes < 20000
    #remove_pixel = mask_size[label]
    #remove_pixel.shape
    #label[remove_pixel] = 0
    
    ##result
    #result=label>0
    #p=fig_result.add_subplot(I,J,i)
    #p.imshow(result,cmap='binary')
 
    #centre=numpy.mean(numpy.where(result))
    #print centre
    
    ##contour
    #result_bin=numpy.ndarray.astype(result,numpy.uint8)
    #co,hi=cv2.findContours(result_bin,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)# .CHAIN_APPROX_SIMPLE)
    
    #if len(co)>0:
        #x=[p[0][0] for p in co[0]]
        #y=[-p[0][1] for p in co[0]]
    #else:
        #x=[]
        #y=[]
        
    #p=fig_contour.add_subplot(I,J,i)
    #p.scatter(x,y)   
    
    ##moments
    #if len(co)>0:    
        #M=cv2.moments(co[0])
        #print M 
        #H=cv2.HuMoments(M)
        #print H
    
    #i=i+1


#pyplot.show(block=True)

#pass
