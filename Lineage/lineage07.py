#
# Read several track files saved from imagej Circadian Gene Expression 
# and plot with matplotlib
#
# jpm jun 2012
#

import numpy

import matplotlib
import matplotlib.pyplot as plt

############
### track data structures
class CellPos:
    def __init__(self):
        self.x=0 # x pos in px (XC)
        self.y=0 # y pos in px (YC)
        self.t=0 # time in mins (Time)
        self.ex=0 # expression in ? (Express)
        
        self.remove_flag=False

class Track:
    def __init__(self):
        self.cells=[]
        self.lineage='' #mother or daughter
        
    def length(self):
        t0=self.cells[0].t
        t1=self.cells[-1].t
        self.length=t1-t0
        return self.length
    
    def compute(self):
        self.ex=[c.ex for c in track.cells]
        self.ti=[c.t for c in track.cells]
        self.x=[c.x for c in track.cells]
        self.y=[c.y for c in track.cells]
        
        #ave expression
        self.ex_mean=numpy.mean(self.ex)
        
        self.dx=[]
        self.dy=[]
        for i in range(1,len(self.x)):
            self.dx.append(self.x[i]-self.x[i-1])
            self.dy.append(self.y[i]-self.y[i-1])
            
        self.dx_mean=numpy.mean(self.dx)
        self.dy_mean=numpy.mean(self.dy)
        
        #overall movement
        self.dx_overall=self.x[-1]-self.x[0]
        self.dy_overall=self.y[-1]-self.y[0]
        
        #start pos
        self.x0=self.x[0]
        self.y0=self.y[0]   
        
    def filter(self):
        dx_std=numpy.std(self.dx)
        dy_std=numpy.std(self.dy)
        
        m=1
        for i in range(0,len(self.x)-1):
            if abs(self.dx[i])>m*dx_std or abs(self.dy[i])>m*dy_std:
                self.cells[i].remove_flag=True
                
        for c in self.cells:
            if c.remove_flag:
                self.cells.remove(c)
                
        self.compute()
                      

# use column heading in csv file to get row index
def find_index_for_column(col):
    return column_headings.index(col)

def movingaverage(interval, window_size):
    window = numpy.ones(int(window_size))/float(window_size)
    return numpy.convolve(interval, window, 'same')

#################
### read csv files

# data[track][time][column]

import numpy as np
import glob
import os
#find all csv filenames in child directories
csv_directorys=[
r'/Users/jpm/DATA/dem41/DATA october/N2B27/raw data',
r'/Users/jpm/DATA/dem41/DATA october/N2B27+Act+Chi 1/raw data',
r'/Users/jpm/DATA/dem41/DATA october/N2B27 + Act + Chi 2/raw data'
]

num_plot_rows=2
num_plot_cols=3
fig,ax=plt.subplots(num_plot_rows,num_plot_cols)
plot_col=0
plot_row=0

for csv_directory in csv_directorys:
    csv_file_names=glob.glob(csv_directory+'/*.csv')
    
    tracks=[]    
    csv_count=0 # number of csv files

     
    # each csv file is a track
    # each line is a cell position   
    for csv_filename in csv_file_names:
        track=Track()
        
        csv_basename=os.path.basename(csv_filename)
        if csv_basename[1:].find('m')>0 or csv_basename[1:].find('M')>0:
            track.lineage='mother'
        else:
            track.lineage='daughter'
        
        csv_file=open(csv_filename)
        lines=csv_file.readlines()    
        
        #data.append([])
        line_count=0
        hash_found=False
        for line in lines:
            line_data_str=line.split(',')[0:-1] #strings
            if line[0]=='#': # find line starting # to get beginning of data
                hash_found=True
                column_headings=line.split(',')
                continue
            
            if hash_found:
                line_data_str2=['0' if len(x)==0 else x for x in line_data_str] # zero fill empty fields
                line_data=[] # floats
                for x in line_data_str2:
                    line_data.append(float(x))
                #line_data=[map(float,x) for x in line_data_str2]
                #data[csv_count].append(line_data)
                cell=CellPos()
                cell.x=line_data[find_index_for_column('XC [px]')]
                cell.y=line_data[find_index_for_column('YC [px]')]
                cell.t=line_data[find_index_for_column('Time [min]')]
                cell.ex=line_data[find_index_for_column('Mean1')]
                   
                if cell.x>0 or cell.y>0:   
                    track.cells.append(cell)    
                
            line_count+=1
        
        csv_count+=1
        
        track.length()
        tracks.append(track)
        
    pass
    
    ##########
    ### filter tracks
    
    for track in tracks:
        track.compute()
        track.filter()
    
    ##########
    ### Line plot
    plot_row=0
    for av in [1,8]:
                
        for track in tracks:#[0:10]:
            if track.length>0.0:        
                ex_ave=movingaverage(track.ex,av)[av/2:-av/2]
                
                #ex_norm=[c.ex/track.cells[0].ex for c in track.cells]
                #ex_ave_norm=numpy.array([e/ex_ave[0] for e in ex_ave])
                ex_ave_norm=numpy.array([e/track.ex_mean for e in ex_ave])
                ti_norm=[(c.t-track.cells[0].t)/track.length for c in track.cells]
                
                #ex_norm_ave=movingaverage(ex_norm,4)
                ti_norm=numpy.array(ti_norm)
                
                # if ti_norm.shape==ex_ave_norm.shape:        
                p=ax[plot_row,plot_col]
                p.plot(ti_norm[0:len(ex_ave_norm)],ex_ave_norm)
                #plt.plot(ti_norm[0:len(ex_ave)],ex_ave)
        
        plot_name=csv_directory.split('/')[-2]
        

        p.set_title(plot_name)
        #p.xlabel('Time')
        #p.ylabel('Expression normalised to Mean')
    
        plot_row+=1
        
    plot_col+=1
    
for c in range(num_plot_cols):
    ax[1,c].set_ylim(ax[0,c].get_ylim())
    
plt.show()    

    ###########
    #### 2D plot
    
    #track_count=0
    #dx_overall=[]
    #dy_overall=[]
    #x0=[]
    #y0=[]
    #ex_mean=[]
    #for track in tracks:#[-3:-1]:
        #if track.length>0.0:
            
            #plt.scatter(track.x,track.y,c=track.ex,s=40,cmap='YlGn')
            #plt.plot(track.x,track.y,color='black')
            
            #x0.append(track.x0)
            #y0.append(track.y0)
            #dx_overall.append(track.dx_overall)
            #dy_overall.append(track.dy_overall)
            #ex_mean.append(track.ex_mean)
            
            #track_count+=1        
            
    #plt.quiver(x0,y0,dx_overall,dy_overall,ex_mean,angles='xy',units='dots',scale_units='xy',scale=1.0,width=5,cmap='YlGn')
    #plt.show()
    

#np_data=np.array(data)

########
####orthogonal projection
#import numpy
#from mpl_toolkits.mplot3d import proj3d
#def orthogonal_proj(zfront, zback):
    #a = (zfront+zback)/(zfront-zback)
    #b = -2*(zfront*zback)/(zfront-zback)
    #return numpy.array([[1,0,0,0],
                        #[0,1,0,0],
                        #[0,0,a,b],
                        #[0,0,0,zback]])
#proj3d.persp_transformation = orthogonal_proj

########
#### 3d plot
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as plt
#import matplotlib as mpl
##import numpy as np

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')

##track_count=np_data.shape[0]
#track_count=0
#for track in tracks:#[0:10]:
    ##xs=np_data[track,:,1] # Time
    ##ys=np_data[track,:,12] # Mean1
    #if track.lineage is 'mother':
        #col='r'
    #elif track.lineage is 'daughter':
        #col='g'
    #else:
        #col='k'
        
    #xs=[c.x for c in track.cells]
    #ys=[c.y for c in track.cells]
    #zs=[c.ex for c in track.cells]
    #ax.plot(xs, ys, zs,c=col)# zs=track_count)
    
    #ax.scatter(xs, ys, zs,c=col,)#, zdir='y', c=c)    
    ##ax.scatter(xs, ys, 0,c=col)#, zdir='y', c=c)    
    
    #track_count+=1

#ax.set_xlabel('X')
#ax.set_ylabel('Y')
#ax.set_zlabel('fluorescence')

#plt.show()
