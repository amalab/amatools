from traits.api import HasTraits, Instance, List, Str, on_trait_change
from traitsui.api import View, Item, EnumEditor
from chaco.api import Plot, ArrayPlotData
from chaco.tools.api import PanTool,ZoomTool
from enable.component_editor import ComponentEditor

import numpy
import glob
import os

def moving_average(interval, window_size):
    window = numpy.ones(int(window_size))/float(window_size)
    return numpy.convolve(interval, window, 'same')   

### track data structures
class CellPos:
    def __init__(self):
        self.x=0 # x pos in px (XC)
        self.y=0 # y pos in px (YC)
        self.t=0 # time in mins (Time)
        self.ex=0 # expression in ? (Express)

class Track:
    def __init__(self):
        self.cells=[]
        self.lineage='' #mother or daughter
        
    def length(self):
        t0=self.cells[0].t
        t1=self.cells[-1].t
        self.length=t1-t0
        return self.length
    
### UI
class LinePlot(HasTraits):
    csv_directorys=List(Str)
    csv_directory=Str

    plot = Instance(Plot) 
    
    traits_view = View(
        Item('csv_directory',editor=EnumEditor(name='csv_directorys')),
        Item('plot',editor=ComponentEditor(), show_label=False),
        width=1200, height=800, resizable=True, title="CGE Tracks")
    
    tracks=[]
    

    #def __init__(self):
        ##super(LinePlot, self).__init__(self)
                    
        ##csv_directorys=[r'/Volumes/home/dem41/DATA october/N2B27/raw data',
                            ##r'/Volumes/home/dem41/DATA october/N2B27 + Act + Chi 2/raw data',
                            ##r'/Volumes/home/dem41/DATA october/N2B27+Act+Chi 1/raw data']        
         
        #csv_directory=csv_directorys[0]
        
    def _plot_default(self):  
        self.plotdata=ArrayPlotData()
        plot=Plot(self.plotdata) 
	
	# Attach some tools to the plot
	pantool=PanTool(plot, constrain_key="shift")
	plot.tools.append(pantool)
	zoomtool=ZoomTool(component=plot,tool_mode="box", always_on=False)
	plot.overlays.append(zoomtool)   	
	
        #self.csv_directory=self.csv_directorys[0]        
     
	#self.update()
        
        return plot
    
    @on_trait_change('csv_directory')
    def update(self):
        self.tracks=[]   
        self.make_tracks()
	self.delete_plots()
        self.make_plot() 	
    
    # use column heading in csv file to get row index
    def find_index_for_column(self,col):
        return self.column_headings.index(col)    
        
    def make_tracks(self):
        #find all csv filenames in child directories
        csv_file_names=glob.glob(self.csv_directory+'/*.csv')
        
        csv_count=0 # number of csv files
        
        # each csv file is a track
        # each line is a cell position        
        self.tracks=[]
        
        for csv_filename in csv_file_names:
            track=Track()
            
            csv_basename=os.path.basename(csv_filename)
            if csv_basename[1:].find('m')>0 or csv_basename[1:].find('M')>0:
                track.lineage='mother'
            else:
                track.lineage='daughter'
            
            csv_file=open(csv_filename)
            lines=csv_file.readlines()    
            
            #data.append([])
            line_count=0
            hash_found=False
            for line in lines:
                line_data_str=line.split(',')[0:-1] #strings
                if line[0]=='#': # find line starting # to get beginning of data
                    hash_found=True
                    self.column_headings=line.split(',')
                    continue
                
                if hash_found:
                    line_data_str2=['0' if len(x)==0 else x for x in line_data_str] # zero fill empty fields
                    line_data=[] # floats
                    for x in line_data_str2:
                        line_data.append(float(x))
                    #line_data=[map(float,x) for x in line_data_str2]
                    #data[csv_count].append(line_data)
                    cell=CellPos()
                    cell.x=line_data[self.find_index_for_column('XC [px]')]
                    cell.y=line_data[self.find_index_for_column('YC [px]')]
                    cell.t=line_data[self.find_index_for_column('Time [min]')]
                    cell.ex=line_data[self.find_index_for_column('Mean1')]
                       
                    if cell.x>0 or cell.y>0:   
                        track.cells.append(cell)    
                    
                line_count+=1
            
            csv_count+=1
            
            #compute length
            track.length()
            self.tracks.append(track)
    
    def make_plot(self):   
        
        t=0
        for track in self.tracks:#[0:10]:#[0:10]:
            if track.length>0.0:
                ex=[c.ex for c in track.cells]
                ti=[c.t for c in track.cells]
                
                av=4
                ex_ave=moving_average(ex,av)[av/2:-av/2]
                ex_mean=numpy.mean(ex)
                
                #ex_norm=[c.ex/track.cells[0].ex for c in track.cells]
                #ex_ave_norm=numpy.array([e/ex_ave[0] for e in ex_ave])
                ex_ave_norm=numpy.array([e/ex_mean for e in ex_ave])
                ti_norm=[(c.t-track.cells[0].t)/track.length for c in track.cells]
                
                #ex_norm_ave=movingaverage(ex_norm,4)
                ti_norm=numpy.array(ti_norm)
                
                # if ti_norm.shape==ex_ave_norm.shape:
                #plt.plot(ti_norm[0:len(ex_ave_norm)],ex_ave_norm)
                #plt.plot(ti_norm[0:len(ex_ave)],ex_ave)
        
                #plt.xlabel('Time')
                #plt.ylabel('Expression')
                #plt.show()  
                
                self.plotdata.set_data('x'+str(t),ti_norm[0:len(ex_ave_norm)])
                self.plotdata.set_data('y'+str(t),ex_ave_norm)
                self.plot.plot(('x'+str(t),'y'+str(t)), type="line", color="auto")
                
                t+=1
	pass
    
    def delete_plots(self):
	for p in self.plot.plots.keys():
	    self.plot.delplot(p)
	    
### MAIN
if __name__ == "__main__":
    lp=LinePlot(csv_directorys=[r'/Users/jpm/DATA/dem41/DATA october/N2B27/raw data',
                            r'/Users/jpm/DATA/dem41/DATA october/N2B27 + Act + Chi 2/raw data',
                            r'/Users/jpm/DATA/dem41/DATA october/N2B27+Act+Chi 1/raw data'])
    
    #lp.make_tracks()
    #lp.make_plot()

    lp.configure_traits()
