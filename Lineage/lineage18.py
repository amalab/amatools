#
# Read several track files saved from imagej Circadian Gene Expression 
# and plot with matplotlib
#
# jpm jun 2012
#

import numpy
import math
import numpy as np
import glob
import os
import matplotlib
import matplotlib.pyplot as plt
import scipy.sparse
import math

############
### track data structures
class CellPos:
	def __init__(self):
		self.id=0
		
		self.x=0 # x pos in px (XC)
		self.y=0 # y pos in px (YC)
		self.t=0 # time in mins (Time)
		self.ex=0 # expression in ? (Express)
		self.v=np.array((0.0,0.0))

		self.remove_flag=False
		
	def v_mag(self):
		return np.linalg.norm(self.v)
	
	def v_norm(self):
		if self.v_mag()>0.0:
			return self.v/self.v_mag()
		else:
			return self.v
	
	def pos(self):
		return numpy.array((self.x,self.y))
		
		
class Track:
	def __init__(self):
		self.cells=[]
		self.lineage='' #mother or daughter

	def calc_length(self):
		self.t_begin=self.cells[0].t
		self.t_end=self.cells[-1].t
		self.length=self.t_end-self.t_begin
		return self.length

	def compute(self):
		if len(self.cells)==0:
			return
		
		self.calc_length()
				
		self.ex=[c.ex for c in self.cells]
		self.ti=[c.t for c in self.cells]
		self.x=[c.x for c in self.cells]
		self.y=[c.y for c in self.cells]

		#ave expression
		self.ex_mean=numpy.mean(self.ex)

		self.dx=[]
		self.dy=[]
		for i in range(0,len(self.x)-1):
			self.dx.append(self.x[i+1]-self.x[i])
			self.dy.append(self.y[i+1]-self.y[i])
			#self.v.append((self.dx,self.dy))
			self.cells[i].v=np.array((self.x[i+1]-self.x[i],self.y[i+1]-self.y[i]))
			
		self.angles=[math.atan2(y,x) for x,y in zip(self.dx,self.dy)]	

		self.dx_mean=numpy.mean(self.dx)
		self.dy_mean=numpy.mean(self.dy)

		#overall movement
		if(len(self.x)>0):
			self.dx_overall=self.x[-1]-self.x[0]
			self.dy_overall=self.y[-1]-self.y[0]
	
			#start pos
			self.x0=self.x[0]
			self.y0=self.y[0]   

	def filter(self,thresh):
		# if v above thresh*std dev, return track split at that point
		#v_mag=[c.v_mag() for c in self.cells]
		#v_mean=numpy.mean(v_mag)
		#v_std=numpy.std(v_mag)

		flag=False
		for i in range(0,len(self.cells)-1):
			c=self.cells[i]
			#if abs(c.v_mag()-v_mean)>thresh*v_std:
			if abs(c.v_mag())>thresh:
				r=self.cells[i+1:]
				self.cells=self.cells[0:i]
				self.compute()
				return r		
				
		return None

		
	#def filter_orig(self):
		#dx_std=numpy.std(self.dx)
		#dy_std=numpy.std(self.dy)

		#m=1
		#for i in range(0,len(self.x)-1):
			#if abs(self.dx[i])>m*dx_std or abs(self.dy[i])>m*dy_std:
				#self.cells[i].remove_flag=True

		#for c in self.cells:
			#if c.remove_flag:
				#self.cells.remove(c)

		#self.compute()		


# use column heading in csv file to get row index
def find_index_for_column(col):
	return column_headings.index(col)

def movingaverage(interval, window_size):
	window = numpy.ones(int(window_size))/float(window_size)
	return numpy.convolve(interval, window, 'same')

class DataSet:
	def  __init__(self):
		self.csv_directory=''	
		self.tracks=[]
		self.cells=[] #all cells for time_frame lookup by int id
		
	def init_track_correlations(self):
		num_tracks=len(self.tracks)		
		# create matrix	
		self.track_correlations=[[[] for i in range(num_tracks)] for j in range(num_tracks)]
		
	def compute_time_frames(self):		
		# get max time for all tracks
		self.t_max=0
		for t in self.tracks:
			if t.t_end>self.t_max:
				self.t_max=t.t_end
				
		self.num_tracks=len(self.tracks)
		# create matrix				
		self.time_frames=scipy.sparse.lil_matrix((int(self.t_max)+1,self.num_tracks+1),dtype=int) #time,track
		
		#fill matrix
		track_count=0
		for track in self.tracks:
			for cell in track.cells:
				self.time_frames[int(cell.t),track_count]=cell.id
			track_count+=1		
			
	def filter_tracks(self,thresh):
		v_mag=[]
		for t in self.tracks:
			for c in t.cells:
				v_mag.append(c.v_mag()) 
		
		v_mean=numpy.mean(v_mag)
		v_std=numpy.std(v_mag)
		
		for t in self.tracks:
			r=t.filter(v_mean+thresh*v_std)
			if r:
				new_track=Track()
				new_track.cells=r
				new_track.compute()
				self.tracks.append(new_track)
			
		#remove zero-length tracks	
		new_tracks=[]
		for t in self.tracks:
			if len(t.cells)>0:
				new_tracks.append(t)
				
		self.tracks=new_tracks

#################
### read datasets

datasets=[]

#find all csv filenames in child directories
csv_directorys=[
r'/Users/jpm/DATA/dem41/DATA october/N2B27/raw data',
r'/Users/jpm/DATA/dem41/DATA october/N2B27 2/Raw Data',
r'/Users/jpm/DATA/dem41/DATA october/N2B27 3/',
r'/Users/jpm/DATA/dem41/DATA october/N2B27+Act+Chi 1/raw data',
r'/Users/jpm/DATA/dem41/DATA october/N2B27 + Act + Chi 2/raw data'
]

#for each dataset
for csv_directory in csv_directorys[3:4]:
	dataset=DataSet()
	datasets.append(dataset)
	dataset.csv_directory=csv_directory
	dataset.tracks=[] 	
	
	csv_file_names=glob.glob(csv_directory+'/*.csv')
   
	track_count=0 # number of tracks/csv files
	cell_count=0 #total cells over all tracks

	# each csv file is a track
	# each line is a cell position   
	for csv_filename in csv_file_names:
		track=Track()
		track.id=track_count

		csv_basename=os.path.basename(csv_filename)
		if csv_basename[1:].find('m')>0 or csv_basename[1:].find('M')>0:
			track.lineage='mother'
		else:
			track.lineage='daughter'

		csv_file=open(csv_filename)
		lines=csv_file.readlines()    

		#data.append([])
		line_count=0
		hash_found=False
		for line in lines:
			line_data_str=line.split(',')[0:-1] #strings
			if line[0]=='#': # find line starting # to get beginning of data
				hash_found=True
				column_headings=line.split(',')
				continue

			if hash_found:
				line_data_str2=['0' if len(x)==0 else x for x in line_data_str] # zero fill empty fields
				line_data=[] # floats
				for x in line_data_str2:
					line_data.append(float(x))
				#line_data=[map(float,x) for x in line_data_str2]
				#data[track_count].append(line_data)
				cell=CellPos()
				cell.id=cell_count
				cell.x=line_data[find_index_for_column('XC [px]')]*0.645 #micrometres
				cell.y=line_data[find_index_for_column('YC [px]')]*0.645 #micrometres
				cell.t=line_data[find_index_for_column('Time [min]')]
				cell.ex=line_data[find_index_for_column('Mean1')]
				cell.track=track

				if cell.x>0 or cell.y>0:   
					track.cells.append(cell) 
					dataset.cells.append(cell)
					cell_count+=1

			line_count+=1

		track.compute()
		
		dataset.tracks.append(track)
		track_count+=1

	print len(dataset.tracks),'-',
	dataset.filter_tracks(2.0)
	print len(dataset.tracks)
		
	dataset.compute_time_frames()
	dataset.init_track_correlations()


#########
## Plot
for dataset in datasets:
	
	###########
	#### 2D plot

	track_count=0
	dx=[] #list of all movements by each cell at each timestep
	dy=[]
	dx_overall=[]#list of all overall track movements
	dy_overall=[]
	x0=[]#list of start posns for each track
	y0=[]
	ex_mean=[]#mean expression for each track

	fig=plt.figure()
	p=fig.add_subplot(111)   

	for track in dataset.tracks:#[-3:-1]:
		if track.length>0.0:

			p.scatter(track.x,track.y,c=track.ex,s=40,cmap='YlGn')
			p.plot(track.x,track.y,color='black')
			p.set_xlabel(r'Cell Tracks - $\mu$m')

			x0.append(track.x0)
			y0.append(track.y0)
			dx_overall.append(track.dx_overall)
			dy_overall.append(track.dy_overall)
			ex_mean.append(track.ex_mean)
			
			#accumulate every cell movement step
			dx+=track.dx #list concat
			dy+=track.dy

			track_count+=1        

	p.quiver(x0,y0,dx_overall,dy_overall,ex_mean,angles='xy',units='dots',scale_units='xy',scale=1.0,width=5,cmap='YlGn')

	plot_name=dataset.csv_directory.split('/')[-2]
	p.set_title(plot_name)

	# vector quiver inset
	ax=fig.add_axes([0.8,0.7,0.2,0.3])
	s=100
	ax.set_xlim([-s,s])
	ax.set_ylim([-s,s])
	ax.set_xlabel(r'Overall Track Movement - $\mu$m')
	z=[0]*len(x0)
	ax.quiver(z,z,dx_overall,dy_overall,ex_mean,angles='xy',units='dots',scale_units='xy',scale=1.0,width=5,cmap='YlGn')	

	# movment histogram inset
	angles=[math.atan2(y,x) for x,y in zip(dx,dy)]
	hi,bins=numpy.histogram(angles,bins=40)
	
	ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
	ax.set_xlabel('Cell Step Direction')
	n=len(hi)
	bar=ax.bar([b for b in np.arange(-np.pi,np.pi,2*np.pi/n)],hi,width=2*np.pi/n,bottom=0.0)
	for i in range(n):
		bar[i].set_facecolor(matplotlib.cm.jet(0.7))
		bar[i].set_alpha(0.5)		
		
	## angle difference histogram
	#angles_diff=[angles[i]-angles[i-1] for i in range(1,len(angles))]
	#hi,bins=numpy.histogram(angles_diff,bins=40)	
	#ax=fig.add_axes([0.0, 0.05, 0.3, 0.3])
	#n=len(hi)
	#bar=ax.bar(bins[:-1],hi)
	#for i in range(n):
		#bar[i].set_facecolor(matplotlib.cm.jet(0.3))
		#bar[i].set_alpha(0.5)
	
	
	## directional correlation	
	## timespans
	#fig=plt.figure()
	#p=fig.add_subplot(111)  

	#N=len(dataset.tracks)
	#t=0
	#s=N
	#for track in dataset.tracks:#[-3:-1]:
		#p.barh(t,track.length,height=1,left=track.t_begin)
		#t+=1
		
	##quivers of time_frame	
	fig=plt.figure()
	p=fig.add_subplot(111) 
	p.set_title(plot_name)	
	
	nz=dataset.time_frames.nonzero()
	ii=zip(nz[0],nz[1])
	tis=[]
	trs=[]
	vxs=[]
	vys=[]
	xs=[]
	ys=[]
	s=0.01
	for i in ii:
		ti=i[0]
		tr=i[1]
		cell_id=dataset.time_frames[ti,tr]
		c=dataset.cells[cell_id]		
		tis.append(ti)
		trs.append(tr)
		vxs.append(c.v[0])
		vys.append(c.v[1])
		
		xs.append(float(ti)/float(dataset.t_max))
		xs.append(float(ti)/float(dataset.t_max)+s*c.v[0])
		xs.append(None)
		ys.append(float(tr)/float(dataset.num_tracks))
		ys.append(float(tr)/float(dataset.num_tracks)+s*c.v[1])
		ys.append(None)
		
	#p.quiver(tis,trs,vxs,vys,scale_units='inches',scale=200,headwidth=0,width=0.001)#)
	#p.plot(tis
	p.plot(xs,ys)	
	
	## directional correlation
	#cors=[]
	#dists=[]
	for t in range(int(dataset.t_max)):
		col=dataset.time_frames[t,:]
		ii=col.nonzero()[1] #time_frames indices where there are cell ids for time t
		if len(ii)>1:
			print t,ii
			for i in range(len(ii)):
				for j in range(i+1,len(ii)):
					#print (ii[i],ii[j])
					c1_id=dataset.time_frames[t,ii[i]]
					c2_id=dataset.time_frames[t,ii[j]]
					c1=dataset.cells[c1_id]
					c2=dataset.cells[c2_id]
					cor=numpy.dot(c1.v_norm(),c2.v_norm()) 
					dist=numpy.linalg.norm(c2.pos()-c1.pos())
					#cors.append(cor)
					#dists.append(dist)
					dataset.track_correlations[c1.track.id][c2.track.id].append((cor,dist))
					#print c1.track.id,c2.track.id,(cor,dist)
	
	cors=[]
	dists=[]		
	num_tracks=len(dataset.track_correlations)			
	for t1 in range(num_tracks):
		for t2 in range(num_tracks):
			a=dataset.track_correlations[t1][t2]
			if len(a)>0:
				aa=np.array(a)
				c=aa[:,0].mean()
				d=aa[:,1].mean()
				cors.append(c)
				dists.append(d)
			
					
	fig=plt.figure()
	p=fig.add_subplot(111)  
	p.set_ylim([-1.0,1.0])
	p.set_title(plot_name)	
	p.scatter(dists,cors)
			

plt.show()    

############
##### Line plot
#num_plot_rows=2
#num_plot_cols=3
#fig,ax=plt.subplots(num_plot_rows,num_plot_cols)
#plot_col=0
#plot_row=0	

#for av in [1,8]:

	#for track in tracks:#[0:10]:
		#if track.length>0.0:        
			#ex_ave=movingaverage(track.ex,av)[av/2:-av/2]

			##ex_norm=[c.ex/track.cells[0].ex for c in track.cells]
			##ex_ave_norm=numpy.array([e/ex_ave[0] for e in ex_ave])
			#ex_ave_norm=numpy.array([e/track.ex_mean for e in ex_ave])
			#ti_norm=[(c.t-track.cells[0].t)/track.length for c in track.cells]

			##ex_norm_ave=movingaverage(ex_norm,4)
			#ti_norm=numpy.array(ti_norm)

			## if ti_norm.shape==ex_ave_norm.shape:        
			#p=ax[plot_row,plot_col]
			#p.plot(ti_norm[0:len(ex_ave_norm)],ex_ave_norm)
			##plt.plot(ti_norm[0:len(ex_ave)],ex_ave)

	#plot_name=csv_directory.split('/')[-2]


	#p.set_title(plot_name)
	##p.xlabel('Time')
	##p.ylabel('Expression normalised to Mean')

	#plot_row+=1
	
	#for c in range(num_plot_cols):
		#ax[1,c].set_ylim(ax[0,c].get_ylim())	
		
	#plot_col+=1	
	
########
####orthogonal projection
#import numpy
#from mpl_toolkits.mplot3d import proj3d
#def orthogonal_proj(zfront, zback):
	#a = (zfront+zback)/(zfront-zback)
	#b = -2*(zfront*zback)/(zfront-zback)
	#return numpy.array([[1,0,0,0],
						#[0,1,0,0],
						#[0,0,a,b],
						#[0,0,0,zback]])
#proj3d.persp_transformation = orthogonal_proj

########
#### 3d plot
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as plt
#import matplotlib as mpl
##import numpy as np

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')

##track_count=np_data.shape[0]
#track_count=0
#for track in tracks:#[0:10]:
	##xs=np_data[track,:,1] # Time
	##ys=np_data[track,:,12] # Mean1
	#if track.lineage is 'mother':
		#col='r'
	#elif track.lineage is 'daughter':
		#col='g'
	#else:
		#col='k'

	#xs=[c.x for c in track.cells]
	#ys=[c.y for c in track.cells]
	#zs=[c.ex for c in track.cells]
	#ax.plot(xs, ys, zs,c=col)# zs=track_count)

	#ax.scatter(xs, ys, zs,c=col,)#, zdir='y', c=c)    
	##ax.scatter(xs, ys, 0,c=col)#, zdir='y', c=c)    

	#track_count+=1

#ax.set_xlabel('X')
#ax.set_ylabel('Y')
#ax.set_zlabel('fluorescence')

#plt.show()