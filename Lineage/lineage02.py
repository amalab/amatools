#
# Read several track files saved from imagej Circadian Gene Expression 
# and plot with matplotlib
#
# jpm jun 2012
#

############
###track data structures
class CellPos:
    def __init__(self):
        self.x=0 # x pos in px (XC)
        self.y=0 # y pos in px (YC)
        self.t=0 # time in mins (Time)
        self.ex=0 # expression in ? (Express)

class Track:
    def __init__(self):
        self.cells=[]
        self.lineage='' #mother or daughter
    
tracks=[]

# use column heading in csv file to get row index
def find_index_for_column(col):
    return column_headings.index(col)

#################
###read csv files

# data[track][time][column]

import numpy as np
import glob
import os
#find all csv filenames in child directories
csv_file_names=glob.glob(r'/Volumes/home/dem41/DATA october/N2B27/raw data/*.csv')

#data=[]
csv_count=0 # number of csv files

# each csv file is a track
# each line is a cell position

for csv_filename in csv_file_names:
    track=Track()
    
    csv_basename=os.path.basename(csv_filename)
    if csv_basename[1:].find('m')>0 or csv_basename[1:].find('M')>0:
        track.lineage='mother'
    else:
        track.lineage='daughter'
    
    csv_file=open(csv_filename)
    lines=csv_file.readlines()    
    
    #data.append([])
    line_count=0
    hash_found=False
    for line in lines:
        line_data_str=line.split(',')[0:-1] #strings
        if line[0]=='#': # find line starting # to get beginning of data
            hash_found=True
            column_headings=line.split(',')
            continue
        
        if hash_found:
            line_data_str2=['0' if len(x)==0 else x for x in line_data_str] # zero fill empty fields
            line_data=[] # floats
            for x in line_data_str2:
                line_data.append(float(x))
            #line_data=[map(float,x) for x in line_data_str2]
            #data[csv_count].append(line_data)
            cell=CellPos()
            cell.x=line_data[find_index_for_column('XC [px]')]
            cell.y=line_data[find_index_for_column('YC [px]')]
            cell.t=line_data[find_index_for_column('Time [min]')]
            cell.ex=line_data[find_index_for_column('Mean1')]
               
            if cell.x>0 or cell.y>0:   
                track.cells.append(cell)    
            
        line_count+=1
    
    csv_count+=1
    
    tracks.append(track)
    
pass

#np_data=np.array(data)

########
####orthogonal projection
#import numpy
#from mpl_toolkits.mplot3d import proj3d
#def orthogonal_proj(zfront, zback):
    #a = (zfront+zback)/(zfront-zback)
    #b = -2*(zfront*zback)/(zfront-zback)
    #return numpy.array([[1,0,0,0],
                        #[0,1,0,0],
                        #[0,0,a,b],
                        #[0,0,0,zback]])
#proj3d.persp_transformation = orthogonal_proj

#######
###plot
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib as mpl
#import numpy as np

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

#track_count=np_data.shape[0]
track_count=0
for track in tracks[0:10]:
    #xs=np_data[track,:,1] # Time
    #ys=np_data[track,:,12] # Mean1
    if track.lineage is 'mother':
        col='r'
    elif track.lineage is 'daughter':
        col='g'
    else:
        col='k'
        
    xs=[c.x for c in track.cells]
    ys=[c.y for c in track.cells]
    zs=[c.ex for c in track.cells]
    ax.plot(xs, ys, zs,c=col)# zs=track_count)
    
    ax.scatter(xs, ys, zs,c=col,)#, zdir='y', c=c)    
    #ax.scatter(xs, ys, 0,c=col)#, zdir='y', c=c)    
    
    track_count+=1
    
#for c, z in zip(['r', 'g', 'b', 'y'], [30, 20, 10, 0]):
    #xs = np.arange(20)
    #ys = np.random.rand(20)

    ## You can provide either a single color or an array. To demonstrate this,
    ## the first bar of each set will be colored cyan.
    #cs = [c] * len(xs)
    #cs[0] = 'c'
    #ax.bar(xs, ys, zs=z, zdir='y', color=cs, alpha=0.8)

#circ=mpl.patches.Circle((0,0))
#cln=mpl.collections.CircleCollection()
#cln.
#ax.add_collection3d(cln, zs=0, zdir='z')

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('fluorescence')

plt.show()