from pylsm import lsmreader
import numpy
import matplotlib
from matplotlib import pyplot
import scipy
import skimage.filter
import skimage.feature
import skimage.morphology
import os
import glob
import pickle

class Track:
    '''Track of cells over time'''
    def __init__(self):	
        self.id=None # unique number from 0..len(tracks)
        self.number=None # mtrackj track number
        self.cluster=None #mtracksj cluster

        self.x=None # x pos in pixels
        self.y=None # y pos in pixels 
        self.t=None # time in frames
        self.f={} #fluorescence measurements

        self.parent=None
        self.children=[]
        self.sibling=None
        self.linked=False

        self.yplot=0 #height for plotting lineage
        
def read_mtrackj_mdf(mdf_file,verbose=False):
    '''Read track data, including cluster info, from MTrackJ mdf file & return as list of Tracks()'''

    mdf_file=open(mdf_file,'rU')
    mdf_lines=mdf_file.readlines()
    tracks=[]

    l=0
    while True:
        line=mdf_lines[l]
        if 'Cluster' in line:
            break
        l+=1

    while 'Cluster' in line:
        line_elems=line.split(' ')
        cluster=int(line_elems[1])
        if verbose:
            print 'Cluster',cluster

        l+=1
        line=mdf_lines[l]

        while 'Track' in line and 'End' not in line:
            track=Track()
            line_elems=line.split(' ')
            track.number=int(line_elems[1])
            track.id=len(tracks)
            track.cluster=cluster
            tracks.append(track)
            if verbose:
                print 'Track',track.id,track.number

            l+=1
            line=mdf_lines[l]
            x=[]
            y=[]
            t=[]
            while 'Point' in line:
                line_elems=line.split(' ')
                x.append(float(line_elems[2]))
                y.append(float(line_elems[3]))
                t.append(float(line_elems[5]))

                l+=1
                line=mdf_lines[l]

            track.x=numpy.array(x)
            track.y=numpy.array(y)
            track.t=numpy.array(t)

    return tracks
    
def measure(filename,segment_channel=DAPI,measure_channels=[],plot_overlay=False,sigma=3.0,block_size=55,r=8):
    '''
    measure filename.lsm image file using filename.mdf clickpoints
    use segment_channel
    measure in measure_channels
    gaussian smooth with sigma
    adaptive thresholding with block_size
    'clickpoint' measurement over square of side 2r centred on clickpoint
    'segmented' measurement over whole cell
    create plots of image and measurment areas if plot_overlay=True 
    
    return measurments[fn][method][ch]
    ''' 
    path,fn=os.path.split(filename)
    print fn

    measurements={}
    measurements[fn]={}
    measurements[fn]['clickpoint']={}
    measurements[fn]['segmented']={} #actually segmented - now filtered when pickle read & analysed
    
    tracks=read_mtrackj_mdf(filename+'.mdf')
    lsm=lsmreader.Lsmimage(filename+'.lsm')
    lsm.open()

    im=numpy.array(lsm.image['data'])[:,:,:,0]
    print im.shape
    num_channels=im.shape[0]

    orig=im[segment_channel]

    #smooth
    smooth=scipy.ndimage.filters.gaussian_filter(orig,(sigma,sigma))

    #thresh
    thresh=skimage.filter.threshold_adaptive(smooth,block_size)#,offset=10)

    #watershed
    distance=scipy.ndimage.distance_transform_edt(thresh)
    local_maxi=skimage.feature.peak_local_max(distance, indices=False, footprint=numpy.ones((3, 3)),labels=orig)
    markers=scipy.ndimage.label(local_maxi)[0]

    click_points=numpy.zeros_like(markers,dtype='uint8')
    for t in tracks:
        x=t.x[0]
        y=t.y[0]
        click_points[y,x]=255
    markers_mdf=scipy.ndimage.label(click_points)[0]

    labels = skimage.morphology.watershed(-distance, markers_mdf, mask=thresh)
    #labels=numpy.mod(labels,256).astype('uint8')
    print 'Segmented'
     
    #measure around click point
    for ch in measure_channelschannels:
        measurements[fn]['clickpoint'][ch]=[]

        for t in tracks:
            x=t.x[0]
            y=t.y[0]
            m=numpy.sum(im[ch,y-r:y+r,x-r:x+r]) 
            a=4*r*r            
            #m,a=t.measure_fluorescence(im,[ch])
            measurements[fn]['clickpoint'][ch].append((m,a)) #(value,area)
 
    #measure segmented
    for ch in measure_channels:
        measurements[fn]['segmented'][ch]=[]

        for i in range(1,labels.max()):
            points=numpy.where(labels==i)
            area=len(points[0])

            if area>0:
                #m,a=t.measure_fluorescence(im,[ch],points=points) 
                v=[im[ch,y,x] for (y,x) in zip(points[0],points[1])] #NOTE points are (y,x)
                m=numpy.sum(v)
                a=len(v)
                
                measurements[fn]['segmented'][ch].append((m,a))

    ##show segmentation            
    #r=numpy.random.rand(256,3)
    #r[0]=(0,0,0)
    #r[255]=(1,1,1)
    #rand_cmap=matplotlib.colors.ListedColormap(r)
    #combined=numpy.maximum(labels,click_points)
    #p1.imshow(combined,cmap=rand_cmap)#,aspect='auto')    
    
    ###overlay
    if plot_overlay:
        for ch in channels:
            fig=pyplot.figure(figsize=(20,20))
            
            p1=fig.add_subplot(1,2,1)
            p1.set_title('%s Segmented'%ch)    
            overlay=numpy.zeros_like(im[ch])
            for i in range(1,labels.max()):
                points=numpy.where(labels==i)
                area=len(points[0])
                if area>FILTERMIN and area<FILTERMAX:
                    overlay[points]=im[ch][points]
                    seg=numpy.zeros_like(im[ch])             
                    seg[points]=255          
                    outline=skimage.filter.sobel(seg)
                    overlay[numpy.where(outline>0)]=255          
            p1.imshow(overlay,cmap='gray')
          
            p1=fig.add_subplot(1,2,2)
            p1.set_title('%s Clickpoint'%ch)    
            overlay=numpy.zeros_like(im[ch])
            for t in tracks:
                x=t.x[0]
                y=t.y[0]
                overlay[y-r:y+r,x-r:x+r]=im[ch][y-r:y+r,x-r:x+r]
            p1.imshow(overlay,cmap='gray')
            
            fig.savefig('%soverlay_%s_%s.png'%(path,fn,ch))
        
    return measurements

###MAIN

#image channels
DAPI=0
BRIGHT=1
BRA=2
T=3
SOX2=4

cols=['r','g','b','c','m','y','olive','gray','plum','pink','crimson','gold']

#segmented cell size filter
FILTERMIN=100
FILTERMAX=2000

path='/Users/jpm/Progs/AMATools/Penny/'
groups=['TBra 0-2N,2-3C ','TBra 0-2N,2-3CF ','TBra 0-3N ','TBra 0-2N,2-3C DOX']
groups+=['dE 0-2N,2-3C (','dE 0-2N,2-3C DOX ','dE 0-2N,2-3CF ','dE 0-3N ']

for group in groups:
    print group
    
    ###measure & pickle
    channels=[BRA,SOX2]
    #mdffiles=glob.glob('%s%s*.mdf'%(path,group))
    #filenames=[os.path.splitext(f)[0] for f in mdffiles]
    #print filenames   
    #measurements={}
    #for i,filename in enumerate(filenames):
        #measure(filename)        
    #pickle.dump(measurements,open('measurements_%s.pickle'%group,'wb'))
    
    ###overlay measurement areas onto fluorescence images
    #mdffiles=glob.glob('/Users/jpm/Progs/AMATools/Penny/%s*.mdf'%group)
    #filenames=[os.path.splitext(f)[0] for f in mdffiles]
    #print filenames   
    #measurements={}
    #for i,filename in enumerate(filenames):
        #measure(filename)     

    ####load and plot 
    measurements=pickle.load(open('measurements_%s.pickle'%group,'rb'))
    FILTERMIN=100
    FILTERMAX=2000
            
    ###write to csv 
    #fi=open('%s.csv'%group,'w')
    #fi.write('AREA, Total BRA, Total SOX2\n\n')
    #for f in measurements.keys():
        #fi.write(f+'\n')
        
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2] #(value,area)
        
        #b=[(v,a) for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #s=[(v,a) for v,a in sox if a>FILTERMIN and a<FILTERMAX]
        
        #for i,m in enumerate(b):
            #fi.write(str(b[i][1])+', ') #area
            #fi.write(str(b[i][0])+', ') #bra
            #fi.write(str(s[i][0])+'\n') #sox
        #fi.write('\n')
            
    ###areas
    #fig=pyplot.figure(figsize=(10,10))
    #fig.suptitle('Areas')
    
    #p1=fig.add_subplot(1,1,1)
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #aa=[a for v,a in bra]# if a>FILTERMIN and a<FILTERMAX]
        #h=p1.hist(aa,bins=range(0,4000,80),color=cols[i],alpha=0.5,label=f)
    #p1.legend()   
    #p1.set_xlabel('area')
    #p1.axvspan(FILTERMIN,FILTERMAX,alpha=0.1,color='k')
    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/areas_%s.svg'%group)
    
    #p2=fig.add_subplot(1,2,2)
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #vv=[v/a for v,a in bra]# if a>FILTERMIN and a<FILTERMAX]    
        #aa=[a for v,a in bra]# if a>FILTERMIN and a<FILTERMAX]    
        #p2.scatter(vv,aa,color=cols[i],alpha=0.5,label=f)
    #p2.legend()
    #p2.set_xlabel('fluor')
    #p2.set_ylabel('area')
    ##p2.set_xlim(50)
    ##p2.set_ylim(4000)      
            
    ####scatter total
    #fig=pyplot.figure(figsize=(10,10))    
    #p=fig.add_subplot(1,1,1)
    #p.set_title('Total fluoresence per cell')
    #p.set_xlabel('Bra')
    #p.set_ylabel('SOX2')
    
    #for i,f in enumerate(measurements.keys()):       
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2]
        #bb=[v for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss=[v for v,a in sox if a>FILTERMIN and a<FILTERMAX]
        #p.scatter(bb,ss,s=3,color=cols[i],label=f) #unzip (value,area) to (value,,,) and (area,,,)      
        #p.set_xlim((0,40000))
        #p.set_ylim((0,100000))    
       
    #p.legend()          
    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/scatter_total_%s.svg'%group)
    
    #####scatter per pixel
    #fig=pyplot.figure(figsize=(10,10))    
    #p=fig.add_subplot(1,1,1)
    #p.set_title('Fluoresence per pixel')
    #p.set_xlabel('Bra')
    #p.set_ylabel('SOX2')
    
    #for i,f in enumerate(measurements.keys()):       
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2]
        #bb=[v/a for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss=[v/a for v,a in sox if a>FILTERMIN and a<FILTERMAX]
        #p.scatter(bb,ss,s=3,color=cols[i],label=f) #unzip (value,area) to (value,,,) and (area,,,)      
        #p.set_xlim((0,10))
        #p.set_ylim((0,100))    
       
    #p.legend()          
    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/scatter_perpixel_%s.svg'%group)
    
    #####scatter per pixel - clickpoint
    #fig=pyplot.figure(figsize=(10,10))    
    #p=fig.add_subplot(1,1,1)
    #p.set_title('Fluoresence per pixel')
    #p.set_xlabel('Bra')
    #p.set_ylabel('SOX2')
    
    #for i,f in enumerate(measurements.keys()):       
        #bra=measurements[f]['clickpoint'][BRA] #(value,area)
        #sox=measurements[f]['clickpoint'][SOX2]
        #bb=[v/a for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss=[v/a for v,a in sox if a>FILTERMIN and a<FILTERMAX]
        #p.scatter(bb,ss,s=3,color=cols[i],label=f) #unzip (value,area) to (value,,,) and (area,,,)      
        #p.set_xlim((0,10))
        #p.set_ylim((0,100))    
       
    #p.legend()          
    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/scatter_clickpoint_%s.svg'%group)
    
    
    ######hist total
    #fig=pyplot.figure(figsize=(20,10))
    #fig.suptitle('Total fluoresence per cell')
    
    #p1=fig.add_subplot(1,2,1)
    #p1.set_title('Bra')
    #p2=fig.add_subplot(1,2,2)
    #p2.set_title(' Sox2')
    
    #MBra=40000 #bins max
    #MSox=100000 #bins max
    #N=30 #num bins
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2]  
        #bb=[v for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss=[v for v,a in sox if a>FILTERMIN and a<FILTERMAX]    
        #h=p1.hist(bb,bins=range(0,MBra,MBra/N),color=cols[i],alpha=0.5,label=f)
        #h=p2.hist(ss,bins=range(0,MSox,MSox/N),color=cols[i],alpha=0.5,label=f)
            
    #p1.legend()
    #p2.legend() 

    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/histogram_total_%s.svg'%group)
    
    #####hist per pixel
    #fig=pyplot.figure(figsize=(20,10))
    #fig.suptitle('Fluoresence per pixel')
    
    #p1=fig.add_subplot(1,2,1)
    #p1.set_title('Bra')
    #p2=fig.add_subplot(1,2,2)
    #p2.set_title(' Sox2')
    
    #MBra=10.0 #bins max
    #MSox=100.0 #bins max
    #N=30 #num bins
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2]  
        #bb=[v/a for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss=[v/a for v,a in sox if a>FILTERMIN and a<FILTERMAX]    
        #h=p1.hist(bb,bins=numpy.arange(0,MBra,MBra/N),color=cols[i],alpha=0.5,label=f)
        #h=p2.hist(ss,bins=numpy.arange(0,MSox,MSox/N),color=cols[i],alpha=0.5,label=f)
        ##h=p1.hist(bb,bins=N,color=cols[i],alpha=0.5,label=f)
        ##h=p2.hist(ss,bins=N,color=cols[i],alpha=0.5,label=f)
            
    #p1.legend()
    #p2.legend() 

    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/histogram_perpixel_%s.svg'%group)
    
    #####hist per pixel - clickpoint
    #fig=pyplot.figure(figsize=(20,10))
    #fig.suptitle('Fluoresence per pixel')
    
    #p1=fig.add_subplot(1,2,1)
    #p1.set_title('Bra')
    #p2=fig.add_subplot(1,2,2)
    #p2.set_title(' Sox2')
    
    #MBra=10.0 #bins max
    #MSox=100.0 #bins max
    #N=30 #num bins
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['clickpoint'][BRA] #(value,area)
        #sox=measurements[f]['clickpoint'][SOX2]  
        #bb=[v/a for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss=[v/a for v,a in sox if a>FILTERMIN and a<FILTERMAX]    
        #h=p1.hist(bb,bins=numpy.arange(0,MBra,MBra/N),color=cols[i],alpha=0.5,label=f)
        #h=p2.hist(ss,bins=numpy.arange(0,MSox,MSox/N),color=cols[i],alpha=0.5,label=f)
        ##h=p1.hist(bb,bins=N,color=cols[i],alpha=0.5,label=f)
        ##h=p2.hist(ss,bins=N,color=cols[i],alpha=0.5,label=f)
            
    #p1.legend()
    #p2.legend() 

    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/histogram_clickpoint_%s.svg'%group)
    
    
    ######pool files
    ######hist total
    #fig=pyplot.figure(figsize=(20,10))
    #fig.suptitle('Total fluoresence per cell')
    
    #p1=fig.add_subplot(1,2,1)
    #p1.set_title('Bra')
    #p2=fig.add_subplot(1,2,2)
    #p2.set_title(' Sox2')
    
    #MBra=40000 #bins max
    #MSox=100000 #bins max
    #N=30 #num bins
    #bb=[]
    #ss=[]
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2]  
        #bb+=[v for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss+=[v for v,a in sox if a>FILTERMIN and a<FILTERMAX]    
    #h=p1.hist(bb,bins=range(0,MBra,MBra/N),color='olive',alpha=1.0,label=f)
    #h=p2.hist(ss,bins=range(0,MSox,MSox/N),color='plum',alpha=1.0,label=f)
            
    ##p1.legend()
    ##p2.legend() 

    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/histogram_pooled_total_%s.svg'%group)
    
    #####hist per pixel
    #fig=pyplot.figure(figsize=(20,10))
    #fig.suptitle('Fluoresence per pixel')
    
    #p1=fig.add_subplot(1,2,1)
    #p1.set_title('Bra')
    #p2=fig.add_subplot(1,2,2)
    #p2.set_title(' Sox2')
    
    #MBra=10.0 #bins max
    #MSox=100.0 #bins max
    #N=30 #num bins
    #bb=[]
    #ss=[]
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['segmented'][BRA] #(value,area)
        #sox=measurements[f]['segmented'][SOX2]  
        #bb+=[v/a for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss+=[v/a for v,a in sox if a>FILTERMIN and a<FILTERMAX]    
    #h=p1.hist(bb,bins=numpy.arange(0,MBra,MBra/N),color='olive',alpha=1.0,label=f)
    #h=p2.hist(ss,bins=numpy.arange(0,MSox,MSox/N),color='plum',alpha=1.0,label=f)
            
    ##p1.legend()
    ##p2.legend() 

    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/histogram_pooled_perpixel_%s.svg'%group)
    
    #####hist per pixel - clickpoint
    #fig=pyplot.figure(figsize=(20,10))
    #fig.suptitle('Fluoresence per pixel')
    
    #p1=fig.add_subplot(1,2,1)
    #p1.set_title('Bra')
    #p2=fig.add_subplot(1,2,2)
    #p2.set_title(' Sox2')
    
    #MBra=10.0 #bins max
    #MSox=100.0 #bins max
    #N=30 #num bins
    #bb=[]
    #ss=[]
    #for i,f in enumerate(measurements.keys()):
        #bra=measurements[f]['clickpoint'][BRA] #(value,area)
        #sox=measurements[f]['clickpoint'][SOX2]  
        #bb+=[v/a for v,a in bra if a>FILTERMIN and a<FILTERMAX]
        #ss+=[v/a for v,a in sox if a>FILTERMIN and a<FILTERMAX]    
    #h=p1.hist(bb,bins=numpy.arange(0,MBra,MBra/N),color='olive',alpha=1.0,label=f)
    #h=p2.hist(ss,bins=numpy.arange(0,MSox,MSox/N),color='plum',alpha=1.0,label=f)
            
    ##p1.legend()
    ##p2.legend() 

    #fig.savefig('/Users/jpm/Progs/AMATools/Penny/histogram_pooled_clickpoint_%s.svg'%group)    
    
#for bra pos plot use bra_pos.ipynb