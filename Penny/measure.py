from pylsm import lsmreader
import numpy
import matplotlib
from matplotlib import pyplot
import scipy
import skimage.filter
import skimage.feature
import skimage.morphology
import os
import glob
import pickle

def read_mtrackj_mdf(mdf_file,verbose=False):
    '''Read track data, including cluster info, from MTrackJ mdf file & return as list of Tracks()'''

    mdf_file=open(mdf_file,'rU')
    mdf_lines=mdf_file.readlines()
    tracks=[]

    l=0
    while True:
        line=mdf_lines[l]
        if 'Cluster' in line:
            break
        l+=1

    while 'Cluster' in line:
        line_elems=line.split(' ')
        cluster=int(line_elems[1])
        if verbose:
            print 'Cluster',cluster

        l+=1
        line=mdf_lines[l]

        while 'Track' in line and 'End' not in line:
            track=Track()
            line_elems=line.split(' ')
            track.number=int(line_elems[1])
            track.id=len(tracks)
            track.cluster=cluster
            tracks.append(track)
            if verbose:
                print 'Track',track.id,track.number

            l+=1
            line=mdf_lines[l]
            x=[]
            y=[]
            t=[]
            while 'Point' in line:
                line_elems=line.split(' ')
                x.append(float(line_elems[2]))
                y.append(float(line_elems[3]))
                t.append(float(line_elems[5]))

                l+=1
                line=mdf_lines[l]

            track.x=numpy.array(x)
            track.y=numpy.array(y)
            track.t=numpy.array(t)

    return tracks

class Track:
    '''Track of cells over time'''
    def __init__(self):	
        self.id=None # unique number from 0..len(tracks)
        self.number=None # mtrackj track number
        self.cluster=None #mtracksj cluster

        self.x=None # x pos in pixels
        self.y=None # y pos in pixels 
        self.t=None # time in frames

        self.parent=None
        self.children=[]
        self.sibling=None
        self.linked=False

        self.yplot=0 #height for plotting lineage
    
def measure(filename,show_segmentation=False, show_overlay=False):
    p,fn=os.path.split(filename)
    print fn

    measurements[fn]={}
    measurements[fn]['clickpoint']={}
    measurements[fn]['segmented']={}
    
    tracks=read_mtrackj_mdf(filename+'.mdf')
    lsm=lsmreader.Lsmimage(filename+'.lsm')
    lsm.open()

    im=numpy.array(lsm.image['data'])[:,:,:,0]
    print im.shape
    num_channels=im.shape[0]

    orig=im[DAPI]

    #smooth
    sigma=3.0
    smooth=scipy.ndimage.filters.gaussian_filter(orig,(sigma,sigma))

    #thresh
    #threshold=skimage.filter.threshold_yen(smooth)
    #thresh=smooth<threshold
    thresh=skimage.filter.threshold_adaptive(smooth,55)#,offset=10)

    #watershed
    distance=scipy.ndimage.distance_transform_edt(thresh)
    local_maxi=skimage.feature.peak_local_max(distance, indices=False, footprint=numpy.ones((3, 3)),labels=orig)
    markers=scipy.ndimage.label(local_maxi)[0]

    click_points=numpy.zeros_like(markers,dtype='uint8')
    for t in tracks:
        x=t.x[0]
        y=t.y[0]
        click_points[y,x]=255
    markers_mdf=scipy.ndimage.label(click_points)[0]

    labels = skimage.morphology.watershed(-distance, markers_mdf, mask=thresh)
    #labels=numpy.mod(labels,256).astype('uint8')
    print 'Segmented'
     
    #measure around click point
    r=8

    for ch in channels:
        measurements[fn]['clickpoint'][ch]=[]

        for t in tracks:
            x=t.x[0]
            y=t.y[0]
            m=numpy.sum(im[ch,y-r:y+r,x-r:x+r]) 
            a=4*r*r            
            measurements[fn]['clickpoint'][ch].append((m,a)) #(value,area)
 
    #measure segmented
    for ch in channels:
        measurements[fn]['segmented'][ch]=[]

        for i in range(1,labels.max()):
            points=numpy.where(labels==i)
            area=len(points[0])

            if area>0:
                v=[im[ch,y,x] for (y,x) in zip(points[0],points[1])] #NOTE points are (y,x)
                m=numpy.sum(v)
                a=len(v)
                
                measurements[fn]['segmented'][ch].append((m,a))

    #show segmentation  
    if show_segmentation:
        fig=pyplot.figure(figsize=(20,20))        
        r=numpy.random.rand(256,3)
        r[0]=(0,0,0)
        r[255]=(1,1,1)
        rand_cmap=matplotlib.colors.ListedColormap(r)
        combined=numpy.maximum(labels,click_points)
        p1.imshow(combined,cmap=rand_cmap)#,aspect='auto')    
        fig.savefig(path+'segmented_%s.png'%fn)            
        
    
    #overlay - shows measurement area on fluorescence image
    if show_overlay:
        for ch in channels:
            fig=pyplot.figure(figsize=(20,20))
            
            p1=fig.add_subplot(1,2,1)
            p1.set_title('Segmented')    
            overlay=numpy.zeros_like(im[ch])
            for i in range(1,labels.max()):
                points=numpy.where(labels==i)
                area=len(points[0])
                if area>FILTERMIN and area<FILTERMAX:
                    overlay[points]=im[ch][points]
                    seg=numpy.zeros_like(im[ch])             
                    seg[points]=255          
                    outline=skimage.filter.sobel(seg)
                    overlay[numpy.where(outline>0)]=255          
            p1.imshow(overlay,cmap='gray')
               
            
            p2=fig.add_subplot(1,2,2)
            p2.set_title('Clickpoint')    
            overlay=numpy.zeros_like(im[ch])
            for t in tracks:
                x=t.x[0]
                y=t.y[0]
                overlay[y-r:y+r,x-r:x+r]=im[ch][y-r:y+r,x-r:x+r]
            p2.imshow(overlay,cmap='gray')
            
            fig.savefig('/Users/jpm/Progs/AMATools/Penny/overlay_%s_%s.png'%(ch,fn))            

###MAIN
#path for data and results
path='/Users/amalab/AMATools/Penny/test/'

#channels
DAPI=0
BRIGHT=1
BRA=2
T=3
SOX2=4

#channels to measure
channels=[BRA,T,SOX2] 

#colours for plots
cols=['r','g','b','c','m','y','olive','gray','plum','pink','crimson','gold']

#segmented cell filter size
FILTERMIN=100
FILTERMAX=2000

#data groups
groups=['0-2N,2-3Chi,3-5Chi ','0-2N,2-3Chi,3-5ChiFGF']

for group in groups:
    print group
    
    ##measure & pickle
    #assumes mdf and lsm files have same name
    mdffiles=glob.glob(path+'%s*.mdf'%group)
    filenames=[os.path.splitext(f)[0] for f in mdffiles]
    print filenames   
    measurements={}
    for i,filename in enumerate(filenames):
        measure(filename)        
    pickle.dump(measurements,open(path+'measurements_%s.pickle'%group,'wb'))

print 'Done'