Measurements

areas - histogram of segmented cell areas in pixels showing upper and lower included thresholds

bra_pos - % Bra +ve cells for each image, and averaged per group
			threshold > 4.0 perpixel intensity for TBra groups
					  > 2000 total cell intensity for dE groups

histogram_clickpoint - intensity per pixel in area 16x16 pixels around click point
histogram_total - total intensity in segmented area
histrogram_perpixel - intensity per pixel in segmented area

scatter_clickpoint - intensity per pixel in area 16x16 pixels around click point
scatter_total - total intensity in segmented area
scatter_perpixel - intensity per pixel in segmented area

*.csv - segmented cell area and total intensities for each group