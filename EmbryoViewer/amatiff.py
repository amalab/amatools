#
# AMA Lab toolkit module
# jpm
# mar 2013	
#
# image file input tools
#

#try:
	#import libtiff #creates memmapped numpy arrays using libtiff.dylib and ctypes 
	#mode='libtiff'
	#print 'using libtiff'
#except:
	#print 'libtiff not working - using tifffile instead - numpy arrays will be limited by memory size'
	#mode='tifffile'
	#import tifffile #loads into numpy memory object
	
print 'libtiff not working - using tifffile instead - numpy arrays will be limited by memory size'
mode='tifffile'
import tifffile #loads into numpy memory object
	
from bs4 import BeautifulSoup #for xml parsing
import matplotlib.pyplot
import numpy
import subprocess
import fnmatch
import os

#get location of the amatools module 
#amatools_dir=os.path.dirname(__file__)
#print 'amatools_dir',amatools_dir
#print 'cwd',os.getcwd()	

# wrap bioformats command line conversion tool
def convert_2_ometiff(filename):
	"""convert any type to .ome.tiff"""

	root=os.path.splitext(filename)[0]

	print 'Running bfconvert...'	
	r=os.system('../bftools/bfconvert "%s" "%s.ome.tiff"'%(filename,root))
	print 'bfconvert returned',r

def batch_convert(dirname,filetype='lsm'):
	"""batch convert all files of filetype in dirname (recursively) to .ome.tiff"""

	matches = []
	for root, dirnames, filenames in os.walk(dirname):
		for filename in fnmatch.filter(filenames,'*.'+filetype):
			matches.append(os.path.join(root, filename))

	for f in matches:
		convert_2_ometiff(f)

class AMATiff:
	"""use pylibtiff's numpy memmap to read an ome.tiff file if available
	   or tifffile to load into memory numpy if not"""

	def open(self,filename):
		"""open tiff file and create numpy array"""
		
		self.filename=filename
		if mode is 'libtiff':
			self.tiff_file=libtiff.TIFFfile(filename)
			self.tiff_array=self.tiff_file.get_tiff_array()[:] #need [:] to convert from TiffArray instance to numpy array
			#self.info() BS4 causing problems
			
		else: #tifffile
			# open file and create TIFFfile object with tifffile.py
	
			# get tiff file as numpy array
			self.tiffimg = tifffile.TIFFfile(filename)
			self.tiff_array=self.tiffimg.asarray()	
			#self.info()
			
			#reshape hack
			#t=int(amatiff.pixel_attributes['sizet'])
			#c=int(amatiff.pixel_attributes['sizec'])
			#x=int(amatiff.pixel_attributes['sizex'])
			#y=int(amatiff.pixel_attributes['sizey'])	
			
			self.tiff_array=self.tiff_array[:,0,:,:]
			self.tiff_array/=256

	def close(self):
		self.tiff_file.close()

	__del__=close

	def info(self):
		'''Create & print header & data shape for ome.tiff'''
		
		if mode is 'libtiff':

			# get header as string
			s=self.tiff_file.get_info()
			# make list of tags
			l=s.splitlines()
			
			try:
				# get ome.tiff xml header
				xml=s[s.find('"""')+3:s.rfind('"""')]
				
			except:
				print 'No XML header'
				
		else: #tifffile
			for page in self.tiffimg:
				for tag in page.tags.values():
					t = tag.name, tag.value
					if tag.name=='image_description' and tag.value is not '':
						xml=tag.value
						
		# parse XML
		self.header=BeautifulSoup(xml,"lxml")

		# get pixel tag attributes
		self.pixel_attributes=self.header.find('pixels').attrs
		print 'Pixel attributes:',self.pixel_attributes

		# get all channel tags
		ch=self.header.find_all('channel')

		# print channel name attributes
		print 'Channels:'
		i=0
		for c in ch:
			try:
				print i,c['name'] # in BeautifulSoup, attrs are dicts of bs tree objects
			except:
				print i,'No name'
			i+=1				

		print 'tiff_array shape:',self.tiff_array.shape # [time*channels,height,width]
		
		pass
	
	def info_fiji(self):
		'''Create & print header & data shape for ome.tiff'''
		
		if mode is 'libtiff':

			# get header as string
			s=self.tiff_file.get_info()
			# make list of tags
			l=s.splitlines()
			
			try:
				# get ome.tiff xml header
				xml=s[s.find('"""')+3:s.rfind('"""')-3]
					
			except:
				print 'No XML header'
				
		else: #tifffile
			for page in self.tiffimg:
				for tag in page.tags.values():
					t = tag.name, tag.value
					if tag.name=='image_description' and tag.value is not '':
						xml=tag.value
						
		# parse XML
		self.header=BeautifulSoup(xml)
		
		print self.header
		print self.tiff_array.shape
		
	def reshape_fiji(self):
		'''Reshape data from Figi tif file using html header info'''
		self.info_fiji()
		
		l=self.header.find('p').decode_contents().split('\n')
		for i in l:
			if i[0:8]=='channels':
				channels=int(i.split('=')[1])
				
		#print channels
		
		sh=self.tiff_array.shape
		self.tiff_array=self.tiff_array.reshape((-1,channels,sh[1],sh[2]))
		print self.tiff_array.shape

	def plot(self,t=0):
		##test plot

		#num_ch=int(self.pixel_attributes['sizec'])	
		num_ch=self.tiff_array.shape[1]	
		fig=matplotlib.pyplot.figure()		

		for c in range(num_ch):
			p=fig.add_subplot(1,num_ch,c+1) 
			p.set_title('Channel '+str(c))	

			im=self.tiff_array[t,c,:,:]
			p.imshow(im)

		#matplotlib.pyplot.show()
		return p

	def plot_overview(self):

		num_ch=int(self.pixel_attributes['sizec'])	
		num_t=int(self.pixel_attributes['sizet'])	

		fig=matplotlib.pyplot.figure()	
		fig.suptitle(self.filename)
		num_t_plots=5

		for c in range(num_ch):
			for t in range(num_t_plots):
				p=fig.add_subplot(num_ch,num_t_plots,c*num_t_plots+t+1) 
				t_index=t*num_t/num_t_plots

				im=self.tiff_array[c+t_index*2,:,:]
				im_max=im.max()
				im_min=im.min()

				im=im.astype(numpy.float32)
				im/=4096.0						
				p.imshow(im,vmin=0.0,vmax=0.25,cmap=matplotlib.cm.Greys_r)

				p.set_title('Ch:'+str(c)+', T:'+str(t_index)+' Min:'+str(im_min)+' Max:'+str(im_max))	


		matplotlib.pyplot.show()


###OLD
#def zvi_2_ometiff(filename):
	#"""convert .zvi to .ome.tiff"""

	#root=os.path.splitext(filename)[0]

	#print 'Running bfconvert...'
	#r=os.system(amatools_dir+'/bfconvert "%s" "%s.ome.tiff"'%(filename,root))
	##args=[amatools_dir+'/bfconvert',filename+'.zvi',filename+'.ome.tiff']
	##r=subprocess.call(args,stdout=subprocess.STDOUT)
	#print 'bfconvert returned',r

#def lsm_2_ometiff(filename):
	#"""convert .lsm to .ome.tiff"""

	#root=os.path.splitext(filename)[0]

	#print 'Running bfconvert...'	
	#r=os.system(amatools_dir+'/bfconvert "%s" "%s.ome.tiff"'%(filename,root))
	#print 'bfconvert returned',r

#def lif_2_ometiff(filename):
	#"""convert .lif to .ome.tiff"""

	#root=os.path.splitext(filename)[0]

	#print 'Running bfconvert...'	
	#r=os.system(amatools_dir+'/bfconvert "%s" "%s.ome.tiff"'%(filename,root))
	#print 'bfconvert returned',r
	
