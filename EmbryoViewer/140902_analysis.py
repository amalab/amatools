"""
Analyse session files from mouse_embryo tool
JPM July 2013
"""

import os
import fnmatch
import pickle
from amatiff import AMATiff
import numpy
import matplotlib.pyplot as plt

#channels
DNA=0
SPRY4=1
BRIGHT=2
NANOG=3
GATA6=4

def get_data_from_tiff(fn):
	amatiff=AMATiff()
	amatiff.open(fn)
	amatiff.info()

	num_channels=int(amatiff.pixel_attributes['sizec'])
	x=int(amatiff.pixel_attributes['sizex'])
	y=int(amatiff.pixel_attributes['sizey'])

	# decimate x and y by D
	#D=4
	a=amatiff.tiff_array[:,::D,::D]
	# reshape to (channels,z,y,x)
	data=numpy.reshape(a,(-1,num_channels,a.shape[2],a.shape[1])) #-1 means calc it
	data=numpy.swapaxes(data,0,1)

	return data,num_channels

def get_matches(dirname,type):
	'''get all files of type recursively from dirname'''
	matches=[]
	for root, dirnames, filenames in os.walk(dirname):
		for filename in fnmatch.filter(filenames,'*'+type):
			matches.append(os.path.join(root, filename))
			
	return matches

#plot	
x=[]
y=[]

matches=get_matches('.','session')
out_file=open('140902_analysis_output.csv','w')
    
for fn in matches:
	pickle_file=open(fn,'rb')
	fn_tiff=pickle.load(pickle_file)
	img_orig=pickle.load(pickle_file)
	shape_img=img_orig.shape
	img_watershed_filtered=pickle.load(pickle_file)
	try:
		D=pickle.load(pickle_file)	
	except:
		print 'D missing from session file'
		D=1
	
	print fn,fn_tiff,shape_img,D
	
	#convert from christian's hardwired dir to my local
	fn_tiff=os.path.split('./'+os.path.split(fn_tiff)[0])[1]+'/'+os.path.split(fn_tiff)[1]
	
	data,num_channels=get_data_from_tiff(fn_tiff)
	print data.shape

	
	#get the data!
	num_labels=numpy.max(img_watershed_filtered)
	
	#output session file name, tiff file name, number of labels
	s=''
	num_cells=0
	for l in range(1,num_labels+1):
		w=numpy.where(img_watershed_filtered==l)
		if len(w[0]>0):	
			s+=(str(l)+',')
			num_cells+=1
			
			signal=[]
			for c in range(num_channels):
				voxels=data[c][w]
				num_voxels=len(voxels)
				signal_tot=numpy.sum(voxels)
				if num_voxels>0:
					signal_ave=float(signal_tot)/float(num_voxels)
				else:
					signal_ave=0.0
				signal.append(signal_ave)
					
				s+=(str(c)+','+str(num_voxels)+','+str(signal_tot)+','+str(signal_ave)+',')
										
			s=s[0:-1]+'\n'
			
			x.append(signal[SPRY4]/signal[DNA])
			y.append(signal[NANOG]/signal[GATA6]) 
			
	#x+=[num_cells]*num_cells
		
	print 'num_cells:',num_cells
	
	out_file.write(fn+','+fn_tiff+','+str(num_cells)+'\n'+s)	
			
fig=plt.figure()
p=fig.add_subplot(1,1,1)
p.scatter(x,y)	
p.set_ylabel('140902_SPRY4:DNA')
p.set_xlabel('NANOG:GATA6')
fig.savefig('140902_SPRY4:DNA_v_NANOG:GATA6.svg')
	
pass
