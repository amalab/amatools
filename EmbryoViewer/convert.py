import sys
import os
import fnmatch

# wrap bioformats command line conversion tool
def convert_2_ometiff(filename):
	"""convert any type to .ome.tiff"""

	root=os.path.splitext(filename)[0]

	print 'Running bfconvert...'	
	r=os.system('bftools/bfconvert "%s" "%s.ome.tiff"'%(filename,root))
	print 'bfconvert returned',r

def batch_convert(dirname,filetype='lsm'):
	"""batch convert all files of filetype in dirname (recursively) to .ome.tiff"""

	matches = []
	for root, dirnames, filenames in os.walk(dirname):
		for filename in fnmatch.filter(filenames,'*.'+filetype):
			matches.append(os.path.join(root, filename))

	for f in matches:
		convert_2_ometiff(f)
		
		
if __name__ == '__main__':
	batch_convert(sys.argv[1])
