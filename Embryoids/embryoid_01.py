import sys
sys.path.insert(0,'../amatools') #adds cwd to path, to pickup modules local to amatools_dist directory

from mayavi import mlab
import amatiff
import numpy

#point this to tiff file location
#fn=r'Activin Day 2-3 Fibronectin 488 E-Cadherin 568 Phalloidin 647 1.ome.tiff'
fn=r'2pm 40xobj 3um stack separation GPI-GFP 1.ome.tiff'

# get tiff file as numpy array
amatiff=amatiff.AMATiff()
amatiff.open(fn)
amatiff.info()

# get size of data
num_channels=int(amatiff.pixel_attributes['sizec'])
x=int(amatiff.pixel_attributes['sizex'])
y=int(amatiff.pixel_attributes['sizey'])

# decimate x and y by D
D=2
a=amatiff.tiff_array[:,::D,::D]
# reshape to (channels,z,y,x)
data=numpy.reshape(a,(-1,num_channels,a.shape[2],a.shape[1])) #-1 means calc it
data=numpy.swapaxes(data,0,1)

#blue channel is just Hoescht, the red is anti-Sox2, the green is the endogenous GFP signal and the greyscale channel is anti-Brachyury. 
# choose channel		
channel=1
img_orig=data[channel]
shape_img=img_orig.shape
print '(Z,Y,X):',shape_img

# volume render	
mlab.figure('volume')
sc=mlab.pipeline.scalar_field(img_orig)
#stretch z out a bit
sc.spacing = [shape_img[2]/shape_img[0], 1, 1]
sc.update_image_data = True
mlab.pipeline.volume(sc)

mlab.show()