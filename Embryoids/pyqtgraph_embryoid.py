#import examples
#examples.run()

# -*- coding: utf-8 -*-
"""
Demonstrates GLVolumeItem for displaying volumetric data.

"""

## Add path to library (just for examples; you do not need this)
#import examples.initExample

from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.opengl as gl

app = QtGui.QApplication([])
w = gl.GLViewWidget()
w.opts['distance'] = 200
w.show()
w.setWindowTitle('pyqtgraph example: GLVolumeItem')

#b = gl.GLBoxItem()
#w.addItem(b)
g = gl.GLGridItem()
g.scale(10, 10, 1)
w.addItem(g)


###
#import numpy as np
### Hydrogen electron probability density
#def psi(i, j, k, offset=(50,50,100)):
    #x = i-offset[0]
    #y = j-offset[1]
    #z = k-offset[2]
    #th = np.arctan2(z, (x**2+y**2)**0.5)
    #phi = np.arctan2(y, x)
    #r = (x**2 + y**2 + z **2)**0.5
    #a0 = 2
    ##ps = (1./81.) * (2./np.pi)**0.5 * (1./a0)**(3/2) * (6 - r/a0) * (r/a0) * np.exp(-r/(3*a0)) * np.cos(th)
    #ps = (1./81.) * 1./(6.*np.pi)**0.5 * (1./a0)**(3/2) * (r/a0)**2 * np.exp(-r/(3*a0)) * (3 * np.cos(th)**2 - 1)
    
    #return ps
    
    ##return ((1./81.) * (1./np.pi)**0.5 * (1./a0)**(3/2) * (r/a0)**2 * (r/a0) * np.exp(-r/(3*a0)) * np.sin(th) * np.cos(th) * np.exp(2 * 1j * phi))**2 


#data = np.fromfunction(psi, (100,100,200))
#positive = np.log(np.clip(data, 0, data.max())**2)
#negative = np.log(np.clip(-data, 0, -data.min())**2)

#d2 = np.empty(data.shape + (4,), dtype=np.ubyte)
#d2[..., 0] = positive * (255./positive.max())
#d2[..., 1] = negative * (255./negative.max())
#d2[..., 2] = d2[...,1]
#d2[..., 3] = d2[..., 0]*0.3 + d2[..., 1]*0.3
#d2[..., 3] = (d2[..., 3].astype(float) / 255.) **2 * 255

#d2[:, 0, 0] = [255,0,0,100]
#d2[0, :, 0] = [0,255,0,100]
#d2[0, 0, :] = [0,0,255,100]
###

import sys
sys.path.insert(0,'../amatools') #adds cwd to path, to pickup modules local to amatools_dist directory

#from mayavi import mlab
import amatiff
import numpy

#point this to tiff file location
#fn=r'Activin Day 2-3 Fibronectin 488 E-Cadherin 568 Phalloidin 647 1.ome.tiff'
fn=r'2pm 40xobj 3um stack separation GPI-GFP 1.ome.tiff'

# get tiff file as numpy array
amatiff=amatiff.AMATiff()
amatiff.open(fn)
amatiff.info()

# get size of data
num_channels=int(amatiff.pixel_attributes['sizec'])
x=int(amatiff.pixel_attributes['sizex'])
y=int(amatiff.pixel_attributes['sizey'])

# decimate x and y by D
D=3
a=amatiff.tiff_array[:,::D,::D]
# reshape to (channels,z,y,x)
data=numpy.reshape(a,(-1,num_channels,a.shape[2],a.shape[1])) #-1 means calc it
data=numpy.swapaxes(data,0,1)

# choose channel		
channel=1
img_orig=data[channel]
shape_img=img_orig.shape
print '(Z,Y,X):',shape_img

# rescale values
L=16 #level and width from imagej's level adjuster
W=27
#L=128
#W=255
img_temp=img_orig.astype(numpy.float32)
img_thresh=255*(img_temp-L)/W+128
numpy.clip(img_thresh,0,255,out=img_thresh)
img_thresh=img_thresh.astype(numpy.uint8,copy=False)

#y of interp=numpy.interp(x of interp, x of orig, y of orig)
#Voxel size: 0.31x0.31x3 micron
numx=shape_img[2]
numy=shape_img[1]
numz=shape_img[0]
numz_tocreate=numz*4
img_interp=numpy.zeros((numz_tocreate,numy,numx))

for i in range(numx): #x
    print '.',
    for j in range(numy): #y
        yorig=img_thresh[:,j,i]
        yinterp=numpy.interp(numpy.linspace(0,numz,numz_tocreate),numpy.linspace(0,numz-1,numz),yorig)
        img_interp[:,j,i]=yinterp
        
d2 = numpy.empty(img_interp.shape + (4,), dtype=numpy.ubyte)
d2[...,1]=img_interp #*(255./img_orig.max()) #G
d2[...,3]=2 #A

v = gl.GLVolumeItem(d2)
v.translate(-50,-50,-100)
w.addItem(v)

ax = gl.GLAxisItem()
w.addItem(ax)

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
