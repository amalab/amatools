import pickle
import pandas
from matplotlib import pyplot
import numpy

filename='Hsp83[e64A]hetOR_PH3_20x_G10_sti+bld.proj'
#filename='greybar.proj'

pickle_file=open(filename,'rb')
tif_filename=pickle.load(pickle_file)
num_tif_channels=pickle.load(pickle_file)
tif_channel=pickle.load(pickle_file)
dataframe=pickle.load(pickle_file)
line_path_points=pickle.load(pickle_file)
view_scale=pickle.load(pickle_file)

fig=pyplot.figure()
num_measure_bins=100

for ch in range(num_tif_channels):
	d=dataframe[dataframe['channel']==ch] #select only records for current channel
	V_hist=d['v_total']/(max(d['v_total'])/num_measure_bins) #scale V_total to be in range num_measure_bins
	V_hist=abs(V_hist.astype(numpy.int16)) #make int and non-neg		
	bc=numpy.bincount(V_hist).astype(numpy.float64) #count pixel numbers at each v_total extent
	bcw=numpy.bincount(V_hist,weights=d['value']) #sum pixel values for each v_total
	h=bcw/bc #get average value per pixel

	p=fig.add_subplot(1,num_tif_channels,ch+1)
	p.plot(h)
	p.set_title('Channel %d'%ch)
	p.set_xlabel('Distance along spline (normalised)')
	p.set_ylabel('Average pixel intensity')
	
pyplot.show(block=True)
