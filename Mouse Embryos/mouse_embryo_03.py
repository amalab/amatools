from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.filter
import skimage.morphology
import chaco.api

import amatools

from tvtk.api import tvtk

# colour maps
# random for segmentation view
r=numpy.random.random((1024,4))
r[0,:]=(1.0,1.0,1.0,0.0) #white background
rand_col_map=chaco.api.ColorMapper.from_palette_array(r)

###LOAD DATA
#fn = tkFileDialog.askopenfilename()
#fn=r'/Users/jpm/DATA/David - Images for tracking/2012-03-19 Wntch 2iLIF to labelled-0002_2i to ESminusLIF_01.zvi - C=2 slices=90-100 8-bit.tif'
#fn=r'/Users/jpm/DATA/_scratch/120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2.ome.tiff'
#fn=r'/Users/jpm/DATA/Christian/Mouse Embryos/130429_slideI_embryo2'
fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
#amatools.lsm_2_ometiff(fn)

# get tiff file as numpy array
amatiff=amatools.AMATiff()
amatiff.open(fn+'.ome.tiff')
amatiff.info()

num_channels=int(amatiff.pixel_attributes['sizec'])

# get first channel
# decimate x and y by 4
imgarray=amatiff.tiff_array[::num_channels,::4,::4]

###PROCESS
sigma=1.0
image_processed=scipy.ndimage.filters.gaussian_filter(imgarray,(sigma,sigma,sigma))

thresh=skimage.filter.threshold_otsu(image_processed)
binary=image_processed>thresh
image_processed=binary.astype('int')	

###block_size=3+self.thresh*100
##if self.block_size%2==0:
	##self.block_size+=1 #must be odd
##block_size=20
##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
##image_processed=image_processed.astype('int')	

#sc.scalar_data=image_processed	

##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

distance = scipy.ndimage.distance_transform_edt(image_processed)
####distance=gaussian_filter(distance)			
distance = scipy.ndimage.gaussian_filter(distance,4)
footprint=3
local_max = skimage.morphology.is_local_maximum(distance, image_processed,numpy.ones((footprint,footprint,footprint)))
markers = scipy.ndimage.label(local_max)[0]
labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)

#stacked=numpy.hstack((imgarray,labels))
#scalar_field.scalar_data=stacked

###MAYAVI	
#imgarray_sf=mlab.pipeline.scalar_field(imgarray)
#imgarray_sf.spacing=[1, 1, 1]
#imgarray_sf.update_image_data=True
#imgarray_ipw=mlab.pipeline.image_plane_widget(imgarray_sf,colormap='black-white')

#labels_sf=mlab.pipeline.scalar_field(labels,extent=(0,256,256,512,0,256))# (xmin, xmax, ymin, ymax, zmin, zmax)
##tf=mlab.pipeline.transform_data()
#labels_sf.spacing=[1, 1, 1]
#labels_sf.update_image_data=True
#labels_ipw=mlab.pipeline.image_plane_widget(labels_sf,colormap='black-white')

## random lut for segmentation view
#r=numpy.random.randint(256,size=(256,4))
#r[0,:]=(0,0,0,0) #transparent background
#lut=labels_ipw.module_manager.scalar_lut_manager.lut.table
#labels_ipw.module_manager.scalar_lut_manager.lut.table=r

imd = tvtk.ImageData(spacing=(1, 1, 1), origin=(0,0,0))
imd.point_data.scalars = imgarray.T.ravel()
imd.point_data.scalars.name = 'scalars'
imd.dimensions = imgarray.shape
#mlab.pipeline.surface(imd)
#mlab.pipeline.surface(mlab.pipeline.image_plane_widget(imd,colormap='black-white'))
ipw_img=mlab.pipeline.image_plane_widget(imd,colormap='black-white')

imd = tvtk.ImageData(spacing=(1, 1, 1), origin=(0,imgarray.shape[1], 0))
imd.point_data.scalars = labels.T.ravel()
imd.point_data.scalars.name = 'scalars'
imd.dimensions = labels.shape
#mlab.pipeline.surface(imd)
#mlab.pipeline.surface(mlab.pipeline.image_plane_widget(imd,colormap='black-white'))
ipw_labels=mlab.pipeline.image_plane_widget(imd)
ipw_labels.ipw.texture_interpolate=0
ipw_labels.ipw.reslice_interpolate='nearest_neighbour'

# random lut for segmentation view
r=numpy.random.randint(256,size=(256,4))
r[0,:]=(0,0,0,255) #black background
#lut=ipw_labels.module_manager.scalar_lut_manager.lut.table
ipw_labels.module_manager.scalar_lut_manager.lut.table=r

#sync ipws
ipw_labels.ipw.sync_trait('slice_position',ipw_img.ipw)

#imgw = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=0.3)
#iso = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=1.0)
#contour=iso.contour.outputs[0]

#mlab.show()

#iso = mlab.pipeline.iso_surface(sc, contours=[image_processed.min()+0.1*image_processed.ptp(),],opacity=1.0)
#contour=iso.contour.outputs[0]
mlab.draw()
mlab.show()
pass