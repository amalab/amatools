# Enthought imports.
from traits.api import HasTraits,Instance,List,Str,Button,Bool,Int,Range,on_trait_change
from traitsui.api import View,Item,HGroup,VGroup,EnumEditor,RangeEditor
from tvtk.pyface.scene_editor import SceneEditor
from enthought.pyface.api import FileDialog, OK, confirm, YES

from mayavi.tools.mlab_scene_model import MlabSceneModel
from mayavi.core.ui.mayavi_scene import MayaviScene
#from mayavi.core.api import PipelineBase
from mayavi.sources.api import ArraySource

from chaco.api import Plot,BarPlot,ArrayPlotData
from enable.component_editor import ComponentEditor

import sklearn.feature_extraction
import sklearn.cluster

#from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.feature
import skimage.filter
import skimage.morphology
#import chaco.api
import colorsys
import pickle
import libtiff
import os
import amatools

from tvtk.api import tvtk

import nipy.algorithms.segmentation

debug=False
#decimation of input image x & y
#D=3

# random lut for segmentation view
random_lut=numpy.random.randint(256,size=(256,4))
random_lut[0,:]=(0,0,0,255) #black background

colors_lut=numpy.zeros((256,4))
for i in range(256):
	r,g,b=colorsys.hls_to_rgb(float((i*17)%256)/256.0,0.4,0.9)
	colors_lut[i,:]=(int(r*256),int(g*256),int(b*256),255)
colors_lut[0,:]=(0,0,0,255) #black background


class EmbryoViewer(HasTraits):

	# The scene model.
	scene = Instance(MlabSceneModel,())
	
	open_tiff_button=Button(label='Open .ome.tiff')
	open_session_button=Button(label='Open Session')
	save_session_button=Button(label='Save Session')
	
	zero=Int(0)
	num_channels=Int(1)	
	channel=Range(low='zero',high='num_channels',value=0,editor=RangeEditor(low_name='zero',high_name='num_channels',mode="spinner"))	
	D=Range(1,5,value=3,editor=RangeEditor(low=1,high=4,mode="spinner"))	

	save_button=Button(label='Save Processed')	
	max_button=Button(label='Remove Seed')	
	undo_button=Button(label='Undo Seed')	

	image_enum=List(['img_orig','img_denoise','img_thresh','img_distance','img_local_max','img_labels','img_markers','img_watershed','img_watershed_filtered'])
	image_type=Str('img_watershed_filtered')

	denoise_weight=Range(0,10,1,editor=RangeEditor(low=0, high=9, mode="slider"))
	thresh_offset=Range(0.0,10.0,8.0,editor=RangeEditor(low=0.0, high=9.9, mode="slider"))
	block_size=Range(0,100,40,editor=RangeEditor(low=0, high=99, mode="slider"))

	sizes_plot = Instance(Plot)
	
	filter_switch=Bool(True)
	filter_size=Range(0,1000,500,editor=RangeEditor(low=0, high=999, mode="slider"))

	#ipw_orig = Instance(PipelineBase)
	#ipw_proc = Instance(PipelineBase)

	######################
	# Using 'scene_class=MayaviScene' adds a Mayavi icon to the toolbar,
	# to pop up a dialog editing the pipeline.
	spc='10'
	view = View(HGroup(
	    VGroup(
	        Item('D',label='Decimate'),
	        Item(spc),
	        Item('open_tiff_button',show_label=False),
	        Item('save_button'),
	        Item(spc),
	        Item('open_session_button',show_label=False),
	        Item('save_session_button',show_label=False),
	        Item(spc),
	        Item('image_type',editor=EnumEditor(name='image_enum')),
	        Item('channel'),
	        Item(spc),
	        Item('undo_button',show_label=False),
	        Item('max_button',show_label=False),
	        Item(spc),
	        #Item('denoise_weight'),
	        #Item('thresh_offset'),
	        #Item('block_size'),        
	        Item('sizes_plot',editor=ComponentEditor(),show_label=False,width=300),
	        Item('filter_switch'),
	        Item('filter_size'),
	        Item('600')),#spacer

	    Item(name='scene',
	         editor=SceneEditor(scene_class=MayaviScene),
	         show_label=False,
	         resizable=True,
	         height=1200,
	         width=1600)),
	    resizable=True,
		height=1200,
		width=1800 
	    )

	def __init__(self, **traits):
		HasTraits.__init__(self, **traits)

		self.clicked_lut=None
		self.selected_label=0
		self.last_position=(0.0,0.0,0.0)
		self.fn_tiff=''
		self.start=True
		
		#self.generate_data()
		#self.create_mlab()

		#x = numpy.linspace(-14, 14, 100)
		#y = numpy.sin(x) * x**3
		#plotdata = ArrayPlotData(x=x, y=y)

		#self.sizes_plot = Plot(plotdata,fixed_preferred_size=[300,200])
		#self.sizes_plot.plot(("x", "y"), type="line", color="blue")
		#self.sizes_plot.title = "sin(x) * x^3"	
		
		#self.sizes_plot.outer_bounds = [300, 200]
		
	def _open_tiff_button_fired(self,events):
		#self.fn_tiff=easygui.fileopenbox(default=data_path+'/')
		dialog = FileDialog(action="open",wildcard='OME Tiff files (*.tiff)|*.tiff|')#, default_path=data_path+'/')
		dialog.open()
		if dialog.return_code == OK:		
			self.fn_tiff=dialog.path
			if self.fn_tiff:
				self.load_data(self.fn_tiff)
				self.process()
				self.display_scene()
				self.update_ipw_orig()
				self.update_ipw_proc()
				
	def _save_session_button_fired(self,events):
		dialog = FileDialog(action="save as",wildcard='Session files (*.session)|*.session|')#, default_path=data_path+'/')
		dialog.open()
		if dialog.return_code == OK:		
			self.session_file=dialog.path
			if self.session_file:
				pickle_file=open(self.session_file,'wb')
				pickle.dump(self.fn_tiff,pickle_file,protocol=2) #binary fast protocol
				pickle.dump(self.img_orig,pickle_file,protocol=2)
				pickle.dump(self.img_local_max,pickle_file,protocol=2)
				pickle.dump(self.img_watershed_filtered,pickle_file,protocol=2) #actual result
				pickle_file.close()		
				
	def _open_session_button_fired(self,events):
		dialog = FileDialog(action="open",wildcard='Session files (*.session)|*.session|')#, default_path=data_path+'/')
		dialog.open()
		if dialog.return_code == OK:		
			self.session_file=dialog.path
			if self.session_file:
				pickle_file=open(self.session_file,'rb')
				self.fn_tiff=pickle.load(pickle_file)
				self.img_orig=pickle.load(pickle_file)
				self.shape_img=self.img_orig.shape
				self.process()
				self.img_local_max=pickle.load(pickle_file)	
				self.proc_watershed()
				self.proc_filter_size()
				self.display_scene()
				self.update_ipw_orig()
				self.update_ipw_proc()			
				
	def load_data(self,fn):
		#fn = tkFileDialog.askopenfilename()
		#fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
		#amatools.lsm_2_ometiff(fn)

		# get tiff file as numpy array
		amatiff=amatools.AMATiff()
		amatiff.open(fn)
		amatiff.info()

		self.num_channels=int(amatiff.pixel_attributes['sizec'])
		x=int(amatiff.pixel_attributes['sizex'])
		y=int(amatiff.pixel_attributes['sizey'])

		# decimate x and y by D
		#D=4
		a=amatiff.tiff_array[:,::self.D,::self.D]
		# reshape to (channels,z,y,x)
		self.data=numpy.reshape(a,(-1,self.num_channels,a.shape[2],a.shape[1])) #-1 means calc it
		self.data=numpy.swapaxes(self.data,0,1)

		# get first channel			
		self.img_orig=self.data[self.channel]
		self.shape_img=self.img_orig.shape

		# sum 
		##s=numpy.sum(data,0)/num_channels
		#s=(data[1]+data[2])/2
		#self.img_orig=s.astype('uint8')
		#pass
		
			
	#def generate_data(self):
	@on_trait_change('scene.activated')
	def display_scene(self):  
		
		#start with zeroed images to setup mayavi objects
		if self.start:
			self.shape_img=(128,128,128) 
			self.img_orig=numpy.zeros(self.shape_img)
			self.img_watershed=numpy.zeros(self.shape_img)
			self.start=False

		###MAYAVI	
		#mlab replaced with self.scene.mlab
		
		#clear pipeline
		for i in ('as_orig','as_proc','ipw_orig','ipw_proc'):
			if hasattr(self,i):
				a=getattr(self,i)
				a.remove()
				
			
		#LHS orig
		self.as_orig=ArraySource(scalar_data=self.img_orig)
		self.ipw_orig=self.scene.mlab.pipeline.image_plane_widget(self.as_orig,colormap='black-white')
		self.ipw_orig.ipw.texture_interpolate=0
		self.ipw_orig.ipw.reslice_interpolate='nearest_neighbour'	

		#RHS proc
		self.as_proc=ArraySource(scalar_data=self.img_watershed,origin=(0,self.img_orig.shape[1],0))	
		self.ipw_proc=self.scene.mlab.pipeline.image_plane_widget(self.as_proc)
		self.ipw_proc.ipw.texture_interpolate=0
		self.ipw_proc.ipw.reslice_interpolate='nearest_neighbour'
		#turn-off autorange so random lut looks-up according to absolute index
		self.ipw_proc.module_manager.scalar_lut_manager.use_default_range=False
		self.ipw_proc.module_manager.scalar_lut_manager.data_range=numpy.array([0.0,255.0])

		#store orig lut and change to random one
		self.orig_lut=self.ipw_proc.module_manager.scalar_lut_manager.lut.table
		#self.ipw_proc.module_manager.scalar_lut_manager.lut.table=random_lut
		self.ipw_proc.module_manager.scalar_lut_manager.lut.table=colors_lut

		#trait stuff
		#sync ipws
		self.ipw_proc.ipw.sync_trait('slice_position',self.ipw_orig.ipw)

		self.ipw_orig.on_trait_change(self.ipw_orig_slice_position,name='ipw.slice_position')	

		self.scene.mlab.view(50,50)

		#picking 	
		self.ipw_orig.ipw.right_button_action = 0
		# Add a callback on the image plane widget interaction
		self.ipw_orig.ipw.add_observer('StartInteractionEvent', self.pick_callback_orig)

		self.ipw_proc.ipw.right_button_action = 0
		# Add a callback on the image plane widget interaction
		self.ipw_proc.ipw.add_observer('StartInteractionEvent', self.pick_callback_proc)		

	@on_trait_change('image_type')
	def update_ipw_proc(self):
		#print self.image_type
		try:
			self.ipw_proc
		except:
			return

		#luts
		if self.image_type in ['img_watershed','img_watershed_filtered','img_labels']:
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=colors_lut
			self.ipw_proc.module_manager.scalar_lut_manager.use_default_range=False
			self.ipw_proc.module_manager.scalar_lut_manager.data_range=numpy.array([0.0,255.0])

		else:
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=self.orig_lut
			self.ipw_proc.module_manager.scalar_lut_manager.use_default_range=True

		#ipw_proc.scalars=eval('self.'+self.image_type)
		#self.imd_proc.point_data.scalars=self.img_denoise.T.ravel()
		self.as_proc.scalar_data=eval('self.'+self.image_type)
		
	def update_ipw_orig(self):
		#print self.image_type
		try:
			self.ipw_orig
		except:
			return
	
		self.as_orig.scalar_data=self.img_orig
		
	def _channel_changed(self):
		self.img_orig=self.data[self.channel]
		self.process()
		self.update_ipw_orig()

	@on_trait_change('denoise_weight,thresh_offset,block_size')
	def process(self):
		self.proc_thresh_max()
		self.proc_watershed()
		self.proc_filter_size()
		#self.proc_nipy()
		self.update_ipw_proc()	
		
	def proc_nipy(self):
		nclasses=2
		nchannels=1
		mu = numpy.zeros((nclasses, nchannels))
		sigma = numpy.zeros((nclasses, nchannels, nchannels))		
		seg=nipy.algorithms.segmentation.segmentation.Segmentation(self.img_orig,mu=mu,sigma=sigma)
		self.img_watershed=seg.map()
		self.img_watershed_filtered=self.img_watershed

	def proc_thresh_max(self):
		#smooth
		sigma=1.0
		self.img_denoise=scipy.ndimage.filters.gaussian_filter(self.img_orig,(sigma,sigma,sigma))	
		#self.img_denoise=skimage.filter.denoise_bilateral(self.img_orig,sigma_range=0.5,sigma_spatial=15)
		#self.img_denoise=skimage.filter.denoise_tv_chambolle(self.img_orig,weight=0.1)#self.denoise_weight)

		print 'denoise done'

		#threshold
		#otsu
		#thresh_value=skimage.filter.threshold_otsu(self.img_denoise)
		#self.img_thresh=self.img_denoise>thresh_value

		#local - too slow for 3d
		self.img_thresh=numpy.zeros(self.shape_img)
		#block_size=33
		for z in range(self.shape_img[2]):
			#print z
			self.img_thresh[:,:,z]=skimage.filter.threshold_adaptive(self.img_denoise[:,:,z],block_size=self.block_size,offset=-self.thresh_offset)

		self.img_thresh=self.img_thresh.astype('uint8')
		print 'threshold done'

		###block_size=3+self.thresh*100
		##if self.block_size%2==0:
				##self.block_size+=1 #must be odd
		##block_size=20
		##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
		##image_processed=image_processed.astype('int')	

		#sc.scalar_data=image_processed	

		##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
		##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

		#label thresholded image
		self.img_labels=scipy.ndimage.label(self.img_thresh)[0]	
		print 'labels done'
		#distance to background from thresholded image
		self.img_distance = scipy.ndimage.distance_transform_edt(self.img_thresh)
		print 'distance done'

		####distance=gaussian_filter(distance)			
		#self.img_distance = scipy.ndimage.gaussian_filter(self.img_distance,4)
		footprint=3
		#self.img_local_max = skimage.morphology.is_local_maximum(self.img_distance,self.img_thresh)#,numpy.ones((footprint,footprint,footprint)))
		#self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_thresh,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))

		#local max of distance within each labelled region
		#self.img_local_max=numpy.zeros(self.shape_img)	
		self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_labels,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))
		self.img_local_max=self.img_local_max.astype('uint8')
		print 'local_max done'

	def proc_watershed(self):
		#label maxima
		self.img_markers = scipy.ndimage.label(self.img_local_max,structure=numpy.ones((3,3,3)))[0]
		#watershed distance image with maxima as seeds
		self.img_watershed = skimage.morphology.watershed(-self.img_distance, self.img_markers, mask=self.img_thresh)
		print 'watershed done'		
		
	def proc_filter_size(self):
		#filter by size of object			
		self.img_watershed_filtered=numpy.copy(self.img_watershed)
				
		num_objects=numpy.max(self.img_watershed)
				
		ii=numpy.arange(1,num_objects) #labels of objects
		s=scipy.ndimage.measurements.sum(self.img_watershed,self.img_watershed,ii) #sum of labelled regions
		s=s/ii #divide by label value to get size of region
				
		for i in range(len(ii)):
			if int(s[i])<self.filter_size:
				region=numpy.where(self.img_watershed==i+1) #zeroth index is label 1
				print 'removing object',i
				self.img_watershed_filtered[region]=0						
					
		#size plot
		#num_objects=numpy.max(self.img_watershed_filtered)
		#sizes=[]
		#for i in range(1,num_objects+1):
			#size=len(numpy.where(self.img_watershed_filtered==i)[0])
			#if size>0: #to catch labels zeroed by size filter
				#sizes.append(size)
			
		hist,bins=numpy.histogram(s)
		plotdata = ArrayPlotData(index=bins,value=hist)
		self.sizes_plot = Plot(plotdata)
		self.sizes_plot.plot(("index","value"), type="line")
		self.sizes_plot.title = "object sizes"	
		
		print 'size filter done'
					
	def _filter_size_changed(self):
		self.proc_filter_size()
		self.update_ipw_proc()	
		
	def _filter_switch_changed(self):
		if self.filter_switch:
			self.image_type='img_watershed_filtered'
		else:
			self.image_type='img_watershed'
			
		self.update_ipw_proc()

	#@on_trait_change('self.ipw_orig.ipw.slice_position')
	def ipw_orig_slice_position(self):
		print self.ipw_orig.ipw.slice_position

	# picking
	def pick_callback_orig(self,widget,event):

		# hack to reject left hand mouse clicks 
		position = widget.GetCurrentCursorPosition()
		if position == (0.0,0.0,0.0) or position==self.last_position:
			#print position
			return

		self.last_position=position
		position=tuple([int(p) for p in position])

		#value clicked on
		val=int(widget.GetCurrentImageValue())
		print position,val

		#make copy and set whole array so image is updated
		ilm=numpy.copy(self.img_local_max)
		ilm[position]=1
		self.img_local_max=ilm
		#self.img_local_max[position]=1

		#redo watershed				
		self.proc_watershed()
		self.proc_filter_size()
		self.update_ipw_proc()


	def pick_callback_proc(self,widget,event):
		
		# hack to reject left hand mouse clicks 
		position = widget.GetCurrentCursorPosition()
		if position == (0.0,0.0,0.0):
			#print position
			return

		if self.image_type not in ('img_watershed','img_watershed_filtered'):
			return

		#value clicked on
		val=int(widget.GetCurrentImageValue())
		print position,val

		if val>0: 
			self.selected_label=val
			#self.ipw_proc.module_manager.scalar_lut_manager.lut.table[val]=(255,255,255,255)
			if not self.clicked_lut:
				self.clicked_lut=self.ipw_proc.module_manager.scalar_lut_manager.lut.table

			#have to set whole table at once to trigger redraw
			new_lut=numpy.zeros((256,4))
			for i in range(256):
				new_lut[i]=self.clicked_lut[i]
			new_lut[val]=(255,255,255,255) #selected=white

			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=new_lut

		else:
			self.selected_label=0
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=self.clicked_lut
			self.clicked_lut=None

	def _max_button_fired(self):
		if self.selected_label==0:
			return

		def count():
			ilm=numpy.copy(self.img_local_max)
			c=0
			for i in range(len(selected_region[0])):
				if ilm[selected_region[0][i],selected_region[1][i],selected_region[2][i]]>0:
					c+=1
			print 'count',c

		#clear local max in selected region
		selected_region=numpy.where(self.img_watershed==self.selected_label)

		count()

		#make copy and set whole array so image is updated
		#ilm=numpy.copy(self.img_local_max)
		#numpy.put(ilm,selected_region,1)	
		#ilm=numpy.zeros(self.img_local_max.shape)
		#for i in range(len(selected_region[0])):
			#ilm[selected_region[0][i],selected_region[1][i],selected_region[2][i]]=1
		for i in range(len(selected_region[0])):
			self.img_local_max[selected_region[0][i],selected_region[1][i],selected_region[2][i]]=0

		#for i in range(100):
			#r1=numpy.random.randint(ilm.shape[0])
			#r2=numpy.random.randint(ilm.shape[1])
			#r3=numpy.random.randint(ilm.shape[2])
			#ilm[r1,r2,r3]=1

		#self.img_local_max=ilm

		count()

		#numpy.put(self.img_local_max,selected_region,0)

		#redo watershed 
		self.proc_watershed()
		self.update_ipw_proc()
		pass
	
	def _undo_button_fired(self):
		#undo change at last_position
		#make copy and set whole array so image is updated
		ilm=numpy.copy(self.img_local_max)
		ilm[self.last_position]=0
		self.img_local_max=ilm		
		
		#redo watershed				
		self.proc_watershed()
		self.proc_filter_size()
		self.update_ipw_proc()		


	def _save_button_fired(self):	
		# save to tiff
		img_proc=eval('self.'+self.image_type)
		tiffout=libtiff.TIFFimage(img_proc,description='')
		sh=img_proc.shape
		# write tiff structure to file
		fn=os.path.splitext(self.fn_tiff)[0]
		fn+='_'+str(sh[0])+'x'+str(sh[1])+'x'+str(sh[2])+'_'+self.image_type+'.tiff'
		tiffout.write_file(fn, compression='none') # or 'lzw'
		del tiffout # flushes data to disk

if __name__ == '__main__':
	a = EmbryoViewer()
	a.configure_traits()