import math
import matplotlib
matplotlib.use('cairo')
from matplotlib import pyplot as plt
import matplotlib.font_manager


font=matplotlib.font_manager.FontProperties(family='Gill Sans',fname='/Library/Fonts/GillSans.ttc')

csv_files=['/Users/jpm/Progs/AMATools/Mouse Embryos/session_analysis_output_slide2.csv','/Users/jpm/Progs/AMATools/Mouse Embryos/session_analysis_output_slide3.csv']

def get_signal(ll,channel):
	s=ll[1+channel*4+3] #fluorescence per voxel
	return float(s)

embryo_sizes=[]
ratios=[]
num_embryos=0

for c in csv_files:
	f=open(c,'r')
	l=f.readlines()
	current_line=0
	
	while current_line<len(l):
		l0=l[current_line]
		session_file,tiff_file,num_cells=l0.split(',')
		num_cells=int(num_cells)
		for i in range(num_cells):
			current_line+=1
			ll=l[current_line].split(',')
			num_channels=(len(ll)-1)/4
			
			embryo_sizes.append(num_cells)
			#y.append(signal[1]/signal[4]) #ratio of Nanog:GATA6
			nanog=get_signal(ll,1)
			GATA6=get_signal(ll,4)
			r=math.log10(nanog/GATA6)
			ratios.append(r)
		current_line+=1
		num_embryos+=1
	
	
plt.scatter(embryo_sizes,ratios,hold=True)	
plt.xlabel('number of cells',fontproperties=font)
plt.ylabel('log ratio nanog:GATA6 cell ave fluorescence (ch1:ch4)')
plt.title(str(num_embryos)+' embryos')	
plt.savefig('slides_2&3.jpg')
plt.show()	