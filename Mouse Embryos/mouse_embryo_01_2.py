import scipy
# sci-kits image processing
import skimage.filter

###PROCESS

sigma=1.0
image_processed=scipy.ndimage.filters.gaussian_filter(imgarray,(sigma,sigma,sigma))

thresh=skimage.filter.threshold_otsu(image_processed)
binary=image_processed>thresh
image_processed=binary.astype('int')	

##block_size=3+self.thresh*100
#if self.block_size%2==0:
	#self.block_size+=1 #must be odd
#block_size=20
#image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
#image_processed=image_processed.astype('int')	

sc.scalar_data=image_processed	

#image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)

#image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

distance = scipy.ndimage.distance_transform_edt(image_processed)
###distance=gaussian_filter(distance)			
distance = scipy.ndimage.gaussian_filter(distance,4)
footprint=3
local_max = skimage.morphology.is_local_maximum(distance, image_processed,numpy.ones((footprint,footprint,footprint)))
markers = scipy.ndimage.label(local_max)[0]
labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)

iso = mlab.pipeline.iso_surface(sc, contours=[image_processed.min()+0.1*image_processed.ptp(),],opacity=1.0)
contour=iso.contour.outputs[0]


#self.imgarray[self.imgarray_sources['proc'],t,self.channel,:,:]=image_processed
#self.imgarray[self.imgarray_sources['seg'],t,self.channel,:,:]=labels

##choose num time points, channel and remove redundancy in ome-tiff
#ch=1
#imgarray2=imgarray[100:110,ch,0,:,:]

#threshold with otsu
#for i in range(0,imgarray.shape[0]):
#	im=imgarray[i,:,:]
#	thresh=skimage.filter.threshold_otsu(im)
#	im_thresh=im > thresh
#	imgarray[i]=im_thresh


###UPDATE MAYAVI