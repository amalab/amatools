# Enthought imports.
from traits.api import HasTraits, Instance, List, Str, on_trait_change
from traitsui.api import View, Item, HGroup, EnumEditor
from tvtk.pyface.scene_editor import SceneEditor

from mayavi.tools.mlab_scene_model import MlabSceneModel
from mayavi.core.ui.mayavi_scene import MayaviScene

#from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.filter
import skimage.morphology
#import chaco.api

import amatools

from tvtk.api import tvtk

debug=True

class EmbryoViewer(HasTraits):

	# The scene model.
	scene = Instance(MlabSceneModel,())

	image_enum=List(['segmented','distance','local_max'])
	image_type=Str('segmented')


	######################
	# Using 'scene_class=MayaviScene' adds a Mayavi icon to the toolbar,
	# to pop up a dialog editing the pipeline.
	view = View(HGroup(
		Item('image_type',editor=EnumEditor(name='image_enum')),
		Item(name='scene',
		     editor=SceneEditor(scene_class=MayaviScene),
		     show_label=False,
		     resizable=True,
		     height=500,
		     width=500)
		),
		        resizable=True
		        )

	def __init__(self, **traits):
		HasTraits.__init__(self, **traits)
		#self.generate_data()
		#self.create_mlab()

	#def generate_data(self):
	@on_trait_change('scene.activated')
	def display_scene(self):    
		if debug:
			imgarray=numpy.random.random((128,128,128))
		else:
			###LOAD DATA
			#fn = tkFileDialog.askopenfilename()
			fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
			#amatools.lsm_2_ometiff(fn)

			# get tiff file as numpy array
			amatiff=amatools.AMATiff()
			amatiff.open(fn+'.ome.tiff')
			amatiff.info()

			num_channels=int(amatiff.pixel_attributes['sizec'])

			# get first channel
			# decimate x and y by 4
			imgarray=amatiff.tiff_array[::num_channels,::4,::4]

		###PROCESS
		sigma=1.0
		image_processed=scipy.ndimage.filters.gaussian_filter(imgarray,(sigma,sigma,sigma))

		thresh=skimage.filter.threshold_otsu(image_processed)
		binary=image_processed>thresh
		image_processed=binary.astype('int')	

		###block_size=3+self.thresh*100
		##if self.block_size%2==0:
				##self.block_size+=1 #must be odd
		##block_size=20
		##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
		##image_processed=image_processed.astype('int')	

		#sc.scalar_data=image_processed	

		##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
		##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

		distance = scipy.ndimage.distance_transform_edt(image_processed)
		####distance=gaussian_filter(distance)			
		distance = scipy.ndimage.gaussian_filter(distance,4)
		footprint=3
		local_max = skimage.morphology.is_local_maximum(distance, image_processed,numpy.ones((footprint,footprint,footprint)))
		markers = scipy.ndimage.label(local_max)[0]
		labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)


		###MAYAVI	
		#mlab replaced with self.scene.mlab

		imd = tvtk.ImageData(spacing=(1, 1, 1), origin=(0,0,0))
		imd.point_data.scalars = imgarray.T.ravel()
		imd.point_data.scalars.name = 'scalars'
		imd.dimensions = imgarray.shape
		ipw_img=self.scene.mlab.pipeline.image_plane_widget(imd,colormap='black-white')

		imd = tvtk.ImageData(spacing=(1, 1, 1), origin=(0,imgarray.shape[1], 0))
		imd.point_data.scalars = labels.T.ravel()
		imd.point_data.scalars.name = 'scalars'
		imd.dimensions = labels.shape
		ipw_labels=self.scene.mlab.pipeline.image_plane_widget(imd)
		ipw_labels.ipw.texture_interpolate=0
		ipw_labels.ipw.reslice_interpolate='nearest_neighbour'

		# random lut for segmentation view
		r=numpy.random.randint(256,size=(256,4))
		r[0,:]=(0,0,0,255) #black background
		ipw_labels.module_manager.scalar_lut_manager.lut.table=r

		#sync ipws
		ipw_labels.ipw.sync_trait('slice_position',ipw_img.ipw)	

		self.scene.mlab.view(50,50)

		#self.scene.mlab.surf(X, Y, Z, colormap='gist_earth')


if __name__ == '__main__':
	a = EmbryoViewer()
	a.configure_traits()