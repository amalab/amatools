import amatools
import numpy
import os
import libtiff

#fn = tkFileDialog.askopenfilename()
#fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117.ome.tiff'
fn='/Users/jpm/DATA/David/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/images/2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_09.ome.tiff'

#amatools.lsm_2_ometiff(fn)

# get tiff file as numpy array
amatiff=amatools.AMATiff()
amatiff.open(fn)
amatiff.info()

num_channels=int(amatiff.pixel_attributes['sizec'])
x=int(amatiff.pixel_attributes['sizex'])
y=int(amatiff.pixel_attributes['sizey'])

# decimate x and y by D
# decimate z by Dz
D=5
Dz=4
a=amatiff.tiff_array[:,::D,::D]
# reshape to (channels,z,y,x)
data=numpy.reshape(a,(-1,num_channels,a.shape[2],a.shape[1])) #-1 means calc it
data=numpy.swapaxes(data,0,1)

# choose channel	
channel=1
img_orig=data[channel,::Dz,:,:]
shape_img=img_orig.shape

# save to tiff
tiffout=libtiff.TIFFimage(img_orig,description='')
# write tiff structure to file
fn=os.path.splitext(fn)[0]
fn+='_'+str(shape_img[0])+'x'+str(shape_img[1])+'x'+str(shape_img[2])+'.tiff'
tiffout.write_file(fn, compression='none') # or 'lzw'
del tiffout # flushes data to disk