from mayavi import mlab

import amatools

###LOAD DATA
#fn = tkFileDialog.askopenfilename()
#fn=r'/Users/jpm/DATA/David - Images for tracking/2012-03-19 Wntch 2iLIF to labelled-0002_2i to ESminusLIF_01.zvi - C=2 slices=90-100 8-bit.tif'
#fn=r'/Users/jpm/DATA/_scratch/120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2.ome.tiff'
#fn=r'/Users/jpm/DATA/Christian/Mouse Embryos/130429_slideI_embryo2'
fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
#amatools.lsm_2_ometiff(fn)

# get tiff file as numpy array
amatiff=amatools.AMATiff()
amatiff.open(fn+'.ome.tiff')
amatiff.info()

num_channels=int(amatiff.pixel_attributes['sizec'])

# get first channel
# decimate x and y by 4
imgarray=amatiff.tiff_array[::num_channels,::4,::4]


###MAYAVI	
sc =mlab.pipeline.scalar_field(imgarray)
sc.spacing = [1, 1, 1]
sc.update_image_data = True

#imgw = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=0.3)
#iso = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=1.0)
#contour=iso.contour.outputs[0]

m=mlab.pipeline.image_plane_widget(sc)
#mlab.show()

pass