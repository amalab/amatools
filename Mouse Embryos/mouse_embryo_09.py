# Enthought imports.
from traits.api import HasTraits,Instance,List,Str,Button,on_trait_change,Range
from traitsui.api import View,Item,HGroup,VGroup,EnumEditor,RangeEditor
from tvtk.pyface.scene_editor import SceneEditor

from mayavi.tools.mlab_scene_model import MlabSceneModel
from mayavi.core.ui.mayavi_scene import MayaviScene
#from mayavi.core.api import PipelineBase
from mayavi.sources.api import ArraySource

import sklearn.feature_extraction
import sklearn.cluster

#from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.feature
import skimage.filter
import skimage.morphology
#import chaco.api
import colorsys

import amatools

from tvtk.api import tvtk

debug=False
#method='cluster'
method='watershed'
#decimation of input image x & y
D=4

# random lut for segmentation view
random_lut=numpy.random.randint(256,size=(256,4))
random_lut[0,:]=(0,0,0,255) #black background

colors_lut=numpy.zeros((256,4))
for i in range(256):
	r,g,b=colorsys.hls_to_rgb(float((i*17)%256)/256.0,0.4,0.9)
	colors_lut[i,:]=(int(r*256),int(g*256),int(b*256),255)
colors_lut[0,:]=(0,0,0,255) #black background


class EmbryoViewer(HasTraits):

	# The scene model.
	scene = Instance(MlabSceneModel,())

	debug_button=Button(label='Debug')	
	max_button=Button(label='Remove Max')	

	image_enum=List(['img_orig','img_denoise','img_thresh','img_distance','img_local_max','img_labels','img_markers','img_watershed'])
	image_type=Str('img_watershed')
	
	denoise_weight=Range(0,10,1,editor=RangeEditor(low=0, high=9, mode="slider"))
	thresh_offset=Range(0.0,1.0,0.03,editor=RangeEditor(low=0.0, high=0.9, mode="slider"))

	#ipw_orig = Instance(PipelineBase)
	#ipw_proc = Instance(PipelineBase)

	######################
	# Using 'scene_class=MayaviScene' adds a Mayavi icon to the toolbar,
	# to pop up a dialog editing the pipeline.
	view = View(HGroup(
	               VGroup(
	                  Item('debug_button'),
	                  Item('max_button'),
		              Item('image_type',editor=EnumEditor(name='image_enum')),
	                  Item('denoise_weight'),
	                  Item('thresh_offset')),
	               Item(name='scene',
	                    editor=SceneEditor(scene_class=MayaviScene),
	                    show_label=False,
	                    resizable=True,
	                    height=800,
	                    width=1000)
	               ),
		        resizable=True
		        )

	def __init__(self, **traits):
		HasTraits.__init__(self, **traits)
		
		self.clicked_lut=None
		self.selected_label=0
		#self.generate_data()
		#self.create_mlab()
		
	def proc_thresh_max(self):
		#smooth
		sigma=1.0
		self.img_denoise=scipy.ndimage.filters.gaussian_filter(self.img_orig,(sigma,sigma,sigma))
		
		#self.img_denoise=skimage.filter.denoise_bilateral(self.img_orig,sigma_range=0.5,sigma_spatial=15)
		#self.img_denoise=skimage.filter.denoise_tv_chambolle(self.img_orig,weight=0.1)#self.denoise_weight)
		
		#threshold
		#otsu
		#thresh_value=skimage.filter.threshold_otsu(self.img_denoise)
		#self.img_thresh=self.img_denoise>thresh_value
		
		#local - too slow for 3d
		self.img_thresh=numpy.zeros(self.shape_img)
		block_size=33
		for z in range(self.shape_img[2]):
			#print z
			self.img_thresh[:,:,z]=skimage.filter.threshold_adaptive(self.img_denoise[:,:,z],block_size=block_size,offset=-self.thresh_offset)
		
		self.img_thresh=self.img_thresh.astype('uint8')
		
		###block_size=3+self.thresh*100
		##if self.block_size%2==0:
				##self.block_size+=1 #must be odd
		##block_size=20
		##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
		##image_processed=image_processed.astype('int')	

		#sc.scalar_data=image_processed	

		##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
		##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

		#label thresholded image
		self.img_labels=scipy.ndimage.label(self.img_thresh)[0]	
		#distance to background from thresholded image
		self.img_distance = scipy.ndimage.distance_transform_edt(self.img_thresh)
		
		####distance=gaussian_filter(distance)			
		#self.img_distance = scipy.ndimage.gaussian_filter(self.img_distance,4)
		footprint=3
		#self.img_local_max = skimage.morphology.is_local_maximum(self.img_distance,self.img_thresh)#,numpy.ones((footprint,footprint,footprint)))
		#self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_thresh,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))

		#local max of distance within each labelled region
		#self.img_local_max=numpy.zeros(self.shape_img)	
		self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_labels,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))
		self.img_local_max=self.img_local_max.astype('uint8')
		
	def proc_watershed(self):
		#label maxima
		self.img_markers = scipy.ndimage.label(self.img_local_max,structure=numpy.ones((3,3,3)))[0]
		#watershed distance image with maxima as seeds
		self.img_watershed = skimage.morphology.watershed(-self.img_distance, self.img_markers, mask=self.img_thresh)
		
		#self.img_distance=(self.img_distance*(255.0/numpy.max(self.img_distance))).astype('uint8') #watershed_ift needs uint input not float
		#self.img_distance=(self.img_distance*(65535.0/numpy.max(self.img_distance))).astype('uint16') #watershed_ift needs uint input not float
		#self.img_watershed=scipy.ndimage.measurements.watershed_ift(self.img_labels.astype('uint8'), self.img_markers)

	#def generate_data(self):
	@on_trait_change('scene.activated')
	def display_scene(self):    
		if debug:
			self.img_orig=numpy.random.random((128,128,128))
		else:
			###LOAD DATA
			#fn = tkFileDialog.askopenfilename()
			fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
			#amatools.lsm_2_ometiff(fn)

			# get tiff file as numpy array
			amatiff=amatools.AMATiff()
			amatiff.open(fn+'.ome.tiff')
			amatiff.info()

			num_channels=int(amatiff.pixel_attributes['sizec'])
			x=int(amatiff.pixel_attributes['sizex'])
			y=int(amatiff.pixel_attributes['sizey'])
			
			# decimate x and y by D
			#D=4
			a=amatiff.tiff_array[:,::D,::D]
			# reshape to (channels,z,y,x)
			data=numpy.reshape(a,(-1,num_channels,y/D,x/D)) #-1 means calc it
			data=numpy.swapaxes(data,0,1)
			
			# get first channel			
			self.img_orig=data[0]
			self.shape_img=self.img_orig.shape
			
			# sum 
			##s=numpy.sum(data,0)/num_channels
			#s=(data[1]+data[2])/2
			#self.img_orig=s.astype('uint8')
			pass

		###PROCESS
		if method=='watershed':
			self.proc_thresh_max()
			self.proc_watershed()

		#else:
			## Convert the image into a graph with the value of the gradient on the
			## edges.
			#graph = sklearn.feature_extraction.image.img_to_graph(self.img_orig)#, mask=mask)

			## Take a decreasing function of the gradient: we take it weakly
			## dependant from the gradient the segmentation is close to a voronoi
			#graph.data = numpy.exp(-graph.data / graph.data.std())

			## Force the solver to be arpack, since amg is numerically
			## unstable on this example
			#self.img_labels = sklearn.cluster.spectral_clustering(graph, n_clusters=2)#, eigen_solver='arpack')
			##label_im = -np.ones(mask.shape)
			##label_im[mask] = labels	    
			#pass


		###MAYAVI	
		#mlab replaced with self.scene.mlab

		#LHS orig
		self.as_orig=ArraySource(scalar_data=self.img_orig)
		self.ipw_orig=self.scene.mlab.pipeline.image_plane_widget(self.as_orig,colormap='black-white')
		self.ipw_orig.ipw.texture_interpolate=0
		self.ipw_orig.ipw.reslice_interpolate='nearest_neighbour'	

		#RHS proc
		self.as_proc=ArraySource(scalar_data=self.img_watershed,origin=(0,self.img_orig.shape[1],0))	
		self.ipw_proc=self.scene.mlab.pipeline.image_plane_widget(self.as_proc)
		self.ipw_proc.ipw.texture_interpolate=0
		self.ipw_proc.ipw.reslice_interpolate='nearest_neighbour'
		#turn-off autorange so random lut looks-up according to absolute index
		self.ipw_proc.module_manager.scalar_lut_manager.use_default_range=False
		self.ipw_proc.module_manager.scalar_lut_manager.data_range=numpy.array([0.0,255.0])
		
		#store orig lut and change to random one
		self.orig_lut=self.ipw_proc.module_manager.scalar_lut_manager.lut.table
		#self.ipw_proc.module_manager.scalar_lut_manager.lut.table=random_lut
		self.ipw_proc.module_manager.scalar_lut_manager.lut.table=colors_lut

		#trait stuff
		#sync ipws
		self.ipw_proc.ipw.sync_trait('slice_position',self.ipw_orig.ipw)

		self.ipw_orig.on_trait_change(self.ipw_orig_slice_position,name='ipw.slice_position')	

		self.scene.mlab.view(50,50)

		#picking 	
		self.ipw_orig.ipw.right_button_action = 0
		# Add a callback on the image plane widget interaction
		self.ipw_orig.ipw.add_observer('StartInteractionEvent', self.pick_callback_orig)
		
		self.ipw_proc.ipw.right_button_action = 0
		# Add a callback on the image plane widget interaction
		self.ipw_proc.ipw.add_observer('StartInteractionEvent', self.pick_callback_proc)		

	@on_trait_change('image_type')
	def update_ipw_proc(self):
		#print self.image_type
		
		#luts
		if self.image_type in ['img_watershed','img_labels']:
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=colors_lut
			self.ipw_proc.module_manager.scalar_lut_manager.use_default_range=False
			self.ipw_proc.module_manager.scalar_lut_manager.data_range=numpy.array([0.0,255.0])
			
		else:
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=self.orig_lut
			self.ipw_proc.module_manager.scalar_lut_manager.use_default_range=True

		#ipw_proc.scalars=eval('self.'+self.image_type)
		#self.imd_proc.point_data.scalars=self.img_denoise.T.ravel()
		self.as_proc.scalar_data=eval('self.'+self.image_type)
		
	@on_trait_change('denoise_weight,thresh_offset')
	def reprocess(self):
		self.proc_thresh_max()
		self.proc_watershed()
		self.update_ipw_proc()

	#@on_trait_change('self.ipw_orig.ipw.slice_position')
	def ipw_orig_slice_position(self):
		print self.ipw_orig.ipw.slice_position

	# picking
	def pick_callback_orig(self,widget,event):

		# hack to reject left hand mouse clicks 
		position = widget.GetCurrentCursorPosition()
		if position == (0.0,0.0,0.0):	
			return
		
		position=tuple([int(p) for p in position])
		
		#value clicked on
		val=int(widget.GetCurrentImageValue())
		print position,val
		
		#make copy and set whole array so image is updated
		ilm=numpy.copy(self.img_local_max)
		ilm[position]=1
		self.img_local_max=ilm
		#self.img_local_max[position]=1
			
		#redo watershed				
		self.proc_watershed()
		self.update_ipw_proc()
		
		
	def pick_callback_proc(self,widget,event):
		# hack to reject left hand mouse clicks 
		position = widget.GetCurrentCursorPosition()
		if position == (0.0,0.0,0.0):	
			return
		
		if self.image_type!='img_watershed':
			return
		
		#value clicked on
		val=int(widget.GetCurrentImageValue())
		print position,val
		
		if val>0: 
			self.selected_label=val
			#self.ipw_proc.module_manager.scalar_lut_manager.lut.table[val]=(255,255,255,255)
			if not self.clicked_lut:
				self.clicked_lut=self.ipw_proc.module_manager.scalar_lut_manager.lut.table

			#have to set whole table at once to trigger redraw
			new_lut=numpy.zeros((256,4))
			for i in range(256):
				new_lut[i]=self.clicked_lut[i]
			new_lut[val]=(255,255,255,255) #selected=white
			
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=new_lut
			
		else:
			self.selected_label=0
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=self.clicked_lut
			self.clicked_lut=None
		
	def _max_button_fired(self):
		if self.selected_label==0:
			return
		
		def count():
			ilm=numpy.copy(self.img_local_max)
			c=0
			for i in range(len(selected_region[0])):
				if ilm[selected_region[0][i],selected_region[1][i],selected_region[2][i]]>0:
					c+=1
			print 'count',c
		
		#clear local max in selected region
		selected_region=numpy.where(self.img_watershed==self.selected_label)
		
		count()
		
		#make copy and set whole array so image is updated
		#ilm=numpy.copy(self.img_local_max)
		#numpy.put(ilm,selected_region,1)	
		#ilm=numpy.zeros(self.img_local_max.shape)
		#for i in range(len(selected_region[0])):
			#ilm[selected_region[0][i],selected_region[1][i],selected_region[2][i]]=1
		for i in range(len(selected_region[0])):
			self.img_local_max[selected_region[0][i],selected_region[1][i],selected_region[2][i]]=0
			
		#for i in range(100):
			#r1=numpy.random.randint(ilm.shape[0])
			#r2=numpy.random.randint(ilm.shape[1])
			#r3=numpy.random.randint(ilm.shape[2])
			#ilm[r1,r2,r3]=1
			
		#self.img_local_max=ilm
		
		count()
		
		#numpy.put(self.img_local_max,selected_region,0)
		
		#redo watershed 
		self.proc_watershed()
		self.update_ipw_proc()
		pass
		
			
	def _debug_button_fired(self):	

		plm=skimage.feature.peak_local_max(self.img_distance)#,labels=self.img_thresh,indices=True)#footprint=numpy.ones((footprint,footprint,footprint)))

		print plm


if __name__ == '__main__':
	a = EmbryoViewer()
	a.configure_traits()