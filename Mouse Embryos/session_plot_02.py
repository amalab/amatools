#
# plot data from embryo session files
#

import math
import matplotlib
from matplotlib import pyplot as plt
import matplotlib.font_manager
import pickle
import os

font=matplotlib.font_manager.FontProperties(family='Gill Sans',fname='/Library/Fonts/GillSans.ttc')

s='/Users/jpm/DATA/Christian/Mouse Embryos/slides/session_files/130504_Embryos_slide4/130504_embryos_slide4_L%d_Sum.session'

session_files=[]
for i in range(1,11):
	session_files.append(s%i)

embryo_sizes=[]
ratios=[]
num_embryos=len(session_files)

for s in session_files:
	pickle_file=open(s,'rb')
	fn_tiff=pickle.load(pickle_file)
	fn_tiff_file=os.path.split(fn_tiff)[1]
	img_orig=pickle.load(pickle_file)
	shape_img=img_orig.shape
	img_watershed_filtered=pickle.load(pickle_file)	
	D=pickle.load(pickle_file)
	mitotic_labels=pickle.load(pickle_file)
	measurements=pickle.load(pickle_file)

	#cell x channel x [posx,posy,posz,num_voxels,signal_tot,signal_ave] dtype=('f4,f4,f4,i4,f4,f4')
	num_cells,num_channels=measurements.shape
	for cell in range(num_cells):
		ch1=measurements[cell][1] #nanog
		ch4=measurements[cell][4] #GATA6

		nanog=ch1[5] #signal ave
		GATA6=ch4[5]

		embryo_sizes.append(num_cells)
		r=math.log(nanog/GATA6)
		ratios.append(r)

plt.scatter(embryo_sizes,ratios,hold=True)	
plt.xlabel('number of cells',fontproperties=font)
plt.ylabel('log ratio nanog:GATA6 cell ave fluorescence (ch1:ch4)')
plt.title(str(num_embryos)+' embryos')	
plt.savefig('session_test.jpg')
plt.show()	