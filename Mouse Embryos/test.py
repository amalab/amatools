from mayavi import mlab
import numpy

x, y = numpy.mgrid[0:3:1,0:3:1]
s = mlab.surf(x, y, numpy.asarray(x*0.1, 'd'))

for i in range(100):
    s.mlab_source.scalars = numpy.asarray(x*0.01*(i+1), 'd')