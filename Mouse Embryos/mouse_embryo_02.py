from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.filter
import skimage.morphology

import amatools

###LOAD DATA
#fn = tkFileDialog.askopenfilename()
#fn=r'/Users/jpm/DATA/David - Images for tracking/2012-03-19 Wntch 2iLIF to labelled-0002_2i to ESminusLIF_01.zvi - C=2 slices=90-100 8-bit.tif'
#fn=r'/Users/jpm/DATA/_scratch/120511_TetGat4_Gata6H2BVenus-0006_500dox_noPDO3_#2.ome.tiff'
#fn=r'/Users/jpm/DATA/Christian/Mouse Embryos/130429_slideI_embryo2'
fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
#amatools.lsm_2_ometiff(fn)

# get tiff file as numpy array
amatiff=amatools.AMATiff()
amatiff.open(fn+'.ome.tiff')
amatiff.info()

num_channels=int(amatiff.pixel_attributes['sizec'])

# get first channel
# decimate x and y by 4
imgarray=amatiff.tiff_array[::num_channels,::4,::4]


###PROCESS

sigma=1.0
image_processed=scipy.ndimage.filters.gaussian_filter(imgarray,(sigma,sigma,sigma))

thresh=skimage.filter.threshold_otsu(image_processed)
binary=image_processed>thresh
image_processed=binary.astype('int')	

###block_size=3+self.thresh*100
##if self.block_size%2==0:
	##self.block_size+=1 #must be odd
##block_size=20
##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
##image_processed=image_processed.astype('int')	

#sc.scalar_data=image_processed	

##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

distance = scipy.ndimage.distance_transform_edt(image_processed)
####distance=gaussian_filter(distance)			
distance = scipy.ndimage.gaussian_filter(distance,4)
footprint=3
local_max = skimage.morphology.is_local_maximum(distance, image_processed,numpy.ones((footprint,footprint,footprint)))
markers = scipy.ndimage.label(local_max)[0]
labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)

stacked=numpy.hstack((imgarray,labels))
#scalar_field.scalar_data=stacked

###MAYAVI	
scalar_field=mlab.pipeline.scalar_field(stacked,colormap='black-white')
scalar_field.spacing=[1, 1, 1]
scalar_field.update_image_data=True

#imgw = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=0.3)
#iso = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=1.0)
#contour=iso.contour.outputs[0]

m=mlab.pipeline.image_plane_widget(scalar_field)#,colormap='black-white')
#mlab.show()

#iso = mlab.pipeline.iso_surface(sc, contours=[image_processed.min()+0.1*image_processed.ptp(),],opacity=1.0)
#contour=iso.contour.outputs[0]

mlab.show()
pass