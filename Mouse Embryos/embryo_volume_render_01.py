from mayavi import mlab
import amatools
import numpy

#point this to tiff file location
fn=r'/Users/jpm/DATA/Christian/Mouse Embryos/slides/130502_Embryos_slide2/130502_Embryos_slide2_L1_Sum.ome.tiff'

# get tiff file as numpy array
amatiff=amatools.AMATiff()
amatiff.open(fn)
amatiff.info()

# get size of data
num_channels=int(amatiff.pixel_attributes['sizec'])
x=int(amatiff.pixel_attributes['sizex'])
y=int(amatiff.pixel_attributes['sizey'])

# decimate x and y by D
D=2
a=amatiff.tiff_array[:,::D,::D]
# reshape to (channels,z,y,x)
data=numpy.reshape(a,(-1,num_channels,a.shape[2],a.shape[1])) #-1 means calc it
data=numpy.swapaxes(data,0,1)

# get dapi channel		
channel=0
img_orig=data[channel]
shape_img=img_orig.shape
print '(Z,Y,X):',shape_img

# volume render	
mlab.figure('volume')
sc=mlab.pipeline.scalar_field(img_orig)
#stretch z out a bit
sc.spacing = [shape_img[2]/shape_img[0], 1, 1]
sc.update_image_data = True
mlab.pipeline.volume(sc)

mlab.show()