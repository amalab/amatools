# Enthought imports.
from traits.api import HasTraits, Instance, List, Str, on_trait_change
from traitsui.api import View, Item, HGroup, EnumEditor
from tvtk.pyface.scene_editor import SceneEditor

from mayavi.tools.mlab_scene_model import MlabSceneModel
from mayavi.core.ui.mayavi_scene import MayaviScene
from mayavi.sources.api import ArraySource

#from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.filter
import skimage.morphology
#import chaco.api

import amatools

from tvtk.api import tvtk

debug=False

class EmbryoViewer(HasTraits):

    # The scene model.
    scene = Instance(MlabSceneModel,())
    
    image_enum=List(['img_orig','img_gauss','img_thresh','img_distance','img_local_max','img_labels'])
    image_type=Str('segmented')
    

    ######################
    # Using 'scene_class=MayaviScene' adds a Mayavi icon to the toolbar,
    # to pop up a dialog editing the pipeline.
    view = View(HGroup(
                   Item('image_type',editor=EnumEditor(name='image_enum')),
                   Item(name='scene',
                     editor=SceneEditor(scene_class=MayaviScene),
                     show_label=False,
                     resizable=True,
                     height=500,
                     width=500)
                   ),
                resizable=True
                )

    def __init__(self, **traits):
        HasTraits.__init__(self, **traits)
        #self.generate_data()
	#self.create_mlab()

    #def generate_data(self):
    @on_trait_change('scene.activated')
    def display_scene(self):    
	if debug:
	    self.img_orig=numpy.random.random((128,128,128))
	else:
	    ###LOAD DATA
	    #fn = tkFileDialog.askopenfilename()
	    fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
	    #amatools.lsm_2_ometiff(fn)
	    
	    # get tiff file as numpy array
	    amatiff=amatools.AMATiff()
	    amatiff.open(fn+'.ome.tiff')
	    amatiff.info()
	    
	    num_channels=int(amatiff.pixel_attributes['sizec'])
	    
	    # get first channel
	    # decimate x and y by 4
	    self.img_orig=amatiff.tiff_array[::num_channels,::4,::4]
	
	###PROCESS
	sigma=1.0
	self.img_gauss=scipy.ndimage.filters.gaussian_filter(self.img_orig,(sigma,sigma,sigma))
	self.img_thresh=skimage.filter.threshold_otsu(self.img_gauss)
	binary=self.img_gauss>self.img_thresh
	self.img_thresh=img_processed=binary.astype('int')	
	
	###block_size=3+self.thresh*100
	##if self.block_size%2==0:
		##self.block_size+=1 #must be odd
	##block_size=20
	##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
	##image_processed=image_processed.astype('int')	
	
	#sc.scalar_data=image_processed	
	
	##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
	##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)
	
	self.img_distance = scipy.ndimage.distance_transform_edt(self.img_thresh)
	####distance=gaussian_filter(distance)			
	self.img_distance = scipy.ndimage.gaussian_filter(self.img_distance,4)
	footprint=3
	self.img_local_max = skimage.morphology.is_local_maximum(self.img_distance,self.img_thresh,numpy.ones((footprint,footprint,footprint)))
	markers = scipy.ndimage.label(self.img_local_max)[0]
	self.img_labels = skimage.morphology.watershed(-self.img_distance, markers, mask=self.img_thresh)
			
	
	###MAYAVI	
	#mlab replaced with self.scene.mlab
	
	self.as_orig=ArraySource(scalar_data=self.img_orig)
	#self.imd_orig = tvtk.ImageData(spacing=(1, 1, 1), origin=(0,0,0))
	#self.imd_orig.point_data.scalars = self.img_orig.T.ravel()
	#self.imd_orig.point_data.scalars.name = 'scalars'
	#self.imd_orig.dimensions = self.img_orig.shape
	self.ipw_orig=self.scene.mlab.pipeline.image_plane_widget(self.as_orig,colormap='black-white')
	self.ipw_orig.ipw.texture_interpolate=0
	self.ipw_orig.ipw.reslice_interpolate='nearest_neighbour'	
	
	self.as_proc=ArraySource(scalar_data=self.img_labels,origin=(0,self.img_orig.shape[1],0))	
	#self.imd_proc = tvtk.ImageData(spacing=(1, 1, 1), origin=(0,self.img_orig.shape[1], 0))#shift to right of orig
	#self.imd_proc.point_data.scalars = self.img_labels.T.ravel()
	#self.imd_proc.point_data.scalars.name = 'scalars'
	#self.imd_proc.dimensions = self.img_labels.shape
	self.ipw_proc=self.scene.mlab.pipeline.image_plane_widget(self.as_proc)
	self.ipw_proc.ipw.texture_interpolate=0
	self.ipw_proc.ipw.reslice_interpolate='nearest_neighbour'
	
	# random lut for segmentation view
	r=numpy.random.randint(256,size=(256,4))
	r[0,:]=(0,0,0,255) #black background
	self.ipw_proc.module_manager.scalar_lut_manager.lut.table=r
	
	#sync ipws
	self.ipw_proc.ipw.sync_trait('slice_position',self.ipw_orig.ipw)	
	
	self.scene.mlab.view(50,50)

        #self.scene.mlab.surf(X, Y, Z, colormap='gist_earth')
	
    @on_trait_change('image_type')
    def update_ipw_proc(self):
	print self.image_type
	#ipw_proc.scalars=eval('self.'+self.image_type)
	#self.imd_proc.point_data.scalars=self.img_gauss.T.ravel()
	self.as_proc.scalar_data=eval('self.'+self.image_type)

if __name__ == '__main__':
    a = EmbryoViewer()
    a.configure_traits()
