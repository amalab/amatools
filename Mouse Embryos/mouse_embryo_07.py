# Enthought imports.
from traits.api import HasTraits, Instance, List, Str, Button,on_trait_change
from traitsui.api import View, Item, HGroup, EnumEditor
from tvtk.pyface.scene_editor import SceneEditor

from mayavi.tools.mlab_scene_model import MlabSceneModel
from mayavi.core.ui.mayavi_scene import MayaviScene
#from mayavi.core.api import PipelineBase
from mayavi.sources.api import ArraySource

import sklearn.feature_extraction
import sklearn.cluster

#from mayavi import mlab
import scipy
import numpy
# sci-kits image processing
import skimage.feature
import skimage.filter
import skimage.morphology
#import chaco.api

import amatools

from tvtk.api import tvtk

debug=False
#method='cluster'
method='watershed'

# random lut for segmentation view
random_lut=numpy.random.randint(256,size=(256,4))
random_lut[0,:]=(0,0,0,255) #black background

class EmbryoViewer(HasTraits):

	# The scene model.
	scene = Instance(MlabSceneModel,())

	debug_button=Button(label='Debug')	

	image_enum=List(['img_orig','img_gauss','img_thresh','img_distance','img_local_max','img_labels'])
	image_type=Str('segmented')

	#ipw_orig = Instance(PipelineBase)
	#ipw_proc = Instance(PipelineBase)

	######################
	# Using 'scene_class=MayaviScene' adds a Mayavi icon to the toolbar,
	# to pop up a dialog editing the pipeline.
	view = View(HGroup(
		Item('debug_button'),
		Item('image_type',editor=EnumEditor(name='image_enum')),
		Item(name='scene',
		     editor=SceneEditor(scene_class=MayaviScene),
		     show_label=False,
		     resizable=True,
		     height=500,
		     width=500)
		),
		        resizable=True
		        )

	def __init__(self, **traits):
		HasTraits.__init__(self, **traits)
		#self.generate_data()
		#self.create_mlab()

	#def generate_data(self):
	@on_trait_change('scene.activated')
	def display_scene(self):    
		if debug:
			self.img_orig=numpy.random.random((128,128,128))
		else:
			###LOAD DATA
			#fn = tkFileDialog.askopenfilename()
			fn='/Users/jpm/DATA/Christian/Mouse Embryos/130430_Embryo#2_red3_512x512x117'
			#amatools.lsm_2_ometiff(fn)

			# get tiff file as numpy array
			amatiff=amatools.AMATiff()
			amatiff.open(fn+'.ome.tiff')
			amatiff.info()

			num_channels=int(amatiff.pixel_attributes['sizec'])

			# get first channel
			# decimate x and y by D
			D=4
			self.img_orig=amatiff.tiff_array[::num_channels,::D,::D]
			pass

		###PROCESS
		if method=='watershed':
			sigma=1.0
			self.img_gauss=scipy.ndimage.filters.gaussian_filter(self.img_orig,(sigma,sigma,sigma))
			self.img_thresh=skimage.filter.threshold_otsu(self.img_gauss)
			binary=self.img_gauss>self.img_thresh
			self.img_thresh=binary.astype('uint8')	

			###block_size=3+self.thresh*100
			##if self.block_size%2==0:
					##self.block_size+=1 #must be odd
			##block_size=20
			##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
			##image_processed=image_processed.astype('int')	

			#sc.scalar_data=image_processed	

			##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
			##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

			self.img_distance = scipy.ndimage.distance_transform_edt(self.img_thresh)
			####distance=gaussian_filter(distance)			
			#self.img_distance = scipy.ndimage.gaussian_filter(self.img_distance,4)
			footprint=3
			#self.img_local_max = skimage.morphology.is_local_maximum(self.img_distance,self.img_thresh)#,numpy.ones((footprint,footprint,footprint)))
			#self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_thresh,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))
			self.img_local_max = skimage.feature.peak_local_max(self.img_distance,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))
			self.img_local_max=self.img_local_max.astype('uint8')

			markers = scipy.ndimage.label(self.img_local_max)[0]
			self.img_labels = skimage.morphology.watershed(-self.img_distance, markers, mask=self.img_thresh)

		else:
			# Convert the image into a graph with the value of the gradient on the
			# edges.
			graph = sklearn.feature_extraction.image.img_to_graph(self.img_orig)#, mask=mask)

			# Take a decreasing function of the gradient: we take it weakly
			# dependant from the gradient the segmentation is close to a voronoi
			graph.data = numpy.exp(-graph.data / graph.data.std())

			# Force the solver to be arpack, since amg is numerically
			# unstable on this example
			self.img_labels = sklearn.cluster.spectral_clustering(graph, n_clusters=2)#, eigen_solver='arpack')
			#label_im = -np.ones(mask.shape)
			#label_im[mask] = labels	    
			pass


		###MAYAVI	
		#mlab replaced with self.scene.mlab

		self.as_orig=ArraySource(scalar_data=self.img_orig)
		self.ipw_orig=self.scene.mlab.pipeline.image_plane_widget(self.as_orig,colormap='black-white')
		self.ipw_orig.ipw.texture_interpolate=0
		self.ipw_orig.ipw.reslice_interpolate='nearest_neighbour'	


		self.as_proc=ArraySource(scalar_data=self.img_labels,origin=(0,self.img_orig.shape[1],0))	
		self.ipw_proc=self.scene.mlab.pipeline.image_plane_widget(self.as_proc)
		self.ipw_proc.ipw.texture_interpolate=0
		self.ipw_proc.ipw.reslice_interpolate='nearest_neighbour'

		#store orig lut and change to random one
		self.orig_lut=self.ipw_proc.module_manager.scalar_lut_manager.lut.table
		self.ipw_proc.module_manager.scalar_lut_manager.lut.table=random_lut

		#trait stuff
		#sync ipws
		self.ipw_proc.ipw.sync_trait('slice_position',self.ipw_orig.ipw)

		self.ipw_orig.on_trait_change(self.ipw_orig_slice_position,name='ipw.slice_position')	

		self.scene.mlab.view(50,50)

		#picking 	
		#self.ipw_proc.ipw.left_button_action = 0
		self.ipw_proc.ipw.right_button_action = 0
		# Add a callback on the image plane widget interaction to
		#self.ipw_proc.ipw.add_observer('InteractionEvent', self.pick_callback)
		self.ipw_proc.ipw.add_observer('StartInteractionEvent', self.pick_callback)
		#self.ipw_proc.ipw.add_observer('RightButtonPressEvent', self.pick_callback)

	@on_trait_change('image_type')
	def update_ipw_proc(self):
		print self.image_type
		#ipw_proc.scalars=eval('self.'+self.image_type)
		#self.imd_proc.point_data.scalars=self.img_gauss.T.ravel()
		self.as_proc.scalar_data=eval('self.'+self.image_type)

		#luts
		if self.image_type=='img_labels':
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=random_lut    
		else:
			self.ipw_proc.module_manager.scalar_lut_manager.lut.table=self.orig_lut

	#@on_trait_change('self.ipw_orig.ipw.slice_position')
	def ipw_orig_slice_position(self):
		print self.ipw_orig.ipw.slice_position

	# picking
	def pick_callback(self,widget,event):
		#print widget.GetLeftButtonAction(),widget.GetRightButtonAction()
		#print widget,event
		#if widget.GetLeftButtonAction():
		position = widget.GetCurrentCursorPosition()
		print position
		
		#if position != (0.0,0.0,0.0):
			#print 'pos',position

	def _debug_button_fired(self):	

		plm=skimage.feature.peak_local_max(self.img_distance)#,labels=self.img_thresh,indices=True)#footprint=numpy.ones((footprint,footprint,footprint)))

		print plm


if __name__ == '__main__':
	a = EmbryoViewer()
	a.configure_traits()