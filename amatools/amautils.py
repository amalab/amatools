import matplotlib

#colour maps for fluorescence channels

# [(x0,col,alpha),(x1,col,alpha)]
# simple linear interpolation between two values at x=0 and x=1

cmap_c=matplotlib.colors.LinearSegmentedColormap('cmap_c',
                                            #x0,col0,alpha0  x1,col1,alpha1
                                {'red':   [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],                                                      
								'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                        
								'blue':  [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)]})

cmap_r=matplotlib.colors.LinearSegmentedColormap('cmap_r',
                                {'red':   [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],
                                 'green': [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],
                                 'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]})

cmap_g=matplotlib.colors.LinearSegmentedColormap('cmap_g',
                                {'red':   [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],                                                      
								'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                        
								'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]})

cmap_m=matplotlib.colors.LinearSegmentedColormap('cmap_m',
                                {'red':   [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                      
								'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],                                                        
								'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]})

cmaps=['gray',cmap_c,cmap_r,cmap_g]