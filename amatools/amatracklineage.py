# coding=utf-8
# for unicode comments

'''
cell track lineage data structure

jpm
 jul 2014
'''

import numpy

class Track:
	'''Track of cells over time'''
	def __init__(self):
		self.cells=[]

class CellTracks:
	'''Collection of tracks'''
	def  __init__(self):
		self.tracks=[]
			
	def read_mtrackj_mdf(self,mdf_file,verbose=False):
		'''Read track data from MTrackJ mdf file'''
		
		mdf_file=open(mdf_file,'rU')
		mdf_lines=mdf_file.readlines()
		
		l=0
		while True:
			line=mdf_lines[l]	
			if line[0:5]=='Track':
				break
			l+=1
				
		while line[0:5]=='Track':
			track=Track()
			track.id=self.track_count
			self.track_count+=1
			self.tracks.append(track)
			if verbose:
				print 'Track',track.id
			
			l+=1
			line=mdf_lines[l]
			while line[0:5]=='Point':
				cell=Cell()
				cell.id=self.cell_count
				cell.track_id=track.id
				self.cell_count+=1
				line_elems=line.split(' ')
				cell.x=float(line_elems[2])
				cell.y=float(line_elems[3])
				cell.t=float(line_elems[5])
				
				track.cells.append(cell)
				
				if verbose:
					print 'Cell',cell.id,cell.t,cell.x,cell.y
				
				l+=1
				line=mdf_lines[l]		
			
	def read_mtrackj_mdf_cluster(self,mdf_file,cluster=1,verbose=False):
		'''Read track data from given cluster of MTrackJ mdf file'''
		
		mdf_file=open(mdf_file,'rU')
		mdf_lines=mdf_file.readlines()
		
		l=0
		while True:
			line=mdf_lines[l]
			l+=1	
			if 'Cluster %s'%cluster in line:
				break
		
		line=mdf_lines[l]		
		while line[0:5]=='Track':
			track=Track()
			track.id=self.track_count
			self.track_count+=1
			self.tracks.append(track)
			if verbose:
				print 'Track',track.id
			
			l+=1
			line=mdf_lines[l]
			while line[0:5]=='Point':
				cell=Cell()
				cell.id=self.cell_count
				cell.track_id=track.id
				self.cell_count+=1
				line_elems=line.split(' ')
				cell.x=float(line_elems[2])
				cell.y=float(line_elems[3])
				cell.t=float(line_elems[5])
				
				track.cells.append(cell)
				
				if verbose:
					print 'Cell',cell.id,cell.t,cell.x,cell.y
				
				l+=1
				line=mdf_lines[l]		

	def read_from_tracker_project(self,proj_file):
		pass
