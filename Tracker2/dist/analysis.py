import pickle
from PySide import QtGui
from matplotlib import pyplot
import numpy.random

project_dir='/Users/jpm/Progs/AMATools/Tracker2/140508_RGB2-9_immuno_2_MMStack_ES-LIF_4.ome'
#project_dir='/Users/jpm/Progs/AMATools/Tracker2/140506_RGB2-9__2_MMStack_PDO3to-0uM_3.ome'
to_pickle=['self.sizeC','self.sizeT','self.sizeX','self.sizeY','self.current_channel','self.current_time','self.lookup',\
           'self.track_id_count','self.divisions']

class Cell(object):
	track_id_count=0
	lookup={} #stores mapping from frame,seg_id -> cell object
	divisions={} #stores sister cells after a division
	
	def __init__(self,frame,seg_id,track_id=-1):
		#cells linked into tracks have the same track_id
		if track_id==-1:
			self.track_id=Cell.track_id_count 
			Cell.track_id_count+=1
		else:
			self.track_id=track_id
			
		self.frame=frame		
		self.seg_id=seg_id #id in segmentation at self.frame	
		Cell.set(frame,seg_id,self)
		
		self.next=[] #link to Cell object(s) in next frame
		self.previous=[] #ditto previous
		
		self.glyph=None
		
		#calc BB and pos
		#read seg
		fn=view.getFilename('segmented',view.channel_to_process,frame)
		if os.path.exists(fn):
			t=tifffile.TIFFfile(fn)	
			img_seg=t.asarray()	
		else:
			print 'No segmented image error'
			return
		
		w=numpy.where(img_seg==self.seg_id)
		self.size=numpy.size(w)
		minx=numpy.min(w[1])
		maxx=numpy.max(w[1])
		miny=numpy.min(w[0])
		maxy=numpy.max(w[0])
		self.bb=(minx,maxx,miny,maxy)
		self.pos=((minx+maxx)/2,(miny+maxy)/2)
		
	def destroy(self):
		#destructor to remove links and lookup ref
		for p in self.previous:
			p.next.remove(self)
			
		for n in self.next:
			n.previous.remove(self)
			
		Cell.lookup[self.frame].pop(self.seg_id)
		
		view.qscene.removeItem(self.glyph)
	
	#lookup table setter and getter
	#lookup[frame][seg_id]
	@staticmethod	
	def set(frame,seg_id,cell):
		if frame not in Cell.lookup.keys():
			Cell.lookup[frame]={}
			
		Cell.lookup[frame][seg_id]=cell
		
		#print 'set',Cell.lookup
	
	@staticmethod	
	def get(frame,seg_id):
		if frame in Cell.lookup.keys():
			if seg_id in Cell.lookup[frame].keys():
				return Cell.lookup[frame][seg_id]
			
		#print 'get',Cell.lookup
			
		return None	
	
	@staticmethod
	def clear(frame):
		#clear all cells/glyphs from given frame
		if frame in Cell.lookup:
			for cell in Cell.lookup[frame].values():
				cell.destroy()
		

class MGlyph(QtGui.QGraphicsPolygonItem):
	def init(self,cell,pixmap):
		#cell associated with this glyph, pixmap to which it is attached in qt

		self.cell=cell 
		cell.glyph=self
		
		self.pixmap=pixmap		
		self.setParentItem(pixmap)
		
		#self.setPos(*cell.pos)
		self.setPos(cell.pos[0]-view.view_rect[0],cell.pos[1]-view.view_rect[1])
		self.id=cell.track_id
		
		sides=[3,4,5]
		num_sides=sides[self.id%len(sides)]
		
		num_cols=7
		cols=[QtGui.QColor.fromHsv(int(i),255,255) for i in numpy.linspace(0,255,num_cols)]
		self.col=cols[self.id%num_cols]
		#col2=cols[(id+3)%num_cols]
		#col.setAlpha(128)
		
		#qpoly=QtGui.QGraphicsPolygonItem()
		poly=QtGui.QPolygon()
		
		size=4
		for i in range(0,num_sides+1):
			#if num_sides>5 and i%2==0:
				#x=y=0
			#else:
			x=size*math.cos(float(i)/float(num_sides)*2.*math.pi)
			y=size*math.sin(float(i)/float(num_sides)*2.*math.pi)
			poly.append(QtCore.QPoint(x,y))
			
		self.setPolygon(poly)
		self.pen=QtGui.QPen(self.col)
		self.setPen(self.pen)
		
		##toggle filled shapes
		#if id%2==0:
		#	self.setBrush(QtGui.QBrush(col))
		self.brush=QtGui.QBrush(self.col)
		self.setBrush(self.brush)
		
		return self
	
	def mousePressEvent(self,event): 
		view.console('Glyph: %d'%self.id)
		
		if self.pixmap==view.mpixmap_orig_t1: #LHS
			self.select()
			
		elif self.pixmap==view.mpixmap_orig_t2: #RHS:
			event.setPos(event.pos()+self.pos()) #intercept mouse event and pass to parent pixmap, changing coords to parent space
			self.pixmap.mousePressEvent(event)
		
	def select(self):
		if view.selected_glyph:
			view.selected_glyph.deselect()
			
		self.setPen(QtGui.QPen('white'))
		self.setBrush(QtGui.QBrush(QtGui.QColor(255,255,255)))
		view.selected_glyph=self
		
		view.selected_glyphs_div=set()
	
	def deselect(self):
		self.setPen(self.pen)
		self.setBrush(self.brush)
		view.selected_glyph=None

class MPixmap(QtGui.QGraphicsPixmapItem):	
	def init(self):
		self.setAcceptHoverEvents(True)	
		
	def mousePressEvent(self,event):
		mouse_pos=event.pos()
		
		#check in spiral until value found
		for x,y in ((0,0),(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)):
			#value=self.image_array[mouse_pos.y()+y,mouse_pos.x()+x]
			value=self.image_array[mouse_pos.y()+y+view.view_rect[1],mouse_pos.x()+x+view.view_rect[0]]
			if value>0:
				break
		
		if event.button()==QtCore.Qt.RightButton and self.image_type=='segmented':
			#get cell
			size=''
			if value>0:
				if self==view.mpixmap_seg_t1:
					t=view.current_time
				else:
					t=view.current_time+1
				cell=Cell.get(t,value)
				if cell:
					size=str(cell.size)
				
			view.console('('+str(int(mouse_pos.x()))+','+str(int(mouse_pos.y()))+') value: '+str(value)+' size:'+size)
		#else:
			#view.console('')
			
		#select points for segmented cell operations (merge, split, delete)	
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t1 and self.image_type=='segmented' and value>0:
			#add point
			size=1
			point=MEllipse(-size,-size,2*size,2*size)
			point.init(view.inv_colortable[value])
			point.setPos(mouse_pos)
			point.setParentItem(self)
			
			view.clicked_points[0].append((point,value))
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t1 and self.image_type=='segmented' and value==0:
			view.pickingClear()
			
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t2 and self.image_type=='segmented' and value>0:
			#add point
			size=1
			point=MEllipse(-size,-size,2*size,2*size)
			point.init(view.inv_colortable[value])
			point.setPos(mouse_pos)
			point.setParentItem(self)
			
			view.clicked_points[1].append((point,value))
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t2 and self.image_type=='segmented' and value==0:
			view.pickingClear()
		
			
		#glyph/prediction editing
		kbm=event.modifiers()
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t2 and view.selected_glyph and kbm==QtCore.Qt.SHIFT:
			to_id=view.mpixmap_seg_t2.image_array[mouse_pos.y()+view.view_rect[1],mouse_pos.x()+view.view_rect[0]]
			if to_id>0:
				view.selected_glyphs_div.add(to_id)
				if len(view.selected_glyphs_div)>1:
					view.addDivision()	
		elif event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t2 and view.selected_glyph:
			from_id=view.selected_glyph.cell.seg_id
			to_id=view.mpixmap_seg_t2.image_array[mouse_pos.y()+view.view_rect[1],mouse_pos.x()+view.view_rect[0]]
			if to_id>0:
				view.editCells(from_id,to_id)
				view.selected_glyph.deselect()
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t1 and view.selected_glyph:
			view.selected_glyph.deselect()
			
		# divisions editing
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t2 and view.selected_division:
			view.selected_division.deselect()
					
	#def mouseMoveEvent(self,event):
		#view.zoom_move(event)
					
	def hoverMoveEvent(self,event):
		mouse_pos=event.pos()
		value=self.image_array[mouse_pos.y()+view.view_rect[1],mouse_pos.x()+view.view_rect[0]]
		view.console('('+str(int(mouse_pos.x()))+','+str(int(mouse_pos.y()))+') value: '+str(value))


class Project:
	def __init__(self,project_dir):
		self.project_dir=project_dir
		
	def load(self):
		pickle_file=open(self.project_dir+'/.project','rb')
		for d in to_pickle:
			try:
				exec('%s=pickle.load(pickle_file)'%d)
				print d,eval(d)
			except:
				print 'pickle.load error',d
		
	def save(self):
		pickle_file=open(self.project_dir+'/.project','wb')
		for d in to_pickle:
			pickle.dump(eval(d),pickle_file,protocol=2) #binary fast protocol
			
	def read_cells(self):
		self.cells=[]
		for frame in self.lookup:
			for cell in self.lookup[frame].values():
				self.cells.append(cell)
				print cell	
				
	def correct(self):
		#trim cell next / previous (error now fixed)
		for cell in self.cells:
			if isinstance(cell.next,list) and cell.next:
				cell.next=cell.next[-1]
			if isinstance(cell.previous,list) and cell.previous:
				cell.previous=cell.previous[-1]	
				
	def get_track_starts(self):
		#get all track starts
		self.start_cells=[]
		for c in self.cells:
			if not c.previous:
				self.start_cells.append(c)
				
	def plot(self):		
		for s in self.start_cells:
			x=[]
			y=[]
			while(s):
				x.append(s.pos[0])
				y.append(-s.pos[1])
				s=s.next
				
			pyplot.scatter(x,y,c=numpy.random.rand(3,),s=40)
		
		x=[]
		y=[]	
		for d in self.divisions.values():
			if d:
				c1=d[0][0]
				c2=d[0][1]
				x.append(c1.pos[0])
				y.append(-c1.pos[1])
				x.append(c2.pos[0])
				y.append(-c2.pos[1])
				
				pyplot.plot([c1.pos[0],c2.pos[0]],[-c1.pos[1],-c2.pos[1]],'k-')
		
		#pyplot.scatter(x,y,s=150,c='r',alpha=0.2)	
			
		pyplot.show(block=False)				

#########

project=Project(project_dir)
project.load()	
project.read_cells()
#project.correct()
project.get_track_starts()
project.plot()
#project.save()

pass
