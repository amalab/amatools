import sys 
import PySide
#sys.modules['PyQt4']=PySide # HACK for ImageQt
from PySide import QtCore,QtGui
from PIL import Image
#import ImageQt
import time 
import numpy
import tifffile #loads into numpy memory object
import glob
import os
import shutil
from lxml import etree 
import pickle
import scipy.ndimage
import skimage.filter
import skimage.feature
import random
import subprocess
import math
import signal
import time
#from enthought.pyface.api import FileDialog,OK,confirm,YES,SystemMetrics,ProgressDialog

class MScene(QtGui.QGraphicsScene):
	def mouseMoveEvent(self,event): 
		#print '.',
		if app.mouseButtons()!=QtCore.Qt.NoButton:
			#print event.scenePos()
			view.zoom_move(event)
			
	def mouseReleaseEvent(self,event):
		view.last_pos=None
		
	def wheelEvent(self,event):		
		view.zoom_wheel(event)
		
	
# own classes overiding QGraphicsItems to access interaction
class MPixmap(QtGui.QGraphicsPixmapItem):	
	def init(self):
		self.setAcceptHoverEvents(True)	

	def hoverMoveEvent(self,event):
		mouse_pos=event.pos()
			
		value=self.image_array[mouse_pos.y()+view.view_rect[1],mouse_pos.x()+view.view_rect[0]]
		view.console('('+str(int(mouse_pos.x()))+','+str(int(mouse_pos.y()))+') value: '+str(value))		
		
	def mousePressEvent(self,event):
		#QtGui.QGraphicsItem.mousePressEvent(self,event)
	
		mouse_pos=event.pos()	
		#check in spiral until value found
		for x,y in ((0,0),(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1)):
			#value=self.image_array[mouse_pos.y()+y,mouse_pos.x()+x]
			value=self.image_array[mouse_pos.y()+y+view.view_rect[1],mouse_pos.x()+x+view.view_rect[0]]
			if value>0:
				break
		
		if event.button()==QtCore.Qt.RightButton and self.image_type=='segmented':
			#get cell
			size=''
			if value>0:
				if self==view.mpixmap_seg_t1:
					t=view.current_time
				else:
					t=view.current_time+1
				cell=Cell.get(t,value)
				if cell:
					size=str(cell.size)
				
			view.console('('+str(int(mouse_pos.x()))+','+str(int(mouse_pos.y()))+') value: '+str(value)+' size:'+size)
		#else:
			#view.console('')
			
		#select points for segmented cell operations (merge, split, delete)	
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t1 and self.image_type=='segmented' and value>0:
			#add point
			size=1
			point=MEllipse(-size,-size,2*size,2*size)
			point.init(view.inv_colortable[value])
			point.setPos(mouse_pos)
			point.setParentItem(self)
			
			view.clicked_points[0].append((point,value))
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t1 and self.image_type=='segmented' and value==0:
			view.pickingClear()
			
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t2 and self.image_type=='segmented' and value>0:
			#add point
			size=1
			point=MEllipse(-size,-size,2*size,2*size)
			point.init(view.inv_colortable[value])
			point.setPos(mouse_pos)
			point.setParentItem(self)
			
			view.clicked_points[1].append((point,value))
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_seg_t2 and self.image_type=='segmented' and value==0:
			view.pickingClear()
		
			
		#glyph/prediction editing
		kbm=event.modifiers()
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t2 and view.selected_glyph and kbm==QtCore.Qt.SHIFT:
			to_id=view.mpixmap_seg_t2.image_array[mouse_pos.y()+view.view_rect[1],mouse_pos.x()+view.view_rect[0]]
			if to_id>0:
				view.selected_glyphs_div.add(to_id)
				if len(view.selected_glyphs_div)>1:
					view.addDivision()	
		elif event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t2 and view.selected_glyph:
			from_id=view.selected_glyph.cell.seg_id
			to_id=view.mpixmap_seg_t2.image_array[mouse_pos.y()+view.view_rect[1],mouse_pos.x()+view.view_rect[0]]
			if to_id>0:
				view.editCells(from_id,to_id)
				view.selected_glyph.deselect()
			
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t1 and view.selected_glyph:
			view.selected_glyph.deselect()
			
		# divisions editing
		if event.button()==QtCore.Qt.LeftButton and self==view.mpixmap_orig_t2 and view.selected_division:
			view.selected_division.deselect()
					
	#def mouseMoveEvent(self,event):
		#view.zoom_move(event)
							
	#def mouseMoveEvent(self,event): 
		#print '.',
		#if app.mouseButtons()!=QtCore.Qt.NoButton:
			##print event.pos()
			#view.zoom_move(event)
			
	#def wheelEvent(self,event):		
		#view.zoom_wheel(event)	


		
class MEllipse(QtGui.QGraphicsEllipseItem):
	# a point  
	def init(self,col=None): #using init functions because super __init__ throws errors
		self.setAcceptHoverEvents(False)
		self.setZValue(1)
		
		if col:
			self.col=col
		else:
			self.col=QtGui.QColor('white')
		
		self.setPen(QtGui.QPen(QtGui.QColor(self.col)))
		self.setBrush(QtGui.QBrush(QtGui.QColor(self.col)))
		
	#def hoverEnterEvent(self,event):
		##print view.qscene.mouseGrabberItem()
		#view.hover_item=self

		#pen=self.pen()
		#pen.setColor(QtGui.QColor(*self.col_hover)) 
		#self.setPen(pen)        
		#brush=self.brush()
		#brush.setColor(QtGui.QColor(*self.col_hover)) 
		#self.setBrush(brush)       

	#def hoverLeaveEvent(self,event):
		#view.hover_item=None

		#pen=self.pen()
		#pen.setColor(QtGui.QColor(*self.col)) 
		#self.setPen(pen)
		#brush=self.brush()
		#brush.setColor(QtGui.QColor(*self.col)) 
		#self.setBrush(brush)           

	#def mousePressEvent(self,event): #need to override this to get mouseMoveEvents
		#pass

	#def mouseMoveEvent(self,event):
		##move a point and lines it's attached to

		#mouse_pos=event.pos()
		#m#ouse_pos_scene=event.scenePos()
		#pos=self.pos()
		##pos_scene=self.scenePos()

		##move point
		#pos_new=pos+mouse_pos
		#self.setPos(pos_new)

class MGlyph(QtGui.QGraphicsPolygonItem):
	def init(self,cell,pixmap):
		#cell associated with this glyph, pixmap to which it is attached in qt

		self.cell=cell 
		cell.glyph=self
		
		self.pixmap=pixmap		
		self.setParentItem(pixmap)
		
		#self.setPos(*cell.pos)
		self.setPos(cell.pos[0]-view.view_rect[0],cell.pos[1]-view.view_rect[1])
		self.id=cell.track_id
		
		sides=[3,4,5]
		num_sides=sides[self.id%len(sides)]
		
		num_cols=7
		cols=[QtGui.QColor.fromHsv(int(i),255,255) for i in numpy.linspace(0,255,num_cols)]
		self.col=cols[self.id%num_cols]
		#col2=cols[(id+3)%num_cols]
		#col.setAlpha(128)
		
		#qpoly=QtGui.QGraphicsPolygonItem()
		poly=QtGui.QPolygon()
		
		size=4
		for i in range(0,num_sides+1):
			#if num_sides>5 and i%2==0:
				#x=y=0
			#else:
			x=size*math.cos(float(i)/float(num_sides)*2.*math.pi)
			y=size*math.sin(float(i)/float(num_sides)*2.*math.pi)
			poly.append(QtCore.QPoint(x,y))
			
		self.setPolygon(poly)
		self.pen=QtGui.QPen(self.col)
		self.setPen(self.pen)
		
		##toggle filled shapes
		#if id%2==0:
		#	self.setBrush(QtGui.QBrush(col))
		self.brush=QtGui.QBrush(self.col)
		self.setBrush(self.brush)
		
		return self
	
	def mousePressEvent(self,event): 
		view.console('Glyph: %d'%self.id)
		
		if self.pixmap==view.mpixmap_orig_t1: #LHS
			self.select()
			
		elif self.pixmap==view.mpixmap_orig_t2: #RHS:
			event.setPos(event.pos()+self.pos()) #intercept mouse event and pass to parent pixmap, changing coords to parent space
			self.pixmap.mousePressEvent(event)
		
	def select(self):
		if view.selected_glyph:
			view.selected_glyph.deselect()
			
		self.setPen(QtGui.QPen('white'))
		self.setBrush(QtGui.QBrush(QtGui.QColor(255,255,255)))
		view.selected_glyph=self
		
		view.selected_glyphs_div=set()
	
	def deselect(self):
		self.setPen(self.pen)
		self.setBrush(self.brush)
		view.selected_glyph=None

class MDivision(QtGui.QGraphicsLineItem):
	def init(self,c0,c1):
		#takes two Cells
		self.c0=c0
		self.c1=c1		
		
		self.setLine(c0.pos[0]-view.view_rect[0],c0.pos[1]-view.view_rect[1],c1.pos[0]-view.view_rect[0],c1.pos[1]-view.view_rect[1])
		self.setZValue(-1)
		self.pen=QtGui.QPen(QtGui.QColor(255,255,255,100))
		self.pen.setWidth(10)
		self.pen.setCapStyle(QtCore.Qt.RoundCap)
		self.setPen(self.pen)
		self.setParentItem(view.mpixmap_orig_t2)	
		
	def mousePressEvent(self,event): 
		view.console('Division: %d'%id(self))
		self.select()
		
	def select(self):
		if view.selected_division:
			view.selected_division.deselect()
		
		self.pen.setColor(QtGui.QColor(255,255,255,255))
		self.setPen(self.pen)
		
		view.selected_division=self
	
	def deselect(self):
		self.pen.setColor(QtGui.QColor(255,255,255,100))
		self.setPen(self.pen)
		
		view.selected_division=None		
		
class Cell(object):
	track_id_count=0
	lookup={} #stores mapping from frame,seg_id -> cell object
	divisions={} #stores sister cells after a division
	
	#each track is of a single cell
	#cell can only have 0 or 1 previous cells
	#after division, 1 new track starts, 1 continues, frame & sister cells registered in divisions dict
	
	def __init__(self,frame,seg_id,track_id=-1):
		#cells linked into tracks have the same track_id
		if track_id==-1:
			self.track_id=Cell.track_id_count 
			Cell.track_id_count+=1
		else:
			self.track_id=track_id
			
		self.frame=frame		
		self.seg_id=seg_id #id in segmentation at self.frame	
		Cell.set(frame,seg_id,self)
		
		self.next=None #link to Cell object in next frame
		self.previous=None #ditto previous
		
		self.glyph=None
		
		#calc BB and pos
		#read seg
		fn=view.getFilename('segmented',view.channel_to_process,frame)
		if os.path.exists(fn):
			t=tifffile.TIFFfile(fn)	
			img_seg=t.asarray()	
		else:
			print 'No segmented image error'
			return
		
		w=numpy.where(img_seg==self.seg_id)
		self.size=numpy.size(w)
		minx=numpy.min(w[1])
		maxx=numpy.max(w[1])
		miny=numpy.min(w[0])
		maxy=numpy.max(w[0])
		self.bb=(minx,maxx,miny,maxy)
		self.pos=((minx+maxx)/2,(miny+maxy)/2)
		
	def destroy(self):
		#destructor to remove links and lookup ref
		#for p in self.previous:
			#p.next.remove(self)
		if self.previous:
			self.previous.next=None
			
		#for n in self.next:
			#n.previous.remove(self)
		if self.next:
			self.next.previous=None
			
		Cell.lookup[self.frame].pop(self.seg_id)
		
		view.qscene.removeItem(self.glyph)
	
	#lookup table setter and getter
	#lookup[frame][seg_id]
	@staticmethod	
	def set(frame,seg_id,cell):
		if frame not in Cell.lookup.keys():
			Cell.lookup[frame]={}
			
		Cell.lookup[frame][seg_id]=cell
		
		#print 'set',Cell.lookup
	
	@staticmethod	
	def get(frame,seg_id):
		if frame in Cell.lookup.keys():
			if seg_id in Cell.lookup[frame].keys():
				return Cell.lookup[frame][seg_id]
			
		#print 'get',Cell.lookup
			
		return None	
	
	@staticmethod
	def clear(frame):
		#clear all cells/glyphs from given frame
		if frame in Cell.lookup:
			for cell in Cell.lookup[frame].values():
				cell.destroy()
		
# View QWidget -> QVBoxLayout -> QGraphicsView(QWidget) -> QGraphicsScene  
class View(QtGui.QMainWindow):
	def __init__(self,parent=None):
		#QtGui.QMainWindow.__init__(self,parent)
		super(View, self).__init__()
		
		app.installEventFilter(self)

		self.project_states=['none','opened']
		self.project_state='none' 

		self.to_pickle=['self.sizeC','self.sizeT','self.sizeX','self.sizeY','self.current_channel','self.current_time','Cell.lookup','Cell.track_id_count','Cell.divisions']

		self.movie_filename=''
		self.project_dir=''		
		self.project_dir='/Users/jpm/Progs/AMATools/Tracker2/131104_TG4Ch2_H2B-Cerul_test-0001_AA01_01'		
		self.sizeC=0
		self.sizeT=0
		self.current_channel=0
		self.current_time=0
		self.image_w=0
		self.image_h=0
		self.image_scale=1.0
		#self.image_scale=0.5
		self.key_pressed=None
		#self.dragging=False
		self.last_edited_time=None
		
		self.clicked_points=[[],[]]
		self.selected_glyph=None
		self.selected_division=None
		self.selected_glyphs_div=set()
		
		self.white_brush=QtGui.QBrush('white')		
		
		#self.proc_images=['img_orig','img_denoise','img_thresh','img_distance','img_labels','img_local_max','img_markers','img_watershed','segmented']#,'img_watershed_filtered']
		self.proc_images=['segmented']
		self.channel_to_process=1
		
		#object size filter both before and after watershed, in pixels
		self.filter_size=50
		#local thresh params
		self.block_size=30
		self.thresh_offset=-200 #more negative = less sensitive
		
		#zoom
		self.last_pos=None
		self.view_rect=[0,0,256,256] #l,r,w,h
		self.zoom=0 #to 9
		
		self.orig_colortables=[[QtGui.qRgb(i,i,i) for i in xrange(256)],[QtGui.qRgb(0,i,i) for i in xrange(256)],\
		                       [QtGui.qRgb(i,0,0) for i in xrange(256)],[QtGui.qRgb(0,i,0) for i in xrange(256)],\
		                       [QtGui.qRgb(0,0,i) for i in xrange(256)],[QtGui.qRgb(i,0,i) for i in xrange(256)],\
		                       [QtGui.qRgb(i,i,0) for i in xrange(256)]]
		self.proc_colortable=[QtGui.qRgb(*QtGui.QColor.fromHsv(int(i),255,255).getRgb()[0:3]) for i in numpy.linspace(359,0,256)]
		self.proc_colortable[0]=QtGui.qRgb(0,0,0)
		random.seed(0)
		r=[(random.randint(0,255),random.randint(0,255),random.randint(0,255)) for i in xrange(256)]
		self.seg_colortable=[QtGui.qRgb(*i) for i in r]
		#self.seg_colortable=[QtGui.qRgb(*QtGui.QColor.fromHsv((i*30)%255,255,255).getRgb()[0:3]) for i in xrange(256)]
		self.seg_colortable[0]=QtGui.qRgb(0,0,0)
		
		self.inv_colortable=[QtGui.qRgb(255-i[0],255-i[1],255-i[2]) for i in r]
	
		###toolbar
		self.toolbar=self.addToolBar('Tools')

		newProjectAction=QtGui.QAction('New Project',self)
		newProjectAction.triggered.connect(self.newProject)
		self.toolbar.addAction(newProjectAction)

		openProjectAction=QtGui.QAction('Open Project',self)
		openProjectAction.triggered.connect(self.openProject)
		self.toolbar.addAction(openProjectAction)        

		saveProjectAction=QtGui.QAction('Save Project',self)
		saveProjectAction.triggered.connect(self.saveProject)
		self.toolbar.addAction(saveProjectAction)  
		
		#seqmentation editing
		mergeCellsAction=QtGui.QAction('Merge Cells',self)
		mergeCellsAction.triggered.connect(self.mergeCells)
		mergeCellsAction.setShortcut(QtGui.QKeySequence('M'))
		self.toolbar.addAction(mergeCellsAction)  
		
		splitCellsAction=QtGui.QAction('Split Cell',self)
		splitCellsAction.triggered.connect(self.splitCell)
		splitCellsAction.setShortcut(QtGui.QKeySequence('S'))
		self.toolbar.addAction(splitCellsAction)  
		
		deleteCellsAction=QtGui.QAction('Delete Cells',self)
		deleteCellsAction.triggered.connect(self.deleteCells)
		deleteCellsAction.setShortcut(QtGui.QKeySequence('D'))
		self.toolbar.addAction(deleteCellsAction)  
		
		undoCellsAction=QtGui.QAction('Undo',self)
		undoCellsAction.triggered.connect(self.undoCells)
		undoCellsAction.setShortcut(QtGui.QKeySequence('U'))
		self.toolbar.addAction(undoCellsAction) 
		
		markCellsAction=QtGui.QAction('Mark Cells',self)
		markCellsAction.triggered.connect(self.markCells)
		markCellsAction.setShortcut(QtGui.QKeySequence('K'))
		self.toolbar.addAction(markCellsAction)  
			
		botAction=QtGui.QAction('BOT',self)
		botAction.triggered.connect(self.onBot)
		botAction.setShortcut(QtGui.QKeySequence('B'))
		self.toolbar.addAction(botAction) 
		
		#testAction=QtGui.QAction('TEST',self)
		#testAction.triggered.connect(self.test)
		#testAction.setShortcut(QtGui.QKeySequence('T'))
		#self.toolbar.addAction(testAction)   
		
		#resetAction=QtGui.QAction('RESET',self)
		#resetAction.triggered.connect(self.reset)
		#self.toolbar.addAction(resetAction)   

		#main widget / main layout is top level layout
		self.main_layout=QtGui.QVBoxLayout()
		self.main_widget=QtGui.QWidget()
		self.main_widget.setLayout(self.main_layout)
		self.setCentralWidget(self.main_widget) #central widget is primary widget of main window
		self.main_widget.setMouseTracking(True)

		#graphics scene & view
		#self.qscene=QtGui.QGraphicsScene()
		self.qscene=MScene()
		self.qview=QtGui.QGraphicsView(self.qscene)
		self.qview.setMouseTracking(True)
		self.main_layout.addWidget(self.qview)
		self.mpixmaps=[] #list of mpixmaps currently in view attached to qscene

		###CONTROLS###
		self.controls_layout=QtGui.QHBoxLayout()
		self.main_layout.addLayout(self.controls_layout)
		
		#channel
		channelViewLabel=QtGui.QLabel('Channel to View')
		channelViewLabel.setFont(QtGui.QFont("Arial",pointSize=12))
		channelViewLabel.setWordWrap(True)
		self.controls_layout.addWidget(channelViewLabel)
		self.channelViewSpinBox=QtGui.QSpinBox(self)
		self.channelViewSpinBox.valueChanged[int].connect(self.onChannelView)
		self.controls_layout.addWidget(self.channelViewSpinBox)
		
		#self.controls_layout.addWidget(QtGui.QLabel(' ')) #space		
		
		channelUseLabel=QtGui.QLabel('Channel to Track')
		channelUseLabel.setFont(QtGui.QFont("Arial",pointSize=12))
		channelUseLabel.setWordWrap(True)
		self.controls_layout.addWidget(channelUseLabel)
		self.channelUseSpinBox=QtGui.QSpinBox(self)
		self.channelUseSpinBox.valueChanged[int].connect(self.onChannelUse)
		self.controls_layout.addWidget(self.channelUseSpinBox)
		self.channelUseSpinBox.setValue(self.channel_to_process)
		
		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space		
		
		#filter
		filterLabel=QtGui.QLabel('Filter Size')
		filterLabel.setFont(QtGui.QFont("Arial",pointSize=12))
		filterLabel.setWordWrap(True)
		self.controls_layout.addWidget(filterLabel)
		self.filterSpinBox=QtGui.QSpinBox(self)
		self.filterSpinBox.setRange(10,200)
		self.filterSpinBox.valueChanged[int].connect(self.onFilter)
		self.filterSpinBox.setKeyboardTracking(False)
		self.controls_layout.addWidget(self.filterSpinBox)
		self.filterSpinBox.setValue(self.filter_size)
		
		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space		
		
		#thresholding
		blockLabel=QtGui.QLabel('Block Size')
		blockLabel.setFont(QtGui.QFont("Arial",pointSize=12))
		blockLabel.setWordWrap(True)
		self.controls_layout.addWidget(blockLabel)
		self.blockSpinBox=QtGui.QSpinBox(self)
		self.blockSpinBox.setRange(1,200)
		self.blockSpinBox.valueChanged[int].connect(self.onBlock)
		self.blockSpinBox.setKeyboardTracking(False)
		self.controls_layout.addWidget(self.blockSpinBox)
		self.blockSpinBox.setValue(self.block_size)
		
		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space		
		
		offsetLabel=QtGui.QLabel('Offset (larger=more sensitive')
		offsetLabel.setFont(QtGui.QFont("Arial",pointSize=12))
		offsetLabel.setWordWrap(True)
		self.controls_layout.addWidget(offsetLabel)
		self.offsetSpinBox=QtGui.QSpinBox(self)
		self.offsetSpinBox.setRange(-500,500)
		self.offsetSpinBox.valueChanged[int].connect(self.onOffset)
		self.offsetSpinBox.setKeyboardTracking(False)
		self.controls_layout.addWidget(self.offsetSpinBox)
		self.offsetSpinBox.setValue(self.thresh_offset)
		
		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space		
		
		#brightness		
		self.autoScaleCheckBox=QtGui.QCheckBox('Autoscale')
		self.autoScaleCheckBox.setLayoutDirection(QtCore.Qt.RightToLeft)
		self.autoScaleCheckBox.stateChanged.connect(self.updateImages)
		self.autoScaleCheckBox.setChecked(True)
		self.controls_layout.addWidget(self.autoScaleCheckBox)
		
		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space		
		
		brightnessLabel=QtGui.QLabel('Brightness')
		brightnessLabel.setFont(QtGui.QFont("Arial",pointSize=12))
		self.controls_layout.addWidget(brightnessLabel)
		
		self.brightness_slider=QtGui.QSlider(QtCore.Qt.Horizontal)
		self.brightness_slider.setFixedWidth(100)
		self.brightness_slider.setMaximum(100)
		self.brightness_slider.setValue(50)
		self.controls_layout.addWidget(self.brightness_slider)
		self.brightness_slider.valueChanged.connect(self.updateImages)
		
		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space
		
		#fast		
		self.resegmentCheckBox=QtGui.QCheckBox('Resegment')
		self.resegmentCheckBox.setLayoutDirection(QtCore.Qt.RightToLeft)
		self.resegmentCheckBox.setChecked(False)
		self.resegmentCheckBox.stateChanged.connect(self.onFast)		
		self.controls_layout.addWidget(self.resegmentCheckBox)

		#self.controls_layout.addWidget(QtGui.QLabel('  ')) #space		

		# processed image viewed
		self.proc_image_combo=QtGui.QComboBox()
		for i in self.proc_images:
			self.proc_image_combo.addItem(i)
		self.proc_image_combo.setCurrentIndex(self.proc_images.index('segmented'))
		self.controls_layout.addWidget(self.proc_image_combo)
		self.proc_image_combo.currentIndexChanged.connect(self.updateImages)
		
		#time
		self.time_slider=QtGui.QSlider(QtCore.Qt.Horizontal)
		self.controls_layout.addWidget(self.time_slider)
		self.time_slider.valueChanged.connect(self.onTimeSlider)
		self.time_slider.setTracking(False)
		#self.time_slider.setTickInterval(10)
		#self.time_slider.setTickPosition(QtGui.QSlider.TicksBelow)

		self.time_label=QtGui.QLabel()
		self.time_label.setFixedWidth(100)
		self.controls_layout.addWidget(self.time_label)
		self.updateTimeLabel()

		self.back_button=QtGui.QPushButton('<')
		self.controls_layout.addWidget(self.back_button)	
		self.back_button.clicked.connect(self.onBackButton)

		self.forward_button=QtGui.QPushButton('>')
		self.controls_layout.addWidget(self.forward_button)	
		self.forward_button.clicked.connect(self.onForwardButton)

		self.resize(1600,1200)
		self.show()  
		self.qscene.setFocus()

		# NB- keep refs to all qt objects to avoid probs

		#install event filter because view is swallowing arrow key events
		self.qview.installEventFilter(self)	
		
		self.setMouseTracking(True)	
		self.console('Ready.')

	#def eventFilter(self,obj,event):#return true to consume the keystroke
		##if event.type()==QtCore.QEvent.KeyPress:
			##self.keyPressEvent(event)
			##result=False;
		##elif event.type()==QtCore.QEvent.KeyRelease:
			##self.keyReleaseEvent(event)
			##result=False;
			
		##zoom
		#if event.type()==QtCore.QEvent.MouseMove:
			#if app.mouseButtons()!=QtCore.Qt.NoButton:
				##print event.pos()
				#self.zoom_move(event)
			#result=False
			
		#elif event.type()==QtCore.QEvent.Wheel:
			#self.zoom_wheel(event)	
			#result=True

		#elif event.type()==QtCore.QEvent.MouseButtonRelease:
			#self.last_pos=None
			#result=False

		##Standard event processing
		#else:
			#result=QtGui.QMainWindow.eventFilter(self,obj,event)	

		#return result

	def keyPressEvent(self,event):
		self.key_pressed=event.key()
		print self.key_pressed

		if self.key_pressed==QtCore.Qt.Key_Comma:
			self.onBackButton()
		elif self.key_pressed==QtCore.Qt.Key_Period:
			self.onForwardButton()
		elif self.key_pressed==QtCore.Qt.Key_BracketLeft:
			self.current_channel+=1
			self.updateChannelSpinBox()
		elif self.key_pressed==QtCore.Qt.Key_BracketRight:
			self.current_channel-=1
			self.updateChannelSpinBox()
		elif self.key_pressed==QtCore.Qt.Key_Escape:
			self.pickingClear()
		elif self.key_pressed==QtCore.Qt.Key_Delete:
			if self.selected_glyph:
				self.deleteCell(self.selected_glyph.cell)
			if self.selected_division:
				self.deleteDivision(self.selected_division)
					

	def keyReleaseEvent(self,event):
		#key_released=event.key()
		self.key_pressed=None

	def resizeEvent(self,event):
		#self.view_size=self.main_widget.size()
		#if self.image_w>0 and self.image_h>0:
			#s_w=(self.view_size.width()-50)/(2.*self.image_w) #fudge to leave margins
			#s_h=(self.view_size.height()-100)/(2.*self.image_h)
			#self.image_scale=min(s_w,s_h)	
			#self.updateImages()
		self.updateImages()
		

	def zoom_move(self,event):
		pos=QtCore.QPointF(event.scenePos())
		#print pos
		#return
	
		if self.last_pos:
			diff=pos-self.last_pos
			
			view_rect_old=self.view_rect[:]
			
			self.view_rect[0]-=int(diff.x())
			self.view_rect[1]-=int(diff.y())
			
			#reset view_rect if out of bounds			
			if self.view_rect[0]<0 or self.view_rect[1]<0 or self.view_rect[0]>self.sizeX or self.view_rect[1]>self.sizeY \
			   or self.view_rect[0]+self.view_rect[2]>self.sizeX or self.view_rect[1]+self.view_rect[3]>self.sizeY \
			   or self.view_rect[2]<20 or self.view_rect[3]<20:
				self.view_rect=view_rect_old[:]
			
				#print self.view_rect
			
			self.updateImages()			
				
		self.last_pos=pos
		
	def zoom_wheel(self,event):
		sign=cmp(event.delta(),0)
		zoom_last=self.zoom
		self.zoom+=sign
		if self.zoom<0:
			self.zoom=0
		if self.zoom>9:
			self.zoom=9
			
		w=self.sizeX*(1.-self.zoom/10.)
		h=self.sizeY*(1.-self.zoom/10.)
		dx=self.sizeX*(1.-zoom_last/10.)-w
		dy=self.sizeY*(1.-zoom_last/10.)-h
		
		x=self.view_rect[0]+dx
		if x<0:
			x=0
		if x>self.sizeX-w:
			x=self.sizeX-w
			
		y=self.view_rect[1]+dy
		if y<0:
			y=0
		if y>self.sizeY-h:
			y=self.sizeY-h
		
		#sizes have to be divisible by 4 for QPixmap 16 bit 	
		x=x-x%4
		y=y-y%4
		w=w-w%4
		h=h-h%4
		
		self.view_rect=[x,y,w,h]
			
		#print self.view_rect	
		self.updateImages()
				
	def console(self,msg):
		self.statusBar().showMessage(msg) 
		#print msg
		#self.resizeEvent(None)
		
	def progress_start(self):
		self.progress=QtGui.QProgressDialog('Please wait...','Cancel',0,100,self)
		self.progress.setWindowModality(QtCore.Qt.WindowModal)
		self.progress.setMinimumDuration(0)
		self.progress.setValue(0)
		
	def progress_update(self,t,label_text=None):
		self.progress.setValue(t)
		if label_text:
			self.progress.setLabelText(label_text)
	
	def newProject(self):
		self.progress_start()
		
		self.movie_filename,sf=QtGui.QFileDialog.getOpenFileName(self,'Choose a .zvi or .ome.tiff file...','.','Movie(*.zvi *.ome.tif)')        
		if self.movie_filename=='':
			return

		#create directory with same name as zvi
		p,f=os.path.split(self.movie_filename)
		project_dir=p+'/'+os.path.splitext(f)[0]
		if os.path.exists(project_dir):
			self.statusBar().showMessage('Project already exists.')
			return
		else:
			os.makedirs(project_dir)  

		self.project_dir=project_dir         

		# move zvi into new directory
		new_name=self.project_dir+'/'+f
		os.rename(self.movie_filename,new_name)
		#self.movie_filename="'"+new_name+"'" #quotes in case of spaces for subsequent cmd line calls
		self.movie_filename=new_name #quotes in case of spaces for subsequent cmd line calls

		# create image subdirs & those needed by BOT
		for d in ['orig','segmented','seg','raw','prediction']:
			os.mkdir(self.project_dir+'/'+d)
			
		# /orig - tifs from all channels for all times
		# /segmented - cache of working segmented images, originally created by segmentImage and edited by split.merge or botCorrect
		# /raw - pair of orig frames used by BOT
		# /seg - pair of segmented frames used by BOT
		# /prediction - BOT output
		
		self.progress_update(10,'Getting metadata...')
		
		#get xml metadata
		s='../bftools/showinf -nopix -omexml-only '+self.movie_filename
		x=os.popen(s).read() #xml metadata
		self.xml_root=etree.fromstring(x)

		self.sizeC=int(self.xml_root.xpath("//*[@SizeC]")[0].xpath('@SizeC')[0])
		self.sizeT=int(self.xml_root.xpath("//*[@SizeT]")[0].xpath('@SizeT')[0])
		self.sizeX=int(self.xml_root.xpath("//*[@SizeX]")[0].xpath('@SizeX')[0])
		self.sizeY=int(self.xml_root.xpath("//*[@SizeY]")[0].xpath('@SizeY')[0])
		
		self.progress_update(40,'Converting zvi...')
		
		# convert zvi to individual tifs & put into orig	
		self.finished=False
		if os.path.isfile(self.movie_filename): 
			#subprocess.check_output(['../bftools/bfconvert','-series','0',self.movie_filename,self.project_dir+'/orig/orig_C%c_T%t.tif'],stderr=subprocess.STDOUT)
					
			class Mthread(QtCore.QThread):
				def run(self):                
					subprocess.check_output(['../bftools/bfconvert','-series','0',view.movie_filename,view.project_dir+'/orig/orig_C%c_T%t.tif'],stderr=subprocess.STDOUT)
					view.finished=True
					
			self.mthread=Mthread()
			self.mthread.start()
			
		while not self.finished:
			files=os.listdir(self.project_dir+'/orig')
			p=40+30*len(files)/(self.sizeC*self.sizeT)
			#print p,
			self.progress_update(p)
			time.sleep(0.5)
			#if os.path.isfile(self.project_dir+'/orig/orig_C%d_T%d.tif'%(self.sizeC,self.sizeT)):
				#self.finished=True
		
		self.progress_update(70,'Processing tifs...')
		
		#add leading 0 to filename so that files sort correctly for BOT
		for c in range(self.sizeC):
			for t in range(0,100):
				if os.path.isfile(self.project_dir+'/orig/orig_C%d_T%d.tif'%(c,t)):
					if t<10:
						os.rename(self.project_dir+'/orig/orig_C%d_T%d.tif'%(c,t),self.project_dir+'/orig/orig_C%d_T00%d.tif'%(c,t))	
					else:
						os.rename(self.project_dir+'/orig/orig_C%d_T%d.tif'%(c,t),self.project_dir+'/orig/orig_C%d_T0%d.tif'%(c,t))		

		max_size=max([self.sizeX,self.sizeY])
		if max_size>512:
			self.decimate(2)
		
		self.saveProject()
		
		self.progress_update(100,'Done.')

		self.initProject()
		
	def decimate(self,d=2):
		tifs=glob.glob(self.project_dir+'/orig/*.tif')
		for tif in tifs:
			print tif
			t=tifffile.TIFFfile(tif)		
			img=t.asarray()
			h,w=img.shape
			img=img[::d,::d]				
			#img=img[h/4:3*h/4,w/4:3*w/4]				
			tifffile.imsave(tif,img)
			
		self.sizeX/=d
		self.sizeY/=d
		

	def openProject(self,project_dir=None):
		if not project_dir:
			self.project_dir=QtGui.QFileDialog.getExistingDirectory(self,'Choose a project directory...')
			if self.project_dir=='':
				return
			
		else:
			self.project_dir=project_dir

		pickle_file=open(self.project_dir+'/.project','rb')
		for d in self.to_pickle:
			try:
				exec('%s=pickle.load(pickle_file)'%d)
				print d,eval(d)
			except:
				print 'pickle.load error',d
			
		self.initProject()
		pass 
	
	def initProject(self):
		#called after open or new project
		self.project_state='opened'
		self.view_rect=[0,0,self.sizeX,self.sizeY]	
		self.current_channel=1
		self.updateChannelSpinBox()			
		self.timeChanged()

		pass
		
	def saveProject(self):
		pickle_file=open(self.project_dir+'/.project','wb')
		for d in self.to_pickle:
			pickle.dump(eval(d),pickle_file,protocol=2) #binary fast protocol

	def onBackButton(self):
		self.current_time-=1
		if self.current_time<0:
			self.current_time=0

		self.timeChanged()

	def onForwardButton(self):
		self.current_time+=1
		if self.current_time>(self.sizeT-2):
			self.current_time=self.sizeT-2
			
		self.timeChanged()

	def onTimeSlider(self):
		self.current_time=self.time_slider.value()
		
		self.timeChanged()
		
	def onChannelView(self):
		self.current_channel=self.channelViewSpinBox.value()
		self.updateImages()
		
	def onChannelUse(self):
		self.channel_to_process=self.channelUseSpinBox.value()
		
	def onFilter(self):
		self.filter_size=self.filterSpinBox.value()
		self.pickingClear()
		self.updateImages()
		
	def onBlock(self):
		self.block_size=self.blockSpinBox.value()
		self.pickingClear()
		self.updateImages()
		
	def onOffset(self):
		self.thresh_offset=self.offsetSpinBox.value()
		self.pickingClear()
		self.updateImages()
		
	def onFast(self):
		if not self.resegmentCheckBox.isChecked():
			self.proc_images=['segmented']
		else:
			self.proc_images=['img_orig','img_denoise','img_thresh','img_distance','img_labels','img_local_max','img_markers','img_watershed','img_watershed_filtered','segmented']
		
		self.proc_image_combo.blockSignals(True)
		self.proc_image_combo.clear()
		for i in self.proc_images:
			self.proc_image_combo.addItem(i)			
		self.proc_image_combo.blockSignals(False)	
		
		self.proc_image_combo.setCurrentIndex(self.proc_images.index('segmented'))
		
		self.updateImages()
	
		
	def onBot(self):
		#BOT toolbar pressed
		print '---BOT---'	
		#process single pair of images with BOT
		
		#run BOT first time
		self.cleanDirs(['raw','seg'])
		self.botCreateSegAndRaw()
		t=time.clock()
		#ret=self.botRunSingle()
		ret=self.botRunSingleTimeout()
		print '~~~~~~~~~TIME~~~~~~~~~',time.clock()-t
		if ret==1:		
			#correct segmentation errors ('split' results from BOT)
			self.botCorrectSingle()
			
			#run BOT again
			self.cleanDirs(['raw','seg'])
			self.botCreateSegAndRaw()
			#self.botRunSingleTime()
			self.botRunSingleTimeout()
			
			#clear Cell lookup table
			self.botClearCells(self.current_time+1)
			#create Cells and glyphs
			self.botCellsFromPredictionsSingle()
			
		self.updateImages()		
		
		print '---------'
				
	def cleanDirs(self,dirs):
		#clean directories
		#dirs=['raw','seg','prediction']
		for d in dirs:
			shutil.rmtree(self.project_dir+'/%s'%d)
			os.mkdir(self.project_dir+'/%s'%d)


	def botCreateSegAndRaw(self):		
		#copy orig->raw and segmented->seg for current pair

		for time in [self.current_time,self.current_time+1]:
			f_src=self.getFilename('orig',self.channel_to_process,time)
			f_dest=self.getFilename('raw',self.channel_to_process,time)
			shutil.copyfile(f_src,f_dest)
			f_src=self.getFilename('segmented',self.channel_to_process,time)
			f_dest=self.getFilename('seg',self.channel_to_process,time)
			shutil.copyfile(f_src,f_dest)

	def botRunSingle(self):	
		#run BOT
		bot_dir=os.getcwd()+'/BOT'
		ret=subprocess.call(('%s/apps/TrackingPredictor'%bot_dir,self.project_dir,'%s/data/event-configuration-cell.ini'%bot_dir))
		
		if ret==1: #BOT mostly returns 1
			os.rename(self.getPredictionFilename(0),self.getPredictionFilename(self.current_time))
		
		else:
			self.console('BOT Error')
			
		return ret
		

	def botRunSingleTimeout(self):
		
		# Register a handler for the timeout
		def handler(signum, frame):
			raise Exception("timeout")
		
		# Register the signal function handler
		signal.signal(signal.SIGALRM, handler)
		
		# Define a timeout 
		signal.alarm(20)

		try:
			ret=self.botRunSingle()
		except Exception, exc: 
			#print exc
			self.console('BOT Error')
			ret=-100
			
		signal.alarm(0)
			
		print '~~~~~~~~~~BOT RET~~~~~~~',ret	
		return ret

	def botCorrectSingle(self):
		num_splits=0
		num_merges=0
		
		frame=self.current_time		
		pf=self.getPredictionFilename(frame)
				
		#find splits and merges for each frame
		splits=[]
		merges=[]
		
		print '--------'
		print 'Frame:',frame
		f=open(pf,'r')
		s=f.read()
		for section in ['Split','Merge']:
			try:
				i1=s.index(section)
				i2=s.index('\n\n',i1)
				e=s[i1:i2].split() #section elements
				print section	
										
				if section == 'Split':
					for j in range(1,len(e),4):
						print e[j],e[j+1],e[j+2],e[j+3]
						splits.append((int(e[j+2]),int(e[j+3])))
						num_splits+=1
												
				if section == 'Merge':
					for j in range(1,len(e),4):
						print e[j],e[j+1],e[j+2],e[j+3]
						merges.append((int(e[j]),int(e[j+1]),int(e[j+3])))	
						num_merges+=1
				
			except:
				print 'no ',section
				
		#correct segmented images
		
		#splits
		t=tifffile.TIFFfile(self.getFilename('segmented',self.channel_to_process,frame+1))		
		img_seg=t.asarray()	
		
		for split in splits:
			cell_pixels=numpy.where(img_seg==split[1])
			img_seg[cell_pixels]=split[0]
			
		#save segmented as tif
		tifffile.imsave(self.getFilename('segmented',self.channel_to_process,frame+1),img_seg)
		
		
		##NO MERGES		
		##merges
		##t=tifffile.TIFFfile(self.getFilename('segmented',self.channel_to_process,frame+1))		
		##img_seg=t.asarray()	
		
		#for merge in merges:
			#cell1=Cell.get(frame,merge[0])
			#cell2=Cell.get(frame,merge[1])
			
			#p1=cell1.pos
			#p2=cell2.pos
			
			#if img_seg[p1[1],p1[0]]!=0 and img_seg[p2[1],p2[0]]!=0:
				
				##change seg
				#val=merge[2]
				#w=numpy.where(img_seg==val) #find all pixels having same value as that of first clicked
				#ws=numpy.vstack(w) #put into array of (x,y) coords
				#ws=numpy.swapaxes(ws,1,0) 							
				##c=numpy.array([(p[0].pos().y(),p[0].pos().x()) for p in self.clicked_points[frame]]) #posns of clicked points
				#c=numpy.array([(p[1],p[0]) for p in (p1,p2)]) #posns with which to split
				#dist=scipy.spatial.distance.cdist(ws,c) #distances from each pixel to each split point 
		
				#labels=numpy.unique(img_seg)
				#max_label=labels[-1] #get max of labels already used
					
				#for i in range(dist.shape[0]):#for each pixel
					#p=ws[i] #point we are examining
					#min_index=numpy.argmin(dist[i])#index of closest selection centre					
					#img_seg[tuple(p)]=max_label+min_index+1 #new label
					##print min_index,img_seg[tuple(p)]
				
			
		##save segmented as tif
		#tifffile.imsave(self.getFilename('segmented',self.channel_to_process,frame+1),img_seg)
		
		print '=========='
		print 'num_splits:',num_splits
		print 'num_merges:',num_merges
		
		
	def botClearCells(self,frame):
		if frame in Cell.lookup.keys():
			Cell.lookup[frame]={}
		
	def botCellsFromPredictionsSingle(self):
		#read predictions into cells for current_time
		sections=['Move','Split','Division','Merge','Appearance','Disappearance']
		self.lookup=Cell.lookup
		
		frame=self.current_time		
		pf=self.getPredictionFilename(frame)
		
		print '~~~~~~~~'
		print 'Frame:',frame
		f=open(pf,'r')
		s=f.read()
		
		#reset divisions
		Cell.divisions[frame+1]=[]
		
		for section in sections:
			i1=s.find(section)
			if i1==-1:
				print 'no ',section
			else:
				i2=s.find('\n\n',i1)
				e=s[i1:i2].split() #section elements
				print section							

				if section == 'Move':
					for j in range(1,len(e),3):
						print e[j],e[j+1],e[j+2]
						from_seg=int(e[j])
						to_seg=int(e[j+2])
						
						#if self.current_time==0:
						#	first_cell=Cell(frame,from_seg)
							
						previous_cell=Cell.get(frame,from_seg)
						if previous_cell:
							next_cell=Cell(frame+1,to_seg,previous_cell.track_id)	
							#next_cell.previous.append(previous_cell)
							next_cell.previous=previous_cell
							#previous_cell.next.append(next_cell)
							previous_cell.next=next_cell
						
				if section == 'Split':
					for j in range(1,len(e),4):
						print e[j],e[j+1],e[j+2],e[j+3]
						from_seg=int(e[j])
						to_seg1=int(e[j+2])
						to_seg2=int(e[j+3])
						
						previous_cell=Cell.get(frame,from_seg)
						if previous_cell:
							next_cell=Cell(frame+1,to_seg1,previous_cell.track_id)	
							#next_cell.previous.append(previous_cell)
							next_cell.previous=previous_cell
							#previous_cell.next.append(next_cell)	
							previous_cell.next=next_cell
							
							new_cell=Cell(frame+1,to_seg2)
						
				if section == 'Division':					
					for j in range(1,len(e),4):
						print e[j],e[j+1],e[j+2],e[j+3]
						from_seg=int(e[j])
						to_seg1=int(e[j+2])
						to_seg2=int(e[j+3])
						
						previous_cell=Cell.get(frame,from_seg)
						if previous_cell:
							next_cell=Cell(frame+1,to_seg1,previous_cell.track_id)	
							#next_cell.previous.append(previous_cell)
							next_cell.previous=previous_cell
							#previous_cell.next.append(next_cell)	
							previous_cell.next=next_cell	
							
							new_cell=Cell(frame+1,to_seg2)
													
							Cell.divisions[frame+1].append([next_cell,new_cell])
						
				if section == 'Merge':
					for j in range(1,len(e),4):
						print e[j],e[j+1],e[j+2],e[j+3]
						from_seg1=int(e[j])
						from_seg2=int(e[j+1])
						to_seg=int(e[j+3])
												
						previous_cell=Cell.get(frame,from_seg1)
						if previous_cell:
							next_cell=Cell(frame+1,to_seg,previous_cell.track_id)	
							#next_cell.previous.append(previous_cell)
							next_cell.previous=previous_cell
							#previous_cell.next.append(next_cell)	
							previous_cell.next=next_cell
				
				##not using 										
				#if section == 'Appearance':
					#for j in range(1,len(e),3):
						#print e[j+2]
						#to_seg=int(e[j+2])
						
						#new_cell=Cell(frame+1,to_seg)													
						
				#if section == 'Disappearance':
					#for j in range(1,len(e),3):
						#print e[j]		
						#from_seg=int(e[j])
						
		pass
		
	def getFilename(self,ftype='orig',channel=0,time=0):
		if time<10:
			fn=self.project_dir+'/'+ftype+'/'+ftype+'_C%d'%channel+'_T00%d.tif'%time
		elif time<100:
			fn=self.project_dir+'/'+ftype+'/'+ftype+'_C%d'%channel+'_T0%d.tif'%time
		else:
			fn=self.project_dir+'/'+ftype+'/'+ftype+'_C%d'%channel+'_T%d.tif'%time
		
		return fn
	
	def getPredictionFilename(self,time):
		if time<10:
			fn=self.project_dir+'/prediction/0000000%d.txt'%time
		elif time<100:
			fn=self.project_dir+'/prediction/000000%d.txt'%time
		else:
			fn=self.project_dir+'/prediction/00000%d.txt'%time
		
		return fn
	
	
	def timeChanged(self):
		#single entry point for any UI time change event
		
		self.pickingClear()	
		
		self.updateImages()
		self.updateTimeSlider()
		self.updateTimeLabel()	
		
	def clean(self):
		self.pickingClear()
		
		#remove pixmaps
		for p in self.mpixmaps:
			self.qscene.removeItem(p)		
		self.mpixmaps=[]
		
		#remove glyphs
		# current_time
		for t in [self.current_time,self.current_time+1]:
			if t in Cell.lookup.keys():
				cells=Cell.lookup[t]
				for cell in cells.values():
					if cell.glyph:
						del cell.glyph
						cell.glyph=None	
								
	def updateImages(self):
		#clear mpixmaps and recreate 2x orig and 2x processed for current_time
		
		# QImage -> QPixmap -> MPixmap(QGraphicsPixmapItem)
		
		if self.project_state=='none':
			return
		
		self.clean()		
				
		brightness=2.0*float(self.brightness_slider.value())/50.0

		#######################
		### orig at current_time
		t=tifffile.TIFFfile(self.getFilename('orig',self.current_channel,self.current_time))
		
		self.image_array_orig_t1=t.asarray()					
		#sh=self.image_array_orig_t1.shape
		#self.image_w=sh[1]
		#self.image_h=sh[0]
		self.image_w=self.sizeX
		self.image_h=self.sizeY
		
		view_width=self.qview.width()
		view_height=self.qview.height()
		
		image_w_zoom=self.view_rect[2]
		image_h_zoom=self.view_rect[3]
		x=self.view_rect[0]
		y=self.view_rect[1]
		w=self.view_rect[2]
		h=self.view_rect[3]
		self.image_scale=float(self.image_w)/float(image_w_zoom)
		#self.image_scale=1.0
		
		##scale images to fit view
		margin=20
		self.image_scale=min([float(self.qview.width()-margin)/(2.0*image_w_zoom),float(self.qview.height()-margin)/(2.0*image_h_zoom)])
		#get rid of scroll bars
		rcontent=self.qview.contentsRect()
		self.qview.setSceneRect(0,0,rcontent.width(),rcontent.height())
		
		# make dimensions good for Qt
		if self.image_w%2==1: #odd # columns causes problems - make even
			self.image_array_orig_t1=numpy.append(self.image_array_orig_t1,numpy.zeros((self.image_h,1),dtype='uint8'),1)
			self.image_w+=1

		# scale intensity
		if self.autoScaleCheckBox.isChecked():
			# auto 
			n=self.image_array_orig_t1.astype('float32')*255.0/numpy.max(self.image_array_orig_t1)
		else:
			# none
			n=numpy.clip(self.image_array_orig_t1.astype('float32')/2**12*255*brightness,0,255) #assuming 12 bit zvi
		
		self.image_array_orig_uint8_t1=n.astype('uint8') 

		# data for Qt QImage object
		#data=numpy.ndarray.flatten(self.image_array_orig_uint8_t1)		

		#data=numpy.ndarray.flatten(self.image_array_orig_uint8_t1)		
		data=numpy.ndarray.flatten(self.image_array_orig_uint8_t1[y:y+h,x:x+w])		

		#self.qimage_orig_t1=QtGui.QImage(data,self.image_w,self.image_h,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.qimage_orig_t1=QtGui.QImage(data,image_w_zoom,image_h_zoom,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.qimage_orig_t1.setColorTable(self.orig_colortables[self.current_channel])        
		self.qpixmap_orig_t1=QtGui.QPixmap.fromImage(self.qimage_orig_t1)
		self.mpixmap_orig_t1=MPixmap(self.qpixmap_orig_t1)
		self.mpixmap_orig_t1.init()
		self.mpixmap_orig_t1.setScale(self.image_scale)
		self.mpixmap_orig_t1.setFlag(QtGui.QGraphicsItem.GraphicsItemFlag.ItemIsFocusable)
		self.mpixmap_orig_t1.image_array=self.image_array_orig_uint8_t1 #for easy picking
		self.mpixmap_orig_t1.image_type='orig'

		self.qscene.addItem(self.mpixmap_orig_t1)
		self.mpixmap_orig_t1.setZValue(0)
		self.mpixmap_orig_t1.setFlag(QtGui.QGraphicsItem.ItemClipsToShape,True)
		self.mpixmaps.append(self.mpixmap_orig_t1)
		self.mpixmap_orig_t1.setPos(view_width/2-image_w_zoom*self.image_scale-margin/6,margin/3)
		#self.qscene.update()	

		###########################
		### orig at current_time + 1
		t=tifffile.TIFFfile(self.getFilename('orig',self.current_channel,self.current_time+1))
		
		self.image_array_orig_t2=t.asarray()	
		#sh=self.image_array_orig_t2.shape
		#self.image_w=sh[1]
		#self.image_h=sh[0]

		# make dimensions good for Qt
		if self.image_w%2==1: #odd # columns causes problems - make even
			self.image_array_orig_t2=numpy.append(self.image_array_orig_t2,numpy.zeros((self.image_h,1),dtype='uint8'),1)
			self.image_w+=1

		# scale intensity
		if self.autoScaleCheckBox.isChecked():
			# auto 
			n=self.image_array_orig_t2.astype('float32')*255.0/numpy.max(self.image_array_orig_t2)
		else:
			# none
			n=numpy.clip(self.image_array_orig_t2.astype('float32')/2**12*255*brightness,0,255) #assuming 12 bit zvi
		
		self.image_array_orig_uint8_t2=n.astype('uint8') 
			
		# data for Qt QImage object
		#data=numpy.ndarray.flatten(self.image_array_orig_uint8_t2)		
		data=numpy.ndarray.flatten(self.image_array_orig_uint8_t2[y:y+h,x:x+w])		

		#self.qimage_orig_t2=QtGui.QImage(data,self.image_w,self.image_h,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.qimage_orig_t2=QtGui.QImage(data,image_w_zoom,image_h_zoom,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.qimage_orig_t2.setColorTable(self.orig_colortables[self.current_channel])        
		self.qpixmap_orig_t2=QtGui.QPixmap.fromImage(self.qimage_orig_t2)
		self.mpixmap_orig_t2=MPixmap(self.qpixmap_orig_t2)
		self.mpixmap_orig_t2.init()
		self.mpixmap_orig_t2.setScale(self.image_scale)
		self.mpixmap_orig_t2.setFlag(QtGui.QGraphicsItem.GraphicsItemFlag.ItemIsFocusable)
		self.mpixmap_orig_t2.image_array=self.image_array_orig_uint8_t2 #for easy picking
		self.mpixmap_orig_t2.image_type='orig'

		self.qscene.addItem(self.mpixmap_orig_t2)
		self.mpixmap_orig_t2.setZValue(0)
		self.mpixmaps.append(self.mpixmap_orig_t2)		
		self.mpixmap_orig_t2.setPos(view_width/2+margin/6,margin/3)
		#self.qscene.update()	
		
		###########################
		### segmented at current_time
				
		fn=self.getFilename('segmented',self.channel_to_process,self.current_time)

		#fast checkbox not clicked, or segmented file doesn't exist, so segment
		if self.resegmentCheckBox.isChecked() or not os.path.exists(fn):	
		
			#self.img_orig=self.image_array_orig_t1
			t=tifffile.TIFFfile(self.getFilename('orig',self.channel_to_process,self.current_time))		
			self.img_orig=t.asarray()
			
			self.proc_thresh_max()		
			self.proc_watershed()
			self.proc_filter_size()
			#self.count_cells()	
					
			##save segmented as tif if it doesn't exist
			#if not os.path.exists(fn):
				#tifffile.imsave(self.getFilename('segmented',self.channel_to_process,self.current_time),self.img_watershed.astype('uint8'))
			tifffile.imsave(self.getFilename('segmented',self.channel_to_process,self.current_time),self.img_watershed_filtered.astype('uint8'))
			
			#clear frame of cells, as no longer referring to same ids
			Cell.clear(self.current_time)

		# display selected image
		selected_image=self.proc_image_combo.currentText()	
		
		if selected_image=='segmented':
			fn=self.getFilename('segmented',self.channel_to_process,self.current_time)
			if os.path.exists(fn):
				t=tifffile.TIFFfile(fn)	
				img_selected=t.asarray()	
			else:
				img_selected=numpy.zeros((self.image_h,self.image_w))
		else:
			img_selected=eval('self.'+selected_image)
			
		if selected_image not in ['img_watershed','img_watershed_filtered','segmented']:
			#scale to use full dynamic range
			img_selected=img_selected.astype('float32')*255.0/numpy.max(img_selected)  			

		# data for Qt QImage object
		img_selected=img_selected.astype('uint8') 
		#data=numpy.ndarray.flatten(img_selected)		
		data=numpy.ndarray.flatten(img_selected[y:y+h,x:x+w])		

		#self.qimage_seg_t1=QtGui.QImage(data,self.image_w,self.image_h,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.qimage_seg_t1=QtGui.QImage(data,image_w_zoom,image_h_zoom,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		
		#colortable
		if selected_image in ['img_watershed','img_watershed_filtered','segmented']:
			self.qimage_seg_t1.setColorTable(self.seg_colortable)
		else:
			self.qimage_seg_t1.setColorTable(self.proc_colortable)  
		self.qpixmap_seg_t1=QtGui.QPixmap.fromImage(self.qimage_seg_t1)
		self.mpixmap_seg_t1=MPixmap(self.qpixmap_seg_t1) 
		self.mpixmap_seg_t1.init()
		self.mpixmap_seg_t1.setScale(self.image_scale)
		self.mpixmap_seg_t1.setFlag(QtGui.QGraphicsItem.GraphicsItemFlag.ItemIsFocusable)
		self.mpixmap_seg_t1.image_array=img_selected #for easy picking
		self.mpixmap_seg_t1.image_type=selected_image

		self.qscene.addItem(self.mpixmap_seg_t1)
		self.mpixmap_seg_t1.setZValue(0)
		self.mpixmaps.append(self.mpixmap_seg_t1)		
		self.mpixmap_seg_t1.setPos(view_width/2-image_w_zoom*self.image_scale-margin/6,view_height/2+margin/3)
		
		###########################
		### segmented at current_time+1
		
		fn=self.getFilename('segmented',self.channel_to_process,self.current_time+1)

		#fast checkbox not clicked, or segmented file doesn't exist
		if self.resegmentCheckBox.isChecked() or not os.path.exists(fn):	
		
			#self.img_orig=self.image_array_orig_t2
			t=tifffile.TIFFfile(self.getFilename('orig',self.channel_to_process,self.current_time+1))		
			self.img_orig=t.asarray()
			
			self.proc_thresh_max()		
			self.proc_watershed()
			self.proc_filter_size()
			#self.count_cells()	
					
			##save segmented as tif if it doesn't exist
			#if not os.path.exists(fn):
				#tifffile.imsave(self.getFilename('segmented',self.channel_to_process,self.current_time+1),self.img_watershed.astype('uint8'))
			tifffile.imsave(self.getFilename('segmented',self.channel_to_process,self.current_time+1),self.img_watershed_filtered.astype('uint8'))
			
			#clear frame of cells, as no longer referring to same ids
			Cell.clear(self.current_time+1)
			
				
		# display selected image
		selected_image=self.proc_image_combo.currentText()	
		
		if selected_image=='segmented':
			fn=self.getFilename('segmented',self.channel_to_process,self.current_time+1)
			if os.path.exists(fn):
				t=tifffile.TIFFfile(fn)	
				img_selected=t.asarray()	
			else:
				img_selected=numpy.zeros((self.image_h,self.image_w))
		else:
			img_selected=eval('self.'+selected_image)
			
		if selected_image not in ['img_watershed','img_watershed_filtered','segmented']:
			#scale to use full dynamic range
			img_selected=img_selected.astype('float32')*255.0/numpy.max(img_selected)  			

		# data for Qt QImage object
		img_selected=img_selected.astype('uint8') 
		#data=numpy.ndarray.flatten(img_selected)		
		data=numpy.ndarray.flatten(img_selected[y:y+h,x:x+w])		

		#self.qimage_seg_t2=QtGui.QImage(data,self.image_w,self.image_h,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		self.qimage_seg_t2=QtGui.QImage(data,image_w_zoom,image_h_zoom,QtGui.QImage.Format_Indexed8) #data,width,height,format  
		
		#colortable
		if selected_image in ['img_watershed','img_watershed_filtered','segmented']:
			self.qimage_seg_t2.setColorTable(self.seg_colortable)
		else:
			self.qimage_seg_t2.setColorTable(self.proc_colortable)  		
		self.qpixmap_seg_t2=QtGui.QPixmap.fromImage(self.qimage_seg_t2)
		self.mpixmap_seg_t2=MPixmap(self.qpixmap_seg_t2) 
		self.mpixmap_seg_t2.init()
		self.mpixmap_seg_t2.setScale(self.image_scale)
		self.mpixmap_seg_t2.setFlag(QtGui.QGraphicsItem.GraphicsItemFlag.ItemIsFocusable)
		self.mpixmap_seg_t2.image_array=img_selected #for easy picking
		self.mpixmap_seg_t2.image_type=selected_image		
		
		self.qscene.addItem(self.mpixmap_seg_t2)
		self.mpixmap_seg_t2.setZValue(0)
		self.mpixmaps.append(self.mpixmap_seg_t2)		
		self.mpixmap_seg_t2.setPos(view_width/2+margin/6,view_height/2+margin/3)
		
		
		#####
		self.qscene.update()		
		#get rid of scroll bars
		#rcontent=self.qview.contentsRect()
		#self.qview.setSceneRect(0,0,rcontent.width(),rcontent.height())
		
		self.addGlyphs()
		self.addDivisions()
		
	def updateTimeLabel(self):
		self.time_label.setText('%d & %d of %d'%(self.current_time,self.current_time+1,self.sizeT-1))

	def updateTimeSlider(self):
		self.time_slider.setMinimum(0)
		self.time_slider.setMaximum(self.sizeT-2) # allowing for image t+1
		self.time_slider.blockSignals(True)
		self.time_slider.setValue(self.current_time)
		self.time_slider.blockSignals(False)
		

	def updateChannelSpinBox(self):
		self.channelViewSpinBox.setMinimum(0)
		self.channelViewSpinBox.setMaximum(self.sizeC-1)
		self.channelUseSpinBox.setMinimum(0)
		self.channelUseSpinBox.setMaximum(self.sizeC-1)

		self.current_channel=self.current_channel%self.sizeC	
		#self.channelSpinBox.blockSignals(True)
		self.channelViewSpinBox.setValue(self.current_channel)
		#self.channelSpinBox.blockSignals(False)

	def addGlyphs(self):		
		# current_time
		if self.current_time in Cell.lookup.keys():
			cells=Cell.lookup[self.current_time]
			for cell in cells.values():
				inside= cell.pos[0]>self.view_rect[0] and cell.pos[0]<self.view_rect[0]+self.view_rect[2]\
				    and cell.pos[1]>self.view_rect[1] and cell.pos[1]<self.view_rect[1]+self.view_rect[3]
				if not cell.glyph and inside: #only add if not already there
					g=MGlyph()
					g.init(cell,self.mpixmap_orig_t1)
			
		# current_time+1
		if self.current_time+1 in Cell.lookup.keys():	
			cells=Cell.lookup[self.current_time+1]
			for cell in cells.values():
				inside= cell.pos[0]>self.view_rect[0] and cell.pos[0]<self.view_rect[0]+self.view_rect[2]\
				    and cell.pos[1]>self.view_rect[1] and cell.pos[1]<self.view_rect[1]+self.view_rect[3]				
				if not cell.glyph and inside: #only add if not already there			
					g=MGlyph()
					g.init(cell,self.mpixmap_orig_t2)
			
	def addDivisions(self):
		if self.current_time+1 not in Cell.divisions:
			return
		
		divs=Cell.divisions[self.current_time+1]
		for d in divs:
			c0=d[0]
			c1=d[1]
			inside= c0.pos[0]>self.view_rect[0] and c0.pos[0]<self.view_rect[0]+self.view_rect[2]\
		        and c0.pos[1]>self.view_rect[1] and c0.pos[1]<self.view_rect[1]+self.view_rect[3]\
			    and c1.pos[0]>self.view_rect[0] and c1.pos[0]<self.view_rect[0]+self.view_rect[2]\
		        and c1.pos[1]>self.view_rect[1] and c1.pos[1]<self.view_rect[1]+self.view_rect[3]
			
			if inside:
				md=MDivision()
				md.init(c0,c1)
				

	def proc_thresh_max(self):
		#smooth
		sigma=1.0
		self.img_denoise=scipy.ndimage.filters.gaussian_filter(self.img_orig,(sigma,sigma))	
		#self.img_denoise=skimage.filter.denoise_bilateral(self.img_orig,sigma_range=0.5,sigma_spatial=15)
		#self.img_denoise=skimage.filter.denoise_tv_chambolle(self.img_orig,weight=0.1)#self.denoise_weight)

		#self.console('denoise done')

		#threshold
		##otsu
		#thresh_value=skimage.filter.threshold_otsu(self.img_denoise)
		#self.img_thresh=self.img_denoise>thresh_value
		#self.img_thresh=self.img_denoise*self.img_thresh

		##local - too slow for 3d
		#self.img_thresh=numpy.zeros(self.shape_img)
		##block_size=33
		#for z in range(self.shape_img[2]):
			##print z
			#self.img_thresh[:,:,z]=skimage.filter.threshold_adaptive(self.img_denoise[:,:,z],block_size=self.block_size,offset=-self.thresh_offset)
			
		self.img_thresh=skimage.filter.threshold_adaptive(self.img_denoise,block_size=self.block_size,offset=self.thresh_offset)
		#self.img_thresh=self.img_thresh.astype('uint8')
		
		#self.console('threshold done')

		###block_size=3+self.thresh*100
		##if self.block_size%2==0:
				##self.block_size+=1 #must be odd
		##block_size=20
		##image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)
		##image_processed=image_processed.astype('int')	

		#sc.scalar_data=image_processed	
		
		#remove small stuff
		skimage.morphology.remove_small_objects(self.img_thresh,min_size=self.filter_size,connectivity=1,in_place=True)

		#fill holes
		self.img_thresh=scipy.ndimage.morphology.binary_fill_holes(self.img_thresh)
		##image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,self.morph_iterations)
		##image_processed=scipy.ndimage.morphology.binary_closing(image_processed,structuring_element,self.morph_iterations)

		#label thresholded image
		try:
			self.img_labels,num_labels=scipy.ndimage.label(self.img_thresh,output='uint8')	
			print 'num_labels=',num_labels
		except:
			self.img_labels=numpy.zeros((self.image_h,self.image_w))
			print 'too many labels - lower offset'
			self.console('Too many labels - lower offset')
		#self.console('labels done')
		
		#distance to background from thresholded image
		self.img_distance=scipy.ndimage.distance_transform_edt(self.img_thresh)

		##smooth
		#sigma=3.0
		#img_denoise=scipy.ndimage.filters.gaussian_filter(self.img_thresh*100,(sigma,sigma))	
		#self.img_distance=scipy.ndimage.distance_transform_edt(img_denoise)
		

		#self.console('distance done')

		####distance=gaussian_filter(distance)			
		#self.img_distance = scipy.ndimage.gaussian_filter(self.img_distance,4)
		footprint=3
		#self.img_local_max = skimage.morphology.is_local_maximum(self.img_distance,self.img_thresh)#,numpy.ones((footprint,footprint,footprint)))
		#self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_thresh,indices=False)#footprint=numpy.ones((footprint,footprint,footprint)))

		#local max of distance within each labelled region
		#self.img_local_max=numpy.zeros(self.shape_img)	
		self.img_local_max = skimage.feature.peak_local_max(self.img_distance,labels=self.img_labels,indices=False,min_distance=10)#footprint=numpy.ones((footprint,footprint,footprint)))
		self.img_local_max=self.img_local_max.astype('uint8')
		#self.console('local_max done')

	def proc_watershed(self):
		#label maxima
		self.img_markers=scipy.ndimage.label(self.img_local_max,structure=numpy.ones((3,3)))[0]
		#watershed distance image with maxima as seeds
		self.img_watershed = skimage.morphology.watershed(-self.img_distance, self.img_markers, mask=self.img_thresh)
		#self.console('watershed done')	

	def proc_filter_size(self):
		#filter by size of object	
		
		self.img_watershed_filtered=skimage.morphology.remove_small_objects(self.img_watershed,min_size=self.filter_size,connectivity=1,in_place=False)
		
		#self.img_watershed_filtered=numpy.copy(self.img_watershed)

		#labels=numpy.unique(self.img_watershed)
			
		#for l in labels:
			#if l!=0:
				#region=numpy.where(self.img_watershed==l)
				#if len(region[0])<self.filter_size:
					##self.console('removing object',l)
					#self.img_watershed_filtered[region]=0						


	####SEGMENTATION EDITING
	# here, cells are labelled segmented regions of the seg images
	def pickingClear(self):
		items=self.qscene.items()
		for frame in [0,1]:
			for p,v in self.clicked_points[frame]:
				if p in items:
					self.qscene.removeItem(p)
			
		self.clicked_points=[[],[]]
		
		if self.selected_glyph:
			self.selected_glyph.deselect()
			self.selected_glyph=None
		
	def mergeCells(self):
		for frame in [0,1]:
			u=self.project_dir+'/seg_undo_%d.tif'%frame
			if os.path.exists(u):
				os.remove(u)	
			if len(self.clicked_points[frame])>0:
				#read seg
				fn=self.getFilename('segmented',self.channel_to_process,self.current_time+frame)
				if os.path.exists(fn):
					t=tifffile.TIFFfile(fn)	
					img_seg=t.asarray()	
				else:
					print 'No segmented image error'
					return
				
				#save copy for undo
				shutil.copyfile(fn,self.project_dir+'/seg_undo_%d.tif'%frame)
				
				#change seg
				val0=self.clicked_points[frame][0][1]
				for point,val in self.clicked_points[frame]:
					w=numpy.where(img_seg==val)
					img_seg[w]=val0		
					
				#save seg
				tifffile.imsave(fn,img_seg.astype('uint8'))		
		
		self.updateImages()
		self.last_edited_time=self.current_time	
		self.clicked_points=[[],[]]
	
	def splitCell(self):
		for frame in [0,1]:	
			u=self.project_dir+'/seg_undo_%d.tif'%frame
			if os.path.exists(u):
				os.remove(u)	
				
			if len(self.clicked_points[frame])>0:			
				#read seg
				fn=self.getFilename('segmented',self.channel_to_process,self.current_time+frame)
				if os.path.exists(fn):
					t=tifffile.TIFFfile(fn)	
					img_seg=t.asarray()	
				else:
					print 'No segmented image error'
					return
				
				#save copy for undo
				shutil.copyfile(fn,self.project_dir+'/seg_undo_%d.tif'%frame)
				
				#change seg
				val=self.clicked_points[frame][0][1]
				w=numpy.where(img_seg==val) #find all pixels having same value as that of first clicked
				ws=numpy.vstack(w) #put into array of (x,y) coords
				ws=numpy.swapaxes(ws,1,0) 							
				#c=numpy.array([(p[0].pos().y(),p[0].pos().x()) for p in self.clicked_points[frame]]) #posns of clicked points
				c=numpy.array([(p[0].pos().y()+self.view_rect[1],p[0].pos().x()+self.view_rect[0]) for p in self.clicked_points[frame]]) #posns of clicked points
				dist=scipy.spatial.distance.cdist(ws,c) #distances from each pixel to each clicked point 
		
				labels=numpy.unique(img_seg)
				max_label=labels[-1] #get max of labels already used
					
				for i in range(dist.shape[0]):#for each pixel
					p=ws[i] #point we are examining
					min_index=numpy.argmin(dist[i])#index of closest selection centre					
					img_seg[tuple(p)]=max_label+min_index+1 #new label
					#print min_index,img_seg[tuple(p)]
				
				#save seg
				tifffile.imsave(fn,img_seg.astype('uint8'))		
			
		self.updateImages()
		self.last_edited_time=self.current_time	
		self.clicked_points=[[],[]]
		
	def deleteCells(self):
		for frame in [0,1]:
			u=self.project_dir+'/seg_undo_%d.tif'%frame
			if os.path.exists(u):
				os.remove(u)	
				
			if len(self.clicked_points[frame])>0:		
				#read seg
				fn=self.getFilename('segmented',self.channel_to_process,self.current_time+frame)
				if os.path.exists(fn):
					t=tifffile.TIFFfile(fn)	
					img_seg=t.asarray()	
				else:
					print 'No segmented image error'
					return
				
				#save copy for undo
				shutil.copyfile(fn,self.project_dir+'/seg_undo_%d.tif'%frame)
				
				#change seg
				for p in self.clicked_points[frame]:
					val=p[1]
					w=numpy.where(img_seg==val)
					img_seg[w]=0		
					
				#save seg
				tifffile.imsave(fn,img_seg.astype('uint8'))		
		
		self.updateImages()
		self.last_edited_time=self.current_time	
		self.clicked_points=[[],[]]
		
	def undoCells(self):
		for frame in [0,1]:
			undo_file=self.project_dir+'/seg_undo_%d.tif'%frame
			if os.path.exists(undo_file) and self.last_edited_time is not None:
				shutil.copyfile(undo_file,self.getFilename('segmented',self.channel_to_process,self.last_edited_time+frame))	
				
		self.updateImages()
		self.last_edited_time=None
		self.clicked_points=[[],[]]
	
	def markCells(self):
		#can only mark in LH frame
		for p in self.clicked_points[0]:
			val=p[1]
			# cannot remark a seg_id
			if not Cell.get(self.current_time,val):
				cell=Cell(self.current_time,val)	
			
		self.addGlyphs()
		self.pickingClear()
			
	####GLYPH/PREDICTION EDITING
	# here cells are the Cell/Glyph pairs added by marking & BOT
	def editCells(self,from_seg,to_seg):
		#reassign cells 
					
		previous_cell=Cell.get(self.current_time,from_seg)
		if previous_cell:
			#remove cell at t+1 with same track_id
			found=None
			if self.current_time+1 in Cell.lookup:
				for c in Cell.lookup[self.current_time+1].values():
					if c.track_id==previous_cell.track_id:
						found=c
						break
				
			if found:
				self.deleteCell(found)
			
			#check if to_seg already assigned to a cell
			to_cell=Cell.get(self.current_time+1,to_seg)
			if to_cell:
				self.deleteCell(to_cell)
						
			next_cell=Cell(self.current_time+1,to_seg,previous_cell.track_id)	
			#next_cell.previous.append(previous_cell)
			#previous_cell.next.append(next_cell)	
			next_cell.previous=previous_cell
			previous_cell.next=next_cell	
			
			g=MGlyph()
			g.init(next_cell,self.mpixmap_orig_t2)
			
	def deleteCell(self,cell):
		cell.destroy()
		del cell
		
	def addDivision(self):
		#add division based on selected glyphs
		c1=Cell.get(self.current_time+1,self.selected_glyphs_div.pop())
		c2=Cell.get(self.current_time+1,self.selected_glyphs_div.pop())
		Cell.divisions[self.current_time+1].append([c1,c2])
		self.addDivisions()
		self.selected_glyph.deselect()
		self.selected_glyphs_div=set()

	def deleteDivision(self,div):
		view.qscene.removeItem(div)
		
		if self.current_time+1 in Cell.divisions:
			divs=Cell.divisions[self.current_time+1]
			for d in divs:
				if d[0]==div.c0 or d[1]==div.c0 or d[0]==div.c1 or d[1]==div.c1:
					divs.remove(d)
					break
		
			
	def test(self):
		#self.cleanDirs(['segmented'])
		#self.openProject('/Users/jpm/Progs/AMATools/Tracker2/140409_CS_RGB2-2_TL_Untitled_1_MMStack_15nMPDO3_A1.ome')
		#self.console('Ready.')
		self.progress_start()
		
	def reset(self):
		self.cleanDirs(['segmented','prediction','raw','seg'])
		Cell.lookup={}
		Cell.track_id_count=0
		Cell.divisions={}
		self.updateImages()
		
if __name__ == "__main__":
	try:
		app=QtGui.QApplication(sys.argv)
		view=View()
		sys.exit(app.exec_())		
	except RuntimeError:
		app=QtCore.QCoreApplication.instance()
		view=View()
		

