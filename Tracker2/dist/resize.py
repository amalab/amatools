import glob
import tifffile

# resize and/or decimate tiff files
project_dir='/Users/jpm/Progs/AMATools/Tracker2/140508_RGB2-9_immuno_2_MMStack_ES-LIF_4.ome'
decimate=2

tifs=glob.glob(project_dir+'/orig/*.tif')
for tif in tifs:
	print tif
	t=tifffile.TIFFfile(tif)		
	img=t.asarray()
	h,w=img.shape
	img=img[::decimate,::decimate]				
	#img=img[h/4:3*h/4,w/4:3*w/4]				
	tifffile.imsave(tif,img)
