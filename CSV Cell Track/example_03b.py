#
# amatools example 3
#
# open and plot MTrackJ data
#
# jpm mar 2013
#

import amatools

import os
#get current working directory
cwd=os.path.dirname(__file__)+'/'

fn='/Users/jpm/DATA/David/moverstest.csv'

ct=amatools.AMACellTracks()
ct.read_mtrackj_csv(fn)
ct.plot()

pass