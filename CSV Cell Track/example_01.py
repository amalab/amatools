#
# amatools example 1
#
# open multidimensional image file
#
# convert zvi to tiff
# read tiff into numpy
# get metadata
# plot overview
#
# jpm mar 2013
#

import amatools
import numpy
import os

#get current working directory
cwd=os.path.dirname(__file__)+'/'
filename=cwd+'2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_09'

if not os.path.exists(filename+'.ome.tiff'):
	amatools.input.zvi_2_ometiff(filename)
	print 'Converted\n'
else:
	print 'Already exists'

amatiff=amatools.AMATiff()
amatiff.open(filename+'.ome.tiff')
amatiff.info()
amatiff.plot_overview()