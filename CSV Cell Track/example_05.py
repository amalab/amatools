#
# amatools example 5
#
# displacement histogram and persistence
#
# jpm mar 2013
#

import amatools

fn='2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_01.csv'

ct=amatools.AMACellTracks()
ct.read_mtrackj_csv(fn)
ct.compute_tracks()

ct.plot_tracks()
#ct.plot_msd()
ct.plot_displacment()
ct.plot_persistence()

ct.show_plots()

pass