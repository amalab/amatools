#
# amatools example 3
#
# open and plot MTrackJ data
#
# jpm mar 2013
#

import amatools

fn='/Volumes/home/dat40/Students/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format/2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_01.csv'

ct=amatools.AMACellTracks()
ct.read_mtrackj_csv(fn)
ct.compute_tracks()
ct.plot()
ct.plot_msd()
pass