#
# amatools example 5
#
# displacement histogram and persistence
#
# jpm mar 2013
#

import amatools

ct=amatools.AMACellTracks()

fn='2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_01.csv'
ct.read_mtrackj_csv(fn)

#csv_directory='/Volumes/home/dat40/Students/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format/AC-TNULL'
#ct.read_mtrackj_csv_dir(csv_directory)

ct.compute_tracks()
ct.filter_by_track_size(50)
ct.plot_tracks()
ct.plot_track_lengths()
#ct.plot_msd()
ct.plot_displacment()
ct.plot_persistence()

ct.show_plots()

pass