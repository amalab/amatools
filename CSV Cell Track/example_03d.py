#
# amatools example 3
#
# open and plot MTrackJ data
#
# jpm mar 2013
#

import amatools

csv_directory='/Volumes/home/dat40/Students/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format'

ct=amatools.AMACellTracks()
ct.read_mtrackj_csv_dir(csv_directory)
ct.compute_tracks()
ct.plot()
ct.plot_msd()
pass