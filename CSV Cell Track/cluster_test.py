#data  = [[0.1,0.1,0.1],
        #[0.1,0.1,0.1],
        #[0.1,0.1,0.1],
        #[0.2,0.2,0.2],
        #[0.2,0.2,0.2],
        #[0.2,0.2,0.2],
        #[0.3,0.3,0.3],
        #[0.3,0.3,0.3],
        #[0.3,0.3,0.3],]

import numpy 

data=[]

for i in range(10):
    t=numpy.random.normal(0.5,0.1,100)
    h=numpy.histogram(t,range=(0.0,1.0))[0]
    data.append(h)
for i in range(10):
    t=numpy.random.normal(0.3,0.3,100)
    h=numpy.histogram(t,range=(0.0,1.0))[0]
    data.append(h)
for i in range(10):
    t=numpy.random.normal(0.7,0.1,100)
    h=numpy.histogram(t,range=(0.0,1.0))[0]
    data.append(h)
    
from scipy import spatial
distance = spatial.distance.pdist(data)

import scipy.cluster.hierarchy as cluster
linkage = cluster.linkage(distance,method="complete")
cluster.dendrogram(linkage)#,color_threshold=60)#0.7*max(linkage[:,2]))

from matplotlib.pyplot import show
show(block=False)
pass