#
# amatools example 7a
#
# displacement histogram and persistence
# Ellie's data - AC-TGFP
#
# jpm mar 2013
#

import amatools

ct=amatools.AMACellTracks()

#fn='2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_01.csv'
#ct.read_mtrackj_csv(fn)

###MUST BE LOGGED ON TO AMASERV VIA HOME
csv_directory='/Volumes/home/dat40/Students/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format/AC-TGFP'
#csv_directory='/Volumes/home/dat40/Students/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format/AC-TNULL'
ct.read_mtrackj_csv_dir(csv_directory)

#d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27/raw data'
#d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27+Act+Chi 1/raw data'
#d='/Users/jpm/DATA/David/Daniel/DATA october/N2B27 + Act + Chi 2/raw data'
#ct.read_cge_csvs(d)

ct.filter_by_track_size(10)
ct.compute_tracks()
#ct.plot_tracks()
ct.plot_movement_distributions()
#ct.plot_track_lengths()
#ct.plot_msd()
#ct.plot_displacment()
#ct.plot_persistence()

ct.show_plots()

pass