#
# amatools example 3
#
# open and plot MTrackJ data
#
# jpm mar 2013
#

import amatools

import os
#get current working directory
cwd=os.path.dirname(__file__)+'/'

fn=cwd+'2012-12-03 A,C,AC TGFP,TNull3_AC T-NULL_01.csv'

ct=amatools.AMACellTracks()
ct.read_mtrackj_csv(fn)
ct.plot()

pass