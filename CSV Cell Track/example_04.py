#
# amatools example 4
#
# open and plot MTrackJ data from directory
#
# jpm mar 2013
#

import amatools
import glob

csv_directory='/Users/jpm/DATA/David/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format'
csv_file_names=glob.glob(csv_directory+'/*.csv')

for fn in csv_file_names:
	ct=amatools.AMACellTracks()
	ct.read_mtrackj_csv(fn)
	ct.plot()

pass