import tifffile #loads into numpy memory object
from pyslic.segmentation import roysam

t=tifffile.TIFFfile('/Users/jpm/Progs/AMATools/Tracker2/140506_RGB2-9__2_MMStack_PDO3to-0uM_3.ome/orig/orig_C1_T174.tif')
image_array=t.asarray()					
sh=image_array.shape
image_w=sh[1]
image_h=sh[0]

w,wl=roysam.roysam_watershed(image_array,blur_factor=1)

tifffile.imsave('watershed.tif',w)

pass