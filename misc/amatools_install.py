''' 
install amatools
copy from amaserv to local canopy site-packages

amaserv must be mounted as 'home' on osx

jpm mar 2013
'''

import os
import shutil

dest_path=os.path.expanduser('~/Library/Enthought/Canopy_32bit/User/lib/python2.7/site-packages/')
src_path=os.path.expanduser('/Volumes/home/lab/Python Teaching/site-packages/')
package_list=['amatools','bs4','libtiff','skimage']

try:
	for p in package_list:
		shutil.rmtree(dest_path+p,ignore_errors=True)
		shutil.copytree(src_path+p,dest_path+p)
		print p,'installed.'
	print 'Amatools installed OK'
except:
	print "Error. Are you sure you have amaserv mounted as 'home'?"

