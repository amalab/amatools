''' 
deploy amatools
copy from local canopy site-packages _to_ amaserv

amaserv must be mounted as 'home' on osx

jpm mar 2013
'''


import os
import shutil

src_path=os.path.expanduser('~/Library/Enthought/Canopy_32bit/User/lib/python2.7/site-packages/')
dest_path=os.path.expanduser('/Volumes/home/lab/Python Teaching/site-packages/')
package_list=['amatools','bs4','libtiff','skimage']

try:
	for p in package_list:
		shutil.rmtree(dest_path+p,ignore_errors=True)
		shutil.copytree(src_path+p,dest_path+p)
		print p,'deployed.'
	print 'Amatools deployed OK'
	
except:
	print "Error."
