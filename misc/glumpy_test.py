import numpy, glumpy

fig = glumpy.figure( (1024,1024) )


@fig.event
def on_draw():
    fig.clear()
    Z = numpy.random.random((1024,1024)).astype(numpy.float32)
    image = glumpy.image.Image(Z)
    image.update()
    image.draw( x=0, y=0, z=0, width=fig.width, height=fig.height )

glumpy.show()