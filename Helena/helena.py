import fnmatch
import os
import libtiff
import tifffile

path='/Users/jpm/Progs/AMATools/Helena'

#find all xls files
xls_files=[]
for root, dirnames, filenames in os.walk(path):
  for filename in fnmatch.filter(filenames, '*coordinates file.xls'):
      xls_files.append(os.path.join(root, filename))
      
for xls_file in xls_files:
  print xls_file
  
  #get corresponding tif_file
  tif_file=xls_file.replace('coordinates file.xls','marked.tif')

  #read tiff
  tif=tifffile.TIFFfile(tif_file)		
  orig=tif.asarray()
  print orig.shape
  
  if len(orig.shape)==4:
    img=(orig[:,0,:,:]+orig[:,1,:,:]+orig[:,2,:,:])/256
  else:
    img=orig
    
  numch=img.shape[0]
  
  #read xls fiji selection
  #ppum=12.7965 #pixels per µm
  #pixels per um from tiff metadata
  ppum=tif.pages[0].x_resolution[0]/1000000.0
  print ppum
  
  xls=open(xls_file,'r')
  lines=xls.readlines()
  line0=lines[0]
  line0=line0.replace('\r\n','')
  
  #results
  measure_file=xls_file.replace('coordinates file','measurements')
  measure=open(measure_file,'w')
  s=''
  for c in range(numch):
    s+=('\t'+'Measurement'+str(c+1))
  measure.write(line0+s+'\n')

  for l in lines[1:]:
    l=l.replace('\r\n','')
    v=l.split('\t')
    #print v
    xum=float(v[5])
    yum=float(v[6])
    xpix=int(xum*ppum)
    ypix=int(yum*ppum)
    
    r=8
    mm=img[:,ypix-r:ypix+r,xpix-r:xpix+r] #measurements 
    m=[a.mean() for a in mm]
    print m
    for x in m:
      l+='\t'+str(x)
    l+='\n'
    measure.write(l)
      
  measure.close()
    
pass