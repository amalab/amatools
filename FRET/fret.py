import os
import tifffile
import libtiff
import matplotlib.pyplot as pyplot

# wrap bioformats command line conversion tool
def convert_2_ometiff(filename):
	"""convert any type to .ome.tiff"""

	root=os.path.splitext(filename)[0]

	print 'Running bfconvert...'	
	r=os.system('../bftools/bfconvert "%s" "%s.ome.tiff"'%(filename,root))
	print 'bfconvert returned',r


###MAIN###	

# open .nd2 in fiji
# choose concatenate option
# save as .ome.tif

f='150227 EKAREV test1'
#convert_2_ometiff(f+'.nd2')

tif=tifffile.TIFFfile(f+'.fiji.ome.tif')
img=tif.asarray()

#tif=libtiff.TIFFfile(f+'.ome.tif')
#img=tif.get_tiff_array()

sh=img.shape	
print sh

numchannels=2
numpositions=12

ratio=(img[0:-2:2,:,:].astype('float')/img[1:-1:2,:,:].astype('float'))
m=ratio.max()
ratio=(ratio*255/m).astype('uint8')

for p in range(numpositions):
	print 'position:',p
	tifffile.imsave(f+'_ratio_pos_%s.tif'%(p+1), ratio[p*numpositions:(p+1)*numpositions,:,:])

pass

#ratios=[]
#for pos in range(numpositions):
	#v=[]
	#for ch in range(numchannels):
		#im=img[pos*numchannels*numpositions+ch,:,:]
		#s=im.sum()
		#v.append(s)
		#print pos,ch,s
	#ratios.append(float(v[1])/float(v[0]))

#print ratios

#pyplot.plot(ratios)
#pyplot.show()
#pass

