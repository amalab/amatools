#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vispy: gallery 2
"""
Example demonstrating a 3D Texture

"""

import numpy as np

from vispy.util.transforms import ortho
from vispy import gloo
from vispy import app
from vispy.gloo import gl

import OpenGL.GL as glext

from vispy.util import logger,use_log_level

use_log_level('debug')


# Image to be displayed
S=64
W, H, D = S,S,S
#I = np.random.uniform(0, 1, (W, H, D)).astype(np.float32)

#gradient
I=np.linspace(0.0,1.0,S).astype(np.float32)
I=np.tile(I,(S,S,1))

# A simple texture quad
data = np.zeros(4, dtype=[('a_position', np.float32, 2),
                          ('a_texcoord', np.float32, 2)])
data['a_position'] = np.array([[0, 0], [W, 0], [0, H], [W, H]])
data['a_texcoord'] = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])


VERT_SHADER = """
// Uniforms
uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform float u_antialias;

// Attributes
attribute vec2 a_position;
attribute vec2 a_texcoord;

// Varyings
varying vec2 v_texcoord;

// Main
void main (void)
{
    v_texcoord = a_texcoord;
    gl_Position = u_projection * u_view * u_model * vec4(a_position,0.0,1.0);
}
"""

FRAG_SHADER = """
uniform sampler3D u_texture;
uniform float i;
varying vec2 v_texcoord;
void main()
{
// step through gradient with i
    gl_FragColor = texture3D(u_texture, vec3(i,v_texcoord));
    gl_FragColor.a = 1.0;
}

"""

############

VERT_RAY="""
// for raycasting
//#version 400

layout(location = 0) in vec3 VerPos;
layout(location = 1) in vec3 VerClr;

out vec3 Color;

uniform mat4 MVP;


void main()
{
    Color = VerClr;
    gl_Position = MVP * vec4(VerPos, 1.0);
}
"""

FRAG_RAY="""
// for raycasting
//#version 400

in vec3 Color;
//layout (location = 0) 
out vec4 FragColor;

void main()
{
    FragColor = vec4(Color, 1.0);
}
"""

class Canvas(app.Canvas):

    def __init__(self):
        app.Canvas.__init__(self, close_keys='escape')
        self.size = W * 5, H * 5

        self.program = gloo.Program(VERT_SHADER, FRAG_RAY)
        self.texture = gloo.Texture3D(I)
        self.texture.interpolation = gl.GL_LINEAR

        #self.program['u_texture'] = self.texture
        #self.program['i']=0.0
        self.program.bind(gloo.VertexBuffer(data))

        self.view = np.eye(4, dtype=np.float32)
        self.model = np.eye(4, dtype=np.float32)
        self.projection = np.eye(4, dtype=np.float32)

        self.program['u_model'] = self.model
        self.program['u_view'] = self.view
        self.projection = ortho(0, W, 0, H, -1, 1)
        self.program['u_projection'] = self.projection
        
        self.i=0;

    def on_initialize(self, event):
        gl.glClearColor(1, 1, 1, 1)

    def on_resize(self, event):
        width, height = event.size
        gl.glViewport(0, 0, width, height)
        self.projection = ortho(0, width, 0, height, -100, 100)
        self.program['u_projection'] = self.projection

        # Compute thje new size of the quad
        r = width / float(height)
        R = W / float(H)
        if r < R:
            w, h = width, width / R
            x, y = 0, int((height - h) / 2)
        else:
            w, h = height * R, height
            x, y = int((width - w) / 2), 0
        data['a_position'] = np.array(
            [[x, y], [x + w, y], [x, y + h], [x + w, y + h]])
        self.program.bind(gloo.VertexBuffer(data))

    def on_draw(self, event):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        #I[...] = np.random.uniform(0, 1, (W, H, D)).astype(np.float32)
        #self.texture.set_data(I)
        #self.program['i']=self.i
        self.program.draw(gl.GL_TRIANGLE_STRIP)
        self.update()
        
        self.i=(self.i+0.01)%1.0
        #gl.check_error()
        
    def display():
        pass
        #glEnable(GL_DEPTH_TEST);
        #// test the gl_error
        #GL_ERROR();
        #// render to texture
        #glBindFramebuffer(GL_DRAW_FRAMEBUFFER, g_frameBuffer);
        #glViewport(0, 0, g_winWidth, g_winHeight);
        #linkShader(g_programHandle, g_bfVertHandle, g_bfFragHandle);
        #glUseProgram(g_programHandle);
        #// cull front face
        render(gl.GL_FRONT);
        #glUseProgram(0);
        #GL_ERROR();
        #glBindFramebuffer(GL_FRAMEBUFFER, 0);
        #glViewport(0, 0, g_winWidth, g_winHeight);
        #linkShader(g_programHandle, g_rcVertHandle, g_rcFragHandle);
        #GL_ERROR();
        #glUseProgram(g_programHandle);
        #rcSetUinforms();
        #GL_ERROR();
        #// glUseProgram(g_programHandle);
        #// cull back face
        #render(GL_BACK);
        #// need or need not to resume the state of only one active texture unit?
        #// glActiveTexture(GL_TEXTURE1);
        #// glBindTexture(GL_TEXTURE_2D, 0);
        #// glDisable(GL_TEXTURE_2D);
        #// glActiveTexture(GL_TEXTURE2);
        #// glBindTexture(GL_TEXTURE_3D, 0);    
        #// glDisable(GL_TEXTURE_3D);
        #// glActiveTexture(GL_TEXTURE0);
        #// glBindTexture(GL_TEXTURE_1D, 0);    
        #// glDisable(GL_TEXTURE_1D);
        #// glActiveTexture(GL_TEXTURE0);
        #glUseProgram(0);
        #GL_ERROR(); 
        
        #// // for test the first pass
        #// glBindFramebuffer(GL_READ_FRAMEBUFFER, g_frameBuffer);
        #// checkFramebufferStatus();
        #// glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        #// glViewport(0, 0, g_winWidth, g_winHeight);
        #// glClearColor(0.0, 0.0, 1.0, 1.0);
        #// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        #// GL_ERROR();
        #// glBlitFramebuffer(0, 0, g_winWidth, g_winHeight,0, 0,
        #// 		      g_winWidth, g_winHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
        #// glBindFramebuffer(GL_FRAMEBUFFER, 0);
        #// GL_ERROR();
        #glutSwapBuffers();


    def render(cullFace):
        pass
        #// both of the two pass use the "render() function"
        #// the first pass render the backface of the boundbox
        #// the second pass render the frontface of the boundbox
        #// together with the frontface, use the backface as a 2D texture in the second pass
        #// to calculate the entry point and the exit point of the ray in and out the box.

        #GL_ERROR();
        #glClearColor(0.2f,0.2f,0.2f,1.0f);
        #glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        #//  transform the box
        #glm::mat4 projection = glm::perspective(60.0f, (GLfloat)g_winWidth/g_winHeight, 0.1f, 400.f);
        #glm::mat4 view = glm::lookAt(glm::vec3(0.0f, 0.0f, 2.0f),
                         #glm::vec3(0.0f, 0.0f, 0.0f), 
                         #glm::vec3(0.0f, 1.0f, 0.0f));
        #glm::mat4 model = mat4(1.0f);
        #model *= glm::rotate((float)g_angle, glm::vec3(0.0f, 1.0f, 0.0f));
        #// to make the "head256.raw" i.e. the volume data stand up.
        #model *= glm::rotate(90.0f, vec3(1.0f, 0.0f, 0.0f));
        #model *= glm::translate(glm::vec3(-0.5f, -0.5f, -0.5f)); 
        #// notice the multiplication order: reverse order of transform
        #glm::mat4 mvp = projection * view * model;
        #GLuint mvpIdx = glGetUniformLocation(g_programHandle, "MVP");
        #if (mvpIdx >= 0)
        #{
            #glUniformMatrix4fv(mvpIdx, 1, GL_FALSE, &mvp[0][0]);
        #}
        #else
        #{
            #cerr << "can't get the MVP" << endl;
        #}
        #GL_ERROR();
        #drawBox(cullFace);
        #GL_ERROR();
        #// glutWireTeapot(0.5);       

    def drawBox(glFaces):
        pass
        #glEnable(GL_CULL_FACE);
        #glCullFace(glFaces);
        #glBindVertexArray(g_vao);
        #glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLuint *)NULL);
        #glDisable(GL_CULL_FACE);

if __name__ == '__main__':
    c = Canvas()
    c.show()
    app.run()
