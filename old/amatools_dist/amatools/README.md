# amatools module for Canopy 32 bit on OSX

## jonathan mackenzie
## 2012-13  

---
###to install:

1. make sure Canopy is up to date

2. make sure you have the following packages installed in Canopy:

	- mayavi
	- bs4	

2. download the zip file of this repository 
unzip
in Canopy console type

	`%run 'path to unzipped folder/amatools_install.py'`

	this will copy the amatools	 module and dependent modules to your local site-packages directory