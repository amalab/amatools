# coding=utf-8
# for unicode comments

'''
cell track data structures

jpm
mar 2013
'''

import numpy
import math
import glob
import os
import matplotlib
import matplotlib.pyplot as pyplot
import scipy.sparse
import scipy.spatial
import scipy.cluster.hierarchy

import math


class AMACell:
	'''Position & velocity of a cell at one time.'''
	def __init__(self):
		self.id=0
		
		self.x=0 # x pos in μm
		self.y=0 # y pos in μm 
		self.t=0 # time in mins
		self.ex=0 # expression in ? (Express)
		self.v=numpy.array((0.0,0.0)) # μm per min
		
	def v_mag(self):
		'''return velocity magnitude'''
		return numpy.linalg.norm(self.v)
	
	def v_norm(self):
		'''return velocity normalised'''
		if self.v_mag()>0.0:
			return self.v/self.v_mag()
		else:
			return self.v
	
	def pos(self):
		'''return (x,y)'''
		return numpy.array((self.x,self.y))
		
		
class AMATrack:
	'''Track of cells over time'''
	def __init__(self):
		self.cells=[]
		self.lineage='' #mother or daughter
		self.duration=0.0
		self.computed=False
		self.d=[]
		self.dx=[]
		self.dy=[]

	def calc_duration(self):
		self.t_begin=self.cells[0].t
		self.t_end=self.cells[-1].t
		self.duration=self.t_end-self.t_begin
		return self.duration

	def compute(self):
		if len(self.cells)<2:
			return
	
		self.computed=True	
		
		self.calc_duration()
				
		self.ex=[c.ex for c in self.cells]
		self.ti=[c.t for c in self.cells]
		self.x=[c.x for c in self.cells]
		self.y=[c.y for c in self.cells]

		#ave expression
		self.ex_mean=numpy.mean(self.ex)

		self.dx=[]
		self.dy=[]
		self.d=[] #magnitude
		for i in range(0,len(self.x)-1):
			dx=self.x[i+1]-self.x[i]
			dy=self.y[i+1]-self.y[i]
			#if abs(dx)>0.0 and abs(dy)>0.0: #only register if both nonzero, or get quirks in histogram TO DO
			self.dx.append(dx)
			self.dy.append(dy)
			self.d.append(math.sqrt(dx*dx+dy*dy))
			#self.v.append((self.dx,self.dy))
			self.cells[i].v=numpy.array((dx,dy))
			
		self.angles=[math.atan2(y,x) for x,y in zip(self.dx,self.dy)]	

		self.dx_mean=numpy.mean(self.dx)
		self.dy_mean=numpy.mean(self.dy)
		
		#print 'Displacement min, max:',min(self.d),max(self.d)
		
		self.d_hist=numpy.histogram(self.d,range=(0.0,25.0))

		#overall movement
		if(len(self.x)>0):
			self.dx_overall=self.x[-1]-self.x[0]
			self.dy_overall=self.y[-1]-self.y[0]
			self.d_overall=math.sqrt(self.dx_overall*self.dx_overall+self.dy_overall*self.dy_overall)
	
			#start pos
			self.x0=self.x[0]
			self.y0=self.y[0]  
			
		#persistence
		dot_acc=0.0
		self.v_norm=[c.v_norm() for c in self.cells]
		for i in range(len(self.v_norm)-1):
			j=i+1
			dot=numpy.dot(self.v_norm[i],self.v_norm[j])
			dot_acc+=dot
		
		if len(self.v_norm)>1:	
			self.persistence=dot_acc/(len(self.v_norm)-1)
		else:
			self.persistence=0.0
				

	def filter(self,thresh):
		# if v above thresh*std dev, return track split at that point

		flag=False
		for i in range(0,len(self.cells)-1):
			c=self.cells[i]
			#if abs(c.v_mag()-v_mean)>thresh*v_std:
			if abs(c.v_mag())>thresh:
				r=self.cells[i+1:]
				self.cells=self.cells[0:i]
				self.compute()
				return r		
				
		return None	
	

class AMACellTracks:
	'''Collection of tracks'''
	def  __init__(self):
		self.plot_name=''	
		self.tracks=[]
		self.cells=[] #all cells for time_frame lookup by int id
		self.csv_directory=''
		self.cell_count=0
		self.track_count=0
		
	def read_cge_csvs(self,directory):
		'''Read collection of tracks from directory of Daniel's .csv files
		generated by imageJ Circadian Gene Expression plugin.'''
		micrometres_per_pixel=0.645 #to convert from pixels to μm
		self.csv_directory=directory
		self.plot_name=self.csv_directory.split('/')[-2]
		
		csv_file_names=glob.glob(self.csv_directory+'/*.csv')
	   
		track_count=0 # number of tracks/csv files
		cell_count=0 #total cells over all tracks
	
		# each csv file is a track
		# each line is a cell position   
		for csv_filename in csv_file_names:
			track=AMATrack()
			track.id=track_count
	
			##for Khushboo's data
			#csv_basename=os.path.basename(csv_filename)
			#if csv_basename[1:].find('m')>0 or csv_basename[1:].find('M')>0:
				#track.lineage='mother'
			#else:
				#track.lineage='daughter'
	
			csv_file=open(csv_filename,'rU')
			lines=csv_file.readlines()    
	
			line_count=0
			hash_found=False
			for line in lines:
				line_data_str=line.split(',')[0:-1] #strings
				if line[0]=='#': # find line starting # to get beginning of data
					hash_found=True
					self.column_headings=line.split(',')
					continue
	
				if hash_found:
					line_data_str2=['0' if len(x)==0 else x for x in line_data_str] # zero fill empty fields
					line_data=[] # floats
					for x in line_data_str2:
						line_data.append(float(x))
					#line_data=[map(float,x) for x in line_data_str2]
					#data[track_count].append(line_data)
					cell=AMACell()
					cell.id=cell_count
					cell.x=line_data[self.find_index_for_column('XC [px]')]*micrometres_per_pixel
					cell.y=line_data[self.find_index_for_column('YC [px]')]*micrometres_per_pixel
					cell.t=line_data[self.find_index_for_column('Time [min]')]
					cell.ex=line_data[self.find_index_for_column('Mean1')]
					cell.track=track
	
					if cell.x>0 or cell.y>0:   
						track.cells.append(cell) 
						self.cells.append(cell)
						cell_count+=1
	
				line_count+=1
	
			track.compute()
			
			self.tracks.append(track)
			track_count+=1
	
		print len(self.tracks),'-',
		self.filter_tracks(2.0)
		print len(self.tracks)
			
		#self.compute_time_frames()
		#self.init_track_correlations()		
			
	def read_mtrackj_csv(self,csv_filename):
		'''Read collection of tracks from single .csv file
		derived from imageJ MTrackJ plugin. (Ellie's data)'''
		
		#MTrackJ csv header info http://www.imagescience.org/meijering/software/mtrackj/manual.html
		
		self.plot_name=csv_filename
		csv_file=open(csv_filename,'rU') #use universal newline support to read both windows and osx csvs with /n and /r respectively
		
		lines=csv_file.readlines()    
		self.column_headings=lines[0].split(',') #from first row
		
		#line_count=0
		
		last_track_id=-1
		
		for line in lines[1:]: #2nd row onwards
			line_data_str=line.split(',')[0:-1] #strings		
			line_data_str2=['0' if len(x)==0 else x for x in line_data_str] # zero fill empty fields
			
			line_data=[] # floats			
			for x in line_data_str2:
				try:
					f=float(x)
				except:
					f='0' #catch 'NAs' and the like
				line_data.append(f)

			track_id=int(line_data[self.find_index_for_column('TID')])
				
			#new track
			if track_id!=last_track_id:
				track=AMATrack()
				track.id=self.track_count
				self.track_count+=1
				self.tracks.append(track)
				last_track_id=track_id	
						
			cell=AMACell()
			cell.id=self.cell_count
			cell.x=line_data[self.find_index_for_column('x')]*0.645 #micrometres
			cell.y=line_data[self.find_index_for_column('y')]*0.645 #micrometres
			cell.t=line_data[self.find_index_for_column('t (sec)')]
			cell.ex=line_data[self.find_index_for_column('I (val)')] #from MTrackJ link above: "I [unit]: The calibrated image intensity value at the point. For 8-bit and 16-bit images, the intensity calibration function and value unit can be set (if necessary) as described above. For RGB-images, the intensity is computed as I = 0.3R + 0.6G + 0.1B. If the Edit > Options > Display > Interpolate Zoomed Images option of ImageJ is selected, the (calibrated) intensities are linearly interpolated where necessary (subpixel coordinate precision)."
			#cell.track_id=int(line_data[self.find_index_for_column('TID')])
			cell.track_id=track.id

			track.cells.append(cell)
			
			self.cells.append(cell)
			self.cell_count+=1
			
			#line_count+=1	
			
		print 'Read',csv_filename
							
	def read_mtrackj_csv_dir(self,csv_directory,n=None):
		'''Read collection of tracks from multiple .csv files in a directory
		derived from imageJ MTrackJ plugin. (Ellie's data).
		
		n=number of files to read, default is all
		'''
		
		#csv_directory='/Users/jpm/DATA/David/Data - Ellie Davies/2012-12-03 A,C,AC TGFP,TNull3/CSV format'
		csv_file_names=glob.glob(csv_directory+'/*.csv')
		
		for fn in csv_file_names[0:n]:
			self.read_mtrackj_csv(fn)	
			
	def read_mtrackj_mdf(self,mdf_file,verbose=False):
		'''Read track data from MTrackJ mdf file'''
		
		mdf_file=open(mdf_file,'rU')
		mdf_lines=mdf_file.readlines()
		
		l=0
		while True:
			line=mdf_lines[l]	
			if line[0:5]=='Track':
				break
			l+=1
				
		while line[0:5]=='Track':
			track=AMATrack()
			track.id=self.track_count
			self.track_count+=1
			self.tracks.append(track)
			if verbose:
				print 'Track',track.id
			
			l+=1
			line=mdf_lines[l]
			while line[0:5]=='Point':
				cell=AMACell()
				cell.id=self.cell_count
				cell.track_id=track.id
				self.cell_count+=1
				line_elems=line.split(' ')
				cell.x=float(line_elems[2])
				cell.y=float(line_elems[3])
				cell.t=float(line_elems[5])
				
				track.cells.append(cell)
				
				if verbose:
					print 'Cell',cell.id,cell.t,cell.x,cell.y
				
				l+=1
				line=mdf_lines[l]		
				
		self.compute_tracks()
		
			
	def read_mtrackj_mdf_cluster(self,mdf_file,cluster=1,verbose=False):
		'''Read track data from given cluster of MTrackJ mdf file'''
		
		mdf_file=open(mdf_file,'rU')
		mdf_lines=mdf_file.readlines()
		
		l=0
		while True:
			line=mdf_lines[l]
			l+=1	
			if 'Cluster %s'%cluster in line:
				break
		
		line=mdf_lines[l]		
		while line[0:5]=='Track':
			track=AMATrack()
			track.id=self.track_count
			self.track_count+=1
			self.tracks.append(track)
			if verbose:
				print 'Track',track.id
			
			l+=1
			line=mdf_lines[l]
			while line[0:5]=='Point':
				cell=AMACell()
				cell.id=self.cell_count
				cell.track_id=track.id
				self.cell_count+=1
				line_elems=line.split(' ')
				cell.x=float(line_elems[2])
				cell.y=float(line_elems[3])
				cell.t=float(line_elems[5])
				
				track.cells.append(cell)
				
				if verbose:
					print 'Cell',cell.id,cell.t,cell.x,cell.y
				
				l+=1
				line=mdf_lines[l]		
				
		self.compute_tracks()
		
			
	def compute_tracks(self):
		#compute all
		for track in self.tracks:
			track.compute()
			
		#aggregate all track info for plots etc
		self.dx=[] #list of all movements by each cell at each timestep
		self.dy=[]
		self.d=[] #magnitude
		self.dx_overall=[]#list of all overall track movements
		self.dy_overall=[]
		self.d_overall=[]
		self.x0=[]#list of start posns for each track
		self.y0=[]
		self.ex_mean=[]#mean expression for each track
		
		self.t_begin=[]
		self.t_end=[]
		self.duration=[]
	
	
		for track in self.tracks:#[-3:-1]:
			if track.duration>0.0:
	
				#p.scatter(track.x,track.y,c=track.ex,s=40,cmap='YlGn')
				#p.plot(track.x,track.y,color='black')
				#p.set_xlabel(r'Cell Tracks - $\mu$m')
	
				self.x0.append(track.x0)
				self.y0.append(track.y0)
				self.dx_overall.append(track.dx_overall)
				self.dy_overall.append(track.dy_overall)
				self.d_overall.append(track.d_overall)
				self.ex_mean.append(track.ex_mean)
				
				#accumulate every cell movement step
				self.dx+=track.dx #list concat
				self.dy+=track.dy
				self.d+=track.d 
				
				self.t_begin.append(track.t_begin)
				self.t_end.append(track.t_end)
				self.duration.append(track.duration)
 			
			
	def filter_by_track_size(self,thresh):
		'''Remove all tracks below thresh data points'''
		tracks=[]
		for t in self.tracks:
			if len(t.cells)>thresh:
				tracks.append(t)
				
		self.tracks=tracks
					
	def init_track_correlations(self):
		num_tracks=len(self.tracks)		
		# create matrix	
		self.track_correlations=[[[] for i in range(num_tracks)] for j in range(num_tracks)]

	def compute_max_time(self):	
		# get max time for all tracks
		self.t_max=0
		for t in self.tracks:
			if t.t_end>self.t_max:
				self.t_max=t.t_end
				
	def compute_max_length(self):	
		# get max length, in cell points, for all tracks
		self.len_max=0
		for t in self.tracks:
			if len(t.cells)>self.len_max:
				self.len_max=len(t.cells)
				
	def compute_time_frames(self):
		'''time_frames is (t_max,num_tracks) sparse matrix holding cell ids'''
		
		self.compute_max_time()
				
		self.num_tracks=len(self.tracks)
		# create matrix				
		self.time_frames=scipy.sparse.lil_matrix((int(self.t_max)+1,self.num_tracks+1),dtype=int) #time,track
		
		#fill matrix
		track_count=0
		for track in self.tracks:
			for cell in track.cells:
				self.time_frames[int(cell.t),track_count]=cell.id
			track_count+=1		
			
	def filter_tracks(self,thresh):
		v_mag=[]
		for t in self.tracks:
			for c in t.cells:
				v_mag.append(c.v_mag()) 
		
		v_mean=numpy.mean(v_mag)
		v_std=numpy.std(v_mag)
		
		for t in self.tracks:
			r=t.filter(v_mean+thresh*v_std)
			if r:
				new_track=AMATrack()
				new_track.cells=r
				new_track.compute()
				self.tracks.append(new_track)
			
		#remove zero-length tracks	
		new_tracks=[]
		for t in self.tracks:
			if len(t.cells)>0:
				new_tracks.append(t)
				
		self.tracks=new_tracks
		
	def plot_tracks(self,p=None,quiver=False):
		'''plot tracks in space'''     
	
		#pass in p to overlay on existing plot
		if not p:
			fig=pyplot.figure()
			p=fig.add_subplot(111)   
		
		for track in self.tracks:
			if track.duration>0.0:
	
				p.scatter(track.x,track.y,c=track.ex,s=40,cmap='YlGn')
				p.plot(track.x,track.y,color='black')
		
		p.set_xlabel(r'Cell Tracks - $\mu$m')
			
		if quiver:
			p.quiver(self.x0,self.y0,self.dx_overall,self.dy_overall,self.ex_mean,angles='xy',units='dots',scale_units='xy',scale=1.0,width=5,cmap='YlGn')
	
		p.set_title(self.plot_name)
		
		return p
		
	def plot_movement_distributions(self):
		'''plot distributions of step vector absolute angles and magnitudes
		and overall movements of each track'''
	
		fig=pyplot.figure()
		fig.suptitle(self.plot_name)
		
		# overall track movements		
		p=fig.add_subplot(221,aspect='equal')   
		s=100		
		z=[0]*len(self.x0)		
		p.quiver(z,z,self.dx_overall,self.dy_overall,self.ex_mean,angles='xy',units='dots',scale_units='xy',scale=1.0,width=5,cmap='YlGn')	

		p.set_xlim([-s,s])
		p.set_ylim([-s,s])
		p.set_xlabel(r'Overall Track Movement - $\mu$m')

		# movment histogram	
		p=fig.add_subplot(222,polar=True) 
		
		#mag_filter=numpy.array(self.d)>10
		dx=numpy.array(self.dx)[mag_filter]
		dy=numpy.array(self.dy)[mag_filter]
		
		angles=[math.atan2(y,x) for x,y in zip(dx,dy)]
		hi,bins=numpy.histogram(angles,bins=40)
		
		#ax=fig.add_axes([-0.05, 0.65, 0.35, 0.35], polar=True)
		p.set_xlabel('Cell Step Direction')
		n=len(hi)
		bar=p.bar([b for b in numpy.arange(-numpy.pi,numpy.pi,2*numpy.pi/n)],hi,width=2*numpy.pi/n,bottom=0.0)
		for i in range(n):
			bar[i].set_facecolor(matplotlib.cm.jet(0.7))
			bar[i].set_alpha(0.5)
			
						
		# combined step vector scatter
		p=fig.add_subplot(223,aspect='equal') 
		p.set_xlabel('Step deltas, dx & dy')
		for t in self.tracks:
			col=(numpy.random.random(3).tolist())
			p.scatter(t.dx,t.dy,marker='o',c='b',s=1,edgecolors='none')
		
		#set equal axes according to largest extent
		v=p.get_xlim()+p.get_ylim()
		va=[abs(x) for x in v]
		m=max(va)/2.0
		p.axis(xmin=-m,xmax=m,ymin=-m,ymax=m)
		
		# hist of step magnitudes
		p=fig.add_subplot(224,aspect='equal') 
		p.set_xlabel('Step magnitudes')
		hi,bins=numpy.histogram(self.d,bins=40)
		p.bar(bins[:-1],numpy.log10(hi+1),width=1.0)
		#p.hist(numpy.log10(numpy.array(self.d)+1.0),bins=40)
		
		# vector scatter per track
		#fig=pyplot.figure()
		#fig.suptitle(self.plot_name)
		
		#n=int(math.ceil(math.sqrt(len(self.tracks))))
			
		#i=1
		#plots=[]
		#m=0
		#for t in self.tracks:
			#p=fig.add_subplot(n,n,i,aspect='equal')  
			#col=(numpy.random.random(3).tolist())
			#p.scatter(t.dx,t.dy,marker='o',c=col,s=30,edgecolors='none')
			#plots.append(p)
			
			## get max extent of all plots
			#v=p.get_xlim()+p.get_ylim()
			#va=[abs(x) for x in v]
			#if max(va)>m:
				#m=max(va)
			
			#i+=1
			
		#m/=2.0
		#for p in plots:
			#p.axis(xmin=-m,xmax=m,ymin=-m,ymax=m)
			
		
		##set equal axes according to largest extent
		#v=p.get_xlim()+p.get_ylim()
		#va=[abs(x) for x in v]
		#m=max(va)/2.0
		#p.axis(xmin=-m,xmax=m,ymin=-m,ymax=m)		
			
		#ax=fig.add_axes([0.8,0.7,0.2,0.3])
		#s=100
		#ax.set_xlim([-s,s])
		#ax.set_ylim([-s,s])
		#ax.set_xlabel(r'Overall Track Movement - $\mu$m')
		#z=[0]*len(x0)
		#ax.quiver(z,z,dx_overall,dy_overall,ex_mean,angles='xy',units='dots',scale_units='xy',scale=1.0,width=5,cmap='YlGn')	
			
		## angle difference histogram
		#angles_diff=[angles[i]-angles[i-1] for i in range(1,len(angles))]
		#hi,bins=numpy.histogram(angles_diff,bins=40)	
		#ax=fig.add_axes([0.0, 0.05, 0.3, 0.3])
		#n=len(hi)
		#bar=ax.bar(bins[:-1],hi)
		#for i in range(n):
			#bar[i].set_facecolor(matplotlib.cm.jet(0.3))
			#bar[i].set_alpha(0.5)
		
		
		## directional correlation	
		## timespans
		#fig=pyplot.figure()
		#p=fig.add_subplot(111)  
	
		#N=len(dataset.tracks)
		#t=0
		#s=N
		#for track in dataset.tracks:#[-3:-1]:
			#p.barh(t,track.length,height=1,left=track.t_begin)
			#t+=1
			
		###quivers of time_frame	
		#fig=pyplot.figure()
		#p=fig.add_subplot(111) 
		#p.set_title(plot_name)	
		
		#nz=self.time_frames.nonzero()
		#ii=zip(nz[0],nz[1])
		#tis=[]
		#trs=[]
		#vxs=[]
		#vys=[]
		#xs=[]
		#ys=[]
		#s=0.01
		#for i in ii:
			#ti=i[0]
			#tr=i[1]
			#cell_id=self.time_frames[ti,tr]
			#c=self.cells[cell_id]		
			#tis.append(ti)
			#trs.append(tr)
			#vxs.append(c.v[0])
			#vys.append(c.v[1])
			
			#xs.append(float(ti)/float(self.t_max))
			#xs.append(float(ti)/float(self.t_max)+s*c.v[0])
			#xs.append(None)
			#ys.append(float(tr)/float(self.num_tracks))
			#ys.append(float(tr)/float(self.num_tracks)+s*c.v[1])
			#ys.append(None)
			
		##p.quiver(tis,trs,vxs,vys,scale_units='inches',scale=200,headwidth=0,width=0.001)#)
		##p.plot(tis
		#p.plot(xs,ys)	
		
		### directional correlation
		##cors=[]
		##dists=[]
		#for t in range(int(self.t_max)):
			#col=self.time_frames[t,:]
			#ii=col.nonzero()[1] #time_frames indices where there are cell ids for time t
			#if len(ii)>1:
				#print t,ii
				#for i in range(len(ii)):
					#for j in range(i+1,len(ii)):
						##print (ii[i],ii[j])
						#c1_id=self.time_frames[t,ii[i]]
						#c2_id=self.time_frames[t,ii[j]]
						#c1=self.cells[c1_id]
						#c2=self.cells[c2_id]
						#cor=numpy.dot(c1.v_norm(),c2.v_norm()) 
						#dist=numpy.linalg.norm(c2.pos()-c1.pos())
						##cors.append(cor)
						##dists.append(dist)
						#self.track_correlations[c1.track.id][c2.track.id].append((cor,dist))
						##print c1.track.id,c2.track.id,(cor,dist)
		
		#cors=[]
		#dists=[]		
		#num_tracks=len(self.track_correlations)			
		#for t1 in range(num_tracks):
			#for t2 in range(num_tracks):
				#a=self.track_correlations[t1][t2]
				#if len(a)>0:
					#aa=numpy.array(a)
					#c=aa[:,0].mean()
					#d=aa[:,1].mean()
					#cors.append(c)
					#dists.append(d)
				
						
		#fig=pyplot.figure()
		#p=fig.add_subplot(111)  
		#p.set_ylim([-1.0,1.0])
		#p.set_title(plot_name)	
		#p.scatter(dists,cors)
				
	
		#pyplot.show(block=False)   
		
	def plot_track_lengths(self):
		'''histogram of track lengths, measured as number of steps'''
		
		fig=pyplot.figure()
		p=fig.add_subplot(111)  
		p.set_title('Track Lengths')
		p.set_xlabel('length')
		p.set_ylabel('frequency')	
		
		l=[len(t.cells) for t in self.tracks]
		h,b=numpy.histogram(l,bins=range(0,200,5))
		p.bar(b[1:],h)
		
	def plot_msd(self):
		'''plot mean squared displacement of tracks as a function of time'''
		self.compute_max_length()
		self.num_tracks=len(self.tracks)
		
		#matrix to store displacement (squared) for each track, with track starts aligned at 0
		self.sd=numpy.zeros((self.num_tracks,self.len_max))
		
		t_index=0
		for t in self.tracks:
			p0=t.cells[0].pos()		
			c_index=0
			for c in t.cells[1:]:
				p=c.pos()
				sd=(p[0]-p0[0])*(p[0]-p0[0])+(p[1]-p0[1])*(p[1]-p0[1]) #squared displacement
				self.sd[t_index,c_index]=sd
				c_index+=1
			t_index+=1
			
		fig=pyplot.figure()
		p=fig.add_subplot(211)  
		
		for t in range(self.num_tracks):
			p.plot(self.sd[t])
		
		p=fig.add_subplot(212)
		self.msd=numpy.sum(self.sd,0)
		self.msd/=self.num_tracks
		
		p.plot(self.msd)
		
		#pyplot.show(block=False)    
		
	def plot_displacment(self):
		'''plot histogram of displacement magnitudes for all steps in all tracks'''
		
		fig=pyplot.figure()
		p=fig.add_subplot(111)  
		p.set_title('Displacement Histogram')
		p.set_xlabel('displacement magnitude')
		p.set_ylabel('frequency')
		
		data=[]
		for t in self.tracks:
			p.plot(t.d_hist[1][1:],t.d_hist[0]) #[0] is histogram, [1] is bin positions
			#p.bar(t.d_hist[1][1:],t.d_hist[0]) #[0] is histogram, [1] is bin positions
			data.append(t.d_hist[0])
			
		p.axis(ymin=0.0,ymax=200.0)
		
		#pyplot.show(block=False)  
		
		### dendrogram of track velocity histogram hierarchical clustering
				
		fig=pyplot.figure()
		p=fig.add_subplot(111)  
		p.set_title('Displacement Histogram Cluster')
		p.set_xlabel('histogram #')
		p.set_ylabel('similarity metric')


		distance=scipy.spatial.distance.pdist(data)
		
		linkage=scipy.cluster.hierarchy.linkage(distance,method="complete")
		scipy.cluster.hierarchy.dendrogram(linkage)		
		
		#p.axis(ymin=0.0,ymax=200.0)

		#pyplot.show(block=False) 
		
	def plot_persistence(self):
		'''plot dot product of consecutive step vectors averaged over whole track, for each track in set'''
		
		fig=pyplot.figure()
		p=fig.add_subplot(111)  
		p.set_title('Track Persistence')
		p.set_xlabel('track #')
		p.set_ylabel('persistence')	
		
		pers=[t.persistence for t in self.tracks]
		x=numpy.arange(len(pers))
		
		p.bar(x,pers)
		p.axis(ymin=-1.0,ymax=1.0)
		
		#pyplot.show(block=False) 
		 	
		
	def show_plots(self,block=True):
		'''use this at end of all plot commands to show all plots and block exit from python'''
		pyplot.show(block=block)
	
	# use column heading in csv file to get row index
	def find_index_for_column(self,col):
		return self.column_headings.index(col)
	
	def movingaverage(self,interval, window_size):
		window = numpy.ones(int(window_size))/float(window_size)
		return numpy.convolve(interval, window, 'same')
	


############
##### Line plot
#num_plot_rows=2
#num_plot_cols=3
#fig,ax=pyplot.subplots(num_plot_rows,num_plot_cols)
#plot_col=0
#plot_row=0	

#for av in [1,8]:

	#for track in tracks:#[0:10]:
		#if track.length>0.0:        
			#ex_ave=movingaverage(track.ex,av)[av/2:-av/2]

			##ex_norm=[c.ex/track.cells[0].ex for c in track.cells]
			##ex_ave_norm=numpy.array([e/ex_ave[0] for e in ex_ave])
			#ex_ave_norm=numpy.array([e/track.ex_mean for e in ex_ave])
			#ti_norm=[(c.t-track.cells[0].t)/track.length for c in track.cells]

			##ex_norm_ave=movingaverage(ex_norm,4)
			#ti_norm=numpy.array(ti_norm)

			## if ti_norm.shape==ex_ave_norm.shape:        
			#p=ax[plot_row,plot_col]
			#p.plot(ti_norm[0:len(ex_ave_norm)],ex_ave_norm)
			##pyplot.plot(ti_norm[0:len(ex_ave)],ex_ave)

	#plot_name=csv_directory.split('/')[-2]


	#p.set_title(plot_name)
	##p.xlabel('Time')
	##p.ylabel('Expression normalised to Mean')

	#plot_row+=1
	
	#for c in range(num_plot_cols):
		#ax[1,c].set_ylim(ax[0,c].get_ylim())	
		
	#plot_col+=1	
	
########
####orthogonal projection
#import numpy
#from mpl_toolkits.mplot3d import proj3d
#def orthogonal_proj(zfront, zback):
	#a = (zfront+zback)/(zfront-zback)
	#b = -2*(zfront*zback)/(zfront-zback)
	#return numpy.array([[1,0,0,0],
						#[0,1,0,0],
						#[0,0,a,b],
						#[0,0,0,zback]])
#proj3d.persp_transformation = orthogonal_proj

########
#### 3d plot
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as pyplot
#import matplotlib as mpl
##import numpy as numpy

#fig = pyplot.figure()
#ax = fig.add_subplot(111, projection='3d')

##track_count=np_data.shape[0]
#track_count=0
#for track in tracks:#[0:10]:
	##xs=np_data[track,:,1] # Time
	##ys=np_data[track,:,12] # Mean1
	#if track.lineage is 'mother':
		#col='r'
	#elif track.lineage is 'daughter':
		#col='g'
	#else:
		#col='k'

	#xs=[c.x for c in track.cells]
	#ys=[c.y for c in track.cells]
	#zs=[c.ex for c in track.cells]
	#ax.plot(xs, ys, zs,c=col)# zs=track_count)

	#ax.scatter(xs, ys, zs,c=col,)#, zdir='y', c=c)    
	##ax.scatter(xs, ys, 0,c=col)#, zdir='y', c=c)    

	#track_count+=1

#ax.set_xlabel('X')
#ax.set_ylabel('Y')
#ax.set_zlabel('fluorescence')

#pyplot.show()