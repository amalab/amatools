import numpy
from mayavi import mlab
import tifffile

# sci-kits image processing
import skimage.filter, skimage.morphology
import scipy.ndimage.morphology
import scipy.ndimage.filters

from sklearn.decomposition import PCA
pca=PCA()

fn=r'/Users/jpm/DATA/OME/David/2012-11-12 to 2012-12-07 E14Tg2A PSDiff Nng488,Bra568,Oct-Sox2633/48h Chi (3) STACK.ome.tiff'

# get tiff file as numpy array
tiffimg = tifffile.TIFFfile(fn)
imgarray = tiffimg.asarray()
print imgarray.shape

# prune array to 3d for scalar field input to mayavi
#imgarray2=imgarray[0,0,:,::4,::4]
#imgarray2=imgarray[0,0,:,::2,::2]
#imgarray2=imgarray[0,:,:,::2,::2] #[channel,z,y,x]
imgarray2=imgarray[0,0:1,:,::2,::2] #[channel,z,y,x]
#imgarray2=imgarray[0,0,:,:,:]
# [time,channel,z,y,x] becomes [z,y,x]

imgarray3=numpy.empty(imgarray2.shape)
num_z=imgarray2.shape[1]
num_ch=imgarray2.shape[0]

def process_data():
	for ch in range(num_ch):
		for z in range(num_z):
			image_processed=imgarray2[ch,z]
	
			#blur
			image_processed=scipy.ndimage.gaussian_filter(image_processed,1)
	
			if ch==0:
				#local threshold
				block_size=101
				image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)	
			else:
				thresh=skimage.filter.threshold_otsu(image_processed)	
				image_processed = image_processed > thresh				
	
			#opening
			morph_iterations=1
			structuring_element=scipy.ndimage.morphology.generate_binary_structure(2,2)
			image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,morph_iterations)
	
			#watershed	
			footprint=1
			distance = scipy.ndimage.distance_transform_edt(image_processed)	
			distance = scipy.ndimage.gaussian_filter(distance,4)
			local_max = skimage.morphology.is_local_maximum(distance,image_processed,numpy.ones((footprint*2+1,footprint*2+1)))
			markers = scipy.ndimage.label(local_max)[0]
			labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)
	
			imgarray3[ch,z]=labels
		#imgarray3[z]=image_processed

class Cell:
	def __init__(self):
		self.id=0 #segmentation id
		self.bb=[0,0,0,0] #bounding box [x1,x2,y1,y2]
		self.pc=(0,0)

	def centre(self):
		return ((self.bb[0]+self.bb[1])/2,(self.bb[2]+self.bb[3])/2)
	
def make_cells():
	image_segmented=imgarray3[0,0] #channel=0, z=0
	image_segmented=image_segmented.astype(int)

	# bounding boxes as python slices of image ndarray
	bb_slices=scipy.ndimage.measurements.find_objects(image_segmented)
	for i in range(len(bb_slices)):	
		s=bb_slices[i]
		y1=s[0].start
		y2=s[0].stop
		x1=s[1].start
		x2=s[1].stop	

		cell=Cell()
		cell.bb=[x1,x2,y1,y2]
		cells.append(cell)
		
		# id 
		c=image_segmented[y1:y2,x1:x2] #subsection of image_segmented within bb
		d=numpy.where(c!=0) #all indices not zero
		cell.id=c[d[0][0],d[1][0]]
		
		# principal components
		dx=numpy.mean(d[0])
		dy=numpy.mean(d[1])
		e=zip(d[0]-dx,d[1]-dy) #indices as tuples
		pca.fit(e)
		cell.pc=pca.components_	
		
		s=(y2-y1+x2-x1)/4
		cell.pc*=s
		
		pass

from traits.api import HasTraits, Range, Instance, on_trait_change
from traitsui.api import View, Item, HGroup
from tvtk.pyface.scene_editor import SceneEditor
from mayavi.tools.mlab_scene_model import MlabSceneModel
from mayavi.core.ui.mayavi_scene import MayaviScene


class Visualization(HasTraits):
	scene = Instance(MlabSceneModel, ())
	channel=Range(low=0,high=num_ch-1,value=0)#,editor=RangeEditor(mode="spinner"))

	def __init__(self):
		# Do not forget to call the parent's __init__
		HasTraits.__init__(self)
		#x, y, z, t = curve(6, 11)
		#self.plot = self.scene.mlab.plot3d(x, y, z, t, colormap='Spectral')
		
		self.sc=self.scene.mlab.pipeline.scalar_field(imgarray3[self.channel])
		self.sc.spacing = [2, 1, 1]
		self.sc.update_image_data = True

		self.scene.mlab.pipeline.image_plane_widget(self.sc)
		self.sc.children[0].scalar_lut_manager.lut_mode='flag'
		self.sc.children[0].scalar_lut_manager.reverse_lut=True

	@on_trait_change('channel')
	def update_plot(self):
		self.sc.scalar_data=imgarray3[self.channel]			
		self.sc.update()

	# the layout of the dialog created
	view = View(Item('scene', editor=SceneEditor(scene_class=MayaviScene),
		             height=800, width=1200, show_label=False),
		        HGroup(
		            '_', 'channel',
		            ),
		        )

####MAIN####
process_data()
cells=[]
make_cells()

centres=[cell.centre() for cell in cells]
x,y=zip(*centres) 

import matplotlib.pyplot as plt
imgplot = plt.imshow(imgarray3[0,0])
scatterplot=plt.scatter(x,y)

xs=[]
ys=[]
for cell in cells:
	xs.append(cell.centre()[0])
	xs.append(cell.centre()[0]+cell.pc[0][0])
	xs.append(None)
	ys.append(cell.centre()[1])
	ys.append(cell.centre()[1]+cell.pc[0][1])
	ys.append(None)
	
plt.plot(xs,ys)

plt.show()

visualization = Visualization()
visualization.configure_traits()