import numpy
from mayavi import mlab
import tifffile

# sci-kits image processing
import skimage.filter, skimage.morphology
import scipy.ndimage.morphology
import scipy.ndimage.filters

fn=r'/Users/jpm/DATA/OME/David/2012-11-12 to 2012-12-07 E14Tg2A PSDiff Nng488,Bra568,Oct-Sox2633/48h Chi (3) STACK.ome.tiff'

# get tiff file as numpy array
tiffimg = tifffile.TIFFfile(fn)
imgarray = tiffimg.asarray()
print imgarray.shape

# prune array to 3d for scalar field input to mayavi
#imgarray2=imgarray[0,0,:,::4,::4]
imgarray2=imgarray[0,0,:,::2,::2]
#imgarray2=imgarray[0,0,:,:,:]
# [time,channel,z,y,x] becomes [z,y,x]

imgarray3=numpy.empty(imgarray2.shape)

for z in range(imgarray2.shape[0]):
	image_processed=imgarray2[z]

	#blur
	image_processed=scipy.ndimage.gaussian_filter(image_processed,1)

	#local threshold
	block_size=100
	image_processed=skimage.filter.threshold_adaptive(image_processed,block_size)	

	#opening
	morph_iterations=1
	structuring_element=scipy.ndimage.morphology.generate_binary_structure(2,2)
	image_processed=scipy.ndimage.morphology.binary_opening(image_processed,structuring_element,morph_iterations)

	#watershed	
	footprint=1
	distance = scipy.ndimage.distance_transform_edt(image_processed)	
	distance = scipy.ndimage.gaussian_filter(distance,4)
	local_max = skimage.morphology.is_local_maximum(distance,image_processed,numpy.ones((footprint*2+1,footprint*2+1)))
	markers = scipy.ndimage.label(local_max)[0]
	labels = skimage.morphology.watershed(-distance, markers, mask=image_processed)

	imgarray3[z]=labels
	#imgarray3[z]=image_processed


sc =mlab.pipeline.scalar_field(imgarray3)
sc.spacing = [2, 1, 1]
sc.update_image_data = True
#imgw = mlab.pipeline.iso_surface(sc, contours=[imgarray.min()+0.1*imgarray.ptp(),],  opacity=0.3)
#iso = mlab.pipeline.iso_surface(sc, contours=[imgarray2.min()+0.1*imgarray2.ptp(),],  opacity=1.0)

#contour=iso.contour.outputs[0]
#c=contour

mlab.pipeline.image_plane_widget(sc)
sc.children[0].scalar_lut_manager.lut_mode='black-white'

mlab.show()